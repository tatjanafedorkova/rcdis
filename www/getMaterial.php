<?
    require_once('./libs/init.inc');
    require_once('./libs/materialHandler/materialHandler.class');  
    
    
    $date1 = new DateTime();
    $date2 = new DateTime();
	
	$date1->modify("-1 day");

    //$date1->modify("-8 day");
    //$date2->modify("-1 day");

    $date1=date_create('2020-06-04');
    $date2=date_create('2020-06-04');
	
    $dDateF = $date1->format('d.m.Y');
    $dDateT = $date2->format('d.m.Y');
	
    
    $client = new SoapClient(MATERIAL_SERVICE_SERVER_PATH, 
							array('trace' => 1,'exceptions' => 1, "connection_timeout" => 180,
							'classmap' => array('get_transactions_request' => "get_transactions_request",
												'get_transactions_response' => "get_transactions_response",
												'inv_transaction' => "inv_transaction",
												'inv_transaction_list' => "inv_transaction_list",
												'unknown_xml' => "unknown_xml",
												'service_status' => "service_status",
												'inv_fault' => "inv_fault"
												)));
	echo '<pre>';
    
	
    function get_material_from_inventory( $code, $client, $date_f, $date_t) {

        
        //echo $date_f;
        //echo $date_t;
        
        $date_from = explode('.', $date_f);
        $date_to = explode('.', $date_t);
        
         //print_r( $date_from );
        //print_r( $date_to );
        
        $objReq = new get_transactions_request();
        $objReq -> subinventory_code = $code;
        $objReq -> from_date = date("Y-m-d\TH:i:s",  mktime(0,0,0,(int)$date_from[1],(int)$date_from[0],(int)$date_from[2]) );
		$objReq -> to_date = date("Y-m-d\TH:i:s",  mktime(23,59,59,(int)$date_to[1],(int)$date_to[0],(int)$date_to[2]) );
        
		print_r($objReq);
		
									
		try {		  
          	return $client->get_transactions($objReq); 			
        } catch(SoapFault $fault){
            
            $error = 'Request : <xmp>'.
            $client->__getLastRequest().
            '</xmp> Error Message : ' . 
			$fault->getMessage();            
        }
    }

    $res = dbProc::getStockAutoList();
	
	//print_r($res);
	
    if (is_array($res)) {
        foreach ($res as $i=>$row) {

            $objResp = get_material_from_inventory($row['MSTK_AUTO_STOCK_CODE'], $client, $dDateF, $dDateT);

            if($objResp && isset($objResp)) {
                
				print_r($objResp);

				//$uploader = new MaterialHandler($objResp, $dDateF, $dDateT, $row['MSTK_AUTO_STOCK_CODE']);
                		//$uploader->run();
            } 			
        }
    }
	echo '</pre>';

   

?>
