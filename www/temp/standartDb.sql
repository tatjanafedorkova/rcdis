ALTER TABLE rcd2012.KL_KALKULACIJA ADD KKAL_NORMATIVS_NP float(5,2) DEFAULT 1 NOT NULL;
ALTER TABLE rcd2011.KL_KALKULACIJA ADD KKAL_NORMATIVS_NP float(5,2) DEFAULT 1 NOT NULL;
ALTER TABLE rcd.KL_KALKULACIJA ADD KKAL_NORMATIVS_NP float(5,2) DEFAULT 1 NOT NULL;

ALTER TABLE rcd2012.KL_AKTA_VEIDS ADD KAKV_IR_PLAN_NORMATIVS tinyint(1) DEFAULT 1 NOT NULL;
ALTER TABLE rcd2011.KL_AKTA_VEIDS ADD KAKV_IR_PLAN_NORMATIVS tinyint(1) DEFAULT 1 NOT NULL;
ALTER TABLE rcd.KL_AKTA_VEIDS ADD KAKV_IR_PLAN_NORMATIVS tinyint(1) DEFAULT 1 NOT NULL;

UPDATE  rcd2012.KL_KALKULACIJA a SET KKAL_NORMATIVS_NP = KKAL_NORMATIVS;
UPDATE  rcd2011.KL_KALKULACIJA SET KKAL_NORMATIVS_NP = KKAL_NORMATIVS;
UPDATE  rcd.KL_KALKULACIJA SET KKAL_NORMATIVS_NP = KKAL_NORMATIVS;

INSERT INTO rcd2012.KL_REF_KOLONNAS (`KLKL_ID` , `KLKL_COLUMN` , `KLKL_TITLE` , `KLKL_CATALOG` ,`KLKL_IS_DEFAULT` )
VALUES (NULL , 'KKAL_NORMATIVS_NP', 'Nepl�na normat�vs', 'KL_KALKULACIJA', '0' );

INSERT INTO rcd2012.KL_REF_KOLONNAS (`KLKL_ID` , `KLKL_COLUMN` , `KLKL_TITLE` , `KLKL_CATALOG` ,`KLKL_IS_DEFAULT` )
VALUES (NULL , 'KAKV_IR_PLAN_NORMATIVS', 'Pl�na normat�vs(1/0)', 'KL_AKTA_VEIDS', '0' );

UPDATE rcd2012.KL_REF_KOLONNAS SET `KLKL_TITLE` =  'Pl�na normat�vs' WHERE  `KLKL_COLUMN` = 'KKAL_NORMATIVS' AND  `KLKL_CATALOG` = 'KL_KALKULACIJA';

INSERT INTO rcd2012.FMK_MESSAGES (`CODE`, `TEXT`) VALUES ('PLAN_HOUR_STANDART', 'Pl�na norm�t�vs');
INSERT INTO rcd2012.FMK_MESSAGES (`CODE`, `TEXT`) VALUES ('NOTPLAN_HOUR_STANDART', 'Nepl�na normat�vs');

INSERT INTO rcd2011.FMK_MESSAGES (`CODE`, `TEXT`) VALUES ('PLAN_HOUR_STANDART', 'Pl�na norm�t�vs');
INSERT INTO rcd2011.FMK_MESSAGES (`CODE`, `TEXT`) VALUES ('NOTPLAN_HOUR_STANDART', 'Nepl�na normat�vs');

INSERT INTO rcd.FMK_MESSAGES (`CODE`, `TEXT`) VALUES ('PLAN_HOUR_STANDART', 'Pl�na norm�t�vs');
INSERT INTO rcd.FMK_MESSAGES (`CODE`, `TEXT`) VALUES ('NOTPLAN_HOUR_STANDART', 'Nepl�na normat�vs');

UPDATE `FMK_MESSAGES` SET `TEXT` = '<table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr>
           <th>Kolonas dati</th>
           <th>Oblig�ts</th>
           <th>Kolonas datu apraksts</th>
        </tr>
        <tr>
            <td>Grupas kods</td>
            <td>+</td>
            <td>Kalkul�cijas grupas kods. Var satur�t tikai sekojo�us simbolus `a`-`z`, `A`-`Z`, `0`-`9`. Maksim�li 5 simboli. </td>
        </tr>
        <tr>
            <td>Kalkul�cijas �ifrs</td>
            <td>+</td>
            <td>Kalkul�cijas �ifrs. Unik�ls. Var satur�t tikai sekojo�us simbolus `0`-`9`. Maksim�li 5 simboli.</td>
        </tr>
        <tr>
            <td>Kalkul�cijas nosaukums</td>
            <td>+</td>
            <td>Kalkul�cijas nosaukums. Maksim�li 150 simboli.</td>
        </tr>
        <tr>
            <td>M�rvien�ba</td>
            <td>+</td>
            <td>Kalkul�cijas m�rvien�ba. Maksim�li 15 simboli. </td>
        </tr>
         <tr>
            <td>Pl�na norm�t�vs</td>
            <td>+</td>
            <td>Pl�na normat�vs. Cipars format� [xxxxx.xx]. </td>
        </tr>
         <tr>
            <td>Nepl�na normat�vs</td>
            <td>+</td>
            <td>Nepl�na normat�vs. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>Apraksts</td>
            <td>-</td>
            <td>Kalkul�cijas apraksts. Maksim�li 2000 simboli. </td>
        </tr>
         <tr>
            <td>Gads</td>
            <td>+</td>
            <td>Cipars format� [xxxx]. </td>
        </tr>
        <tr>
            <td>Fiziskais r�d�t�js</td>
            <td>-</td>
            <td>Fiziska r�d�t�ja pazime. Var satur�t tikai sekojo�us simbolus [`2`-`9`]`z` vai [`2`-`11`]`v`. Maksim�li 3 simboli. </td>
        </tr>
        <tr>
            <td>Koeficents</td>
            <td>+</td>
            <td>Koeficents priek� fiziska r�d�t�ja. Cipars format� [xxx.xx]. V�rt�ba p�c noklus�juma ir 1.</td>
        </tr>

        <tr>
            <td>1</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>2</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>3</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>4</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>5</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>6</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>7</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>8</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>9</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>10</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
       </table>'
WHERE `CODE` = 'REQUIREMENTS_CALCULATION';



