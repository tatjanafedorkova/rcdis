CREATE TABLE IF NOT EXISTS `KL_REGIONI` (
  `REGI_ID` int(11) NOT NULL auto_increment,
  `REGI_KODS` char(5) collate utf8_latvian_ci NOT NULL,
  `REGI_NOSAUKUMS` varchar(250) collate utf8_latvian_ci NOT NULL,
  `REGI_IR_AKTIVS` tinyint(1) default '1',
  PRIMARY KEY  (`REGI_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci AUTO_INCREMENT=1 ;


INSERT INTO `KL_REF_KOLONNAS` (
`KLKL_COLUMN` ,
`KLKL_TITLE` ,
`KLKL_CATALOG` ,
`KLKL_IS_DEFAULT`
)
VALUES ( 'REGI_KODS', 'Kods', 'KL_REGIONI', '1' ),
( 'REGI_NOSAUKUMS', 'Nosaukums', 'KL_REGIONI', '0' ),
( 'REGI_IR_AKTIVS', 'Ir akt�vs (1/0)', 'KL_REGIONI', '0' );

INSERT INTO `KL_REGIONI` (`REGI_ID`, `REGI_KODS`, `REGI_NOSAUKUMS`, `REGI_IR_AKTIVS`) VALUES
(1, 'AR', 'Austrumu', 1),
(2, 'CR', 'Centr�lais', 1),
(3, 'DR', 'Dienvidu', 1),
(4, 'RR', 'Rietumu', 1),
(5, 'ZAR', 'Zieme�austrumu', 1),
(6, 'ZR', 'Zieme�u', 1),
(7, 'PR', 'Pier�gas', 1);

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES ('REGIONS', 'Re�ioni');
INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('TRASE_ACT_HEADER1', 'PAKALPOJUMA PIE�EM�ANAS - NODO�ANAS AKTS'),
('SUPORT_ACT_HEADER', 'Par uztur��anas remonta  darbu pie�em�anu'),
('REPORT_WORK_DONE', 'Darbu izpild�t�js'),
('WORK_FUNCTION', 'Remontu un celtniec�bas funkcija');
INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES ('REPORT_DEPARTMENT', 'ekspluat�cijas da�a');
INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES ('WORKS_AND_TERITORY', 'Darb�bas un atbild�bas teritorija');
INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES ('REPORT_REGION', 're�ions');

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES ('ACT_CREATER', 'Aktu izveidoja:');
INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES ('REPORT_WORKER_FINISHED', 'Darbus nodeva: RFC meistars');
INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES ('REPORT_SIGNED_ED', 'Aktu elektroniski viz�ja:');
INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES ('REPORT_SIGNED_INGENEER', 'In�enierekonomists');