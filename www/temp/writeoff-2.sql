﻿
CREATE TABLE IF NOT EXISTS `kl_material_stock` (
  `MSTK_ID` int(11) NOT NULL auto_increment,
  `MSTK_KEDI_SECTION` varchar(250) collate utf8_latvian_ci NOT NULL,
  `MSTK_COST_CENTER` varchar(3) NOT NULL,
  `MSTK_STOCK_CODE` varchar(12) NOT NULL,
  `MSTK_PLACE_CODE` varchar(50) NOT NULL collate utf8_latvian_ci NOT NULL,
  `MSTK_IR_AKTIVS` tinyint(1) default '1',
  PRIMARY KEY  (`MSTK_ID`),
  UNIQUE KEY `MSTK_PLACE_CODE` (`MSTK_PLACE_CODE`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci PACK_KEYS=0 AUTO_INCREMENT=1 ;


INSERT INTO `kl_material_stock` (`MSTK_KEDI_SECTION`,  `MSTK_COST_CENTER`,  `MSTK_STOCK_CODE`, `MSTK_PLACE_CODE`)
VALUES
('Daugavpils nodaļa', 'SE1', 'RCF_ARN_ST', 'DAUGAVPILS'),
('Daugavpils nodaļa', 'SE1', 'RCF_ARN_ST', 'ILŪKSTE'),
('Krāslavas nodaļa', 'SE1', 'RCF_ARN_ST', 'KRĀSLAVA'),
('Ludzas nodaļa', 'SE1', 'RCF_ARN_ST', 'LUDZA'),
('Preiļu nodaļa', 'SE1', 'RCF_ARN_ST', 'LĪVĀNI'),
('Rēzeknes nodaļa', 'SE1', 'RCF_ARN_ST', 'RĒZEKNE'),
('Aizkraukles nodaļa', 'SE5', 'RCF_CRN_ST', 'AIZKRAUKLE'),
('Jēkabpils nodaļa', 'SE5', 'RCF_CRN_ST', 'AKNĪSTE'),
('Alūksnes nodaļa', 'SE5', 'RCF_CRN_ST', 'ALŪKSNE'),
('Balvu nodaļa', 'SE5', 'RCF_CRN_ST', 'BALVI'),
('Gulbenes nodaļa', 'SE5', 'RCF_CRN_ST', 'GULBENE'),
('Jēkabpils nodaļa', 'SE5', 'RCF_CRN_ST', 'JĒKABPILS'),
('Madonas nodaļa', 'SE5', 'RCF_CRN_ST', 'LUBĀNA'),
('Madonas nodaļa', 'SE5', 'RCF_CRN_ST', 'MADONA'),
('Ogres nodaļa', 'SE6', 'RCF_CRN_ST', 'OGRE'),
('Aizkraukles nodaļa', 'SE5', 'RCF_CRN_ST', 'PĻAVIŅAS'),
('Bauskas nodaļa', 'SE2', 'RCF_DRN_ST', 'BAUSKA'),
('Dobeles nodaļa', 'SE2', 'RCF_DRN_ST', 'DOBELE'),
('Jelgavas nodaļa', 'SE2', 'RCF_DRN_ST', 'JELGAVA'),
('Tukuma nodaļa', 'SE2', 'RCF_DRN_ST', 'TUKUMS'),
('Talsu nodaļa', 'SE2', 'RCF_DRN_ST', 'VALDEMĀRPILS_DR'),
('Kuldīgas nodaļa', 'SE3', 'RCF_RRN_ST', 'KULDĪGA'),
('Liepājas nodaļa', 'SE3', 'RCF_RRN_ST', 'LIEPĀJA'),
('Aizputes nodaļa', 'SE3', 'RCF_RRN_ST', 'PRIEKULE'),
('Saldus nodaļa', 'SE3', 'RCF_RRN_ST', 'SALDUS'),
('Ventspils nodaļa', 'SE3', 'RCF_RRN_ST', 'VENTSPILS'),
('Limbažu nodaļa', 'SE7', 'RCF_ZRN_ST', 'ALOJA'),
('Cēsu nodaļa', 'SE7', 'RCF_ZRN_ST', 'CĒSIS'),
('Limbažu nodaļa', 'SE7', 'RCF_ZRN_ST', 'LIMBAŽI'),
('Cēsu nodaļa', 'SE7', 'RCF_ZRN_ST', 'PIEBALGA'),
('Valmieras nodaļa', 'SE7', 'RCF_ZRN_ST', 'RŪJIENA'),
('Juglas nodaļa', 'SE6', 'RCF_ZRN_ST', 'SIGULDA'),
('Smiltenes nodaļa', 'SE7', 'RCF_ZRN_ST', 'SMILTENE'),
('Smiltenes nodaļa', 'SE7', 'RCF_ZRN_ST', 'VALKA'),
('Valmieras nodaļa', 'SE7', 'RCF_ZRN_ST', 'VALMIERA');

INSERT INTO `KL_REF_KOLONNAS` (
`KLKL_COLUMN` ,
`KLKL_TITLE` ,
`KLKL_CATALOG` ,
`KLKL_IS_DEFAULT`
)
VALUES
('MSTK_KEDI_SECTION', 'ED nodaļa', 'KL_MATERIAL_STOCK', '0' ),
('MSTK_COST_CENTER', 'Struktūrvienība/Izmaksu centrs', 'KL_MATERIAL_STOCK', '0'),
('MSTK_STOCK_CODE', 'Noliktavas kods', 'KL_MATERIAL_STOCK', '0'),
('MSTK_IR_AKTIVS', 'Ir aktīvs (1/0)', 'KL_MATERIAL_STOCK', '0'),
('MSTK_PLACE_CODE', 'Novietojuma kods', 'KL_MATERIAL_STOCK', '1');

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('STOCK_TITLE', 'Materiālu novietne'),
('COST_CENTER', 'Struktūrvienība/Izmaksu centrs'),
('STOCK_CODE', 'Noliktavas kods'),
('PLACE_CODE', 'Novietojuma kods'),
('STOCK_INFO', 'Materiālu novietne informācija'),
('ERROR_EXISTS_STOCK', 'Materiālu novietne ar šādu novietojuma kodu jau ir definēta.')
;


ALTER TABLE `kl_objekti` ADD `KOBJ_ORA_ID` varchar( 20 ) NULL ;
ALTER TABLE `kl_objekti` ADD `KOBJ_ORA_TITLE` varchar( 200 ) collate utf8_latvian_ci NULL ;

UPDATE `kl_objekti` SET  `KOBJ_ORA_ID` = '00SPD03ST',  `KOBJ_ORA_TITLE` = 'Zemsprieguma GVL uzturēšana' WHERE `KOBJ_ID` = 1;
UPDATE `kl_objekti` SET  `KOBJ_ORA_ID` = '00SPD04ST',  `KOBJ_ORA_TITLE` = 'Zemsprieguma KL uzturēšana' WHERE `KOBJ_ID` = 2;
UPDATE `kl_objekti` SET  `KOBJ_ORA_ID` = '00SPD02ST',  `KOBJ_ORA_TITLE` = 'Vidsprieguma (6 , 10 , 20 kV) KL uzturēšana' WHERE `KOBJ_ID` = 8;
UPDATE `kl_objekti` SET  `KOBJ_ORA_ID` = '00SPD01ST',  `KOBJ_ORA_TITLE` = 'Vidsprieguma (6 , 10 , 20 kV) GVL uzturēšana' WHERE `KOBJ_ID` = 3;
UPDATE `kl_objekti` SET  `KOBJ_ORA_ID` = '00SPD02ST',  `KOBJ_ORA_TITLE` = 'Vidsprieguma (6 , 10 , 20 kV) KL uzturēšana' WHERE `KOBJ_ID` = 4;
UPDATE `kl_objekti` SET  `KOBJ_ORA_ID` = '00SPD04ST',  `KOBJ_ORA_TITLE` = 'Zemsprieguma KL uzturēšana' WHERE `KOBJ_ID` = 6;
UPDATE `kl_objekti` SET  `KOBJ_ORA_ID` = '00SPD01ST',  `KOBJ_ORA_TITLE` = 'Vidsprieguma (6 , 10 , 20 kV) GVL uzturēšana' WHERE `KOBJ_ID` = 7;
UPDATE `kl_objekti` SET  `KOBJ_ORA_ID` = '00SPD05ST',  `KOBJ_ORA_TITLE` = 'TP eletroiekārtas uzturēšana' WHERE `KOBJ_ID` = 5;

INSERT INTO `KL_REF_KOLONNAS` (
`KLKL_COLUMN` ,
`KLKL_TITLE` ,
`KLKL_CATALOG` ,
`KLKL_IS_DEFAULT`
)
VALUES
('KOBJ_ORA_ID', 'Oracle objekta ID', 'KL_OBJEKTI', '0' ),
('KOBJ_ORA_TITLE', 'Oracle objekta nosaukums', 'KL_OBJEKTI', '0')
;
INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('ORACLE_ID', 'Oracle objekta ID'),
('ORACLE_TITLE', 'Oracle objekta nosaukums')
;

ALTER TABLE `rcd_lietotaji` ADD `RLTT_MSTK_ID` int( 11 )  NULL ;

UPDATE `rcd_lietotaji` SET  `RLTT_MSTK_ID` = 19  WHERE `RLTT_ID` = 69;
UPDATE `rcd_lietotaji` SET  `RLTT_MSTK_ID` = 25  WHERE `RLTT_ID` = 84;
UPDATE `rcd_lietotaji` SET  `RLTT_MSTK_ID` = 29  WHERE `RLTT_ID` = 85;
UPDATE `rcd_lietotaji` SET  `RLTT_MSTK_ID` = 35  WHERE `RLTT_ID` = 88;
UPDATE `rcd_lietotaji` SET  `RLTT_MSTK_ID` = 28  WHERE `RLTT_ID` = 89;
UPDATE `rcd_lietotaji` SET  `RLTT_MSTK_ID` = 22  WHERE `RLTT_ID` = 83;
UPDATE `rcd_lietotaji` SET  `RLTT_MSTK_ID` = 21  WHERE `RLTT_ID` = 82;
UPDATE `rcd_lietotaji` SET  `RLTT_MSTK_ID` = 18  WHERE `RLTT_ID` = 70;
UPDATE `rcd_lietotaji` SET  `RLTT_MSTK_ID` = 18  WHERE `RLTT_ID` = 71;
UPDATE `rcd_lietotaji` SET  `RLTT_MSTK_ID` = 20  WHERE `RLTT_ID` = 72;



INSERT INTO `KL_REF_KOLONNAS` (
`KLKL_COLUMN` ,
`KLKL_TITLE` ,
`KLKL_CATALOG` ,
`KLKL_IS_DEFAULT`
)
VALUES
('STOCK', 'Novietojuma kods', 'KL_LIETOTAJI', '0' )
;

ALTER TABLE `akti` ADD `RAKT_MSTK_ID` int( 11 )  NULL ;


CREATE TABLE IF NOT EXISTS `excel_faili` (
  `EFLS_ID` int(11) NOT NULL auto_increment,
  `EFLS_NOSAUKUMS` varchar(50) collate utf8_latvian_ci NOT NULL,
  `EFLS_DATUMS` datetime NOT NULL default '0000-00-00 00:00:00',
  `EFLS_META_INFO` varchar(50) collate utf8_latvian_ci NOT NULL default 'application/vnd.ms-excel',
  `EFLS_IS_WRITED` int(1) NOT NULL default 0,
  `EFLS_RLTT_ID` int(11) NOT NULL,
  PRIMARY KEY  (`EFLS_ID`)

) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `writeof_material` (
   `WOMT_ID` int(11) NOT NULL auto_increment,
   `WOMT_RAKT_ID` int(11) NOT NULL,
   `WOMT_MATR_ID` int(11) NOT NULL,
   `WOMT_EFLS_ID` int(11) NOT NULL DEFAULT 0,
   PRIMARY KEY  (`WOMT_ID`),
   KEY `WOMT_RAKT_ID` (`WOMT_RAKT_ID`) ,
   KEY `WOMT_EFLS_ID` (`WOMT_EFLS_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci AUTO_INCREMENT=1;
ALTER TABLE `writeof_material` ADD INDEX ( `WOMT_MATR_ID` ) ;
ALTER TABLE `materiali` ADD INDEX ( `MATR_ID` ) ;
ALTER TABLE `akti` ADD INDEX ( `RAKT_ID` ) ;

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('WRITE_OFF_ACT_ORA', 'Norakstīt materiālus (Oracle)'),
('WRITE_OFF_LIST_DONE', 'Norakstīti'),
('WRITE_OFF_LIST_PROCESS', 'Norakstītšnā'),
('ERROR_INVALID_DATE', 'Nepareiz datums.')
;

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('DOCUMENT', 'Operācijas pamatojuma dokuments')
;