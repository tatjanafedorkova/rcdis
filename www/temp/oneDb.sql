//ALTER TABLE `KL_MATERIALI` ADD KMAT_GADS INT(4) DEFAULT 2012 NOT NULL ;
//ALTER TABLE `KL_TRANSPORTS` ADD KMEH_GADS INT(4) DEFAULT 2012 NOT NULL ;
ALTER TABLE rcd2012.KL_KALKULACIJA ADD KKAL_GADS INT(4) DEFAULT 2012 NOT NULL;
ALTER TABLE rcd2011.KL_KALKULACIJA ADD KKAL_GADS INT(4) DEFAULT 2011 NOT NULL;
ALTER TABLE rcd.KL_KALKULACIJA ADD KKAL_GADS INT(4) DEFAULT 2010 NOT NULL;

ALTER TABLE rcd2012.KL_KALKULACIJA ADD INDEX ( `KKAL_GADS` ) ;
//ALTER TABLE `KL_MATERIALI` ADD INDEX ( `KMAT_GADS` ) ;
//ALTER TABLE `KL_TRANSPORTS` ADD INDEX ( `KMEH_GADS` ) ;
ALTER TABLE `KL_MMS_DARBI` ADD INDEX ( `KMSD_GADS` ) ;

ALTER TABLE `KL_MMS_DARBI` DROP INDEX `KMSD_KODS`,   ADD UNIQUE `KMSD_KODS` ( `KMSD_KODS` , `KMSD_GADS` ) ;
ALTER TABLE rcd2012.KL_KALKULACIJA DROP INDEX `KKAL_SHIFRS`, ADD UNIQUE `KKAL_SHIFRS` (`KKAL_SHIFRS`,`KKAL_GADS`);
//ALTER TABLE `KL_MATERIALI` DROP INDEX `KMAT_KODS`, ADD UNIQUE `KMAT_KODS` ( `KMAT_KODS` , `KMAT_GADS` ) ;
//ALTER TABLE `KL_TRANSPORTS` DROP INDEX `KMEH_VALSTS_NUMURS_2`,ADD UNIQUE `KMEH_VALSTS_NUMURS_2` ( `KMEH_VALSTS_NUMURS` , `KMEH_GADS` );

INSERT INTO `KL_REF_KOLONNAS`(`KLKL_ID` , `KLKL_COLUMN` , `KLKL_TITLE` , `KLKL_CATALOG` ,`KLKL_IS_DEFAULT` )
VALUES (NULL , 'KKAL_GADS', 'Gads', 'KL_KALKULACIJA', '0' );

UPDATE `FMK_MESSAGES` SET `TEXT` = '<table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr>
           <th>Kolonas dati</th>
           <th>Oblig�ts</th>
           <th>Kolonas datu apraksts</th>
        </tr>
        <tr>
            <td>Grupas kods</td>
            <td>+</td>
            <td>Kalkul�cijas grupas kods. Var satur�t tikai sekojo�us simbolus `a`-`z`, `A`-`Z`, `0`-`9`. Maksim�li 5 simboli. </td>
        </tr>
        <tr>
            <td>Kalkul�cijas �ifrs</td>
            <td>+</td>
            <td>Kalkul�cijas �ifrs. Unik�ls. Var satur�t tikai sekojo�us simbolus `0`-`9`. Maksim�li 5 simboli.</td>
        </tr>
        <tr>
            <td>Kalkul�cijas nosaukums</td>
            <td>+</td>
            <td>Kalkul�cijas nosaukums. Maksim�li 150 simboli.</td>
        </tr>
        <tr>
            <td>M�rvien�ba</td>
            <td>+</td>
            <td>Kalkul�cijas m�rvien�ba. Maksim�li 15 simboli. </td>
        </tr>
         <tr>
            <td>Normat�vs</td>
            <td>+</td>
            <td>Cilv�kstundas normat�vs. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>Apraksts</td>
            <td>-</td>
            <td>Kalkul�cijas apraksts. Maksim�li 2000 simboli. </td>
        </tr>
        <tr>
            <td>Fiziskais r�d�t�js</td>
            <td>-</td>
            <td>Fiziska r�d�t�ja pazime. Var satur�t tikai sekojo�us simbolus [`2`-`9`]`z` vai [`2`-`11`]`v`. Maksim�li 3 simboli. </td>
        </tr>
        <tr>
            <td>Koeficents</td>
            <td>+</td>
            <td>Koeficents priek� fiziska r�d�t�ja. Cipars format� [xxx.xx]. V�rt�ba p�c noklus�juma ir 1.</td>
        </tr>
        <tr>
            <td>Gads</td>
            <td>+</td>
            <td>Cipars format� [xxxx]. </td>
        </tr>
        <tr>
            <td>1</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>2</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>3</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>4</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>5</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>6</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>7</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>8</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>9</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
        <tr>
            <td>10</td>
            <td>-</td>
            <td>Papildus kolonna. Cipars format� [xxxxx.xx]. </td>
        </tr>
       </table>'
WHERE `CODE` = 'REQUIREMENTS_CALCULATION';



