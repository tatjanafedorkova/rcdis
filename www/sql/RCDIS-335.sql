ALTER TABLE `rcd_lietotaji`
	CHANGE COLUMN `RLTT_ID` `RLTT_ID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST;

CREATE TABLE `event_log` (
	`EVNT_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Ieraksta identifikātors',
	`EVNT_TYPE` CHAR(1) NOT NULL DEFAULT 'A' COMMENT 'Notikuma tips (A, W, C) (Activity, Warning, Critical)' COLLATE 'utf8_latvian_ci',
	`EVNT_FORM_ID` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Formas unikalais identifikātors' COLLATE 'utf8_latvian_ci',
	`EVNT_OP_ID` CHAR(1) NULL DEFAULT NULL COMMENT 'Operacijas identifikātors' COLLATE 'utf8_latvian_ci',
	`EVNT_IS_EPLA` SMALLINT(1) NOT NULL DEFAULT 0 COMMENT 'Pazīme norada uz EPLA aktu',
	`EVNT_USER` INT(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Atsauce uz lietotāja ID',
	`EVNT_DESCRIPTION` VARCHAR(4000) NULL DEFAULT '0' COMMENT 'Notikuma apraksts' COLLATE 'utf8_latvian_ci',
	`EVNT_SECTION` VARCHAR(100) NULL DEFAULT '0' COMMENT 'Moduļa apakšsadaļa' COLLATE 'utf8_latvian_ci',
	`EVNT_TIMESTAMP` DATETIME NOT NULL DEFAULT current_timestamp() COMMENT 'Notikuma datums',
	`EVNT_REQUEST` VARCHAR(4000) NULL DEFAULT NULL COMMENT 'Requesta dati' COLLATE 'utf8_latvian_ci',
	PRIMARY KEY (`EVNT_ID`)
)
COMMENT='Kritisko kļūdu un darbību reģistrācijas žurnāls'
COLLATE='utf8_latvian_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1147
;

CREATE TABLE `event_modules` (
    `EVMD_MODULE` VARCHAR(50) NOT NULL  COMMENT 'Sistēmas daļa' COLLATE 'utf8_latvian_ci',
    `EVMD_FORM_ID` VARCHAR(50) NOT NULL  COMMENT 'Formas unikalais identifikātors' COLLATE 'utf8_latvian_ci',
    `EVMD_FORM_NAME` VARCHAR(250) NOT NULL COMMENT 'Formas nosaukums' COLLATE 'utf8_latvian_ci',
    PRIMARY KEY (`EVMD_FORM_ID`)
)
COMMENT='Kritisko kļūdu un darbību reģistrācijas žurnāla sadaļas'
COLLATE='utf8_latvian_ci'
AUTO_INCREMENT=1
;

ALTER TABLE `event_modules` ADD COLUMN `EVMD_CHECK_SECTION` SMALLINT NOT NULL DEFAULT 0 COMMENT 'Pārbaudīt saturu event_log.evnt_section kolonnā';
ALTER TABLE `event_modules` ADD COLUMN `EVMD_CHECK_OP_ID` SMALLINT NOT NULL DEFAULT 0 COMMENT 'Pārbaudīt saturu event_log.evnt_op_id kolonnā';
ALTER TABLE `event_modules` ADD COLUMN `EVMD_CHECK_IS_EPLA` SMALLINT NOT NULL DEFAULT 0 COMMENT 'Pārbaudīt saturu event_log.evnt_is_epla kolonnā';

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES 
('ACT', 'Akts'),
('SYSTEM', 'Sistēma'),
('AUTENTIFICATION', 'Autentifikācija'),
('MAMUAL_WORK_ADD', 'Darbu/materiālu pievienošana'),
('MAMUAL_MATERIAL_ADD', 'Materiālu pievienošana'),
('RFC_FAVORIT_WORK_ADD', 'Darbu pievienošana no faviritiem'),
('CORE', 'Iekšējais process');

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES 
('AUTOPROC', 'Fona process'),
('INPUT_ACT_STATUS', 'Akta status no KS'),
('INPUT_PROJECT_INFO', 'Project info no KS'),
('SET_ACT_STATUS', 'Akta statusa atjaunošana');

INSERT INTO `kl_ref_kodi` (`KRFK_NOSAUKUMS`, `KRFK_VERTIBA`, `KRFK_NOZIME`) VALUES 
('OP', 'I', 'Ievadīt'),
('OP', 'D', 'Dzēst'),
('OP', 'U', 'Atjaunot'),
('OP', 'E', 'Eksportēt'),
('OP', 'T', 'Eksportēt tame'),
('OP', 'R', 'Atgriezt'),
('OP', 'A', 'Apstiprināt'),
('OP', 'C', 'Kopēt'),
('OP', 'S', 'Meklēt'),
('OP', 'X', 'Atgriezts ar administrātoru'),
('OP', 'Y', 'Dzēsts ar administrātoru'),
('OP', 'P', 'Palaist process'),
('OP', 'K', 'Pabeigt process'),
('OP', 'M', 'Starprezultāti'),
('OP', 'L', 'Dzēst rindu')
;

CREATE OR REPLACE VIEW v_event_log
AS
SELECT * FROM (
SELECT 	
   l.`EVNT_ID`,
	l.`EVNT_TYPE`
	,l.`EVNT_FORM_ID`
	,l.`EVNT_OP_ID`
	,l.`EVNT_IS_EPLA`
	,l.`EVNT_USER`
	,NVL(CASE WHEN l.`EVNT_OP_ID` IS NOT NULL AND l.`EVNT_DESCRIPTION` = '' THEN
		(SELECT `EVNT_DESCRIPTION`
					FROM `event_log`  l1
					WHERE l1.`EVNT_OP_ID` = ''
					AND l1.`EVNT_DESCRIPTION` IS NOT NULL
					AND l1.EVNT_ID = l.EVNT_ID + 1
					AND l.EVNT_USER = l1.EVNT_USER 
					AND l.EVNT_FORM_ID = l1.EVNT_FORM_ID
		)
	ELSE l.`EVNT_DESCRIPTION` END, '') AS `DESCRIPTION`
	,CASE WHEN 1=1 THEN
		(SELECT l1.`EVNT_ID`
					FROM `event_log`  l1
					WHERE l1.`EVNT_OP_ID` IS NOT NULL
					AND l1.`EVNT_DESCRIPTION` = ''
					AND l1.EVNT_ID = l.EVNT_ID - 1
					AND l.EVNT_USER = l1.EVNT_USER 
					AND l.EVNT_FORM_ID = l1.EVNT_FORM_ID
		)
	 END AS `SECOND_EVNT_ID`
	,l.`EVNT_SECTION`
	,l.`EVNT_TIMESTAMP`
	,l.`EVNT_REQUEST`
	,m.`EVMD_MODULE`
	,msg1.text AS `MODULE_TITLE`
	,m.`EVMD_FORM_ID`
	,m.`EVMD_FORM_NAME`
	,CONCAT(msg2.text, ' ', NVL(msg4.text, ''),' ', NVL(k1.KRFK_NOZIME, ''), ' ', NVL(msg3.text, ''), ' ', NVL(k.KRFK_NOZIME, '')) AS `FORM_NAME`
	,CONCAT(u.RLTT_VARDS, ' ', u.RLTT_UZVARDS) AS `USER_NAME`
	FROM `event_log` l
	INNER JOIN `event_modules` m ON l.EVNT_FORM_ID = m.EVMD_FORM_ID
	INNER JOIN `rcd_lietotaji` u ON l.EVNT_USER = u.RLTT_ID
	INNER JOIN `fmk_messages` msg1 ON m.EVMD_MODULE = msg1.code
	INNER JOIN `fmk_messages` msg2 ON m.EVMD_FORM_NAME = msg2.code
	LEFT JOIN `fmk_messages` msg3 ON m.EVMD_CHECK_SECTION = 1 AND l.EVNT_SECTION= msg3.code 
	LEFT JOIN `fmk_messages` msg4 ON m.EVMD_CHECK_IS_EPLA = 1 AND 'TRASES' = msg4.code 
	LEFT JOIN `kl_ref_kodi` k ON m.EVMD_CHECK_OP_ID = 1 AND k.KRFK_NOSAUKUMS = 'OP' AND l.EVNT_OP_ID = k.KRFK_VERTIBA
	LEFT JOIN `kl_ref_kodi` k1 ON m.EVMD_CHECK_SECTION = 1 AND k1.KRFK_NOSAUKUMS = 'IMPORTS' AND l.EVNT_SECTION = k1.KRFK_VERTIBA
) X
 WHERE 
 x.`SECOND_EVNT_ID` IS NULL OR 
 (x.`EVNT_OP_ID` !='' AND x.`EVNT_ID` = x.`SECOND_EVNT_ID`+1) ;

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES 
('EVENT_LOG', 'Notikumu žurnāls'),
('EVENT_SECTION', 'Sadaļa'),
('EVENT_MODULE', 'Modulis'),
('EVENT_TYPE_ACTIVITY', 'Activity'),
('EVENT_TYPE_WARNING', 'Warning'),
('EVENT_TYPE_CRITICAL', 'Critical');

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES 
('EVENT_LOG_DELETE_INFO', 'Dzēst notikumus vecākus par šodienu.');

INSERT INTO  `event_modules` (`EVMD_MODULE`,`EVMD_FORM_NAME`,`EVMD_FORM_ID`) VALUES
('CATALOG', 'MATERIALS', 'f.ktl.s.1'),
('CATALOG', 'CUSTOMERS', 'f.ktl.s.2'),
('CATALOG', 'TRANSPORT', 'f.ktl.s.3'),
('CATALOG', 'DV_AREA', 'f.ktl.s.4'),
('CATALOG', 'ED_AREA', 'f.ktl.s.5'),
('CATALOG', 'VOLTAGE', 'f.ktl.s.6'),
('CATALOG', 'OBJECTS', 'f.ktl.s.7'),
('CATALOG', 'SOURCE_OF_FOUNDS', 'f.ktl.s.8'),
('CATALOG', 'CALCULATION_GROUP', 'f.ktl.s.9'),
('CATALOG', 'CALCULATION', 'f.ktl.s.10'),
('CATALOG', 'ACT_TYPE', 'f.ktl.s.11'),
('CATALOG', 'USERS', 'f.ktl.s.12'),
('CATALOG', 'DATA_IMPORT', 'f.ktl.s.13'),
('CATALOG', 'SINGLE_MMS', 'f.ktl.s.14'),
('CATALOG', 'REGIONS', 'f.ktl.s.15'),
('CATALOG', 'EF_ORDER_TITLE', 'f.ktl.s.16'),
('CATALOG', 'STOCK_TITLE', 'f.ktl.s.17'),
('CATALOG', 'WORK_TYPES', 'f.ktl.s.18'),
('CATALOG', 'MMS_CALCULATION', 'f.ktl.s.20'),
('CATALOG', 'CALCULATION_MATERIAL', 'f.ktl.s.21'),
('CATALOG', 'INVESTITION_PRICES', 'f.ktl.s.22'),
('USER', 'AUTENTIFICATION', 'f.lgn.s.1'), 
('REPORT', 'REPORT', 'f.rpt.s.0'),
('REPORT', 'REPORT_CUSTOMER_WORK_TIME', 'f.rpt.s.2'),
('REPORT', 'REPORT_CUSTOMER_WORK_TIME_DETALS', 'f.rpt.s.3'),
('REPORT', 'REPORT_MATERIALS_IN_MONTH', 'f.rpt.s.4'),
('REPORT', 'REPORT_CALCULATION', 'f.rpt.s.7'),
('REPORT', 'REPORT_MAIN_ACT', 'f.rpt.s.8'),
('REPORT', 'REPORT_MATERIAL_BY_CODE', 'f.rpt.s.9'),
('REPORT', 'REPORT_MATERIAL_BY_GROUP', 'f.rpt.s.10'),
('REPORT', 'REPORT_CALCULATION_BY_GROUP', 'f.rpt.s.11'),
('REPORT', 'REPORT_WORK_PLAN_AND_NOT_PLAN', 'f.rpt.s.12'),
('REPORT', 'REPORT_PHISICAL_SHOWING', 'f.rpt.s.13'),
('REPORT', 'REPORT_WORK_WITH_PLAN', 'f.rpt.s.14'),
('REPORT', 'REPORT_WORK_WITH_NO_PLAN', 'f.rpt.s.15'),
('REPORT', 'REPORT_NOT_STARTED_WORK', 'f.rpt.s.16'),
('REPORT', 'REPORT_FINISHED_WORK', 'f.rpt.s.17'),
('REPORT', 'REPORT_MMS_GROUPS', 'f.rpt.s.18'),
('REPORT', 'REPORT_RFC_INNER_ACT', 'f.rpt.s.20'),
('OPTIONS', 'WARNING', 'f.adm.s.1'),
('OPTIONS', 'USERS_ACTIONS', 'f.adm.s.2'),
('OPTIONS', 'ACT_NUMBER_TEMPLATE', 'f.adm.s.3'),
('OPTIONS', 'SYSTEM_PROCESS', 'f.adm.s.4'),
('OPTIONS', 'CALC_PERIOD_START_DATE', 'f.adm.s.5'),
('OPTIONS', 'EVENT_LOG', 'f.adm.s.6'),
('FAVORITES', 'MAKE_NFAVORITESEW', 'f.fvr.s.1'),
('ACT', 'SINGLE_ACT', 'f.akt.s.1'),
('ACT', 'SEARCH', 'f.akt.s.9'),
('ACT', 'RFC_ACT', 'f.akt.s.18'),
('ACT', 'MAMUAL_WORK_ADD', 'f.akt.s.8'),  
('ACT', 'MAMUAL_MATERIAL_ADD', 'f.akt.s.11'), 
('ACT', 'VIEW_ACT', 'f.akt.s.12'),
('ACT', 'RFC_FAVORIT_WORK_ADD', 'f.akt.s.20'),
('ACT', 'VIEW_ACT', 'f.akt.s.21'),
('SYSTEM', 'CORE', 'class'), 
('SYSTEM', 'CORE', 'php'),
('AUTOPROC', 'SYSTEM_PROCESS_INFO', 'ActStatus'),
('AUTOPROC', 'SYSTEM_PROCESS_INFO', 'updateActStatus'),
('AUTOPROC', 'SYSTEM_PROCESS_INFO', 'getMaterial')
;


UPDATE `event_modules` SET `EVMD_CHECK_OP_ID` = 1 WHERE `EVMD_FORM_ID` IN ('f.akt.s.1', 'f.akt.s.11','f.akt.s.18', 'f.adm.s.4', 'f.fvr.s.1');
UPDATE `event_modules` SET `EVMD_CHECK_OP_ID` = 1 WHERE `EVMD_FORM_ID` LIKE 'f.ktl.s.%';
UPDATE `event_modules` SET `EVMD_CHECK_SECTION` = 1 WHERE `EVMD_FORM_ID` IN ('f.ktl.s.13', 'f.akt.s.1', 'f.akt.s.18');
UPDATE `event_modules` SET `EVMD_CHECK_IS_EPLA` = 1 WHERE `EVMD_FORM_ID` IN ('f.akt.s.1','f.akt.s.8','f.akt.s.11','f.akt.s.12');

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES 
('TYPE', 'Veids');