ALTER TABLE `kl_mms_darbi` RENAME `kl_mms_darbi_old`;

CREATE TABLE `kl_mms_darbi` (
	`KMSD_ID` INT(11) NOT NULL AUTO_INCREMENT,
	`KMSD_KODS` VARCHAR(20) NOT NULL COLLATE 'utf8_latvian_ci',
    `KMSD_OPERATIVAS_APZIM` VARCHAR(255) NULL  COLLATE 'utf8_latvian_ci',
    `KMSD_WORK_TITLE` VARCHAR(2550) NULL  COLLATE 'utf8_latvian_ci',
    `KMSD_IZPILDES_DATUMS` DATETIME NOT NULL,
    `KMSD_NOSACIJUMA_KODS` INT NULL,
    `KMSD_NOSACIJUMS` VARCHAR(2550) NULL  COLLATE 'utf8_latvian_ci',
    `KMSD_IZPILDITAJA_KODS` VARCHAR(20) NULL COLLATE 'utf8_latvian_ci',
    `KMSD_VV_NUMURS` VARCHAR(20) NULL COLLATE 'utf8_latvian_ci',
    `KMSD_KEDI_KODS` INT(5) NOT NULL,
    `KMSD_PRIORITATE` SMALLINT(1) NOT NULL DEFAULT 0,
    `KMSD_IR_EPLA` SMALLINT(1) NOT NULL DEFAULT 0,
    `KMSD_TO_ID` VARCHAR(10) NULL  COLLATE 'utf8_latvian_ci',
    `KMSD_TEH_OBJEKTS` VARCHAR(50) NULL  COLLATE 'utf8_latvian_ci',
    `KMSD_TIKLA_ELEMENTS` VARCHAR(255) NULL  COLLATE 'utf8_latvian_ci',
    `KMSD_TIKLA_ELEMENTA_KLASE` VARCHAR(255) NULL  COLLATE 'utf8_latvian_ci',
    `KMSD_DAUDZUMS` FLOAT(8,2)   NULL,
    `KMSD_VADU_SKAITS` INT NULL,
    `KMSD_X` FLOAT(12,6)   NULL,
    `KMSD_Y` FLOAT(12,6)   NULL,
    `KMSD_KSRG_KODS` VARCHAR(10) NULL  COLLATE 'utf8_latvian_ci',
    `KMSD_KOBJ_KODS` VARCHAR(10) NULL  COLLATE 'utf8_latvian_ci',
    `KMSD_KFNA_KODS` VARCHAR(10) NULL  COLLATE 'utf8_latvian_ci',
	`KMSD_EXPORT_DATUMS` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,	
	PRIMARY KEY (`KMSD_ID`),
	INDEX `KMSD_NOSACIJUMA_KODS` (`KMSD_NOSACIJUMA_KODS`)
)
COLLATE='utf8_latvian_ci'
ENGINE=MyISAM
AUTO_INCREMENT=1
;

ALTER TABLE `kl_mms_darbi` ADD `KMSD_CREATOR` INT NULL;
ALTER TABLE `kl_mms_darbi` ADD `KMSD_CREATED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `kl_mms_darbi` ADD `KMSD_EDITOR` INT NULL;
ALTER TABLE `kl_mms_darbi` ADD `KMSD_EDITED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

DELETE FROM `kl_ref_kolonnas` WHERE klkl_catalog = 'KL_MMS_DARBI' AND klkl_column = 'KMSD_CILVEKSTUNDAS';
DELETE FROM `kl_ref_kolonnas` WHERE klkl_catalog = 'KL_MMS_DARBI' AND klkl_column = 'KMSD_IR_PABEIGTS';
DELETE FROM `kl_ref_kolonnas` WHERE klkl_catalog = 'KL_MMS_DARBI' AND klkl_column = 'KMSD_MATR_IZMAKSAS';
DELETE FROM `kl_ref_kolonnas` WHERE klkl_catalog = 'KL_MMS_DARBI' AND klkl_column = 'KMSD_CETUKSNIS';
DELETE FROM `kl_ref_kolonnas` WHERE klkl_catalog = 'KL_MMS_DARBI' AND klkl_column = 'KMSD_GADS';

INSERT INTO `kl_ref_kolonnas`(KLKL_COLUMN,KLKL_TITLE,KLKL_CATALOG,KLKL_IS_DEFAULT) VALUES 
('KMSD_IZPILDES_DATUMS', 'Plānotais beigu datums', 'KL_MMS_DARBI', 0),
('KMSD_OPERATIVAS_APZIM', 'Operatīvais apzīmējums', 'KL_MMS_DARBI', 0),
('KMSD_WORK_TITLE', 'Darba apraksts', 'KL_MMS_DARBI', 0),
('KMSD_NOSACIJUMA_KODS', 'Nosacījuma ID', 'KL_MMS_DARBI', 0);

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('PROCESS_DATE', 'Plānotais beigu datums'),
('TECHNIC_OBJECT', 'Tehniskais objekts'),
('REQUIREMENT_CODE', 'Nosacījuma ID'),
('REQUIREMENT_TITLE', 'Nosacījums'),
('NETWORK_ELEMENT','Tīkla elementa iezīme'),
('NETWORK_EL_CODE', 'Tīkla elementa klase'),
('MMS_QUANTITY', 'Daudzums'),
('MMS_LINE_COUNT', 'Vadu skaits'),
('MMS_X', 'X'),
('MMS_Y', 'Y'),
('EXPORT_DATE','Eksporta datums, laiks');

ALTER TABLE `kl_objekti` ADD `KOBJ_KODS` CHAR(5);

INSERT INTO `kl_ref_kolonnas`(KLKL_COLUMN,KLKL_TITLE,KLKL_CATALOG,KLKL_IS_DEFAULT) VALUES 
('KOBJ_KODS', 'Objekta kods', 'KL_OBJEKTI', 0);

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES ('ERROR_EXISTS_OBJECT_CODE', 'Jau eksistē cits objekts ar šādu kodu.');

UPDATE `kl_objekti` SET `KOBJ_ORA_ID` = '00SPD012ST'  WHERE `KOBJ_NOSAUKUMS` = 'EUF Tarifu darbi';
UPDATE `kl_objekti` SET `KOBJ_KODS` = '4'  WHERE `KOBJ_NOSAUKUMS` = '0,4 kV gaisvadu līnija';
UPDATE `kl_objekti` SET `KOBJ_KODS` = '5'  WHERE `KOBJ_NOSAUKUMS` = '0,4 kV kabeļi';
UPDATE `kl_objekti` SET `KOBJ_KODS` = '10'  WHERE `KOBJ_NOSAUKUMS` = '10 kV kabeļi';
UPDATE `kl_objekti` SET `KOBJ_KODS` = '20'  WHERE `KOBJ_NOSAUKUMS` = '20 kV gaisvadu līnija';
UPDATE `kl_objekti` SET `KOBJ_KODS` = '25'  WHERE `KOBJ_NOSAUKUMS` = '20 kV kabeļi';
UPDATE `kl_objekti` SET `KOBJ_KODS` = '7'  WHERE `KOBJ_NOSAUKUMS` = 'CK (cilpu kastes)';
UPDATE `kl_objekti` SET `KOBJ_KODS` = '60'  WHERE `KOBJ_NOSAUKUMS` = 'EUF Tarifu darbi';
UPDATE `kl_objekti` SET `KOBJ_KODS` = '35'  WHERE `KOBJ_NOSAUKUMS` = 'KP (komutāc. punkts)';
UPDATE `kl_objekti` SET `KOBJ_KODS` = '30'  WHERE `KOBJ_NOSAUKUMS` = 'TP (transformat. punkti)';
UPDATE `kl_objekti` SET `KOBJ_KODS` = '80'  WHERE `KOBJ_NOSAUKUMS` = 'NĪAD';
UPDATE `kl_objekti` SET `KOBJ_KODS` = '90'  WHERE `KOBJ_NOSAUKUMS` = 'Bojājumu likvidēšana';
UPDATE `kl_objekti` SET `KOBJ_KODS` = '95'  WHERE `KOBJ_NOSAUKUMS` = 'K00';


UPDATE `FMK_MESSAGES` SET TEXT = '<table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr><th>Kolonas dati</th><th>Obligāts</th><th>Kolonas datu apraksts</th></tr>        
        <tr><td>Darba kods</td><td>+</td><td>Maksimāli 20 simboli.</td></tr>
        <tr><td>Operatīvais apzīmējums</td><td>-</td><td>Maksimāli 255 simboli.</td></tr>
        <tr><td>Darba apraksts</td><td>-</td><td>Maksimāli 255 simboli.</td></tr>
        <tr><td>Plānotais izpildes datums</td><td>-</td><td>Datums formatā [YYYY-MM-DD].</td></tr>
        <tr><td>MMS nosacījuma kods</td><td>-</td><td>Cipars formatā [xxxxxxxxxx].</td></tr>
        <tr><td>MMS nosacījums</td><td>-</td><td>Maksimāli 255 simboli.</td></tr>
        <tr><td>Izpildītāja kods</td><td>-</td><td>Maksimāli 20 simboli.</td></tr>
        <tr><td>VV.Nr.</td><td>-</td><td>Maksimāli 20 simboli.</td></tr>
        <tr><td>Iecirkņa kods</td><td>+</td><td>Maksimāli 10 simboli.</td></tr>
        <tr><td>Steidzamības kods</td><td>-</td><td>Atļautas vērtības: 0, 1 </td></tr>
        <tr><td>Trašu darba pazīme</td><td>-</td><td>Atļautas vērtības: 0, 1 </td></tr>
        <tr><td>TO_ID</td><td>-</td><td>Maksimāli 10 simboli.</td></tr>
        <tr><td>Tehniskais objekts</td><td>-</td><td>Maksimāli 50 simboli.</td></tr>
        <tr><td>Tīkla elementa iezīme</td><td>-</td><td>Maksimāli 255 simboli.</td></tr>
        <tr><td>Tīkla elementa klase</td><td>-</td><td>Maksimāli 255 simboli.</td></tr>
        <tr><td>Daudzums</td><td>-</td><td>Cipars formatā [xxxxxxx.xx].</td></tr>
        <tr><td>vadu skaits</td><td>-</td><td>Cipars formatā [xxxxxxxxxx].</td></tr>
        <tr><td>X</td><td>-</td><td>Cipars formatā [xxxxxx.xxxxxx].</td></tr>
        <tr><td>Y</td><td>-</td><td>Cipars formatā [xxxxxx.xxxxxx].</td></tr>
        <tr><td>Spriegums, kods</td><td>-</td><td>Maksimāli 10 simboli.</td></tr>        
       </table>'
WHERE code = 'REQUIREMENTS_MMS_WORKS';

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('CREATED', 'Importa daums'),
('CREATOR', 'Importa veiceis');

--ALTER TABLE `kl_materiali` DROP `KMAT_IR_AKTIVS`;
ALTER TABLE `kl_materiali` ADD `KMAT_GRUPAS_KODS` INT  NULL;
ALTER TABLE `kl_materiali` ADD `KMAT_GRUPAS_NOSAUKUNS` VARCHAR(250)  NULL  COLLATE 'utf8_latvian_ci';
ALTER TABLE `kl_materiali` ADD `KMAT_APAKSGRUPAS_KODS` INT  NULL;
ALTER TABLE `kl_materiali` ADD `KMAT_APAKSGRUPAS_NOSAUKUNS` VARCHAR(250) NULL  COLLATE 'utf8_latvian_ci';
ALTER TABLE `kl_materiali` ADD `KMAT_KATEGORIJAS_KODS` INT NOT NULL;
ALTER TABLE `kl_materiali` ADD `KMAT_KATEGORIJAS_NOSAUKUNS` VARCHAR(250) NOT NULL  COLLATE 'utf8_latvian_ci';
ALTER TABLE `kl_materiali` ADD `KMAT_KATEGORIJAS_KLASE` VARCHAR(250) NULL COLLATE 'utf8_latvian_ci';
ALTER TABLE `kl_materiali` ADD `KMAT_KATEGORIJAS_MERVIENIBA` VARCHAR(15) NOT NULL  COLLATE 'utf8_latvian_ci';
ALTER TABLE `kl_materiali` ADD `KMAT_PRECIZEJUMS` INT NOT NULL DEFAULT 1;
ALTER TABLE `kl_materiali` ADD `KMAT_NOKLUSETAIS` INT NOT NULL DEFAULT 0;

UPDATE `FMK_MESSAGES` SET TEXT = '<table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr><th>Kolonas dati</th><th>Obligāts</th><th>Kolonas datu apraksts</th></tr> 
        <tr><td>Grupas kods</td><td>+</td><td>Cipars formatā [xxxxxxxxxx].</td></tr>
        <tr><td>Grupa</td><td>+</td><td>Maksimāli 255 simboli.</td></tr>
        <tr><td>Apakšgrupas kods</td><td>+</td><td>Cipars formatā [xxxxxxxxxx].</td></tr>
        <tr><td>Apakšgrupa</td><td>+</td><td>Maksimāli 255 simboli.</td></tr>
        <tr><td>Nomenklatūras kategorijas kods</td><td>+</td><td>Cipars formatā [xxxxxxxxxx].</td></tr>
        <tr><td>Nomenklatūras kategorijas nosaukums</td><td>+</td><td>Maksimāli 255 simboli.</td></tr>
        <tr><td>Nomenklatūras kods</td><td>+</td><td>Materiāla oracla kods. Unikāls. Var saturēt tikai sekojošus simbolus `a`-`z`, `A`-`Z`, `0`-`9`. Maksimāli 7 simboli. </td></tr>
        <tr><td>Nomenklatūras apraksts</td><td>+</td><td>Materiāla nosaukums. Maksimāli 160 simboli. </td></tr>
        <tr><td>Mērvienība</td><td>+</td><td>Materiāla mērvienība. Maksimāli 15 simboli. </td></tr>
        <tr><td>Daudzums noliktavā</td><td>+</td><td>Cipars formatā [xxxxxxx.xx].</td></tr>
        <tr><td>Nomenklatūras vidējā cena</td><td>+</td><td>Cipars formatā [xxxxxxx.xx]. </td></tr>
        <tr><td>Kategorijas klase</td><td>-</td><td>Maksimāli 255 simboli. </td></tr>
        <tr><td>Kategorijas mērvienība</td><td>+</td><td>Maksimāli 15 simboli. </td></tr>
        <tr><td>Kategorijas precizējums</td><td>+</td><td>Cipars formatā [xxx].</td></tr>
        <tr><td>Noklusētais materiāls</td><td>-</td><td>Atļautas vērtības: 0, 1 </td></tr>
       </table>'
WHERE code = 'REQUIREMENTS_MATERIAL';

ALTER TABLE `kl_materiali` ADD `KMAT_CREATOR` INT NULL;
ALTER TABLE `kl_materiali` ADD `KMAT_CREATED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `kl_materiali` ADD `KMAT_EDITOR` INT NULL;
ALTER TABLE `kl_materiali` ADD `KMAT_EDITED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

DELETE FROM `kl_ref_kolonnas` WHERE klkl_catalog = 'KL_MATERIALI' AND klkl_column = 'KMAT_IR_AKTIVS';

INSERT INTO `kl_ref_kolonnas`(KLKL_COLUMN,KLKL_TITLE,KLKL_CATALOG,KLKL_IS_DEFAULT) VALUES 
('KMAT_KATEGORIJAS_KODS', 'Kategorijas kods', 'KL_MATERIALI', 0),
('KMAT_KATEGORIJAS_NOSAUKUNS', 'Kategorijas nosaukums', 'KL_MATERIALI', 0);

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('MATERIAL_CATEGORY', 'Nomenklatūras kategorija'),
('MATERIAL_CATEGORY_CLAS', 'Kategorijas klase'),
('MATERIAL_CATEGORY_MEASURE', 'Kategorijas mērvienība'),
('MATERIAL_CATEGORY_DESCR', 'Kategorijas precizējums'),
('MATERIAL_BY_DEFAULT', 'Noklusētais materiāls');

ALTER TABLE `kl_personals` ADD `KPRF_CREATOR` INT NULL;
ALTER TABLE `kl_personals` ADD `KPRF_CREATED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `kl_personals` ADD `KPRF_EDITOR` INT NULL;
ALTER TABLE `kl_personals` ADD `KPRF_EDITED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

UPDATE `FMK_MESSAGES` SET TEXT = '<table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr><th>Kolonas dati</th><th>Obligāts</th><th>Kolonas datu apraksts</th></tr> 
        <tr><td>RCF kods</td><td>+</td><td>Darbinieka RCF kods. Unikāls. Var saturēt tikai sekojošus simbolus `a`-`z`, `A`-`Z`, `0`-`9`. Maksimāli 4 simboli. </td></tr>
        <tr><td>Vārds</td><td>+</td><td>Darbinieka vārds. Maksimāli 30 simboli. </td></tr>
        <tr><td>Uzvārds</td><td>+</td><td>Darbinieka uzvārds. Maksimāli 30 simboli. </td></tr>
        <tr><td>Darba vieta Nr.</td><td>+</td><td>Darbinieka darba vieta numurs. Maksimāli 10 simboli. </td></tr>
        <tr><td>Reģions</td><td>-</td><td>Darbinieka reģion. Var saturēt tikai sekojošus simbolus `A`,`C`,`D`,`R`,`Z`. Maksimāli 1 simbols. </td></tr>        
       </table>'
WHERE code = 'REQUIREMENTS_PERSONAL';

ALTER TABLE `kl_transports` ADD `KMEH_CREATOR` INT NULL;
ALTER TABLE `kl_transports` ADD `KMEH_CREATED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `kl_transports` MODIFY `KMEH_PIEDERIBA` CHAR(2) NULL;
ALTER TABLE `kl_transports` ADD `KMEH_EDITOR` INT NULL;
ALTER TABLE `kl_transports` ADD `KMEH_EDITED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

UPDATE `FMK_MESSAGES` SET TEXT = '<table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr><th>Kolonas dati</th><th>Obligāts</th><th>Kolonas datu apraksts</th></tr> 
        <tr><td>Apakšgrupa</td><td>+</td><td>Apakšgrupas nosaukums. Maksimāli 30 simboli. </td></tr>
        <tr><td>Grupa</td><td>+</td><td>Grupas nosaukums. Maksimāli 30 simboli. </td></tr>
        <tr><td>Apakšgrupas kods</td><td>+</td><td>Apakšgrupas kods. Cipars 1-999. </td></tr>
        <tr><td>Valsts numurs.</td><td>+</td><td>Transporta valsts numurs. Var saturēt tikai sekojošus simbolus `A`-`Z`, `0`-`9`. Maksimāli 10 simboli. </td></tr>
        <tr><td>Piederība</td><td>-</td><td>Transporta piederība. Var saturēt tikai sekojošus simbolus `A`,`C`,`D`,`R`,`Z`,`TN`. Maksimāli 2 simboli. </td></tr>
       </table>'
WHERE code = 'REQUIREMENTS_TRANSPORT';

ALTER TABLE `kl_ef_pasutijumi` ADD `KEFP_CREATOR` INT NULL;
ALTER TABLE `kl_ef_pasutijumi` ADD `KEFP_CREATED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `kl_ef_pasutijumi` ADD `KEFP_EDITOR` INT NULL;
ALTER TABLE `kl_ef_pasutijumi` ADD `KEFP_EDITED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

UPDATE `FMK_MESSAGES` SET TEXT = '<table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr><th>Kolonas dati</th><th>Obligāts</th><th>Kolonas datu apraksts</th></tr> 
        <tr><td>Gads</td><td>+</td><td>Gads. Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 2 simboli. </td></tr>
        <tr><td>Mēnesis</td><td>+</td><td>Mēnesis. Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 2 simboli.</td></tr>
        <tr><td>EF daļa</td><td>+</td><td>EF daļa.  Maksimāli 100 simboli.</td></tr>
        <tr><td>EF nodaļa</td><td>+</td><td>EF nodaļa. Maksimāli 100 simboli. </td></tr>
        <tr><td>CVH</td><td>+</td><td>CVH. Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 6 simboli. </td></tr>
       </table>'
WHERE code = 'REQUIREMENTS_EF_ORDER';

CREATE TABLE `auto_act_status` (
	`AAST_ID` INT(11) NOT NULL AUTO_INCREMENT,
	`AAST_KODS` VARCHAR(20) NOT NULL COLLATE 'utf8_latvian_ci',
    `AAST_STATUS` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8_latvian_ci',
	`AAST_CREATED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,	
    `AAST_DESCRIPTION` VARCHAR(2000) NULL DEFAULT NULL COLLATE 'utf8_latvian_ci',
	PRIMARY KEY (`AAST_ID`)
)
COLLATE='utf8_latvian_ci'
ENGINE=MyISAM
AUTO_INCREMENT=1
;

CREATE TABLE `auto_act_status_log` (
	`AAST_ID` INT(11) NOT NULL AUTO_INCREMENT,
	`AAST_KODS` VARCHAR(20) NOT NULL COLLATE 'utf8_latvian_ci',
    `AAST_STATUS` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8_latvian_ci',
	`AAST_CREATED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,	
    `AAST_DESCRIPTION` VARCHAR(2000) NULL DEFAULT NULL COLLATE 'utf8_latvian_ci',
	PRIMARY KEY (`AAST_ID`)
)
COLLATE='utf8_latvian_ci'
ENGINE=MyISAM
AUTO_INCREMENT=1
;

DELETE FROM `kl_ref_kodi` WHERE `KRFK_VERTIBA` IN ('MMS_WORKS_ALL', 'MMS_WORKS');

DELETE FROM `favoriti` WHERE `FAVR_LTFV_ID`  IN (
    SELECT `LTFV_ID` FROM `lietotaju_favoriti` WHERE `LTFV_TIPS` IN ('MMS_WORKS_ALL', 'MMS_WORKS')
);
DELETE FROM `lietotaju_favoriti` WHERE `LTFV_TIPS` IN ('MMS_WORKS_ALL', 'MMS_WORKS');

--ALTER TABLE `kl_materiali` ADD `KMAT_IR_AKTIVS` TINYINT(1) NOT NULL DEFAULT 1;


ALTER TABLE `kl_kalkulacija` RENAME `kl_kalkulacija_old`;
CREATE TABLE `kl_kalkulacija` (
	`KKAL_ID` INT(11) NOT NULL AUTO_INCREMENT,
	`KKAL_GRUPAS_KODS` CHAR(5) NOT NULL COLLATE 'utf8_latvian_ci',
	`KKAL_SHIFRS` CHAR(5) NOT NULL COLLATE 'utf8_latvian_ci',
	`KKAL_NOSAUKUMS` VARCHAR(150) NOT NULL COLLATE 'utf8_latvian_ci',
    `KKAL_MERVIENIBA` VARCHAR(15) NOT NULL COLLATE 'utf8_latvian_ci',
	`KKAL_APRAKSTS` TEXT NULL DEFAULT NULL COLLATE 'utf8_latvian_ci',
    `KKAL_FIZ_RADIJUMS` VARCHAR(4) NULL DEFAULT NULL COLLATE 'utf8_latvian_ci',
    `KKAL_KWOI_KODS` VARCHAR(13) NULL DEFAULT NULL COLLATE 'utf8_latvian_ci',
    `KKAL_VV_NUMBER` VARCHAR(20) NOT NULL COLLATE 'utf8_latvian_ci',
    `KKAL_KEDI_SECTION` VARCHAR(250) NULL DEFAULT NULL COLLATE 'utf8_latvian_ci',
	`KKAL_PLANA` FLOAT(5,2) NOT NULL,
	`KKAL_NEPLANA` FLOAT(5,2) NOT NULL,
	`KKAL_KOEFICENT` FLOAT(5,3) NOT NULL DEFAULT 1.000,
    `KKAL_IS_ELEMENT` TINYINT(1) NOT NULL DEFAULT 0,
    `KKAL_TRASE` TINYINT(1) NOT NULL DEFAULT 0,
	`KKAL_IR_AKTIVS` TINYINT(1) NOT NULL DEFAULT 1,
    `KKAL_CREATOR` INT NULL,
    `KKAL_CREATED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `KKAL_EDITOR` INT NULL,
    `KKAL_EDITED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`KKAL_ID`),
	UNIQUE INDEX `U_KKAL_SHIFRS` (`KKAL_SHIFRS`),
	INDEX `KKAL_GRUPAS_KODS` (`KKAL_GRUPAS_KODS`)
)
COLLATE='utf8_latvian_ci'
ENGINE=MyISAM
AUTO_INCREMENT=1
;
UPDATE `FMK_MESSAGES` SET TEXT = '<table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr><th>Kolonas dati</th><th>Obligāts</th><th>Kolonas datu apraksts</th></tr> 
        <tr><td>Grupas kods</td><td>+</td><td>Kalkulācijas grupas kods. Var saturēt tikai sekojošus simbolus `a`-`z`, `A`-`Z`, `0`-`9`. Maksimāli 5 simboli.</td></tr>
        <tr><td>Kalkulācijas šifrs</td><td>+</td><td>Kalkulācijas šifrs. Unikāls. Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 5 simboli.</td></tr>
        <tr><td>Kalkulācijas nosaukums</td><td>+</td><td>Kalkulācijas nosaukums. Maksimāli 150 simboli.</td></tr>
        <tr><td>Mērvienība</td><td>+</td><td>Kalkulācijas mērvienība. Maksimāli 15 simboli.</td></tr>
        <tr><td>Apraksts</td><td>+</td><td>Kalkulācijas apraksts. Maksimāli 2000 simboli.</td></tr>
        <tr><td>Fiziskais rādītājs</td><td>+</td><td>Fiziska rādītāja pazime. Var saturēt tikai sekojošus simbolus [`2`-`9`]`z` vai [`2`-`11`]`v`. Maksimāli 3 simboli.</td></tr>
        <tr><td>Darbuzņēmējs</td><td>+</td><td>0</td></tr>
        <tr><td>Gads</td><td>+</td><td>Cipars. Maksimāli 4 simboli.</td></tr>
        <tr><td>ED nodaļa</td><td>+</td><td>0</td></tr>
        <tr><td>Plāna normatīvs</td><td>+</td><td>Cipars formātā [xxxxx.xx].</td></tr>
        <tr><td>Neplāna normatīvs</td><td>+</td><td>Cipars formātā [xxxxx.xx].</td></tr>
        <tr><td>Fiz.rād. koefic.</td><td>+</td><td>Cipars formātā [xxxxx.xx].</td></tr>
        <tr><td>Stihijas kalkul.</td><td>+</td><td>Stihijas kalkul. Cipars formātā [0/1]</td></tr>
        <tr><td>Trašu kalkulācija</td><td>+</td><td>Kalkulācija tiek izmantota trašu tīrīšanas darbiem. Cipars formātā [0/1]</td></tr>
        <tr><td>Ir aktīvs</td><td>+</td><td>Kalkulācija ir aktuāla. Cipars formātā [0/1]</td></tr>
       </table>'
WHERE code = 'REQUIREMENTS_CALCULATION';

DELETE FROM `kl_ref_kolonnas` WHERE klkl_catalog = 'KL_KALKULACIJA' AND klkl_column = 'KKAL_NORMATIVS';
DELETE FROM `kl_ref_kolonnas` WHERE klkl_catalog = 'KL_KALKULACIJA' AND klkl_column = 'KKAL_GADS';
DELETE FROM `kl_ref_kolonnas` WHERE klkl_catalog = 'KL_KALKULACIJA' AND klkl_column = 'KKAL_NORMATIVS_NP';

INSERT INTO `kl_ref_kolonnas`(KLKL_COLUMN,KLKL_TITLE,KLKL_CATALOG,KLKL_IS_DEFAULT) VALUES 
('KKAL_PLANA', 'Plāna normatīvs', 'KL_KALKULACIJA', 0),
('KKAL_NEPLANA', 'Neplāna normatīvs', 'KL_KALKULACIJA', 0),
('KKAL_VV_NUMBER', 'Gads', 'KL_KALKULACIJA', 0);

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('EDITED', 'Labošanas datums'),
('EDITOR', 'Laboja lietotājs');

CREATE TABLE `kl_mms_kalkulacija` (
	`KMKL_ID` INT(11) NOT NULL AUTO_INCREMENT,
	`KMKL_KMSD_NOSACIJUMA_KODS` INT(11) NOT NULL,
	`KMKL_KKAL_SHIFRS` CHAR(5) NOT NULL COLLATE 'utf8_latvian_ci',
    `KMKL_NOKL_SPRIEGUMS_10` TINYINT(1) NOT NULL DEFAULT 1,
    `KMKL_NOKL_SPRIEGUMS_NE_10` TINYINT(1) NOT NULL DEFAULT 1,
    `KMKL_DAUDZUMS` FLOAT(7,3) NOT NULL DEFAULT 1.000,
    `KMKL_IZMANTOT_LINIJU` TINYINT(1) NOT NULL DEFAULT 0,
    `KMKL_KOEFICENTS` FLOAT(7,3) NOT NULL DEFAULT 1.000,
    `KMKL_IZMANTOT_VADU` TINYINT(1) NOT NULL DEFAULT 0,
    `KMKL_IR_AKTIVS` TINYINT(1) NOT NULL DEFAULT 1,
    `KMKL_TRASE` TINYINT(1) NOT NULL DEFAULT 0,
    `KMKL_CREATOR` INT NULL,
    `KMKL_CREATED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `KMKL_EDITOR` INT NULL,
    `KMKL_EDITED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY `PK_KMKL_ID` (`KMKL_ID`),
	UNIQUE INDEX `U_KMKL_NOS_KALK` (`KMKL_KMSD_NOSACIJUMA_KODS`, `KMKL_KKAL_SHIFRS`, `KMKL_TRASE`),
	INDEX `IDX_KMKL_KMSD_NOSACIJUMA_KODS` (`KMKL_KMSD_NOSACIJUMA_KODS`)
)
COLLATE='utf8_latvian_ci'
ENGINE=MyISAM
AUTO_INCREMENT=1
;

INSERT INTO `kl_ref_kolonnas`(KLKL_COLUMN,KLKL_TITLE,KLKL_CATALOG,KLKL_IS_DEFAULT) VALUES 
('KMKL_KMSD_NOSACIJUMA_KODS', 'MMS nosacījuma kods', 'KL_MMS_KALKULACIJA', 1),
('KMKL_KKAL_SHIFRS', 'Kalkulācijas kods', 'KL_MMS_KALKULACIJA', 0),
('KMKL_NOKL_SPRIEGUMS_10', 'Noklus.kalk. spriegums 10(1/0)', 'KL_MMS_KALKULACIJA', 0),
('KMKL_NOKL_SPRIEGUMS_NE_10', 'Noklus.kalk. spriegums<>10(1/0)', 'KL_MMS_KALKULACIJA', 0),
('KMKL_DAUDZUMS', 'Daudzums', 'KL_MMS_KALKULACIJA', 0),
('KMKL_IZMANTOT_LINIJU', 'Izmantot līnijas garumu(1/0)', 'KL_MMS_KALKULACIJA', 0),
('KMKL_KOEFICENTS', 'Koeficents', 'KL_MMS_KALKULACIJA', 0),
('KMKL_IZMANTOT_VADU', 'Izmantot vadu skaitu(1/0)', 'KL_MMS_KALKULACIJA', 0),
('KMKL_IR_AKTIVS', 'Aktīvs (1/0)', 'KL_MMS_KALKULACIJA', 0),
('KMKL_TRASE', 'Trases (0/1)', 'KL_MMS_KALKULACIJA', 0)
;

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('MMS_REQUIREMENT_CODE', 'MMS nosacījuma kods'),
('CALCULATION_CODE', 'Kalkulācijas kods'),
('DEFAULT_VOLTAGE_10', 'Noklusētā kalk. sprieguma kodam 10'),
('DEFAULT_VOLTAGE_NOT_10', 'Noklusētā kalk. sprieguma kodam <> 10'),
('USE_LINE', 'Izmantot līnijas garumu'),
('USE_CORD', 'Izmantot vadu skaitu');

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES ('REQUIREMENTS_MMS_CALCULATION',
       '<table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr><th>Kolonas dati</th><th>Obligāts</th><th>Kolonas datu apraksts</th></tr> 
        <tr><td>MMS nosacījuma kods</td><td>+</td><td>Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 11 simboli.</td></tr>
        <tr><td>Kalkulācijas šifrs</td><td>+</td><td>Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 5 simboli.</td></tr>
        <tr><td>Noklusētā kalk. sprieguma kodam 10</td><td>+</td><td>Cipars formātā [0/1]</td></tr>
        <tr><td>Noklusētā kalk. sprieguma kodam <> 10</td><td>+</td><td>Cipars formātā [0/1]</td></tr>
        <tr><td>Daudzums</td><td>+</td><td>Cipars formātā [xxxxx.xxx].</td></tr>
        <tr><td>Izmantot līnijas garumu</td><td>+</td><td>Cipars formātā [0/1]</td></tr>
        <tr><td>Koeficents</td><td>+</td><td>Cipars formātā [xxxxx.xxx].</td></tr>
        <tr><td>Izmantot vadu skaitu</td><td>+</td><td>Stihijas kalkul. Cipars formātā [0/1]</td></tr>
        <tr><td>Ir aktīvs</td><td>+</td><td>Kalkulācija ir aktuāla. Cipars formātā [0/1]</td></tr>
        <tr><td>Trašu kalkulācija</td><td>+</td><td>Kalkulācija tiek izmantota trašu tīrīšanas darbiem. Cipars formātā [0/1]</td></tr>        
       </table>'    
);

INSERT INTO `kl_ref_kodi` (`KRFK_NOSAUKUMS`, `KRFK_VERTIBA`, `KRFK_NOZIME`)
VALUES ('IMPORTS', 'MMS_KALKULATION', 'MMS-kalkulācijas');

UPDATE `FMK_MESSAGES` SET TEXT = '
        <ul>
            <li>Importējamais fails jābūt CSV formātā ar standarta specifikāciju: atdalītājs komats, ja teksta lauks satur komatu, tad teksta lauks ir "pēdiņās", kodējums UTF-8. </li>
            <li>Kolonnas virsraksti netiek importēti. </li>
            <li>Kolonas secība ir obligāta. </li>
            <li>Failā jābūt tikai predefinētas kolonnas. </li>
        </ul>
        Importējama faila kolonas saturs:'
WHERE code = 'TOTAL_REQUIREMENTS';

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES 
('MMS_CALCULATION', 'MMS-kalkulācijas'),
('MMS_CALCULATION_INFO', 'MMS-kalkulācijas informācija'),
('ERROR_EXISTS_MMS_CALCULATION_CODE', 'Jau eksistē cita MMS-kalkulācija ar šādu kodu un nosacījumu.');

ALTER TABLE `kl_personals`
	CHANGE COLUMN `KPRF_REGIONS` `KPRF_REGIONS` CHAR(1) NULL COLLATE 'utf8_latvian_ci' AFTER `KPRF_DARBA_VIETA`;

CREATE TABLE `kl_kalkulacija_materiali` (
	`KKMT_ID` INT(11) NOT NULL AUTO_INCREMENT,
	`KKMT_KKAL_SHIFRS` CHAR(5) NOT NULL COLLATE 'utf8_latvian_ci',
    `KKMT_KMAT_GRUPA` CHAR(4) NOT NULL COLLATE 'utf8_latvian_ci',
    `KKMT_KMAT_APAKSGRUPA` CHAR(3) NOT NULL COLLATE 'utf8_latvian_ci',
    `KKMT_KMAT_KODS` CHAR(7) NOT NULL COLLATE 'utf8_latvian_ci',
    `KKMT_IZMANTOT_NOMENKLATURU` TINYINT(1) NOT NULL DEFAULT 1,
    `KKMT_NOKLUSETAIS` TINYINT(1) NOT NULL DEFAULT 1,
    `KKMT_DAUDZUMS` FLOAT(7,3) NOT NULL DEFAULT 1.000,
    `KKMT_IZMANTOT_LINIJU` TINYINT(1) NOT NULL DEFAULT 0,
    `KKMT_KOEFICENTS` FLOAT(7,3) NOT NULL DEFAULT 1.000,
    `KKMT_IZMANTOT_VADU` TINYINT(1) NOT NULL DEFAULT 0,
    `KKMT_IR_AKTIVS` TINYINT(1) NOT NULL DEFAULT 1,
    `KKMT_CREATOR` INT NULL,
    `KKMT_CREATED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `KKMT_EDITOR` INT NULL,
    `KKMT_EDITED` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY `PK_KKMT_ID` (`KKMT_ID`),
	UNIQUE INDEX `U_KKMT_GROUP_MATERI` (`KKMT_KKAL_SHIFRS`, `KKMT_KMAT_GRUPA`, `KKMT_KMAT_APAKSGRUPA`, `KKMT_KMAT_KODS`),
	INDEX `IDX_KKMT_KKAL_SHIFRS` (`KKMT_KKAL_SHIFRS`)
)
COLLATE='utf8_latvian_ci'
ENGINE=MyISAM
AUTO_INCREMENT=1
;

INSERT INTO `kl_ref_kolonnas`(KLKL_COLUMN,KLKL_TITLE,KLKL_CATALOG,KLKL_IS_DEFAULT) VALUES 
('KKMT_KKAL_SHIFRS', 'Kalkulācijas kods', 'KL_KALKULACIJA_MATERIALI', 1),
('KMAT_GRUPA', 'Materiālu kategorija', 'KL_KALKULACIJA_MATERIALI', 0),
('KKMT_KMAT_KODS', 'Materiālu nomenklatūra', 'KL_KALKULACIJA_MATERIALI', 0),
('KKMT_IZMANTOT_NOMENKLATURU', 'Izmantot nomenklatūru(1/0)', 'KL_KALKULACIJA_MATERIALI', 0),
('KKMT_NOKLUSETAIS', 'Noklusētais materiāls(1/0)', 'KL_KALKULACIJA_MATERIALI', 0),
('KKMT_DAUDZUMS', 'Daudzums', 'KL_KALKULACIJA_MATERIALI', 0),
('KKMT_IZMANTOT_LINIJU', 'Izmantot līnijas garumu(1/0)', 'KL_KALKULACIJA_MATERIALI', 0),
('KKMT_KOEFICENTS', 'Koeficents', 'KL_KALKULACIJA_MATERIALI', 0),
('KKMT_IZMANTOT_VADU', 'Izmantot vadu skaitu(1/0)', 'KL_KALKULACIJA_MATERIALI', 0),
('KKMT_IR_AKTIVS', 'Aktīvs (1/0)', 'KL_KALKULACIJA_MATERIALI', 0)
;

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('MATERIAL_GROUP_CODE', 'Materiālu kategorija'),
('MATERIAL_SUBGROUP_CODE', 'Materiālu apakškategorija'),
('JUSE_MATERIAL', 'Izmantot nomenklatūru'),
('DEFAULT_MATERIAL', 'Noklusētais materiāls'),
('CALCULATION_MATERIAL', 'Kalkulācijas-materiāli'),
('CALCULATION_MATERIAL_INFO', 'Kalkulācijas-materiālu informācija'),
('ERROR_EXISTS_CALC_MATERIAL_CODE', 'Jau eksistē cits Kalkulācijas-materiāls ar šādu kodu un kategoriju.');

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES ('REQUIREMENTS_CALCULATION_MATERIAL',
       '<table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr><th>Kolonas dati</th><th>Obligāts</th><th>Kolonas datu apraksts</th></tr>         
        <tr><td>Kalkulācijas šifrs</td><td>+</td><td>Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 5 simboli.</td></tr>
        <tr><td>Materiālu kategorija</td><td>+</td><td>Var saturēt tikai sekojošus simbolus `0`-`9`, `.`. Formāts [xxxx.xxx]. Maksimāli 8 simboli.</td></tr>
        <tr><td>Materiālu nomenklatūra</td><td>+</td><td>Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 7 simboli.</td></tr>
        <tr><td>Izmantot nomenklatūru</td><td>+</td><td>Cipars formātā [0/1]</td></tr>
        <tr><td>Noklusētais materiāls</td><td>+</td><td>Cipars formātā [0/1]</td></tr>
        <tr><td>Daudzums</td><td>+</td><td>Cipars formātā [xxxxx.xxx].</td></tr>
        <tr><td>Izmantot līnijas garumu</td><td>+</td><td>Cipars formātā [0/1]</td></tr>
        <tr><td>Koeficents</td><td>+</td><td>Cipars formātā [xxxxx.xxx].</td></tr>
        <tr><td>Izmantot vadu skaitu</td><td>+</td><td>Stihijas kalkul. Cipars formātā [0/1]</td></tr>
        <tr><td>Ir aktīvs</td><td>+</td><td>Kalkulācija ir aktuāla. Cipars formātā [0/1]</td></tr>        
       </table>'    
);

INSERT INTO `kl_ref_kodi` (`KRFK_NOSAUKUMS`, `KRFK_VERTIBA`, `KRFK_NOZIME`)
VALUES ('IMPORTS', 'KALKULATION_MATERIAL', 'Kalkulācijas-materiāli');

-- auto act
ALTER TABLE `AKTI` ADD `RAKT_STARPAKTI` INT(11)  NOT NULL DEFAULT 0;
ALTER TABLE `AKTI` ADD `RAKT_IS_AUTO` TINYINT(1) NOT NULL DEFAULT '0';

ALTER TABLE `DARBI` ADD `DRBI_STARPAKT` INT(11)  NOT NULL DEFAULT 0;
ALTER TABLE `TRANSPORTS` ADD `TRNS_STARPAKT` INT(11)  NOT NULL DEFAULT 0;
ALTER TABLE `PERSONALS` ADD `PRSN_STARPAKT` INT(11)  NOT NULL DEFAULT 0;

ALTER TABLE `AKTI` ADD `RAKT_IZPILDES_DATUMS` DATETIME ;

UPDATE `AKTI` SET `RAKT_DATUMS` = CAST(CONCAT(RAKT_GADS, "-", RAKT_MENESIS, "-", "01") AS DATE) WHERE `RAKT_DATUMS` IS NULL;
UPDATE `AKTI` SET `RAKT_IZPILDES_DATUMS` = LAST_DAY(CAST(CONCAT(RAKT_GADS, "-", RAKT_MENESIS, "-", "01") AS DATE));

ALTER TABLE `akti` CHANGE `RAKT_DATUMS` `RAKT_DATUMS` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ;
ALTER TABLE `akti` CHANGE COLUMN `RAKT_IZPILDES_DATUMS` `RAKT_IZPILDES_DATUMS` DATETIME NOT NULL;

INSERT INTO `kl_ref_kodi` (`KRFK_NOSAUKUMS`, `KRFK_VERTIBA`, `KRFK_NOZIME`)
VALUES ('STATUS', 'AUTO', 'Ģenerēts'),
('IMPORTS', 'MMS_WORKS', 'MMS-darbi');

INSERT INTO `FMK_MESSAGES` (CODE, TEXT) VALUES ('AMOUNT_KOR', 'Korekcijas');
CREATE TABLE `TAME_DARBI` (
	`DRBI_ID` INT(11) NOT NULL AUTO_INCREMENT,
	`DRBI_KKAL_SHIFRS` CHAR(5) NOT NULL COLLATE 'utf8_latvian_ci',
	`DRBI_KKAL_NOSAUKUMS` VARCHAR(150) NOT NULL COLLATE 'utf8_latvian_ci',
	`DRBI_KKAL_NORMATIVS` FLOAT(5,2) NOT NULL,
	`DRBI_RAKT_ID` INT(11) NULL DEFAULT NULL,
	`DRBI_DAUDZUMS` FLOAT(8,3) NULL DEFAULT NULL,
	`DRBI_CILVEKSTUNDAS` FLOAT(10,2) NULL DEFAULT NULL,
	`DRBI_MERVIENIBA` VARCHAR(15) NOT NULL COLLATE 'utf8_latvian_ci',
	`DRBI_KKAL_ID` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`DRBI_ID`),
	INDEX `DRBI_RAKT_ID` (`DRBI_RAKT_ID`)
)
 COLLATE 'utf8_latvian_ci' ENGINE=MyISAM ROW_FORMAT=Dynamic AUTO_INCREMENT=1;
 ALTER TABLE `TAME_DARBI` ADD `DRBI_DAUDZUMS_KOR` FLOAT(8,3) NULL DEFAULT NULL;

CREATE TABLE `TAME_MATERIALI` (
	`MATR_ID` INT(11) NOT NULL AUTO_INCREMENT,
	`MATR_RAKT_ID` INT(11) NOT NULL,
	`MATR_KODS` CHAR(7) NULL DEFAULT NULL COLLATE 'utf8_latvian_ci',
	`MATR_NOSAUKUMS` VARCHAR(250) NULL DEFAULT NULL COLLATE 'utf8_latvian_ci',
	`MATR_MERVIENIBA` VARCHAR(15) NOT NULL COLLATE 'utf8_latvian_ci',
	`MATR_CENA` FLOAT(8,2) NULL DEFAULT NULL,
	`MATR_DAUDZUMS` FLOAT(8,3) NULL DEFAULT NULL,
	`MATR_CENA_KOPA` FLOAT(13,2) NULL DEFAULT NULL,
	`MATR_IS_WRITEDOFF` INT(1) NULL DEFAULT '0',
	`MATR_TRANSACTION_ID` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`MATR_ID`),
	INDEX `MATR_RAKT_ID` (`MATR_RAKT_ID`),
	INDEX `MATR_ID` (`MATR_ID`)
)
 COLLATE 'utf8_latvian_ci' ENGINE=MyISAM ROW_FORMAT=Dynamic AUTO_INCREMENT=1;
 ALTER TABLE `TAME_MATERIALI` ADD `MATR_DAUDZUMS_KOR` FLOAT(8,3) NULL DEFAULT NULL;

// PROCESS
CREATE TABLE `SISTEMAS_PROCESI` (
    `PROC_KODS`  VARCHAR(50) NOT NULL COLLATE 'utf8_latvian_ci',
    `PROC_PID` INT NULL,
	`PROC_NOSAUKUMS` VARCHAR(150) NOT NULL COLLATE 'utf8_latvian_ci',
    `PROC_KOMANDA` VARCHAR(150) NOT NULL COLLATE 'utf8_latvian_ci',
	`PROC_START_DATUMS` DATETIME 	NULL,
    `PROC_STOP_DATUMS` DATETIME NULL,
	`PROC_IR_AKTIVS` TINYINT(1) NOT NULL DEFAULT 0,
	PRIMARY KEY (`PROC_KODS`)
)
 COLLATE 'utf8_latvian_ci' ENGINE=MyISAM ROW_FORMAT=Dynamic;
ALTER TABLE `SISTEMAS_PROCESI` ADD `PROC_INTERVAL` INT NOT NULL DEFAULT 600;

 INSERT INTO SISTEMAS_PROCESI(`PROC_KODS`, `PROC_NOSAUKUMS`, `PROC_KOMANDA`)
 VALUES('KVIKSTEP_RCDIS_STATUS','RCDIS akta statusa sinhronizācija ar KVIKSTEP', 'php -f /u01/rcd-test/autoProc/updateActStatus.php');

 INSERT INTO `FMK_MESSAGES` (CODE, TEXT) VALUES 
 ('SYSTEM_PROCESS', 'Sistēmas procesi'),
 ('SYSTEM_PROCESS_INFO', 'Sistēmas procesa informācija'),
 ('SYSTEM_PROCESS_TITLE', 'Sistēmas procesa nosaukums'),
 ('SYSTEM_PROCESS_COMAND', 'Sistēmas procesa komanda'),
 ('SYSTEM_PROCESS_PID', 'PID'),
 ('SYSTEM_PROCESS_START', 'Sākuma datums'),
 ('SYSTEM_PROCESS_STOP', 'Beigu datums'),
 ('SYSTEM_PROCESS_INTERVAL', 'Sistēmas procesa intervāls (sec.)'),
 ('SYSTEM_PROCESS_RUNNING', 'Process palaists'),
 ('SYSTEM_PROCESS_KILLED', 'Process apturēts')
 ;

 INSERT INTO `kl_ref_kolonnas`(KLKL_COLUMN,KLKL_TITLE,KLKL_CATALOG,KLKL_IS_DEFAULT) VALUES 
('PROC_NOSAUKUMS', 'Nosaukums', 'KL_SISTEMAS_PROCESI', 1),
('PROC_KOMANDA', 'Komanda', 'KL_SISTEMAS_PROCESI', 0),
('PROC_PID', 'PID', 'KL_SISTEMAS_PROCESI', 0),
('PROC_IR_AKTIVS', 'Aktīvs (1/0)', 'KL_SISTEMAS_PROCESI', 0);

-- pēc sapulces
--ALTER TABLE `kl_ed_iecirkni` ADD COLUMN `KEDI_KPRF_DARBA_VIETA` VARCHAR(10);
--ALTER TABLE `kl_ed_iecirkni` ADD COLUMN `KEDI_MSTK_ID`	INT(11);

--INSERT INTO `kl_ref_kolonnas`(KLKL_COLUMN,KLKL_TITLE,KLKL_CATALOG,KLKL_IS_DEFAULT) VALUES 
--('KEDI_KPRF', 'Autors', 'KL_ED_IECIRKNI', 0),
--('KEDI_MSTK', 'Novietojuma kods', 'KL_ED_IECIRKNI', 0);

ALTER TABLE `kl_materiali` CHANGE `KMAT_NOSAUKUMS` `KMAT_NOSAUKUMS` VARCHAR( 250 ) CHARACTER SET utf8 COLLATE utf8_latvian_ci NOT NULL;
ALTER TABLE `kl_materiali` CHANGE `KMAT_CENA` `KMAT_CENA` FLOAT( 8, 2 ) NOT NULL;
ALTER TABLE `kl_materiali` CHANGE `KMAT_DAUDZUMS` `KMAT_DAUDZUMS` FLOAT( 9, 3 ) NULL DEFAULT NULL;

INSERT INTO `kl_ref_kolonnas`(KLKL_COLUMN,KLKL_TITLE,KLKL_CATALOG,KLKL_IS_DEFAULT) VALUES 
('KMAT_KATEGORIJAS_KLASE', 'Kategorijas klase', 'KL_MATERIALI', 0);

UPDATE `kl_ref_kolonnas` SET `KLKL_TITLE`='RBF kods' WHERE  `KLKL_CATALOG`='KL_MATERIALI' AND `KLKL_COLUMN` = 'KPRF_KODS';
UPDATE `fmk_messages` SET `text`='RBF kods' WHERE  `code` = 'RCD_KODS';

ALTER TABLE `kl_personals` DROP COLUMN `KPRF_KBZS_ID`;

UPDATE `FMK_MESSAGES` SET `TEXT` = 'Kalkulāciju-materiālu informācija' WHERE `CODE` = 'CALCULATION_MATERIAL_INFO';
ALTER TABLE `kl_materiali` CHANGE COLUMN `KMAT_KATEGORIJAS_KODS` `KMAT_KATEGORIJAS_KODS` CHAR(4) NOT NULL DEFAULT '' ;

INSERT INTO `FMK_MESSAGES` (CODE, TEXT) VALUES 
 ('MATERIAL_KODS', 'Materiālu nomenklatūra');

 -- UPDATE KL_MATERIALI katalogu

 -- izmaiņa akta
 ALTER TABLE `akti`
	DROP COLUMN `RAKT_CITAS_IZMAKSAS`,
	DROP COLUMN `RAKT_CITAS_IZMAKSAS_PIEZIMES`,
	DROP COLUMN `RAKT_STARPAKTS`,
	DROP COLUMN `RAKT_TO_IDN`,
	DROP COLUMN `RAKT_STARPAKTI`,
    DROP COLUMN `RAKT_MMS_KODS`;

ALTER TABLE `akti` ADD  COLUMN `RAKT_KPRF_DARBA_VIETA` VARCHAR(10) NULL COLLATE 'utf8_latvian_ci';

ALTER TABLE `DARBI` DROP COLUMN `DRBI_STARPAKT`;
ALTER TABLE `TRANSPORTS` DROP COLUMN  `TRNS_STARPAKT`;
ALTER TABLE `PERSONALS` DROP COLUMN  `PRSN_STARPAKT`;

ALTER TABLE `DARBI` ADD `DRBI_APPROVE_DATE` DATETIME NULL;
ALTER TABLE `TRANSPORTS` ADD `TRNS_APPROVE_DATE` DATETIME NULL;
ALTER TABLE `PERSONALS` ADD `PRSN_APPROVE_DATE` DATETIME NULL;
ALTER TABLE `TAME_DARBI` ADD `DRBI_APPROVE_DATE` DATETIME NULL;

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES 
('TAB_WORKS_T', 'Darbi'),
('TAB_MATERIAL_T', 'Materiāli');

ALTER TABLE `personals` CHANGE COLUMN `PRSN_REGION` `PRSN_REGION` CHAR(1) NULL COLLATE 'utf8_latvian_ci' ;


INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES 
('AKT_EXPORT', 'Objekta nodošana'),
('ACT_MIDDLE_RESULT', 'Starprezultāti');

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES 
('DETETE_WORK', 'Dzēst darbu'),
('DETETE_ROW', 'Dzēst rindu');

UPDATE `FMK_MESSAGES` SET TEXT = 'Importējamais fails jābūt CSV formātā.' WHERE code = 'ERROR_NOT_CORRECT_TYPE_OF_FILE';

-- pēc Isapulces
--DELETE FROM  `kl_ref_kolonnas` WHERE KLKL_COLUMN = 'KEDI_KPRF';
--DELETE FROM `kl_ref_kolonnas` WHERE KLKL_COLUMN ='KEDI_MSTK';

--ALTER TABLE  `kl_ed_iecirkni` DROP COLUMN  `KEDI_KPRF_DARBA_VIETA`;
--ALTER TABLE `kl_ed_iecirkni` DROP COLUMN `KEDI_MSTK_ID`;

ALTER TABLE AUTO_ACT_STATUS ADD COLUMN `user_name` VARCHAR(50) NULL;

ALTER TABLE `DARBI` ADD `DRBI_PLAN_DATE` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `PERSONALS` ADD `PRSN_PLAN_DATE` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;	
ALTER TABLE `TAME_DARBI` ADD `DRBI_PLAN_DATE` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES ('FINISHING_DATE', 'Izpildes datums');

ALTER TABLE `TRANSPORTS` ADD `TRNS_PLAN_DATE` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;	
--
ALTER TABLE `DARBI` ADD `DRBI_DAY_TOTAL` FLOAT(10,2) ;
ALTER TABLE `TAME_DARBI` ADD `DRBI_DAY_TOTAL` FLOAT(10,2) ;

ALTER TABLE `PERSONALS` ADD `PRSN_DAY_WORK_TOTAL` FLOAT(10,2) ;
ALTER TABLE `PERSONALS` ADD `PRSN_DAY_OWER_TOTAL` FLOAT(10,2) ;

-- 
ALTER TABLE AUTO_ACT_STATUS ADD COLUMN `END_DATE` DATETIME NULL ;	
ALTER TABLE AUTO_ACT_STATUS_LOG ADD COLUMN `user_name` VARCHAR(50) NULL;
ALTER TABLE AUTO_ACT_STATUS_LOG ADD COLUMN `END_DATE` DATETIME NULL ;
--
UPDATE `FMK_MESSAGES` SET TEXT = '<h3>Kalkulācijas importal lūdzam izmantot daus Excel 97-2003 formātā</h3>
        <table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr><th>Kolonas dati</th><th>Obligāts</th><th>Kolonas datu apraksts</th></tr> 
        <tr><td>Grupas kods</td><td>+</td><td>Kalkulācijas grupas kods. Var saturēt tikai sekojošus simbolus `a`-`z`, `A`-`Z`, `0`-`9`. Maksimāli 5 simboli.</td></tr>
        <tr><td>Kalkulācijas šifrs</td><td>+</td><td>Kalkulācijas šifrs. Unikāls. Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 5 simboli.</td></tr>
        <tr><td>Kalkulācijas nosaukums</td><td>+</td><td>Kalkulācijas nosaukums. Maksimāli 150 simboli.</td></tr>
        <tr><td>Mērvienība</td><td>+</td><td>Kalkulācijas mērvienība. Maksimāli 15 simboli.</td></tr>
        <tr><td>Apraksts</td><td>+</td><td>Kalkulācijas apraksts. Maksimāli 2000 simboli.</td></tr>
        <tr><td>Fiziskais rādītājs</td><td>+</td><td>Fiziska rādītāja pazime. Var saturēt tikai sekojošus simbolus [`2`-`9`]`z` vai [`2`-`11`]`v`. Maksimāli 3 simboli.</td></tr>
        <tr><td>Darbuzņēmējs</td><td>+</td><td>0</td></tr>
        <tr><td>Gads</td><td>+</td><td>Cipars. Maksimāli 4 simboli.</td></tr>
        <tr><td>ED nodaļa</td><td>+</td><td>0</td></tr>
        <tr><td>Plāna normatīvs</td><td>+</td><td>Cipars formātā [xxxxx.xx].</td></tr>
        <tr><td>Neplāna normatīvs</td><td>+</td><td>Cipars formātā [xxxxx.xx].</td></tr>
        <tr><td>Fiz.rād. koefic.</td><td>+</td><td>Cipars formātā [xxxxx.xx].</td></tr>
        <tr><td>Stihijas kalkul.</td><td>+</td><td>Stihijas kalkul. Cipars formātā [0/1]</td></tr>
        <tr><td>Trašu kalkulācija</td><td>+</td><td>Kalkulācija tiek izmantota trašu tīrīšanas darbiem. Cipars formātā [0/1]</td></tr>
        <tr><td>Ir aktīvs</td><td>+</td><td>Kalkulācija ir aktuāla. Cipars formātā [0/1]</td></tr>
       </table>'
WHERE code = 'REQUIREMENTS_CALCULATION';

RENAME TABLE `material_uploaded` TO `auto_material_uploaded`;
RENAME TABLE `material_uploaded_log` TO `auto_material_uploaded_log`;

INSERT INTO SISTEMAS_PROCESI(`PROC_KODS`, `PROC_NOSAUKUMS`, `PROC_KOMANDA`)
 VALUES('RCDIS_MATERIAL_UPLOAD','RCDIS automatiska materiālu ielāde aktam', 'php -f /u01/rcd/autoProc/getMaterial.php');

ALTER TABLE `darbi` ADD COLUMN `DRBI_BRIGADE` INT NOT NULL DEFAULT 1;
ALTER TABLE `tame_darbi` ADD COLUMN `DRBI_BRIGADE` INT NOT NULL DEFAULT 1;
ALTER TABLE `personals` ADD COLUMN `PRSN_BRIGADE` INT NOT NULL DEFAULT 1;

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES ('BRIGADE', 'Brigāde');

ALTER TABLE `kl_mms_darbi` 	ALTER `KMSD_ID` DROP DEFAULT;
ALTER TABLE `kl_mms_darbi` 	CHANGE COLUMN `KMSD_ID` `KMSD_ID` INT(11) NOT NULL FIRST;
UPDATE `FMK_MESSAGES` SET TEXT = '<table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr><th>Kolonas dati</th><th>Obligāts</th><th>Kolonas datu apraksts</th></tr>  
        <tr><td>Unikāls identifikātors</td><td>+</td><td>Cipars formatā [xxxxxxxxxxx].</td></tr>      
        <tr><td>Darba kods</td><td>+</td><td>Maksimāli 20 simboli.</td></tr>
        <tr><td>Operatīvais apzīmējums</td><td>-</td><td>Maksimāli 255 simboli.</td></tr>
        <tr><td>Darba apraksts</td><td>-</td><td>Maksimāli 255 simboli.</td></tr>
        <tr><td>Plānotais izpildes datums</td><td>-</td><td>Datums formatā [YYYY-MM-DD].</td></tr>
        <tr><td>MMS nosacījuma kods</td><td>-</td><td>Cipars formatā [xxxxxxxxxx].</td></tr>
        <tr><td>MMS nosacījums</td><td>-</td><td>Maksimāli 255 simboli.</td></tr>
        <tr><td>Izpildītāja kods</td><td>-</td><td>Maksimāli 20 simboli.</td></tr>
        <tr><td>VV.Nr.</td><td>-</td><td>Maksimāli 20 simboli.</td></tr>
        <tr><td>Iecirkņa kods</td><td>+</td><td>Maksimāli 10 simboli.</td></tr>
        <tr><td>Steidzamības kods</td><td>-</td><td>Atļautas vērtības: 0, 1 </td></tr>
        <tr><td>Trašu darba pazīme</td><td>-</td><td>Atļautas vērtības: 0, 1 </td></tr>
        <tr><td>TO_ID</td><td>-</td><td>Maksimāli 10 simboli.</td></tr>
        <tr><td>Tehniskais objekts</td><td>-</td><td>Maksimāli 50 simboli.</td></tr>
        <tr><td>Tīkla elementa iezīme</td><td>-</td><td>Maksimāli 255 simboli.</td></tr>
        <tr><td>Tīkla elementa klase</td><td>-</td><td>Maksimāli 255 simboli.</td></tr>
        <tr><td>Daudzums</td><td>-</td><td>Cipars formatā [xxxxxxx.xx].</td></tr>
        <tr><td>vadu skaits</td><td>-</td><td>Cipars formatā [xxxxxxxxxx].</td></tr>
        <tr><td>X</td><td>-</td><td>Cipars formatā [xxxxxx.xxxxxx].</td></tr>
        <tr><td>Y</td><td>-</td><td>Cipars formatā [xxxxxx.xxxxxx].</td></tr>
        <tr><td>Spriegums, kods</td><td>-</td><td>Maksimāli 10 simboli.</td></tr>        
       </table>'
WHERE code = 'REQUIREMENTS_MMS_WORKS';

--
    ALTER TABLE `kl_kalkulacija` ADD `KKAL_REM` TINYINT(1) NOT NULL DEFAULT 1;

    UPDATE `FMK_MESSAGES` SET TEXT = '<table cellpadding="3" cellspacing="0" border="1" width="100%">
            <tr><th>Kolonas dati</th><th>Obligāts</th><th>Kolonas datu apraksts</th></tr> 
            <tr><td>Grupas kods</td><td>+</td><td>Kalkulācijas grupas kods. Var saturēt tikai sekojošus simbolus `a`-`z`, `A`-`Z`, `0`-`9`. Maksimāli 5 simboli.</td></tr>
            <tr><td>Kalkulācijas šifrs</td><td>+</td><td>Kalkulācijas šifrs. Unikāls. Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 5 simboli.</td></tr>
            <tr><td>Kalkulācijas nosaukums</td><td>+</td><td>Kalkulācijas nosaukums. Maksimāli 150 simboli.</td></tr>
            <tr><td>Mērvienība</td><td>+</td><td>Kalkulācijas mērvienība. Maksimāli 15 simboli.</td></tr>
            <tr><td>Apraksts</td><td>+</td><td>Kalkulācijas apraksts. Maksimāli 2000 simboli.</td></tr>
            <tr><td>Fiziskais rādītājs</td><td>+</td><td>Fiziska rādītāja pazime. Var saturēt tikai sekojošus simbolus [`2`-`9`]`z` vai [`2`-`11`]`v`. Maksimāli 3 simboli.</td></tr>
            <tr><td>Darbuzņēmējs</td><td>+</td><td>0</td></tr>
            <tr><td>Gads</td><td>+</td><td>Cipars. Maksimāli 4 simboli.</td></tr>
            <tr><td>ED nodaļa</td><td>+</td><td>0</td></tr>
            <tr><td>Plāna normatīvs</td><td>+</td><td>Cipars formātā [xxxxx.xx].</td></tr>
            <tr><td>Neplāna normatīvs</td><td>+</td><td>Cipars formātā [xxxxx.xx].</td></tr>
            <tr><td>Fiz.rād. koefic.</td><td>+</td><td>Cipars formātā [xxxxx.xx].</td></tr>
            <tr><td>Stihijas kalkul.</td><td>+</td><td>Stihijas kalkul. Cipars formātā [0/1]</td></tr>
            <tr><td>Trašu kalkulācija</td><td>+</td><td>Kalkulācija tiek izmantota trašu tīrīšanas darbiem. Cipars formātā [0/1]</td></tr>
            <tr><td>Ir aktīvs</td><td>+</td><td>Kalkulācija ir aktuāla. Cipars formātā [0/1]</td></tr>
            <tr><td>Kalkulācija remontdarbiem</td><td>+</td><td>Kalkulācija tiek izmantota remontdarbiem. Cipars formātā [0/1]</td></tr>
        </table>'
    WHERE code = 'REQUIREMENTS_CALCULATION';
    INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES ('IS_REM', 'Remonta darbs');
    --
    INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES ('CALC_PERIOD_START_DATE', 'Atskaites perioda sākuma datums');
    ALTER TABLE `system_info`
	ADD COLUMN `SNFO_CALC_START_DATE` DATE NULL DEFAULT CURRENT_TIMESTAMP() AFTER `SNFO_LINK_TMPL`;
    INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) 
    VALUES ('ERROR_CALC_PERIOD_DATE', ' Izvēlēts datumu vecāks, nekā Atskaites perioda sākuma datums `%s`');
--
ALTER TABLE `search_criteria`
	ADD COLUMN `planDateFrom` DATETIME NULL DEFAULT NULL ,
	ADD COLUMN `planDateUntil` DATETIME NULL DEFAULT NULL;

ALTER TABLE `darbi` Add COLUMN `DRBI_WORK_APROVE_DATE` DATETIME NULL DEFAULT NULL COMMENT 'Darbu apstiprināšanas datums';
ALTER TABLE `personals` ADD COLUMN `PRSN_WORK_APROVE_DATE`  DATETIME NULL DEFAULT NULL COMMENT 'Darbu apstiprināšanas datums';
ALTER TABLE `search_criteria`
	ADD COLUMN `workAproveDateFrom` DATETIME NULL DEFAULT NULL ,
	ADD COLUMN `workAproveDateUntil` DATETIME NULL DEFAULT NULL;
INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES ('WORK_APROVE_DATE', 'Darbu apstiprināšanas datums;');
--
CREATE TABLE `kl_investicijas_cenas` (
	`INVC_ID` INT(11) NOT NULL AUTO_INCREMENT,
	`INVC_GRUPA` VARCHAR(250) NOT NULL COLLATE 'utf8_latvian_ci',
    `INVC_NOSAUKUMS` VARCHAR(250) NOT NULL COLLATE 'utf8_latvian_ci',
    `INVC_CENA_KM` FLOAT(7,3) NOT NULL DEFAULT 00.000,
    `INVC_CENA_MOTORST` FLOAT(7,3) NOT NULL DEFAULT 00.000,
    `INVC_CENA_DARBAST` FLOAT(7,3) NOT NULL DEFAULT 1.000,
    `INVC_MERVIENIBA` CHAR(3) NOT NULL DEFAULT 'EUR' COLLATE 'utf8_latvian_ci',
    `INVC_IR_AKTIVS` TINYINT(1) NOT NULL DEFAULT 1,
	PRIMARY KEY `PK_INVC_ID` (`INVC_ID`),
	UNIQUE INDEX `U_KKMT_GROUP_MATERI` (`INVC_GRUPA`)
)
COLLATE='utf8_latvian_ci'
ENGINE=MyISAM
AUTO_INCREMENT=1
;

INSERT INTO `kl_ref_kolonnas`(KLKL_COLUMN,KLKL_TITLE,KLKL_CATALOG,KLKL_IS_DEFAULT) VALUES 
('INVC_GRUPA', 'Grupa', 'KL_INVESTICIJAS_CENAS', 1),
('INVC_NOSAUKUMS', 'Nosaukums', 'KL_INVESTICIJAS_CENAS', 0),
('INVC_CENA_KM', 'Vienības cena, km', 'KL_INVESTICIJAS_CENAS', 0),
('INVC_CENA_MOTORST', 'Vienības cena, motorstunda', 'KL_INVESTICIJAS_CENAS', 0),
('INVC_CENA_DARBAST', 'Vienības cena, darba stunda', 'KL_INVESTICIJAS_CENAS', 0),
('INVC_MERVIENIBA', 'Mērvienība', 'KL_INVESTICIJAS_CENAS', 0),
('INVC_IR_AKTIVS', 'Ir aktīvs (1/0)', 'KL_INVESTICIJAS_CENAS', 0)
;

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('INVESTITION_PRICES', 'Investīciju cenas'),
('INVESTITION_PRICES_INFO', 'Investīciju cenas informācija'),
('INVESTITION_KM', 'Vienības cena, km'),
('INVESTITION_MOTOR', 'Vienības cena, motorstunda'),
('INVESTITION_WORK', 'Vienības cena, darba stunda');
INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES ('ERROR_EXISTS_INVESTITION_CODE', 'Investīcijas cenu grupa jau eksistē');
INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES ('INVESTICIJAS_PRICES_WORK_GROUP', 'Personāla izmaksas');

ALTER TABLE `DARBI` ADD `DRBI_INVESTITION_WORK` FLOAT(10,2) NULL;
ALTER TABLE `TAME_DARBI` ADD `DRBI_INVESTITION_WORK` FLOAT(10,2) NULL;
ALTER TABLE `DARBI` ADD `DRBI_TOTAL_COAST` FLOAT(10,2) NULL;
ALTER TABLE `TAME_DARBI` ADD `DRBI_TOTAL_COAST` FLOAT(10,2) NULL;

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES 
('INVESTICIJAS_PRICE', 'Vienības cena'),
('COAST_TOTAL', 'Izmaksas kopā');

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`) VALUES 
('TAB_TRANSPORT_T', 'Transports');

CREATE TABLE `tame_transports` (
	`TRNS_ID` INT(11) NOT NULL AUTO_INCREMENT,
	`TRNS_VALSTS_NUMURS` VARCHAR(10) NOT NULL COLLATE 'utf8_latvian_ci',
	`TRNS_GRUPAS_NOSAUKUMS` VARCHAR(30) NOT NULL COLLATE 'utf8_latvian_ci',
	`TRNS_APAKSGRUPAS_KODS` INT(3) NULL DEFAULT NULL,
	`TRNS_APAKSGRUPAS_NOSAUKUMS` VARCHAR(30) NOT NULL COLLATE 'utf8_latvian_ci',
	`TRNS_PIEDERIBA` CHAR(2) NOT NULL COLLATE 'utf8_latvian_ci',
	`TRNS_RAKT_ID` INT(11) NOT NULL,
	`TRNS_KM` INT(4) NULL DEFAULT 0,
	`TRNS_STUNDAS` FLOAT(5,2) NULL DEFAULT 0.00,
	`TRNS_DARBA_STUNDAS` FLOAT(5,2) NULL DEFAULT 0.00,
	`TRNS_APPROVE_DATE` DATETIME NULL DEFAULT NULL,
	`TRNS_PLAN_DATE` DATETIME NULL DEFAULT current_timestamp(),
	PRIMARY KEY (`TRNS_ID`)
)
COLLATE='utf8_latvian_ci';

ALTER TABLE `transports` CHANGE COLUMN `TRNS_PLAN_DATE` `TRNS_PLAN_DATE` DATETIME NULL DEFAULT current_timestamp() ;
ALTER TABLE `tame_transports` CHANGE COLUMN `TRNS_PLAN_DATE` `TRNS_PLAN_DATE` DATETIME NULL DEFAULT current_timestamp() ;

ALTER TABLE `TRANSPORTS` ADD `TRNS_INVESTITION_KM` FLOAT(7,3) NOT NULL DEFAULT 00.000;
ALTER TABLE `TRANSPORTS` ADD  `TRNS_INVESTITION_MOTOR` FLOAT(7,3) NOT NULL DEFAULT 00.000;
ALTER TABLE `TRANSPORTS` ADD  `TRNS_INVESTITION_WORK` FLOAT(7,3) NOT NULL DEFAULT 1.000;

ALTER TABLE `tame_transports` ADD `TRNS_INVESTITION_KM` FLOAT(7,3)  NULL DEFAULT 00.000;
ALTER TABLE `tame_transports` ADD  `TRNS_INVESTITION_MOTOR` FLOAT(7,3)  NULL DEFAULT 00.000;
ALTER TABLE `tame_transports` ADD  `TRNS_INVESTITION_WORK` FLOAT(7,3)  NULL DEFAULT 1.000;

ALTER TABLE `TRANSPORTS` ADD `TRNS_TOTAL_COAST_KM` FLOAT(7,3) NOT NULL DEFAULT 00.000;
ALTER TABLE `TRANSPORTS` ADD  `TRNS_TOTAL_COAST_MOTOR` FLOAT(7,3) NOT NULL DEFAULT 00.000;
ALTER TABLE `TRANSPORTS` ADD  `TRNS_TOTAL_COAST_WORK` FLOAT(7,3) NOT NULL DEFAULT 0.000;

ALTER TABLE `tame_transports` ADD `TRNS_TOTAL_COAST_KM` FLOAT(7,3) NULL DEFAULT NULL;
ALTER TABLE `tame_transports` ADD  `TRNS_TOTAL_COAST_MOTOR` FLOAT(7,3) NULL DEFAULT NULL;
ALTER TABLE `tame_transports` ADD  `TRNS_TOTAL_COAST_WORK` FLOAT(7,3) NULL DEFAULT NULL;

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('COAST_TOTAL_KM', 'Izmaksas kopā, km'),
('COAST_TOTAL_MOTOR', 'Izmaksas kopā, motorstunda'),
('COAST_TOTAL_WORK', 'Izmaksas kopā, darba stunda');

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES 
('TAB_ADMINISTRATIVE', 'Administrācijas izmaksas'),
('TAB_ADMINISTRATIVE_TITLE', 'Ekspluatācijas un administratīvās izmaksas'),
('EUR','EUR'),
('COAST_TOTAL_ESTIMATE', 'Izmaksas kopā, bez PVN');

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES 
('ANNEX', 'Pielikums'),
('APPROVED', 'Saskaņots'),
('YEAR_DATE','gada');

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES 
('WORK_INVESTITION', 'Personāla izmaksas'),
('TRANSPORT_INVESTITION', 'Transporta izmaksas'),
('MATERIAL_INVISTITION','Materiālu izmaksas');

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES 
('UNIT_KM', 'km'),
('UNIT_MOTORH', 'motorstunda'),
('UNIT_WORKH','darba stunda'),
('UNIT_SERVICE','pakalpojums'),
('PRICE_OFFER', 'cenu piedāvājums');

ALTER TABLE `kl_investicijas_cenas` CHANGE `INVC_NOSAUKUMS` `INVC_NOSAUKUMS` VARCHAR( 500 ) CHARACTER SET utf8 COLLATE utf8_latvian_ci NOT NULL;
ALTER TABLE `tame_darbi` ADD `DRBI_WORK_APROVE_DATE` DATE NULL DEFAULT NULL;
INSERT INTO `fmk_messages` (`code` ,`text`) VALUES ('WORK_APPROVE_DATE', 'Apstiprināšanas datums');

--
ALTER TABLE `kl_investicijas_cenas` ADD `INVC_APAKSGRUPAS_KODS` CHAR(3);
INSERT INTO `kl_ref_kolonnas`(KLKL_COLUMN,KLKL_TITLE,KLKL_CATALOG,KLKL_IS_DEFAULT) VALUES 
('APAKSGRUPA', 'Apakšgrupa', 'KL_INVESTICIJAS_CENAS', 0);
ALTER TABLE `kl_investicijas_cenas`
	DROP INDEX `U_INVC_GROUPS`,
	ADD UNIQUE `U_INVC_GROUPS` (`INVC_GRUPA`, `INVC_APAKSGRUPAS_KODS`);


ALTER TABLE AUTO_ACT_STATUS ADD COLUMN `ACCEPT_DATE` DATETIME NULL ;
ALTER TABLE AUTO_ACT_STATUS_LOG ADD COLUMN `ACCEPT_DATE` DATETIME NULL ;
--
ALTER TABLE `kl_kalkulacija_materiali`
	ADD COLUMN `KKMT_APJOMS_ROUND` TINYINT(1) NOT NULL DEFAULT 0 ,
	ADD COLUMN `KKMT_DVD` TINYINT(1) NOT NULL DEFAULT 0 ,
	DROP INDEX `U_KKMT_GROUP_MATERI`,
	ADD UNIQUE INDEX `U_KKMT_GROUP_MATERI` (`KKMT_KKAL_SHIFRS`, `KKMT_KMAT_GRUPA`, `KKMT_KMAT_APAKSGRUPA`, `KKMT_KMAT_KODS`, `KKMT_DVD`);

INSERT INTO `kl_ref_kolonnas`(KLKL_COLUMN,KLKL_TITLE,KLKL_CATALOG,KLKL_IS_DEFAULT) VALUES 
('KKMT_APJOMS_ROUND', 'Apjums apaļots', 'KL_KALKULACIJA_MATERIALI', 1),
('KKMT_DVD', 'Darbība visam darbam', 'KL_KALKULACIJA_MATERIALI', 0);

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('APJOMS_ROUND', 'Apjums apaļots'),
('DVD', 'Darbība visam darbam');

UPDATE `FMK_MESSAGES` 
SET `TEXT` = '<table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr><th>Kolonas dati</th><th>Obligāts</th><th>Kolonas datu apraksts</th></tr>         
        <tr><td>Kalkulācijas šifrs</td><td>+</td><td>Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 5 simboli.</td></tr>
        <tr><td>Materiālu kategorija</td><td>+</td><td>Var saturēt tikai sekojošus simbolus `0`-`9`, `.`. Formāts [xxxx.xxx]. Maksimāli 8 simboli.</td></tr>
        <tr><td>Materiālu nomenklatūra</td><td>+</td><td>Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 7 simboli.</td></tr>
        <tr><td>Izmantot nomenklatūru</td><td>+</td><td>Cipars formātā [0/1]</td></tr>
        <tr><td>Noklusētais materiāls</td><td>+</td><td>Cipars formātā [0/1]</td></tr>
        <tr><td>Daudzums</td><td>+</td><td>Cipars formātā [xxxxx.xxx].</td></tr>
        <tr><td>Izmantot līnijas garumu</td><td>+</td><td>Cipars formātā [0/1]</td></tr>
        <tr><td>Koeficents</td><td>+</td><td>Cipars formātā [xxxxx.xxx].</td></tr>
        <tr><td>Izmantot vadu skaitu</td><td>+</td><td>Stihijas kalkul. Cipars formātā [0/1]</td></tr>
        <tr><td>Ir aktīvs</td><td>+</td><td>Kalkulācija ir aktuāla. Cipars formātā [0/1]</td></tr>     
        <tr><td>Apjums apaļots</td><td>+</td><td>Cipars formātā [0/1]</td></tr>     
        <tr><td>Darbība visam darbam</td><td>+</td><td>Cipars formātā [0/1]</td></tr>        
       </table>'  WHERE `CODE`  = 'REQUIREMENTS_CALCULATION_MATERIAL';

ALTER TABLE `kl_kalkulacija_materiali`
	DROP COLUMN `KKMT_APJOMS_ROUND` ,
	DROP COLUMN `KKMT_DVD`,
	DROP INDEX `U_KKMT_GROUP_MATERI`;

ALTER TABLE `kl_mms_kalkulacija`
	ADD COLUMN `KMKL_APJOMS_ROUND` TINYINT(1) NOT NULL DEFAULT 0 ,
	ADD COLUMN `KMKL_DVD` TINYINT(1) NOT NULL DEFAULT 0 ,
	DROP INDEX `U_KMKL_NOS_KALK`,
	ADD UNIQUE INDEX `U_KMKL_NOS_KALK` (`KMKL_KMSD_NOSACIJUMA_KODS`, `KMKL_KKAL_SHIFRS`, 
    `KMKL_TRASE`, `KMKL_NOKL_SPRIEGUMS_10`, `KMKL_NOKL_SPRIEGUMS_NE_10`, `KMKL_DVD`);

DELETE FROM  `kl_ref_kolonnas` WHERE KLKL_COLUMN IN ('KKMT_APJOMS_ROUND', 'KKMT_DVD');
INSERT INTO `kl_ref_kolonnas`(KLKL_COLUMN,KLKL_TITLE,KLKL_CATALOG,KLKL_IS_DEFAULT) VALUES 
('KMKL_APJOMS_ROUND', 'Apjums apaļots', 'KL_MMS_WORK_CALCULALATION', 1),
('KMKL_DVD', 'Darbība visam darbam', 'KL_MMS_WORK_CALCULALATION', 0);


UPDATE `FMK_MESSAGES` 
SET `TEXT` = '<table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr><th>Kolonas dati</th><th>Obligāts</th><th>Kolonas datu apraksts</th></tr>         
        <tr><td>Kalkulācijas šifrs</td><td>+</td><td>Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 5 simboli.</td></tr>
        <tr><td>Materiālu kategorija</td><td>+</td><td>Var saturēt tikai sekojošus simbolus `0`-`9`, `.`. Formāts [xxxx.xxx]. Maksimāli 8 simboli.</td></tr>
        <tr><td>Materiālu nomenklatūra</td><td>+</td><td>Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 7 simboli.</td></tr>
        <tr><td>Izmantot nomenklatūru</td><td>+</td><td>Cipars formātā [0/1]</td></tr>
        <tr><td>Noklusētais materiāls</td><td>+</td><td>Cipars formātā [0/1]</td></tr>
        <tr><td>Daudzums</td><td>+</td><td>Cipars formātā [xxxxx.xxx].</td></tr>
        <tr><td>Izmantot līnijas garumu</td><td>+</td><td>Cipars formātā [0/1]</td></tr>
        <tr><td>Koeficents</td><td>+</td><td>Cipars formātā [xxxxx.xxx].</td></tr>
        <tr><td>Izmantot vadu skaitu</td><td>+</td><td>Stihijas kalkul. Cipars formātā [0/1]</td></tr>
        <tr><td>Ir aktīvs</td><td>+</td><td>Kalkulācija ir aktuāla. Cipars formātā [0/1]</td></tr>
       </table>'  WHERE `CODE`  = 'REQUIREMENTS_CALCULATION_MATERIAL';

UPDATE `FMK_MESSAGES` 
SET `TEXT` = '<table cellpadding="3" cellspacing="0" border="1" width="100%">
         <tr><th>Kolonas dati</th><th>Obligāts</th><th>Kolonas datu apraksts</th></tr> 
        <tr><td>MMS nosacījuma kods</td><td>+</td><td>Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 11 simboli.</td></tr>
        <tr><td>Kalkulācijas šifrs</td><td>+</td><td>Var saturēt tikai sekojošus simbolus `0`-`9`. Maksimāli 5 simboli.</td></tr>
        <tr><td>Noklusētā kalk. sprieguma kodam 10</td><td>+</td><td>Cipars formātā [0/1]</td></tr>
        <tr><td>Noklusētā kalk. sprieguma kodam <> 10</td><td>+</td><td>Cipars formātā [0/1]</td></tr>
        <tr><td>Daudzums</td><td>+</td><td>Cipars formātā [xxxxx.xxx].</td></tr>
        <tr><td>Izmantot līnijas garumu</td><td>+</td><td>>Cipars formātā [0/1]</td></tr>
        <tr><td>Koeficents</td><td>+</td><td>Cipars formātā [xxxxx.xxx].</td></tr>
        <tr><td>Izmantot vadu skaitu</td><td>+</td><td>Stihijas kalkul. Cipars formātā [0/1]</td></tr>
        <tr><td>Ir aktīvs</td><td>+</td><td>Kalkulācija ir aktuāla. Cipars formātā [0/1]</td></tr>
        <tr><td>Trašu kalkulācija</td><td>+</td><td>Kalkulācija tiek izmantota trašu tīrīšanas darbiem. Cipars formātā [0/1]</td></tr>       
        <tr><td>Apjums apaļots</td><td>+</td><td>Cipars formātā [0/1]</td></tr>     
        <tr><td>Darbība visam darbam</td><td>+</td><td>Cipars formātā [0/1]</td></tr>        
       </table>'  WHERE `CODE`  = 'REQUIREMENTS_MMS_CALCULATION';

--
ALTER TABLE `AKTI` ADD COLUMN `RAKT_PROJECT_NO` VARCHAR(250) NULL;
ALTER TABLE `AKTI` ADD COLUMN `RAKT_TEH_REQ_NO` VARCHAR(250) NULL;

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('PROJECT_NO', 'Projekta nr'),
('TEH_REQ_NO', 'Tehnisko noteikumu nr');

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('INV_PRINT_IZPILDITAJS', 'Izpildītājs: AS "Sadales tīkls"'),
('INV_PRINT_PASUTITAJS', 'Pasūtītājs: AS "Sadales tīkls"'),
('INV_PRINT_RBF', 'Remontu un būvniecības funkcija'),
('INV_PRINT_TPF', 'Tīkla pārvaldības funkcija'),
('INV_PRINT_DATE', 'Par {DATE} izpildītajiem darbiem '),
('INV_PRINT_TITLE', 'Objekts būvēts saskaņā ar tehnisko projektu Nr. {NUMBER}, kurš izstrādāts pamatojoties uz izdotajiem'),
('INV_PRINT_TEH_REQ', 'tehniskajiem noteikumiem Nr.'),
('INV_PRINT_PROJECT_NO', 'Kapitālieguldījuma projekta Nr.'),
('INV_PRINT_OBJ_ADRESE', 'Objekts adrese'),
('INV_PRINT_DARBI', 'Būvniecības darbi'),
('INV_PRINT_KALK', 'Kalk. nr.'),
('INV_PRINT_MERV', 'Mērv.'),
('INV_PRINT_ATLIDZIBA', 'Atlīdzība par darbu'),
('INV_PRINT_SOC_APDR', 'Sociālā apdrošināšana'),
('INV_PRINT_ATL_FORMULA', 'Cilvēkstundas x Personāla izmaksas x 0.7591'),
('INV_PRINT_SOC_FORMULA', 'Cilvēkstundas x Personāla izmaksas x 0.2409'),
('INV_PRINT_PAREJAS', 'Pārējās izmaksas'),
('INV_PRINT_LIMITKARTI', 'saskaņā ar Limitkarti'),
('INV_PRINT_KVALITATE', 'Darbu apjoms un kvalitāte pārbaudīti.'),
('INV_PRINT_NODEVA', 'Darbus nodeva'),
('INV_PRINT_PIENIEMA', 'Darbus pieņēma'),
('INV_PRINT_PARAKSTS', '(ieņemamais amats, paraksts, datums)')
;
INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES
('INV_PRINT_TOTAL', 'Pavisam kopā');

ALTER TABLE `transports` ALTER `TRNS_PIEDERIBA` DROP DEFAULT;
ALTER TABLE `transports` CHANGE COLUMN `TRNS_PIEDERIBA` `TRNS_PIEDERIBA` CHAR(2) NULL COLLATE 'utf8_latvian_ci' ;
ALTER TABLE `tame_transports` ALTER `TRNS_PIEDERIBA` DROP DEFAULT;
ALTER TABLE `tame_transports` CHANGE COLUMN `TRNS_PIEDERIBA` `TRNS_PIEDERIBA` CHAR(2) NULL COLLATE 'utf8_latvian_ci' ;

UPDATE  `FMK_MESSAGES` SET `TEXT` = 'Tehniskajiem noteikumiem Nr.'
WHERE `CODE` = 'INV_PRINT_TEH_REQ';

UPDATE  `FMK_MESSAGES` SET `TEXT` = 'Objekta adrese'
WHERE `CODE` = 'INV_PRINT_OBJ_ADRESE';

UPDATE  `FMK_MESSAGES` SET `TEXT` = '(paraksts, datums)'
WHERE `CODE` = 'INV_PRINT_PARAKSTS';

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES ('INV_PRINT_PARAKSTS2', '(vārds, uzvārds, ieņemamais amats)');

INSERT INTO `FMK_MESSAGES` (`CODE`, `TEXT`)
VALUES ('INV_PRINT_POSITION', 'RBF DID darbu vadītājs');