<?

require_once(dirname(__FILE__).'/../config/main.conf.php');  
require_once(dirname(__FILE__).'/../libs/dbLayer/dbLayer.class');
require_once(dirname(__FILE__).'/../libs/dbProc/dbProc.class');	
require_once(dirname(__FILE__).'/../libs/requestHandler/requestHandler.class');
require_once(dirname(__FILE__).'/../libs/text/text.class');
require_once(dirname(__FILE__).'/../libs/process/Process.class');  
require_once(dirname(__FILE__).'/../libs/materialHandler/materialHandler.class'); 
require_once(dirname(__FILE__).'/../libs/files/files.class');
require_once(dirname(__FILE__).'/../libs/errorhandling.php');

function get_material_from_inventory( $code, $client, $date_f, $date_t)
{         
	$date_from = explode('.', $date_f);
	$date_to = explode('.', $date_t);        
	//print_r( $date_from );
	//print_r( $date_to );        
	$objReq = new get_transactions_request();
	$objReq -> subinventory_code = $code;
	$objReq -> from_date = date("Y-m-d\TH:i:s",  mktime(0,0,0,(int)$date_from[1],(int)$date_from[0],(int)$date_from[2]) );
	$objReq -> to_date = date("Y-m-d\TH:i:s",  mktime(23,59,59,(int)$date_to[1],(int)$date_to[0],(int)$date_to[2]) );
	
	//print_r($objReq);		
								
	try {		  
		return $client->get_transactions($objReq); 			
	} catch(SoapFault $fault){
		
		$error = 'Request : <xmp>'. $client->__getLastRequest().'</xmp> Error Message : ' . $fault->getMessage();            
	}
}

$processId;
$interval = 10;

while(true) 
{
    
		$date1 = new DateTime();
		$date2 = new DateTime();
		
		$date1->modify("-1 day");

		//$date1->modify("-8 day");
		//$date2->modify("-1 day");

		//$date1=date_create('2020-05-27');
		//$date2=date_create('2020-05-27');
		
		$dDateF = $date1->format('d.m.Y');
		$dDateT = $date2->format('d.m.Y');
        
        
        $client = new SoapClient(MATERIAL_SERVICE_SERVER_PATH, 
                                array('trace' => 1,'exceptions' => 1, "connection_timeout" => 180,
                                'classmap' => array('get_transactions_request' => "get_transactions_request",
                                                    'get_transactions_response' => "get_transactions_response",
                                                    'inv_transaction' => "inv_transaction",
                                                    'inv_transaction_list' => "inv_transaction_list",
                                                    'unknown_xml' => "unknown_xml",
                                                    'service_status' => "service_status",
                                                    'inv_fault' => "inv_fault"
                                                 )));
											 
		
        //echo '<pre>';

        $res = dbProc::getStockAutoList();
        
        //print_r($res);
        
        if (is_array($res)) {
            foreach ($res as $i=>$row) {

                $objResp = get_material_from_inventory($row['MSTK_AUTO_STOCK_CODE'], $client, $dDateF, $dDateT);

                if($objResp && isset($objResp)) {
                    
                    //print_r($objResp);
                    $uploader = new MaterialHandler($objResp, $dDateF, $dDateT, $row['MSTK_AUTO_STOCK_CODE']);
                    try {
        
                        if (!$uploader->run()) {
                          throw new Exception("Cannot upload material.");
                        }
                    } catch (Exception $e) {        
                        files::wh_log('Upload material: ' .date("d.m.Y H:i:s").PHP_EOL. 
                                            'MSTK_AUTO_STOCK_CODE: '. $row['MSTK_AUTO_STOCK_CODE'].PHP_EOL. 
                                            'dDateF: '. $dDateF.PHP_EOL. 
                                            'dDateT: '. $dDateT.PHP_EOL. 
                                                $e->getMessage()
                                            );	
                        continue;
                    }    
                    
                } 			
            }
        }
        //echo '</pre>';
		
        // get info about job
        $jobInfo = dbProc::getJobList(PROC_RCDIS_MATERIAL_UPLOAD);			
        if(count($jobInfo)>0)
        {
            $processId = $jobInfo[0]['PROC_PID'];
            $interval = $jobInfo[0]['PROC_INTERVAL'];
            $isStoped = ($jobInfo[0]['PROC_IR_AKTIVS'] == 0) ? true  : false ;
        }
        // pārbaude, vai jobs nav apturēts
        if($isStoped) break;

        // Время сна Демона между итерациями (зависит от потребностей системы)
        sleep($interval); //sec 
		
}

$r=dbProc::saveJob(PPROC_RCDIS_MATERIAL_UPLOAD, false, 0, $interval);
// gadijumā, ja jobs ir bijis apturēts, nokilot arī procesu
$process = new Process();
$process->setPid($processId); 
$stopped = $process->stop();


?>
