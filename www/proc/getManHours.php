<?

    require_once(dirname(__FILE__).'/../config/main.conf.php');  
    require_once(dirname(__FILE__).'/../libs/dbLayer/dbLayer.class');
    require_once(dirname(__FILE__).'/../libs/dbProc/dbProc.class');	
    require_once(dirname(__FILE__).'/../libs/requestHandler/requestHandler.class');
    require_once(dirname(__FILE__).'/../libs/text/text.class');
    require_once(dirname(__FILE__).'/../libs/process/Process.class'); 
    require_once(dirname(__FILE__).'/../libs/files/files.class');
    require_once(dirname(__FILE__).'/../libs/userAuthorization/userauthorization.class');
    require_once(dirname(__FILE__).'/../libs/session/session.class');

    function json_response($code = 200, $message = null)
	{
		// clear the old headers
		header_remove();
		// set the actual code
		http_response_code($code);
		// set the header to make sure cache is forced
		header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");
		// treat this as json
		header('Content-Type: application/json');
		$status = array(
			200 => '200 OK',
			400 => '400 Bad Request',
			422 => 'Unprocessable Entity',
			500 => '500 Internal Server Error'
			);
		// ok, validation error, or failure
		header('Status: '.$status[$code]);
		// return the encoded json
		return json_encode($message);
	}

    ob_start();
	$r = false;
	// if you are doing ajax with application-json headers
	$data = json_decode(file_get_contents("php://input")) ? : 'nifiga net';	

	files::wh_log('Date for report from DRPR: ' .date("d.m.Y H:i:s").PHP_EOL. 
									'date_from: '. (isset($data->date_from) ? $data->date_from : '').PHP_EOL.
									'date_to: '.(isset($data->date_to) ? $data->date_to : '').PHP_EOL
							);								
	//var_dump($data);
	if( isset($data->date_from) && isset($data->date_to) )
	{
        
        $firstDay = $data->date_from;//->format('Y-m-d');
        $lastDay = $data->date_to;//->format('Y-m-d');
    }    
    else {
       
        //$dateToday =date_create('2021-11-19');
        $dateToday = new DateTime();
        
        $dateToday->modify('first day of this month');
        $firstDay = $dateToday->format('Y-m-d');
        $dateToday->modify('last day of this month');
        $lastDay = $dateToday->format('Y-m-d');

	    //$firstDay = date('Y-m-d', strtotime('2022-01-12'));
	    //$lastDay = date('Y-m-d', strtotime('2022-01-13'));

        //echo json_response(400, array('Invalid input date'));
        //die;
    }
    
    // define search critery
    $Criteria = '';    
    $Criteria .= 'INSERT*ACCEPT*RETURN*CLOSE*CLOSEWRITEOFF^';   
    $Criteria .=  $firstDay.'^';
    $Criteria .=  $lastDay.'^';
    

    // gel list of users
    $res = dbProc::getCustomerWorkTimeDescriptionList1($Criteria);
    /*echo '<pre>';
    print_r($res);
    echo '</pre>'; */
    $processOk = true;
    $customer = array();
    if (is_array($res))
    {

        foreach ($res as $i=>$row)
        {
            $customer[$row['PRSN_RCD_KODS']]  = array (
            'code' => $row['PRSN_RCD_KODS'],
            'workPlace' => $row['PRSN_DARBA_VIETA'],
            'name' => $row['KPRF_VARDS'],
            'surname' => $row['KPRF_UZVARDS'],
            'act' => array()
            );
        }
        foreach ($res as $i=>$row)
        {

            $customer[$row['PRSN_RCD_KODS']]['act'][$row['POSTFIX'].$row['PRSN_PLAN_DATE']]  = array(
                'mainTime' => $customer[$row['PRSN_RCD_KODS']]['act'][$row['POSTFIX'].$row['PRSN_PLAN_DATE']]['mainTime'] += $row['PRSN_PAMATSTUNDAS'],
                'routTime' => $customer[$row['PRSN_RCD_KODS']]['act'][$row['POSTFIX'].$row['PRSN_PLAN_DATE']]['routTime'] += $row['PRSN_LAIKSCELA'],
                'actNumber' => $row['NUMURS'],
                'actPostfix' => $row['POSTFIX'],
                'status' => $row['STATUS_NAME'],
                'type' => $row['KAKV_NOSAUKUMS'],
                'planDate'  => date_create($row['PRSN_PLAN_DATE'])->format('Y-m-d')
            );
        }
    }
  /*  
    echo '<pre>';
    print_r($customer);
    echo '</pre>';
  */  
    $params = array(
        'query' => array(
            'match' => array(
                'content' => 'quick brown fox'
            )
        ),
        'sort' => array(    
            array('time' => array('order' => 'desc')),  
            array('popularity' => array('order' => 'desc')) 
        )
    );

    

    $result = array('man_hours' => array());

    if (is_array($customer) && count($customer) > 0)
    {
        foreach ($customer as $row)
        {
            $worker =array(
                'worker_name' => $row['name'],  
                'worker_surname'=>  $row['surname'],  
                'worker_code'=>  $row['code']  ,
                'work_place'=>  $row['workPlace'] ,
                'acts' => array()                
            );
            if (is_array($row['act']) && count($row['act']) > 0)
            {
		foreach ($row['act'] as $r)
		{
                

                    $act = array(
                        'act_number'=>  $r['actNumber'],  
                        'kvik_step_code'=>  $r['actPostfix'],
                        'act_status'=>  $r['status'],  
                        'act_type'=>  $r['type'], 
                        'act_izpild_date'=>  $r['planDate'],  
                        'main_time'=>  $r['mainTime'], 
                        'rout_time'=>  $r['routTime']
                    );
                
if($r['actPostfix'] == 'REM-151627'){
 /*echo '<pre>';
    print_r($act);
    echo '</pre>'; */
 }
		
		

                $worker['acts'][] = array('act' => $act);
		
		}
    /*echo '<pre>';
    print_r($worker['acts']);
    echo '</pre>';
*/
            }
            $result['man_hours'][] = array('worker' => $worker);
        }
/*
    echo '<pre>';
    print_r($result);
    echo '</pre>';
*/
    }
    else 
    {
        $processOk = false;
    }

    if($processOk) {
        echo json_response(200, $result);
    }
    else {
        echo json_response(422, array('No data'));
    }

   

     
?>
