<?

require_once(dirname(__FILE__).'/../config/main.conf.php');  
require_once(dirname(__FILE__).'/../libs/dbLayer/dbLayer.class');
require_once(dirname(__FILE__).'/../libs/dbProc/dbProc.class');	
require_once(dirname(__FILE__).'/../libs/requestHandler/requestHandler.class');
require_once(dirname(__FILE__).'/../libs/text/text.class');
require_once(dirname(__FILE__).'/../libs/process/Process.class'); 
require_once(dirname(__FILE__).'/../libs/files/files.class');
require_once(dirname(__FILE__).'/../libs/userAuthorization/userauthorization.class');
require_once(dirname(__FILE__).'/../libs/session/session.class');
//require_once(dirname(__FILE__).'/../libs/errorhandling.php');


    //INSERT into event_modules(EVMD_MODULE,EVMD_FORM_ID,EVMD_FORM_NAME) 
    //VALUES('AUTOPROC', 'getManHours', 'SYSTEM_PROCESS_INFO');

    $dateToday =date_create('2020-10-19');
    $dateToday = new DateTime();
    //$dateToday = $dateToday->format('Y-m-d');


    $dateToday->modify('first day of this month');
    $firstDay = $dateToday->format('Y-m-d');
    $dateToday->modify('last day of this month');
    $lastDay = $dateToday->format('Y-m-d');
    
    // define search critery
    $Criteria = '';    
    $Criteria .= 'INSERT*ACCEPT*RETURN*CLOSE*CLOSEWRITEOFF^';   
    $Criteria .=  $firstDay.'^';
    $Criteria .=  $lastDay.'^';
    

    // gel list of users
    $res = dbProc::getCustomerWorkTimeDescriptionList1($Criteria);
    /*echo '<pre>';
    print_r($res);
    echo '</pre>'; */
    $customer = array();
    if (is_array($res))
    {

        foreach ($res as $i=>$row)
        {
            $customer[$row['PRSN_RCD_KODS']]  = array (
            'code' => $row['PRSN_RCD_KODS'],
            'workPlace' => $row['PRSN_DARBA_VIETA'],
            'name' => $row['KPRF_VARDS'],
            'surname' => $row['KPRF_UZVARDS'],
            'act' => array()
            );
        }
        foreach ($res as $i=>$row)
        {

            $customer[$row['PRSN_RCD_KODS']]['act'][$row['POSTFIX'].$row['PRSN_PLAN_DATE']]  = array(
                'mainTime' => $customer[$row['PRSN_RCD_KODS']]['act'][$row['POSTFIX'].$row['PRSN_PLAN_DATE']]['mainTime'] += $row['PRSN_PAMATSTUNDAS'],
                'routTime' => $customer[$row['PRSN_RCD_KODS']]['act'][$row['POSTFIX'].$row['PRSN_PLAN_DATE']]['routTime'] += $row['PRSN_LAIKSCELA'],
                'actNumber' => $row['NUMURS'],
                'actPostfix' => $row['POSTFIX'],
                'status' => $row['STATUS_NAME'],
                'type' => $row['KAKV_NOSAUKUMS'],
                'planDate'  => date_create($row['PRSN_PLAN_DATE'])->format('Y-m-d')
            );
        }
    }

    /*echo '<pre>';
    print_r($customer);
    echo '</pre>'; */ 

   $writer = new XMLWriter();  
   $writer->openMemory();
    $writer->openURI('php://output');   
    $writer->startDocument('1.0','UTF-8');   
    $writer->setIndent(4);   
    $writer->startElement('man_hours');  
    // Start the namespaced attribute
	$writer->startAttributeNs('pre', 'inv', 'value');
   
	// Add value to the attribute
	$writer->text('http://www.w3.org/2000/xmlns');
	//$writer->text('http://latvenergo.lv/EBS/INV');
   
	// End the attribute
	$writer->endAttribute();
    

    if (is_array($customer) && count($customer) > 0)
	{
		$i = 0;
		foreach ($customer as $row)
		{
			$i++;
			//if ($i > 1) break;

            $writer->startElement("worker"); 
	    
            $writer->writeElement('worker_name', $row['name']);  
            $writer->writeElement('worker_surname', $row['surname']);  
            $writer->writeElement('worker_code', $row['code']);  
            $writer->writeElement('work_place', $row['workPlace']); 

            $writer->startElement("acts"); 

            if (is_array($row['act']) && count($row['act']) > 0)
            {
                foreach ($row['act'] as $r)
                {
                    $writer->startElement("act"); 

                    $writer->writeElement('act_number', $r['actNumber']);  
                    $writer->writeElement('kvik_step_code', $r['actPostfix']);  
                    $writer->writeElement('act_status', $r['status']);  
                    $writer->writeElement('act_type', $r['type']);  
                    $writer->writeElement('act_izpild_date', $r['planDate']);  
                    $writer->writeElement('main_time', $r['mainTime'] );  
                    $writer->writeElement('rout_time', $r['routTime'] );  
                    $writer->endElement(); 
                }
            }
            $writer->endElement();
            $writer->endElement();  
	        $writer->flush(); 

        }
    }

    $writer->endElement(); 
    $writer->endDocument(); 
    //header('Content-type: text/xml');
    header("Content-type: text/xml; charset=utf-8");
    //echo sendResponse($type,$cause);
    //$writer->flush(); 
    echo $writer->outputMemory(true);
    die;
     
?>
