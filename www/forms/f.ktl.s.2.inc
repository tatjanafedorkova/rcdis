﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAuditor =  dbProc::isUserInRole($userId, ROLE_AUDIT_USER);
// tikai administratoram ir pieeja!
if (userAuthorization::isAdmin() || $isAuditor)
{
	$editMode = reqVar::get('editMode');
	// bottom frame
	// if insert/update

     if($editMode)
	{
        // get region list
       $region=array();
       $region['']=text::get('EMPTY_SELECT_OPTION');
       $res=dbProc::getKrfkName(KRFK_REGION);
       if (is_array($res))
       {
       	foreach ($res as $row)
       	{
       		$region[$row['nosaukums']]=$row['nosaukums'];
       	}
       }
       unset($res);

		$customerId = reqVar::get('customerId');

		$oLink=new urlQuery();
		$oLink->addPrm(FORM_ID, 'f.ktl.s.2');
		$oLink->addPrm('editMode', 1 );
		// form inicialization
		$oForm = new Form('frmMain','post',$oLink->getQuery());
		unset($oLink);

		// if operation is success, show success message and redirect top frame
		if (reqVar::get('successMessage'))
		{
			$oForm->addSuccess(reqVar::get('successMessage'));
			$oForm->addElement('static','jsRefresh2','','<script>refreshFrame(1);</script>');
		}

		// inicial state: customerId=0;
		// if customerId>0 ==> voltage selected from the list
		if($customerId != false)
		{
			// get info about voltage
			$customerInfo = dbProc::getCustomerList($customerId);
			//print_r($customerInfo);
			if(count($customerInfo)>0)
			{
				$customer = $customerInfo[0];
			}

			// get user info
			$userInfo = dbProc::getUserInfo($customer['KPRF_CREATOR']);
			if(count($userInfo) >0 )
			{
				$creator = $userInfo[0]['RLTT_VARDS']. ' ' .$userInfo[0]['RLTT_UZVARDS'];
			}
			$userInfo1 = dbProc::getUserInfo($customer['KPRF_EDITOR']);
			if(count($userInfo1) >0 )
			{
				$editor = $userInfo1[0]['RLTT_VARDS']. ' ' .$userInfo1[0]['RLTT_UZVARDS'];
			}
        }

		// form elements
		$oForm -> addElement('hidden', 'action', '');
		$oForm -> addElement('hidden', 'customerId', null, isset($customer['KPRF_ID'])? $customer['KPRF_ID']:'');

        $oForm -> addElement('text', 'kods',  text::get('RCD_KODS'), isset($customer['KPRF_KODS'])?$customer['KPRF_KODS']:'', ' tabindex=1  maxlength="4"' . (($customerId || $isAuditor)?' disabled' : ''));
		$oForm -> addRule('kods', text::get('ERROR_REQUIRED_FIELD'), 'required');
        $oForm -> addRule('kods', text::get('ERROR_ALPHANUMERIC_VALUE'), 'alphanumeric');

        $oForm -> addElement('text', 'uzvards',  text::get('USER_SURNAME'), isset($customer['KPRF_UZVARDS'])?$customer['KPRF_UZVARDS']:'', ' tabindex=3 maxlength="30"'.(($isAuditor)?' disabled' : ''));
		$oForm -> addRule('uzvards', text::get('ERROR_REQUIRED_FIELD'), 'required');

        $oForm -> addElement('text', 'vards',  text::get('USER_NAME'), isset($customer['KPRF_VARDS'])?$customer['KPRF_VARDS']:'', ' tabindex=5 maxlength="30"'.(($isAuditor)?' disabled' : ''));
		$oForm -> addRule('vards', text::get('ERROR_REQUIRED_FIELD'), 'required');

        $oForm -> addElement('text', 'workPlace',  text::get('WORKING_PLACE_NUMBER'), isset($customer['KPRF_DARBA_VIETA'])?$customer['KPRF_DARBA_VIETA']:'', ' tabindex=2 maxlength="10"'.(($isAuditor)?' disabled' : ''));
        $oForm -> addRule('workPlace', text::get('ERROR_REQUIRED_FIELD'), 'required');

        $oForm -> addElement('select', 'region',  text::get('REGION'), isset($customer['KPRF_REGIONS'])?$customer['KPRF_REGIONS']:'', ' tabindex=4'.(($isAuditor)?' disabled' : ''), '', '', $region);
        //$oForm -> addRule('region', text::get('ERROR_REQUIRED_FIELD'), 'required');

		$oForm -> addElement('checkbox', 'isActive',  text::get('IS_ACTIVE'), isset($customer['KPRF_IR_AKTIVS'])?$customer['KPRF_IR_AKTIVS']:1, 'tabindex=6'.(($isAuditor)?' disabled' : ''));
		
		

		// importa dati
		$oForm -> addElement('text', 'created',  text::get('CREATED'), isset($customer['KPRF_CREATED'])?$customer['KPRF_CREATED']:dtime::now(), 'tabindex=21 maxlength="20" disabled');
		$oForm -> addElement('text', 'creator',  text::get('CREATOR'), isset($creator)?$creator:'', 'tabindex=21 maxlength="20" disabled');
		$oForm -> addElement('text', 'edited',  text::get('EDITED'), isset($customer['KPRF_EDITED'])?$customer['KPRF_EDITED']:dtime::now(), 'tabindex=21 maxlength="20" disabled');
		$oForm -> addElement('text', 'editor',  text::get('EDITOR'), isset($customer)?$editor:'', 'tabindex=21 maxlength="20" disabled');

		// form buttons
        if(!$isAuditor)
        {
		$oForm -> addElement('submitImg', 'add', text::get('ADD'), 'img/btn_pievienot.gif', 'width="70" height="20" onclick="setValue(\'action\',\''.OP_INSERT.'\');"');
		$oForm -> addElement('submitImg', 'save', text::get('SAVE'), 'img/btn_saglabat.gif', 'width="70" height="20" onclick="if (!isEmptyField(\'customerId\')) {setValue(\'action\',\''.OP_UPDATE.'\');} else {return false;}"');
		$oForm -> addElement('clearImg', 'clear', text::get('CLEAR'), 'img/btn_attirit.gif', 'width="70" height="20"','','',array('onClick'=>'refreshButton(\'customerId\');parent.frame_1.unActiveRow();'));
        $oForm -> addElement('buttonImg', 'deleteFavorit', text::get('CLEAN_FAVORITS'), 'img/btn_iztirit_favoritus.gif', 'width="90" height="20" onclick="setValue(\'action\',\''.OP_DELETE.'\'); document.all[\'frmMain\'].submit();"');
     	$oForm -> addElement('static', 'jsButtonsControl', '', '
			<script>
				function refreshButton(name)
				{
					if (!isEmptyField(name))
					{
						document.all["save"].src="img/btn_saglabat.gif";

					}
					else
					{
						document.all["save"].src="img/btn_saglabat_disabled.gif";
						document.all["isActive"].checked=true;

					}
				}
				refreshButton("customerId");
			</script>
		');
        }
		// if operation
		if ($oForm -> isFormSubmitted())
		{
			$check = true;
			$r = false;
			$checkRequiredFields = false;
            // delete favorits
            if ($oForm->getValue('action')== OP_DELETE )
            {
               $favorit = dbProc::getKrfkInfo(21);

               $r = dbProc::deleteNotActivFromUserFavorit($favorit['KRFK_VERTIBA']) ;

            }
            // save
			elseif ($oForm->getValue('action')==OP_INSERT || ($oForm->getValue('action')==OP_UPDATE  && is_numeric($oForm->getValue('customerId'))))
			{
    			if ($oForm -> getValue('kods') && $oForm -> getValue('vards') &&
                    $oForm -> getValue('uzvards') && /*$oForm -> getValue('region') &&*/
                    $oForm -> getValue('workPlace') )
    			{
    				$checkRequiredFields = true;
    			}
    			if ($checkRequiredFields)
    			{

        			// chech that ED area with same region and code not set
        			if (dbProc::customerWithCodeExists(
                                                                  $oForm->getValue('kods'),
                                                                  ($oForm->getValue('action')==OP_UPDATE)?$oForm->getValue('customerId'):false)
                                                                  )
        			{
        				$oForm->addError(text::get('ERROR_EXISTS_CUSTOMER_CODE'));
        				$check = false;
        			}

        			// if all is ok, do operation
        			if($check)
        			{
        				$r=dbProc::saveCustomer(($oForm->getValue('action')==OP_UPDATE?$oForm->getValue('customerId'):false),
        				$oForm->getValue('kods'),
                              $oForm->getValue('uzvards'),
                              $oForm->getValue('vards'),
                              $oForm->getValue('workPlace'),
                              $oForm->getValue('region'),
        					  $oForm->getValue('isActive',0),
							  $userId
        				);

        				if (!$r)
        				{
        					$oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
        				}
        			}
                }
			}
			else
			{
				$oForm->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
			}
            // if operation was compleated succefully, show success mesage and redirect current frame
    		if ($r)
    		{
    			$oLink=new urlQuery();
    			$oLink->addPrm(FORM_ID, 'f.ktl.s.2');
    			$oLink->addPrm('editMode', '1');
    			switch($oForm->getValue('action'))
    			{
    				case OP_INSERT:
    					$oLink->addPrm('successMessage', text::get('RECORD_WAS_ADDED'));
    					break;
    				case OP_UPDATE:
    					$oLink->addPrm('successMessage', text::get('RECORD_WAS_SAVED'));
    					break;
                           case OP_DELETE:
          					$oLink->addPrm('successMessage', text::get('FAVORITS_IS_CLENED'));
          					break;
    			}
    			RequestHandler::makeRedirect($oLink->getQuery());
    		}
		}

       	$oForm -> makeHtml();
		include('f.ktl.s.2.2.tpl');
	}
	// top frale
	else
	{
        // Check if this form was requested via xmlHttpRequest and search
    	if (isset($_GET['xml']) && isset($_GET['search_q']) && isset($_GET['search_c']))
    	{
    		ob_clean();
    		$q = $_GET['search_q'];
            if (isset($q) || $q == 0)
    		{
    			echo 'q = "'.trim($q).'";';
    		}
            else
            {
    			echo 'q = "";';
    		}
            $c = $_GET['search_c'];
            if ($c)
    		{
    			echo 'c = "'.$c.'";';
    		}
            else
            {
    			echo 'c = "";';
    		}
		    exit;
    	}
        if (isset($_GET['xml']) && isset($_GET['sort_k']) && isset($_GET['sort_o']))
    	{
    		ob_clean();
    		$o = $_GET['sort_o'];
            if (isset($o))
    		{
    			echo 'o = "'.$o.'";';
    		}
            else
            {
    			echo 'o = "";';
    		}
            $k = $_GET['sort_k'];
            if ($k)
    		{
    			echo 'k = "'.$k.'";';
    		}
            else
            {
    			echo 'k = "";';
    		}
		    exit;
    	}
        // set list title
        $title = text::toUpper(text::get('CUSTOMERS'));
        // get search column list
        $columns=dbProc::getKlklName(KL_CUSTOMERS);
        $searchText = ((reqVar::get('search') && reqVar::get('search') != '') || reqVar::get('search') == 0) ? reqVar::get('search') : false;
        $searchColumn = (reqVar::get('column') && reqVar::get('column') != '') ? reqVar::get('column') : false;
        $sortOrder = (reqVar::get('order') && reqVar::get('order') != '') ? reqVar::get('order') : false;
        $orderColumn = (reqVar::get('kol') && reqVar::get('kol') != '') ? reqVar::get('kol') : false;


        // default sort column
        $defaultColumn = dbProc::getKlklDefaultColumn(KL_CUSTOMERS);
        dbProc::getCalculationGroupActivList();
        // URL to search Zinojums by number.
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.ktl.s.2');
    	$oLink->addPrm('isNew', '1');


        // add search/order params to listing
        $oLink->addPrm('search', reqVar::get('search'));
        $oLink->addPrm('column', reqVar::get('column'));
        $oLink->addPrm('order', reqVar::get('order'));
        $oLink->addPrm('kol', reqVar::get('kol'));
        $searchLink = $oLink -> getQuery();

		// inicialising of listing object
		$amount=dbProc::getCustomerCount($searchText,
                (($searchColumn === false) ? $defaultColumn : $searchColumn));
		$listing=new listing();
		$listing->setAmount($amount);

		$listingHtml=$listing->getHtml($oLink ->getQuery());
		unset($oLink);

		//link for frame2
		$oLink=new urlQuery();
		$oLink->addPrm(FORM_ID, 'f.ktl.s.2');
		$oLink->addPrm('editMode', 1);

		// gel list of users
		$res = dbProc::getCustomerList( false,
                                        $listing->getStartPosition(),
                                        $listing->getAmountOnPage(),
                                        $searchText,
                                        (($searchColumn === false) ? $defaultColumn : $searchColumn),
                                        $sortOrder,
                                        $orderColumn);

		if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
				// add dv area Id to each link
				$oLink->addPrm('customerId', $row['KPRF_ID']);
				$res[$i]['URL']=$oLink ->getQuery();
			}
		}
		unset($oLink);

		// inicial state of users number
		$number=$listing->getStartPosition()+1;

		include('f.ktl.s.x.1.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
