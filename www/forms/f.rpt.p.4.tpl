﻿<body class="print">
<div class="print_header"><?= text::get('REPORT_MATERIALS_IN_MONTH'); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_landscape" >
<?
  foreach($searchCr as $i=>$s)
  {
    if($i%3 == 0)
    {
      ?>
        <tr>
      <?
    }
    ?>
      <td align="right" class="print_table_total" width="13%"><?=$s['label'];?>:</td>
      <td align="left" class="print_table_data" width="20%"><?=$s['value'];?></td>
    <?
  }
?>
</table>
<br />
<table cellpadding="0" cellspacing="0" class="print_table_landscape" >

		<tr>
			<td width="12%" class="print_table_header"><?= text::get('REPORT_MATERIAL_CODE'); ?></td>
			<td width="40%" class="print_table_header"><?= text::get('REPORT_MATERIAL_TITLE'); ?></td>
            <td width="12%" class="print_table_header"><?=text::get('REPORT_MATERIAL_MIASURE');?></td>
            <td width="12%" class="print_table_header"><?=text::get('REPORT_MATERIAL_PRICE');?></td>
            <td width="12%" class="print_table_header"><?=text::get('REPORT_MATERIAL_AMOUNT');?></td>
            <td width="12%" class="print_table_header"><?=text::get('REPORT_MATERIAL_PRICE_TOTAL');?></td>
            <!--td width="8%" class="print_table_header"><?=text::get('ACT_NUMBER');?> </td>
            <td width="15%" class="print_table_header"><?=text::get('SINGLE_SOURCE_OF_FOUNDS');?> </td>
            <td width="5%" class="print_table_header"><?=text::get('RCD_REGION');?> </td>
            <td width="15%" class="print_table_header"><?=text::get('SINGL_DV_AREA');?> </td>
            <td width="5%" class="print_table_header"><?=text::get('ED_REGION');?> </td>
            <td width="15%" class="print_table_header"><?=text::get('ED_IECIKNIS');?> </td-->
 		</tr>

<?
	if (is_array($res) && count($res) > 0)
	{
		foreach ($res as $row)
		{
?>
			<tr >
			   	<td align="center" class="print_table_data"><?= $row['MATR_KODS']; ?></td>
                <td align="left" class="print_table_data"><?= $row['MATR_NOSAUKUMS']; ?></td>
                <td align="center" class="print_table_data"><?= $row['MATR_MERVIENIBA']; ?></td>
                <td align="center" class="print_table_data"><?= $row['MATR_CENA']; ?></td>
                <td align="center" class="print_table_data"><?= $row['DAUDZUMS']; ?></td>
                <td align="center" class="print_table_data"><?= $row['CENA_KOPA']; ?></td>

                <!--td align="center" class="print_table_data"><?= $row['NUMURS']; ?></td>
                <td align="center" class="print_table_data"><?= $row['KFNA_NOSAUKUMS']; ?></td>
                <td align="center" class="print_table_data"><?= $row['KDVI_REGIONS']; ?></td>
                <td align="center" class="print_table_data"><?= $row['DV']; ?></td>
                <td align="center" class="print_table_data"><?= $row['KEDI_REGIONS']; ?></td>
                <td align="center" class="print_table_data"><?= $row['ED']; ?></td-->
			</tr>
<?
		}
?>
        <tr>
             <td align="right" class="print_table_total" colspan="5"><?=text::get('TOTAL');?>:</td>
             <td align="center" class="print_table_total"><?= $summa; ?></td>
       </tr>
<?
	}
?>

</table>

</body>