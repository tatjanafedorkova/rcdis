﻿<body class="frame_2">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><h1><?= text::toUpper(text::get('REPORT_NOT_STARTED_WORK')); ?></h1></td>
		<td align="right">
            <img src="img/ico_print.gif" alt="" width="16" height="16" border="0">&nbsp;
            <a href="<?= $printUrl; ?>" target="new"><?=text::get('PRINT');?></a>&nbsp;&nbsp;
            <!-- img src="img/ico_excel.gif" alt="" width="16" height="16">&nbsp;
            <a href="<?= $exportUrl; ?>" ><?=text::get('EXPORT');?></a -->&nbsp;
        </td>
	</tr>
</table>

<table cellpadding="5" cellspacing="1" border="0" width="100%" >
<?
	if (is_array($area) && count($area) > 0)
	{
		foreach ($area as $row)
		{

          if (is_array($row['ed']) && count($row['ed']) > 0)
          {
            foreach ($row['ed'] as $rrS)
            {
?>
            <tr class="table_cell_3">
                <td align="left"><b><?=text::get('ED_IECIKNIS');?>:</b></td>
                <td colspan="2" align="left"><?= $rrS['name']; ?></td>
                <td colspan="2" align="left"><?= $rrS['code']; ?></td>
            </tr>
            <tr class="table_head_2">
    			<td width="40%"><?= text::get('SINGLE_MMS'); ?></td>
                <td width="15%"><?=text::get('QUARTER');?></td>
                <td width="15%"><?= text::get('MAN_HOUR_STANDART_TOTAL'); ?></td>
                <td width="15%"><?= text::get('MATERIAL_PRICE'); ?></td>
                <td width="15%"><?=text::get('PRIORITY');?></td>

            </tr>
             <?
                if (is_array($rrS['mms']) && count($rrS['mms']) > 0)
                {
                   foreach ($rrS['mms'] as $r)
                   {
                     ?>
                   <tr class="table_cell_3" >
      			      <td align="left"><?= $r['code']; ?></td>
                      <td align="center"><?= $r['quarter']; ?></td>
                      <td align="center"><?= $r['stundas']; ?></td>
                      <td align="center"><?= $r['cena']; ?></td>
                      <td align="center"><?= $r['priority']; ?></td>

                  </tr>
                     <?

                   }
                }
            ?>
            <tr class="table_head_2">
             <td colspan="2" align="left"><b><?=$rrS['name'];?>: </b></td>
             <td align="center"><b><?=number_format($rrS['totalStundas'],2,'.','');?></b></td>
             <td align="center"><b><?=number_format($rrS['totalCena'],2,'.','');?></b></td>
             <td>&nbsp;</td>
            </tr>
            <tr><td colspan="5">&nbsp;</td></tr>
          <? }}?>
            <tr class="table_head_2">
             <td colspan="2" align="left"><b><?=$row['section'];?>:</b></td>
             <td align="center"><b><?=number_format($row['totalStundas'],2,'.','');?></b></td>
             <td align="center"><b><?=number_format($row['totalCena'],2,'.','');?></b></td>
             <td>&nbsp;</td>
            </tr>

        <?
		}
        ?>
        <tr>
             <td colspan="2" align="right"><b><?=text::get('TOTAL');?>:</b></td>
             <td align="center"><b><?= $stundas; ?></b></td>
             <td align="center"><b><?= $summa; ?></b></td>
             <td>&nbsp;</td>
        </tr>
<?
	}
?>
	</tbody>
</table>

</body>