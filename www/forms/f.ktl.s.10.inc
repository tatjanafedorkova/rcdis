<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isEdUser = (dbProc::isUserInRole($userId, ROLE_ED_USER) || dbProc::isUserInRole($userId, ROLE_AUDIT_USER));
if (userAuthorization::isAdmin() || $isEdUser )
{
	$editMode = reqVar::get('editMode');
	// bottom frame
	// if insert/update

     if($editMode)
	{
		$calcId = reqVar::get('calcId');

		$oLink=new urlQuery();
		$oLink->addPrm(FORM_ID, 'f.ktl.s.10');
		$oLink->addPrm('editMode', 1 );
		// form inicialization
		$oForm = new Form('frmMain','post',$oLink->getQuery());
		unset($oLink);

		// if operation is success, show success message and redirect top frame
		if (reqVar::get('successMessage'))
		{
			$oForm->addSuccess(reqVar::get('successMessage'));
			$oForm->addElement('static','jsRefresh2','','<script>refreshFrame(1);</script>');
		}

		// inicial state: calcId=0;
		// if calcId>0 ==> voltage selected from the list
		if($calcId != false)
		{
			// get info about voltage
			$calculationInfo = dbProc::getCalculationList($calcId);
			//print_r($calculationInfo);
			if(count($calculationInfo)>0)
			{
				$calc = $calculationInfo[0];
			}
			// get user info
			$userInfo = dbProc::getUserInfo($calc['KKAL_CREATOR']);
			if(count($userInfo) >0 )
			{
				$creator = $userInfo[0]['RLTT_VARDS']. ' ' .$userInfo[0]['RLTT_UZVARDS'];
			}
			$userInfo1 = dbProc::getUserInfo($calc['KKAL_EDITOR']);
			if(count($userInfo1) >0 )
			{
				$editor = $userInfo1[0]['RLTT_VARDS']. ' ' .$userInfo1[0]['RLTT_UZVARDS'];
			}
        }

		// form elements
		$oForm -> addElement('hidden', 'action', '');
		$oForm -> addElement('hidden', 'calcId', null, isset($calc['KKAL_ID'])? $calc['KKAL_ID']:'');

        $oForm -> addElement('text', 'chipher',  text::get('CHIPHER'), isset($calc['KKAL_SHIFRS'])?$calc['KKAL_SHIFRS']:'', 'tabindex=3 maxlength="5"' . (($calcId || $isEdUser)?' disabled' : ''));
		$oForm -> addRule('chipher', text::get('ERROR_REQUIRED_FIELD'), 'required');
        $oForm -> addRule('chipher', text::get('ERROR_NUMERIC_VALUE'), 'numeric');

        $oForm -> addElement('kls', 'kods',  text::get('SINGL_CALCULATION_GROUP'), array('classifName'=>KL_CALCULALATION_GROUP,'value'=>isset($calc['KKAL_GRUPAS_KODS'])?$calc['KKAL_GRUPAS_KODS']:'','readonly'=>false), 'tabindex=1 maxlength="2000"'.(($isEdUser)?' disabled' : ''));
        $oForm -> addRule('kods', text::get('ERROR_REQUIRED_FIELD'), 'required');

        $oForm -> addElement('text', 'nosaukums',  text::get('NAME'), isset($calc['KKAL_NOSAUKUMS'])?$calc['KKAL_NOSAUKUMS']:'', 'tabindex=5 maxlength="150"'.(($isEdUser)?' disabled' : ''));
		$oForm -> addRule('nosaukums', text::get('ERROR_REQUIRED_FIELD'), 'required');

        $oForm -> addElement('textarea', 'description', text::get('DESCRIPTION'), isset($calc['KKAL_APRAKSTS'])?$calc['KKAL_APRAKSTS']:'','tabindex=7'.(($isEdUser)?' disabled' : ''));
		$oForm -> addRule('description', text::get('ERROR_MAXLENGHT_REACHED'), 'maxlength',2000);

        $oForm -> addElement('text', 'fizRadijums',  text::get('FIZ_RADIJUMS'), isset($calc['KKAL_FIZ_RADIJUMS'])?$calc['KKAL_FIZ_RADIJUMS']:'', 'tabindex=8 maxlength="4"'.(($isEdUser)?' disabled' : ''));

        $oForm -> addElement('text', 'koeficent',  text::get('KOEFICENT'), isset($calc['KKAL_KOEFICENT'])?$calc['KKAL_KOEFICENT']:'', 'tabindex=9 maxlength="6"'.(($isEdUser)?' disabled' : ''));
        $oForm -> addRule('koeficent', text::get('ERROR_DOUBLE_VALUE'), 'double', 3);

        $oForm -> addElement('text', 'standart',  text::get('PLAN_HOUR_STANDART'), isset($calc['KKAL_PLANA'])?$calc['KKAL_PLANA']:'', 'tabindex=2 maxlength="6"'.(($isEdUser)?' disabled' : ''));
		$oForm -> addRule('standart', text::get('ERROR_REQUIRED_FIELD'), 'required');
        $oForm -> addRule('standart', text::get('ERROR_DOUBLE_VALUE'), 'double', 2);
        // check Standart field
        $oForm->addElement('hidden','minStandart','','0.00');
        $oForm->addElement('hidden','maxStandart','','1000.00');
        $v1=&$oForm->createValidation('standartValidation', array('standart'), 'ifonefilled');
        // ja Cilv�kstundas normat�vs ir defin�ts un > 1000
        //$r1=&$oForm->createRule(array('standart', 'minStandart'), text::get('ERROR_AMOUNT_VALUE'), 'comparedouble','>');
        $r2=&$oForm->createRule(array('standart', 'maxStandart'), text::get('ERROR_AMOUNT_VALUE'), 'comparedouble','<');
        $oForm->addRule(array( $r2), 'groupRuleStandart', 'group', $v1);


        $oForm -> addElement('text', 'standart_np',  text::get('NOTPLAN_HOUR_STANDART'), isset($calc['KKAL_NEPLANA'])?$calc['KKAL_NEPLANA']:'', 'tabindex=2 maxlength="6"'.(($isEdUser)?' disabled' : ''));
		$oForm -> addRule('standart_np', text::get('ERROR_REQUIRED_FIELD'), 'required');
        $oForm -> addRule('standart_np', text::get('ERROR_DOUBLE_VALUE'), 'double', 2);
        // check Standart field
        $oForm->addElement('hidden','minStandart_np','','0.00');
        $oForm->addElement('hidden','maxStandart_np','','1000.00');
        $v1=&$oForm->createValidation('standartValidationNp', array('standart_np'), 'ifonefilled');
        // ja Cilv�kstundas normat�vs ir defin�ts un > 1000
        //$r1=&$oForm->createRule(array('standart', 'minStandart'), text::get('ERROR_AMOUNT_VALUE'), 'comparedouble','>');
        $r2=&$oForm->createRule(array('standart_np', 'maxStandart_np'), text::get('ERROR_AMOUNT_VALUE'), 'comparedouble','<');
        $oForm->addRule(array( $r2), 'groupRuleStandart_np', 'group', $v1);

        $oForm -> addElement('text', 'measure',  text::get('UNIT_OF_MEASURE'), isset($calc['KKAL_MERVIENIBA'])?$calc['KKAL_MERVIENIBA']:'', 'tabindex=4 maxlength="15"'.(($isEdUser)?' disabled' : ''));
		$oForm -> addRule('measure', text::get('ERROR_REQUIRED_FIELD'), 'required');

		$oForm -> addElement('checkbox', 'isEpla',  text::get('IS_EPLA'), isset($calc['KKAL_TRASE'])?$calc['KKAL_TRASE']:0, 'tabindex=6'.(($isEdUser)?' disabled' : ''));
		$oForm -> addElement('checkbox', 'isRem',  text::get('IS_REM'), isset($calc['KKAL_REM'])?$calc['KKAL_REM']:0, 'tabindex=7'.(($isEdUser)?' disabled' : ''));
		$oForm -> addElement('checkbox', 'isActive',  text::get('IS_ACTIVE'), isset($calc['KKAL_IR_AKTIVS'])?$calc['KKAL_IR_AKTIVS']:1, 'tabindex=8'.(($isEdUser)?' disabled' : ''));

        $oForm -> addElement('select', 'year',  text::get('YEAR'), isset($calc['KKAL_VV_NUMBER'])?$calc['KKAL_VV_NUMBER']:dtime::getCurrentYear(), 'tabindex=9 style="width:50;"'.(($isEdUser)?' disabled' : ''), '', '', dtime::getYearArray());
		$oForm -> addRule('year', text::get('ERROR_REQUIRED_FIELD'), 'required');
		
		// importa dati
		$oForm -> addElement('text', 'created',  text::get('CREATED'), isset($calc['KKAL_CREATED'])?$calc['KKAL_CREATED']:dtime::now(), 'tabindex=21 maxlength="20" disabled');
		$oForm -> addElement('text', 'creator',  text::get('CREATOR'), isset($calc)?$creator:'', 'tabindex=21 maxlength="20" disabled');
		$oForm -> addElement('text', 'edited',  text::get('EDITED'), isset($calc['KKAL_EDITED'])?$calc['KKAL_EDITED']:dtime::now(), 'tabindex=21 maxlength="20" disabled');
		$oForm -> addElement('text', 'editor',  text::get('EDITOR'), isset($calc)?$editor:'', 'tabindex=21 maxlength="20" disabled');


		// form buttons
        if(!$isEdUser)
        {
    		$oForm -> addElement('submitImg', 'add', text::get('ADD'), 'img/btn_pievienot.gif', 'width="70" height="20" onclick="if(document.all[\'standart\'].value == 0) {setValue(\'standart\',\'0.00\');} setValue(\'action\',\''.OP_INSERT.'\');"');
    		$oForm -> addElement('submitImg', 'save', text::get('SAVE'), 'img/btn_saglabat.gif', 'width="70" height="20" onclick="if(document.all[\'standart\'].value == 0) {setValue(\'standart\',\'0.00\');} if (!isEmptyField(\'calcId\')) {setValue(\'action\',\''.OP_UPDATE.'\');} else {return false;}"');
    		$oForm -> addElement('clearImg', 'clear', text::get('CLEAR'), 'img/btn_attirit.gif', 'width="70" height="20"','','',array('onClick'=>'refreshButton(\'calcId\');parent.frame_1.unActiveRow();'));
            $oForm -> addElement('buttonImg', 'deleteFavorit', text::get('CLEAN_FAVORITS'), 'img/btn_iztirit_favoritus.gif', 'width="90" height="20" onclick="setValue(\'action\',\''.OP_DELETE.'\'); document.all[\'frmMain\'].submit();"');
         	$oForm -> addElement('static', 'jsButtonsControl', '', '
    			<script>
    				function refreshButton(name)
    				{
    					if (!isEmptyField(name))
    					{
    						document.all["save"].src="img/btn_saglabat.gif";

    					}
    					else
    					{
    						document.all["save"].src="img/btn_saglabat_disabled.gif";

    					}
    				}
    				refreshButton("calcId");
    			</script>
    		');
        }
		// if operation
		if ($oForm -> isFormSubmitted())
		{
			$check = true;
			$r = false;
			$checkRequiredFields = false;
            // delete favorits
            if ($oForm->getValue('action')== OP_DELETE )
            {
               $favorit = dbProc::getKrfkInfo(25);

               $r = dbProc::deleteNotActivFromUserFavorit($favorit['KRFK_VERTIBA']) ;

            }
            // save
			elseif ($oForm->getValue('action')==OP_INSERT || ($oForm->getValue('action')==OP_UPDATE  && is_numeric($oForm->getValue('calcId'))))
			{
    			if ($oForm -> getValue('kods') && $oForm -> getValue('nosaukums') &&
                    $oForm -> getValue('chipher') && $oForm -> getValue('standart') &&
                     $oForm -> getValue('standart_np') && $oForm -> getValue('measure') )
    			{
    				$checkRequiredFields = true;
    			}
    			if ($checkRequiredFields)
    			{

					// chech 
					if (dbProc::calculationWithChipherExists(
                                                            $oForm->getValue('chipher'),
															$oForm->getValue('year'),
															$oForm->getValue('isEpla',0),
															$oForm->getValue('isRem',0),
                                                            ($oForm->getValue('action')==OP_UPDATE)?$oForm->getValue('calcId'):false)
                                                            )
					{
						$oForm->addError(text::get('ERROR_EXISTS_CALCULATION_CODE'));
						$check = false;
					}

					// if all is ok, do operation
					if($check)
					{
						$r=dbProc::saveCalculation(($oForm->getValue('action')==OP_UPDATE?$oForm->getValue('calcId'):false),
						$oForm->getValue('chipher'),
                        $oForm->getValue('kods'),
                        $oForm->getValue('nosaukums'),
                        $oForm->getValue('description'),
                        $oForm->getValue('standart'),
                        $oForm->getValue('standart_np'),
                        $oForm->getValue('measure'),
						$oForm->getValue('isActive',0),
						$oForm->getValue('isEpla',0),
						$oForm->getValue('isRem',0),
                        $oForm->getValue('fizRadijums'),
                        $oForm->getValue('koeficent'),
						$oForm->getValue('year'),
						$userId
						);

						if (!$r)
						{
							$oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
						}
					}
				}
			}
			else
			{
				$oForm->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
			}
            // if operation was compleated succefully, show success mesage and redirect current frame
    		if ($r)
    		{
    			$oLink=new urlQuery();
    			$oLink->addPrm(FORM_ID, 'f.ktl.s.10');
    			$oLink->addPrm('editMode', '1');
    			switch($oForm->getValue('action'))
    			{
    				case OP_INSERT:
    					$oLink->addPrm('successMessage', text::get('RECORD_WAS_ADDED'));
    					break;
    				case OP_UPDATE:
    					$oLink->addPrm('successMessage', text::get('RECORD_WAS_SAVED'));
    					break;
                    case OP_DELETE:
    					$oLink->addPrm('successMessage', text::get('FAVORITS_IS_CLENED'));
    					break;
    			}
    		   	RequestHandler::makeRedirect($oLink->getQuery());
    		}
		}

       	$oForm -> makeHtml();
		include('f.ktl.s.10.2.tpl');
	}
	// top frale
	else
	{
        // Check if this form was requested via xmlHttpRequest and search
    	if (isset($_GET['xml']) && isset($_GET['search_q']) && isset($_GET['search_c']))
    	{
    		ob_clean();
    		$q = $_GET['search_q'];
            if (isset($q) || $q == 0)
    		{
    			echo 'q = "'.trim($q).'";';
    		}
            else
            {
    			echo 'q = "";';
    		}
            $c = $_GET['search_c'];
            if ($c)
    		{
    			echo 'c = "'.$c.'";';
    		}
            else
            {
    			echo 'c = "";';
    		}
		    exit;
    	}
        if (isset($_GET['xml']) && isset($_GET['sort_k']) && isset($_GET['sort_o']))
    	{
    		ob_clean();
    		$o = $_GET['sort_o'];
            if (isset($o))
    		{
    			echo 'o = "'.$o.'";';
    		}
            else
            {
    			echo 'o = "";';
    		}
            $k = $_GET['sort_k'];
            if ($k)
    		{
    			echo 'k = "'.$k.'";';
    		}
            else
            {
    			echo 'k = "";';
    		}
		    exit;
    	}
        // set list title
        $title = text::toUpper(text::get('CALCULATION'));
        // get search column list
        $columns=dbProc::getKlklName(KL_CALCULALATION);
        $searchText = ((reqVar::get('search') && reqVar::get('search') != '') || reqVar::get('search') == 0) ? reqVar::get('search') : false;
        $searchColumn = (reqVar::get('column') && reqVar::get('column') != '') ? reqVar::get('column') : false;
        $sortOrder = (reqVar::get('order') && reqVar::get('order') != '') ? reqVar::get('order') : false;
        $orderColumn = (reqVar::get('kol') && reqVar::get('kol') != '') ? reqVar::get('kol') : false;


        // default sort column
        $defaultColumn = dbProc::getKlklDefaultColumn(KL_CALCULALATION);
        dbProc::getCalculationGroupActivList();
        // URL to search Zinojums by number.
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.ktl.s.10');
    	$oLink->addPrm('isNew', '1');

        // add search/order params to listing
        $oLink->addPrm('search', reqVar::get('search'));
        $oLink->addPrm('column', reqVar::get('column'));
        $oLink->addPrm('order', reqVar::get('order'));
        $oLink->addPrm('kol', reqVar::get('kol'));
        $searchLink = $oLink -> getQuery();

		// inicialising of listing object
		$amount=dbProc::getCalculationCount($searchText,
                (($searchColumn === false) ? $defaultColumn : $searchColumn));
		$listing=new listing();
		$listing->setAmount($amount);

		$listingHtml=$listing->getHtml($oLink ->getQuery());
		unset($oLink);

		//link for frame2
		$oLink=new urlQuery();
		$oLink->addPrm(FORM_ID, 'f.ktl.s.10');
		$oLink->addPrm('editMode', 1);

		// gel list of users
		$res = dbProc::getCalculationList( false,
                                        $listing->getStartPosition(),
                                        $listing->getAmountOnPage(),
                                        $searchText,
                                        (($searchColumn === false) ? $defaultColumn : $searchColumn),
                                        $sortOrder,
                                        $orderColumn);

		if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
				// add dv area Id to each link
				$oLink->addPrm('calcId', $row['KKAL_ID']);
				$res[$i]['URL']=$oLink ->getQuery();
			}
		}
		unset($oLink);

		// inicial state of users number
		$number=$listing->getStartPosition()+1;

		include('f.ktl.s.x.1.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
