﻿<?
// Created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);
$isEditor = ( dbProc::isUserInRole($userId, ROLE_EDITOR) ||  dbProc::isUserInRole($userId, ROLE_EDITOR_VIEWER));

// act ID
$actId  = reqVar::get('actId');
// search form
$isSearch  = reqVar::get('isSearch');
// search act by number
$searchNum  = reqVar::get('searchNum');
// EPLA act
$isEpla  = reqVar::get('isEpla');

// Main form with XMLHTTP method load this code:
if (reqVar::get('xmlHttp') == 1)
{
    // if operation is DELETE , delete record from DB
    if (reqVar::get('deleteId')!='')
	{ 
         dbProc::deleteActPersonalRecord(reqVar::get('deleteId'));
         ?>
           rowNode = this.parentNode.parentNode;
           rowNode.parentNode.removeChild(rowNode);
         <?
        
        // get act virsstundu koef
        $actInfo = dbProc::getActInfo($actId);
        if(count($actInfo)>0)
        {
            $koef = $actInfo[0]['RAKT_VIRSSTUNDU_KOEF'];
        }
        // get act works total time
        $totalTimeFixed = dbProc::getActTotalWorkTime($actId);
        $totalProportion = dbProc::getActPersonalProportion($actId);     
        

        $day = reqVar::get('day');
        $brigade = reqVar::get('brigade');
        $work_key = $day.'-'.$brigade;

        foreach ($totalTimeFixed as $d => $val)
        {
            if($work_key == $d) {
              $totalTime['cvs'] = $totalTimeFixed[$d]['cvs'];
              $totalTime['koef'] = round(($totalTimeFixed[$d]['cvs']*($koef-1)), 2);  
            }
        }  
        if(array_key_exists($work_key, $totalProportion)) {
          $proportion = $totalProportion[$work_key];
        }
        else{
          $proportion = 1;
        }
        $totalUnPropTime['baseTime'] = 0;
        $totalUnPropTime['overTime'] = 0;

        //Ja eksistee akta materiāli, tad nolasaam taas darbus no datu baazes
  	    $aCustomerDb=dbProc::getActPersonalList($actId);
        
        foreach($aCustomerDb as $i => $row)
        {
          if($row['PRSN_IS_PROPORTIONAL'] == 1 && $row['PRSN_PLAN_DATE'].'-'.$row['PRSN_BRIGADE'] == $work_key)
          {
            $totalUnPropTime['baseTime'] = ($totalUnPropTime['baseTime'] + $row['PRSN_PAMATSTUNDAS']);
            $totalUnPropTime['overTime'] = ($totalUnPropTime['overTime'] + $row['PRSN_VIRSSTUNDAS']);
          }
        }
        $totalTime['cvs'] = ($totalTime['cvs'] - $totalUnPropTime['baseTime']);
        $totalTime['koef'] = ($totalTime['koef'] - $totalUnPropTime['overTime']);
        
    
        if($proportion > 0)
        {
          ?>
          var start = <?=reqVar::get('start');?>;
          var index = <?=reqVar::get('index');?>;
          var idx = '' + start + '' + index;
          var rowCount = getFormValue("frmPersonalTab", "rowCount["+start+"]") ;          

          var totalTimeTmp = <?=$totalTime['cvs'];?> ;
          var totalTimeWithKoefTmp = <?=$totalTime['koef'];?>;

          var proportion;
          var baseTime;
          var overTime;

          var baseTime0;
          var overTime0;
          var j = -1;

          for(i=start; i < rowCount; i++)
          {  if(getFormValue("frmPersonalTab", "isProportional["+i+"]") != 1)
             if(isSetFormField("frmPersonalTab", "code["+i+"]"))
             {
               if(i != index)
               {
                 proportion = 1*(getFormValue("frmPersonalTab", "proportion["+i+"]"));
                 baseTime = Math.round(((<?=round($totalTime['cvs']/$proportion, 2);?>)*proportion)*Math.pow(10,2))/Math.pow(10,2);
                 overTime = Math.round(((<?=round($totalTime['koef']/$proportion, 2);?>)*proportion)*Math.pow(10,2))/Math.pow(10,2);
                 setFormValue("frmPersonalTab","baseTime["+i+"]", ((baseTime == 0)? '0.00' : baseTime));
                 setFormValue("frmPersonalTab","overTime["+i+"]", ((overTime == 0)? '0.00' : overTime));

                 j = i;
                 baseTime0 = baseTime;
                 overTime0 = overTime;

                 totalTimeTmp = Math.round( (totalTimeTmp - baseTime)*Math.pow(10,2))/Math.pow(10,2);
                 totalTimeWithKoefTmp = Math.round( (totalTimeWithKoefTmp - overTime)*Math.pow(10,2))/Math.pow(10,2);

               }
             }
          }
          if( totalTimeTmp != 0)
          {
             baseTime0 = Math.round((baseTime0 + totalTimeTmp)*Math.pow(10,2))/Math.pow(10,2);
             setFormValue("frmPersonalTab","baseTime["+j+"]", ((baseTime0 == 0)? '0.00' : baseTime0));
          }

          if( totalTimeWithKoefTmp != 0)
          {
             overTime0 = Math.round((overTime0 + totalTimeWithKoefTmp)*Math.pow(10,2))/Math.pow(10,2);
             setFormValue("frmPersonalTab","overTime["+j+"]", ((overTime0 == 0)? '0.00' : overTime0));
          }

          <?
        }
        
    }
    exit();
}

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
    if($actId !== false )
    {
      $oLink=new urlQuery();
      $oLink->addPrm(FORM_ID, 'f.akt.s.1');
      $oLink->addPrm('actId', $actId);
      $oLink->addPrm('isSearch', $isSearch);
      $oLink->addPrm('searchNum', $searchNum);
      $oLink->addPrm('isEpla', $isEpla);
      $oLink->addPrm('tab', TAB_PERONAL);
      $oFormPersonalTab = new Form('frmPersonalTab','post',$oLink->getQuery().'#tab');
      unset($oLink);

      // if operation is success, show success message and redirect top frame
      if (reqVar::get('successMessageTab') && $isNew === false)
      {
      	$oFormPersonalTab->addSuccess(reqVar::get('successMessageTab'));
      }

      $isReadonly   = true;
      // Ir pieejams mainīšanai, ja  AKTS:LIETOTĀJS = ’Tekošais lietotājs’ un lietotāja loma ir ‘Ievadītais’ un AKTS:STATUS = ‘Izveide’ vai ‘Atgriezts’ vai ‘Tāme’.
      if($isAdmin || ($act['RAKT_RLTT_ID'] == $userId || ($isAuto && $isEditor)) 
            && ($status == STAT_INSERT || $status == STAT_RETURN )  )       {
        $isReadonly   = false;
      }

      //Popup saites sagatave
      $oPopLink=new urlQuery;
      $oPopLink->addPrm(FORM_ID, 'f.akt.s.8');
      $oPopLink->addPrm('actId',$actId);
      $oPopLink->addPrm('catalog',KL_CUSTOMERS);
      $oPopLink->addPrm('isSearch', $isSearch);
      $oPopLink->addPrm('searchNum', $searchNum);
      $oPopLink->addPrm('isEpla', $isEpla);

      // get favorit
      $favorit=array();
      $krfk = dbProc::getKrfkInfo(KRFK_CUSTOMER_ID);
      if($krfk !== false)
      {

        $res=dbProc::getFavoritTypeDefinitionList($userId, $krfk['KRFK_VERTIBA']);
        if (is_array($res))
        {
         	foreach ($res as $i => $row)
         	{
              $favorit[$i]['TITLE'] = $row['LTFV_NOSAUKUMS'];
          	  $favorit[$i]['URL'] = RequestHandler::popupHref($oPopLink -> getQuery().'&favoritId='.$row['LTFV_ID'], false, 500, 450);
            }
        }
        unset($res);
      }

      $next =  count($favorit);
      $favorit[$next]['TITLE'] = text::get('ALL_VALUES');   ;
      $favorit[$next]['URL'] = RequestHandler::popupHref($oPopLink -> getQuery().'&all=1', false, 500, 450);
      
       unset($res);
       unset($oPopLink);

      // brigade
      $brigade = array();
      for ($i = 1; $i<16; $i++){
        $brigade[$i] = $i;
      }

      // get act works total time
      $totalTimeFixed = dbProc::getActTotalWorkTime($actId);
            
      foreach ($totalTimeFixed as $day => $val){
        //if($day != 'cvs') {
          $totalTimeFixed[$day]['cvs'] = number_format($totalTimeFixed[$day]['cvs'], 2, '.', '');
          $totalTimeFixed[$day]['koef'] = number_format(($totalTimeFixed[$day]['cvs']*($koef-1)), 2, '.', '');
        //}
      }
      $totalWorkTime = array();
      $totalOverTime = array();
      //echo '<pre>',print_r($totalTimeFixed,true),'</pre>';
      $work_key;
      $aCustomers = array();
      if ($oFormPersonalTab -> isFormSubmitted() && $isNew === false)
      {
          $isRowValid = true;
          $isError = false;
          $aCodes =  $oFormPersonalTab->getValue('code');
          if(is_array($aCodes) )
          {

            $tProp = 0;
            $tPamat = 0;
            $tVirs = 0;
            $aCustomersFr = array();
            $totalProportion = array();
            foreach ($aCodes as $i => $val)
            {
                 $aCustomersFr[$i]['PRSN_ID'] = $oFormPersonalTab->getValue('id['.$i.']');
                 $aCustomersFr[$i]['PRSN_RCD_KODS'] = $oFormPersonalTab->getValue('code['.$i.']');
                 $aCustomersFr[$i]['PRSN_VARDS_UZVARDS'] = $oFormPersonalTab->getValue('nameSurname['.$i.']');
                 $aCustomersFr[$i]['PRSN_DARBA_VIETA'] = $oFormPersonalTab->getValue('place['.$i.']');
                 $aCustomersFr[$i]['PRSN_REGION'] = $oFormPersonalTab->getValue('region['.$i.']');
                 $aCustomersFr[$i]['PRSN_PROPORCIJA'] = $oFormPersonalTab->getValue('proportion['.$i.']');
                 $aCustomersFr[$i]['PRSN_PAMATSTUNDAS'] = $oFormPersonalTab->getValue('baseTime['.$i.']');
                 $aCustomersFr[$i]['PRSN_VIRSSTUNDAS'] = $oFormPersonalTab->getValue('overTime['.$i.']');
                 if($oFormPersonalTab->getValue('isProportional['.$i.']') == 'on') {
                  $aCustomersFr[$i]['PRSN_IS_PROPORTIONAL'] = 1;
                } else {
                  $aCustomersFr[$i]['PRSN_IS_PROPORTIONAL'] = 0;
            
                }              
	               $aCustomersFr[$i]['PRSN_LAIKSCELA'] = $oFormPersonalTab->getValue('laiksCela['.$i.']');
                 $aCustomersFr[$i]['PRSN_APPROVE_DATE'] = $oFormPersonalTab->getValue('edited['.$i.']');
                 $aCustomersFr[$i]['PRSN_PLAN_DATE'] = $oFormPersonalTab->getValue('plan_date['.$i.']');
                 $aCustomersFr[$i]['PRSN_BRIGADE'] = $oFormPersonalTab->getValue('brigade['.$i.']');

                  // validate proportion value
                  if (!empty($aCustomersFr[$i]['PRSN_PROPORCIJA']))
                  {
                    // assign error message
                  //$oFormPersonalTab->addErrorPerElem('proportion['.$i.']', text::get('ERROR_REQUIRED_FIELD'));
                  //$isRowValid = false;

                    if (!is_numeric($aCustomersFr[$i]['PRSN_PROPORCIJA']))
                    {
                      // assign error message
                      $oFormPersonalTab->addErrorPerElem('proportion['.$i.']', text::get('ERROR_DOUBLE_VALUE'));
                      $isRowValid = false;
                    }
                    elseif ($aCustomersFr[$i]['PRSN_PROPORCIJA'] >= 100)
                    {
                      // assign error message
                      $oFormPersonalTab->addErrorPerElem('proportion['.$i.']', text::get('ERROR_AMOUNT_VALUE'));
                      $isRowValid = false;
                    }
                     else
                     {

                      $work_key = $aCustomersFr[$i]['PRSN_PLAN_DATE'].'-'.$aCustomersFr[$i]['PRSN_BRIGADE'];

                      if(isset($totalProportion[$work_key])) {
                        $totalProportion[$work_key] = round(($totalProportion[$work_key] + $aCustomersFr[$i]['PRSN_PROPORCIJA']), 2);
                      } else {
                        $totalProportion[$work_key] = round($aCustomersFr[$i]['PRSN_PROPORCIJA'], 2);
                      }
                     }
                 }


                 // validate baseTime value
                  if (empty($aCustomersFr[$i]['PRSN_PAMATSTUNDAS']))
                  {
                      // assign error message
                    $oFormPersonalTab->addErrorPerElem('baseTime['.$i.']', text::get('ERROR_REQUIRED_FIELD'));
                    $isRowValid = false;
                  }
                        elseif (!is_numeric($aCustomersFr[$i]['PRSN_PAMATSTUNDAS']))
                  {
                    // assign error message
                    $oFormPersonalTab->addErrorPerElem('baseTime['.$i.']', text::get('ERROR_DOUBLE_VALUE'));
                    $isRowValid = false;
                  }
                        elseif ($aCustomersFr[$i]['PRSN_PAMATSTUNDAS'] >= 100000000)
                  {
                    // assign error message
                    $oFormPersonalTab->addErrorPerElem('baseTime['.$i.']', text::get('ERROR_AMOUNT_VALUE_100000'));
                    $isRowValid = false;
                  }
                 else
                 {
                   $tPamat = round(($tPamat + $aCustomersFr[$i]['PRSN_PAMATSTUNDAS']), 2);
                 }


                 // validate overTime value
                  if (empty($aCustomersFr[$i]['PRSN_VIRSSTUNDAS']))
                  {
                      // assign error message
                    //$oFormPersonalTab->addErrorPerElem('overTime['.$i.']', text::get('ERROR_REQUIRED_FIELD'));
                    //$isRowValid = false;
                  }
                        elseif (!is_numeric($aCustomersFr[$i]['PRSN_VIRSSTUNDAS']))
                  {
                    // assign error message
                    $oFormPersonalTab->addErrorPerElem('overTime['.$i.']', text::get('ERROR_DOUBLE_VALUE_100000'));
                    $isRowValid = false;
                  }
                        elseif ($aCustomersFr[$i]['PRSN_VIRSSTUNDAS'] >= 100000000)
                  {
                    // assign error message
                    $oFormPersonalTab->addErrorPerElem('overTime['.$i.']', text::get('ERROR_AMOUNT_VALUE'));
                    $isRowValid = false;
                  }
                 else
                 {
                   $tVirs = round(($tVirs + $aCustomersFr[$i]['PRSN_VIRSSTUNDAS']), 2);
                 }

                // prepare database record data
                $aAllRows[]  = array(
                  'code' => $aCustomersFr[$i]['PRSN_RCD_KODS'],
                  'nameSurname' => $aCustomersFr[$i]['PRSN_VARDS_UZVARDS'],
                  'place' => $aCustomersFr[$i]['PRSN_DARBA_VIETA'],
                  'region' => $aCustomersFr[$i]['PRSN_REGION'],
                  'proportion' => $aCustomersFr[$i]['PRSN_PROPORCIJA'],
                  'baseTime' => number_format($aCustomersFr[$i]['PRSN_PAMATSTUNDAS'],2,'.',''),
                  'overTime' => number_format($aCustomersFr[$i]['PRSN_VIRSSTUNDAS'],2,'.',''),
                  'isProportional' => $aCustomersFr[$i]['PRSN_IS_PROPORTIONAL'],
                  'laiksCela' => $aCustomersFr[$i]['PRSN_LAIKSCELA'],
                  'rowNum' => $i,
                  'edited' => $aCustomersFr[$i]['PRSN_APPROVE_DATE'],
                  'plan_date'=> $aCustomersFr[$i]['PRSN_PLAN_DATE'],
                  'brigade'=> $aCustomersFr[$i]['PRSN_BRIGADE'],
                );
             }

              // transform for calculate tota summ for the date
              $aSumms = array();
              foreach ($aAllRows as $i => $row){    
                $work_key = $row['plan_date'].'-'.$row['brigade'];

                if(isset($aSumms[$work_key])) {
                  
                  $aSumms[$work_key]['baseTime'] += $row['baseTime'];
                  $aSumms[$work_key]['overTime'] += $row['overTime'];
                } else {
                  $aSumms[$work_key] = array('baseTime' => $row['baseTime'],
                                                      'overTime' => $row['overTime']
                                                    );
                }              
              }
              foreach ($aAllRows as $i => $row){
                $work_key = $row['plan_date'].'-'.$row['brigade'];
                $aValidRows[$i] = $row;
                $aValidRows[$i]['baseTime_total'] = $aSumms[$work_key]['baseTime'];
                $aValidRows[$i]['overTime_total'] = $aSumms[$work_key]['overTime'];
              }

              foreach ($aCustomersFr as $customer)
              {
                $work_key = $customer['PRSN_PLAN_DATE'].'-'.$customer['PRSN_BRIGADE'];
                $aCustomers[$work_key][] = $customer;                
              }

              foreach ($aCustomers as $d => $aDayCust)
              {
                foreach ($aDayCust as $i => $customer)
                {
                  $work_key = $customer['PRSN_PLAN_DATE'].'-'.$customer['PRSN_BRIGADE'];
                  $aCustomers[$work_key][$i]['PRSN_DAY_WORK_TOTAL'] = $aSumms[$work_key]['baseTime'];
                  $aCustomers[$work_key][$i]['PRSN_DAY_OWER_TOTAL'] = $aSumms[$work_key]['overTime'];
                }
              }

            //echo '<pre>',print_r($aWorkFr,true),'</pre>';
            //echo '<pre>',print_r($aWorks,true),'</pre>';

            /* if($totalTimeWithKoef != $tVirs || $totalTime != $tPamat)
             {
                $oFormPersonalTab->addError(text::get('ERROR_CUSTOMER_SUMM_CHECK'));
                $isError = true;
             }*/
           }
      }
      else
      {
        //Ja eksistee akta materiāli, tad nolasaam taas darbus no datu baazes
        $aCustomerDb=dbProc::getActPersonalList($actId);
        
        foreach ($aCustomerDb as $customer)
        {
          $customer['PRSN_PAMATSTUNDAS'] = number_format($customer['PRSN_PAMATSTUNDAS'],2,'.','');
          $customer['PRSN_VIRSSTUNDAS'] = number_format($customer['PRSN_VIRSSTUNDAS'],2,'.','');
          $customer['PRSN_LAIKSCELA'] = number_format($customer['PRSN_LAIKSCELA'],1,'.','');
          $work_key = $customer['PRSN_PLAN_DATE'].'-'.$customer['PRSN_BRIGADE'];
          $aCustomers[$work_key][] = $customer;          
        }
      }

      foreach ($totalTimeFixed as $day => $val){
        if(!array_key_exists($day, $aCustomers)) {
          $aCustomers[$day][] = array();
          $totalWorkTime[$day] = number_format(0, 2, '.', '');
          $totalOverTime[$day] = number_format(0, 2, '.', '');
        }
      }

      //echo '<pre>',print_r($aCustomers,true),'</pre>';

      $oFormPersonalTab -> addElement('hidden', 'isSearch', '', $isSearch);
      $oFormPersonalTab -> addElement('hidden', 'searchNum', '', $searchNum);
      $oFormPersonalTab->addElement('hidden','actId','',$actId);
      $oFormPersonalTab->addElement('hidden','maxAmaunt','','1000.00');
      $oFormPersonalTab->addElement('hidden','maxAmaunt1','','100.00');
      $oFormPersonalTab->addElement('hidden','maxAmaunt2','','100000000.00');
      $oFormPersonalTab->addElement('hidden','minAmaunt','','0.00');
      
      $rowCount = 0;
      $i = 0;
      $z = 0;

      // form elements
      foreach ($aCustomers as $d => $aDayCust)
      { 
        $z = $i;       
        foreach ($aDayCust as $j => $actCustomer)
        {  
           if(!isset($actCustomer['PRSN_PLAN_DATE'])) continue;
            $oFormPersonalTab->addElement('hidden', 'id['.$i.']', '', $actCustomer['PRSN_ID']);        
            $oFormPersonalTab->addElement('hidden', 'place['.$i.']', '', $actCustomer['PRSN_DARBA_VIETA']);
            $oFormPersonalTab->addElement('hidden', 'region['.$i.']', '', $actCustomer['PRSN_REGION']);
            $oFormPersonalTab->addElement('hidden', 'edited['.$i.']', '', $actCustomer['PRSN_APPROVE_DATE']);

            $oFormPersonalTab->addElement('label', 'code['.$i.']', '', $actCustomer['PRSN_RCD_KODS']);
            $oFormPersonalTab->addElement('label', 'nameSurname['.$i.']', '', $actCustomer['PRSN_VARDS_UZVARDS']);
            $oFormPersonalTab->addElement('label', 'brigade['.$i.']', '', $actCustomer['PRSN_BRIGADE']); 

            if ($oFormPersonalTab -> isFormSubmitted()){
              $oFormWorkTab->setNewValue('code['.$i.']',$actCustomer['PRSN_RCD_KODS']);
              $oFormWorkTab->setNewValue('nameSurname['.$i.']',$actCustomer['PRSN_VARDS_UZVARDS']);
            }
            if(empty($actCustomer['PRSN_APPROVE_DATE']) ) {

              $oFormPersonalTab->addElement('text', 'proportion['.$i.']', '', $actCustomer['PRSN_PROPORCIJA'], (($isReadonly)?' disabled ':'').'maxlength="6"');
              //$oFormPersonalTab -> addRule('proportion['.$i.']', text::get('ERROR_REQUIRED_FIELD'), 'required');
              $oFormPersonalTab -> addRule('proportion['.$i.']', text::get('ERROR_DOUBLE_VALUE'), 'double', 2);
              // check Amaunt field
              $v1=&$oFormPersonalTab->createValidation('proportionValidation_'.$i, array('proportion['.$i.']'), 'ifonefilled');
              // ja Amount ir definēts un > 1000
              $r1=&$oFormPersonalTab->createRule(array('proportion['.$i.']', 'maxAmaunt1'), text::get('ERROR_AMOUNT_VALUE_100'), 'comparedouble','<');
              // ja Amount ir definēts un < 0
              $r2=&$oFormPersonalTab->createRule(array('proportion['.$i.']', 'minAmaunt'), text::get('ERROR_AMOUNT_VALUE_100'), 'comparedouble','>');
              $oFormPersonalTab->addRule(array($r1, $r2), 'groupRuleProportion_'.$i, 'group', $v1);

              $oFormPersonalTab->addElement('text', 'baseTime['.$i.']', '', $actCustomer['PRSN_PAMATSTUNDAS'], (($isReadonly)?' disabled ':'').'maxlength="11"');
              $oFormPersonalTab -> addRule('baseTime['.$i.']', text::get('ERROR_REQUIRED_FIELD'), 'required');
              $oFormPersonalTab -> addRule('baseTime['.$i.']', text::get('ERROR_DOUBLE_VALUE'), 'double', 2);
              // check Amaunt field
              $v2=&$oFormPersonalTab->createValidation('baseTimeValidation_'.$i, array('baseTime['.$i.']'), 'ifonefilled');
              // ja Amount ir definēts un > 100000
              $r1=&$oFormPersonalTab->createRule(array('baseTime['.$i.']', 'maxAmaunt2'), text::get('ERROR_AMOUNT_VALUE_100000'), 'comparedouble','<');
              // ja Amount ir definēts un < 0
              //$r2=&$oFormPersonalTab->createRule(array('baseTime['.$i.']', 'minAmaunt'), text::get('ERROR_AMOUNT_VALUE'), 'comparedouble','>');
              $oFormPersonalTab->addRule(array($r1), 'groupRuleBaseTime_'.$i, 'group', $v2);

              $oFormPersonalTab->addElement('text', 'overTime['.$i.']', '', $actCustomer['PRSN_VIRSSTUNDAS'], (($isReadonly)?' disabled ':'').'maxlength="11"');
              //$oFormPersonalTab -> addRule('overTime['.$i.']', text::get('ERROR_REQUIRED_FIELD'), 'required');
              $oFormPersonalTab -> addRule('overTime['.$i.']', text::get('ERROR_DOUBLE_VALUE'), 'double', 2);
              // check Amaunt field
              $v3=&$oFormPersonalTab->createValidation('overTimeValidation_'.$i, array('overTime['.$i.']'), 'ifonefilled');
              // ja Amount ir definēts un > 1000
              $r1=&$oFormPersonalTab->createRule(array('overTime['.$i.']', 'maxAmaunt2'), text::get('ERROR_AMOUNT_VALUE_100000'), 'comparedouble','<');
              // ja Amount ir definēts un < 0
              //$r2=&$oFormPersonalTab->createRule(array('overTime['.$i.']', 'minAmaunt'), text::get('ERROR_AMOUNT_VALUE'), 'comparedouble','>');
              $oFormPersonalTab->addRule(array($r1), 'groupRuleOverTime_'.$i, 'group', $v3);

              // checkbox
              $checkboxJs = "	var index=" . $i . ";
                              if(this.checked) {
                      document.forms['frmPersonalTab']['proportion['+index+']'].value = '';
                                  document.forms['frmPersonalTab']['proportion['+index+']'].disabled = true;
                              }else{
                                  document.forms['frmPersonalTab']['proportion['+index+']'].value = '1.00';
                                  document.forms['frmPersonalTab']['proportion['+index+']'].disabled = false;
                              }";
              $oFormPersonalTab -> addElement('checkbox', 'isProportional['.$i.']',  '', $actCustomer['PRSN_IS_PROPORTIONAL'], 'onclick="' . $checkboxJs . '"'.(($isReadonly)?' disabled ':''));
          
              $oFormPersonalTab->addElement('text', 'laiksCela['.$i.']', '', $actCustomer['PRSN_LAIKSCELA'], (($isReadonly)?' disabled ':'').'maxlength="11"');
              //$oFormPersonalTab -> addRule('laiksCela['.$i.']', text::get('ERROR_REQUIRED_FIELD'), 'required');
              $oFormPersonalTab -> addRule('laiksCela['.$i.']', text::get('ERROR_DOUBLE_VALUE'), 'double', 1);
              // check Amaunt field
              $v2=&$oFormPersonalTab->createValidation('laiksCelaValidation_'.$i, array('laiksCela['.$i.']'), 'ifonefilled');
              // ja Amount ir definēts un > 100000
              $r1=&$oFormPersonalTab->createRule(array('laiksCela['.$i.']', 'maxAmaunt2'), text::get('ERROR_AMOUNT_VALUE_100000'), 'comparedouble','<');
              // ja Amount ir definēts un < 0
              //$r2=&$oFormPersonalTab->createRule(array('laiksCela['.$i.']', 'minAmaunt'), text::get('ERROR_AMOUNT_VALUE'), 'comparedouble','>');
              $oFormPersonalTab->addRule(array($r1), 'groupRuleLaiksCela_'.$i, 'group', $v2);

              //datums
              $oFormPersonalTab -> addElement('date', 'plan_date['.$i.']',  '', $actCustomer['PRSN_PLAN_DATE'], (($isReadonly)?' disabled ':''));
              $oFormPersonalTab -> addRule('plan_date['.$i.']', text::get('ERROR_REQUIRED_FIELD'), 'required');
              $oFormPersonalTab -> addRule('plan_date['.$i.']', text::get('ERROR_INCORRECT_DATE'),'validatedate');
              

            } else {
              $oFormPersonalTab->addElement('label', 'proportion['.$i.']', '', $actCustomer['PRSN_PROPORCIJA']);
              $oFormPersonalTab->addElement('label', 'baseTime['.$i.']', '', $actCustomer['PRSN_PAMATSTUNDAS']);
              $oFormPersonalTab->addElement('label', 'overTime['.$i.']', '', $actCustomer['PRSN_VIRSSTUNDAS']);
              $oFormPersonalTab -> addElement('checkbox', 'isProportional['.$i.']',  '', $actCustomer['PRSN_IS_PROPORTIONAL'], ' disabled ');
              $oFormPersonalTab->addElement('label', 'laiksCela['.$i.']', '', $actCustomer['PRSN_LAIKSCELA']);
              $oFormPersonalTab->addElement('label', 'plan_date['.$i.']', '', $actCustomer['PRSN_PLAN_DATE']); 
              
            }

            if ($oFormPersonalTab -> isFormSubmitted()){
              $oFormPersonalTab->setNewValue('proportion['.$i.']',$actCustomer['PRSN_PROPORCIJA']);
              $oFormPersonalTab->setNewValue('baseTime['.$i.']',$actCustomer['PRSN_PAMATSTUNDAS']);
              $oFormPersonalTab->setNewValue('overTime['.$i.']',$actCustomer['PRSN_VIRSSTUNDAS']);
              $oFormPersonalTab->setNewValue('isProportional['.$i.']',$actCustomer['PRSN_IS_PROPORTIONAL']);
              $oFormPersonalTab->setNewValue('laiksCela['.$i.']',$actCustomer['PRSN_LAIKSCELA']);
              $oFormPersonalTab->setNewValue('plan_date['.$i.']',$actCustomer['PRSN_PLAN_DATE']);
              
            }

            $totalWorkTime[$d] = number_format($actCustomer['PRSN_DAY_WORK_TOTAL'], 2, '.', '');
            $totalOverTime[$d] = number_format($actCustomer['PRSN_DAY_OWER_TOTAL'], 2, '.', '');
            
            
              // delete button
            if(empty($actCustomer['PRSN_APPROVE_DATE']) ) {
              // ajax handler link for delete
              $delLink = new urlQuery();
              $delLink->addPrm(FORM_ID, 'f.akt.s.5');
              $delLink->addPrm(DONT_USE_GLB_TPL, '1');
              $delLink->addPrm('actId', $actId);
              $delLink->addPrm('xmlHttp', '1');
          
              $delButtonJs = "if(isDelete()) {
                    var index=" . $i . ";
                    var start =" . $z . ";
                    var day ='" . $actCustomer['PRSN_PLAN_DATE'] . "';
                    var brigade =" . $actCustomer['PRSN_BRIGADE'] . ";
                    var rowId = document.forms['frmPersonalTab']['id['+index+']'];
                    rowId = rowId ? rowId.value : '';
                    eval(xmlHttpGetValue('" .$delLink -> getQuery() ."&deleteId='+rowId+'&start='+start+'&day='+day+'&brigade='+brigade+'&index='+index));
                  }";
              $oFormPersonalTab->addElement('button', 'cust_del['.$i.']', '', '', 'onclick="' . $delButtonJs . ';return false;" style="background: url(img/ico_del.gif); cursor:hand; border:0px; width: 20px; height: 20px;" title="' . text::get('DELETE') . '"');
              unset($delLink);
            }
            $i ++;
            $rowCount++;
          }
          if(is_array($aDayCust[0]) && count($aDayCust[0]) > 0 ){
            $oFormPersonalTab->addElement('hidden','rowCount['.$z.']','',$rowCount);
          }

          $totalWork = 0;
          foreach($totalWorkTime as $d => $t) {
            $totalWork += $t;
          }
          $totalWork = number_format($totalWork, 2, '.', '');
          $totalOver = 0;
          foreach($totalOverTime as $d => $t) {
            $totalOver += $t;
          }
          $totalOver = number_format($totalOver, 2, '.', '');
      }
      
      

      // form buttons
      if (!$isReadonly)
      {
        foreach ($favorit as $i => $group)
        {
           $oFormPersonalTab->addElement('link', 'favorit['.$i.']', $group['TITLE'], '#', 'onClick="'.$group['URL'].'"', '');
        }
        
        $oFormPersonalTab -> addElement('submitImg', 'save', text::get('SAVE'), 'img/btn_save_personal.gif', 'width="120" height="20" ');
        //$oFormPersonalTab -> addElement('clearImg', 'clear', text::get('CLEAR'), 'img/btn_attirit.gif', 'width="70" height="20"');
      }

      // if form iz submit (save button was pressed)
	  if ($oFormPersonalTab -> isFormSubmitted()&& $isNew === false)
	  {
		  if($oFormPersonalTab ->isValid() && $isRowValid && !$isError )
		  {		  
            /*echo "<pre>";
		        print_r($aValidRows);
             echo "</pre>";*/
            
		        if(isset($aValidRows) && is_array($aValidRows))
            {   
                
               // $totalProportion = dbProc::getActPersonalProportion($actId);
                
                // inicializācija
                $totalTime = array();
                $totalTimeTmp = array();
                $totalUnPropTime = array();

                foreach ($totalTimeFixed as $day => $val)
                {
                    $totalTime[$day]['cvs'] = 0;
                    $totalTime[$day]['koef'] = 0;

                    $totalTimeTmp[$day]['cvs'] = 0;
                    $totalTimeTmp[$day]['koef'] = 0;

                    $totalUnPropTime[$day]['baseTime'] = 0;
                    $totalUnPropTime[$day]['overTime'] = 0;    
                    
                    $baseTime0[$day] = 0;
                    $overTime0[$day] = 0;
                    $proportion0[$day] = 0;
                    $Id0[$day] = false;

                }

                $n = 0;
                foreach($aValidRows as $i => $row)
                {
                    $work_key = $row['plan_date'].'-'.$row['brigade'];
                    if($row['isProportional']==1) {
                      $totalUnPropTime[$work_key]['baseTime'] += $row['baseTime'];
                      $totalUnPropTime[$work_key]['overTime'] += $row['overTime'];
                    }
                } 

                foreach ($totalTimeFixed as $day => $val)
                {
                    $totalTime[$day]['cvs'] = ($totalTimeFixed[$day]['cvs'] - $totalUnPropTime[$day]['baseTime']);
                    $totalTime[$day]['koef'] = ($totalTimeFixed[$day]['koef'] - $totalUnPropTime[$day]['overTime']);
         
                    if(($totalTime[$day]['cvs'] < 0 || $totalTime[$day]['koef'] < 0) ||
                    (count($aValidRows) == $n && ($totalTime[$day]['cvs'] != 0 || $totalTime[$day]['koef'] != 0)))
                    {
                      $oFormPersonalTab->addError(text::get('WORK_TIME_NOT_EQUAL_PERSONAL_TIME'));
                      $isError = true;
                    }                  
                }

                if(!$isError) {
                    dbProc::deleteActPersonals($actId);
                }

                foreach ($totalTimeFixed as $day => $val)
                {

                  if(!is_array($totalProportion) || !array_key_exists($day, $totalProportion)) {
                    $totalProportion[$day] = 1;
                  }
                  
                  if(!$isError)
                  {
                        $totalTimeTmp[$day]['cvs'] = $totalTime[$day]['cvs'];
                        $totalTimeTmp[$day]['koef'] = $totalTime[$day]['koef'];
                        $baseTime0[$day] = 0;
                        $overTime0[$day] = 0;
                        $proportion0[$day] = 0;
                        $Id0[$day] = false;                       
                        
                        foreach($aValidRows as $i => $row)
                        {
                          $d = explode('-', $day );
                          if( $row['plan_date'] == $d[0] && $row['brigade'] == $d[1]) 
                          { 
                            
                              if(!$row['isProportional'])
                              {                                  
                                    $baseTime = round(($totalTimeFixed[$day]['cvs']/$totalProportion[$day])*$row['proportion'], 2);
                                    $overTime = round(($totalTimeFixed[$day]['koef']/$totalProportion[$day])*$row['proportion'], 2);
        
                                    $baseTime0[$day] = $baseTime;
                                    $overTime0[$day] = $overTime;
                                    $proportion0[$day] = $row['proportion'];
                                    $rowNum0[$day] = $row['rowNum'];
                              }
                              
                              $r=dbProc::writeActPersonal($actId,
                                          $row['code'],
                                          $row['nameSurname'],
                                          $row['place'],
                                          $row['region'],
                                          $row['proportion'],
                                          ((!$row['isProportional'])?$baseTime:$row['baseTime']),
                                          ((!$row['isProportional'])?$overTime:$row['overTime']),
                                          (($row['isProportional'])?$row['isProportional']:0),
                                          $row['laiksCela'],
                                          $row['edited'],
                                          $row['plan_date'],
                                          $row['baseTime_total'],
                                          $row['overTime_total'],
                                          $row['brigade']
                                        );


                              // if work not inserted in to the DB
                              $totalTime[$day]['cvs'] += ((!$row['isProportional'])?$baseTime:$row['baseTime']);
                              $totalTime[$day]['koef']  += ((!$row['isProportional'])?$overTime:$row['overTime']);
                              // delete  task record and show error message
                              if($r === false)
                              {
                                  dbProc::deleteActPersonals($actId);
                                  $oFormPersonalTab->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
                                  break;
                              }
                              else
                              {
                                  $Id=dbLayer::getInsertId();
                                  if(!$row['isProportional'])
                                  {
                                    $Id0[$day] = $Id;
                                  }

                                  $oFormPersonalTab -> setNewValue('id['.$row['rowNum'].']', $Id);

                                  if(!$row['isProportional'])
                                  {
                                      $oFormPersonalTab -> setNewValue('baseTime['.$row['rowNum'].']', number_format($baseTime,2,'.',''));
                                      $oFormPersonalTab -> setNewValue('overTime['.$row['rowNum'].']', number_format($overTime,2,'.',''));
                                      
                                      $totalTimeTmp[$day]['cvs'] = $totalTimeTmp[$day]['cvs'] -  $baseTime;
                                      $totalTimeTmp[$day]['koef'] = $totalTimeTmp[$day]['koef'] -  $overTime;
                                  }
                              }
                          }
                      }
                      
                      if($Id0[$day] !== false && ($totalTimeTmp[$day]['cvs'] != 0 || $totalTimeTmp[$day]['koef'] != 0))
                      {
                          
                          if( $totalTimeTmp[$day]['cvs'] != 0)
                          {
                              $baseTime0[$day] = round(($baseTime0[$day] + $totalTimeTmp[$day]['cvs']), 2);
                              $totalTime[$day]['cvs'] += $totalTimeTmp[$day]['cvs'];
                          }
                          if( $totalTimeTmp[$day]['koef'] != 0)
                          {
                              $overTime0[$day] = round(($overTime0[$day] + $totalTimeTmp[$day]['koef']), 2);
                              $totalTime[$day]['koef'] += $totalTimeTmp[$day]['koef'];
                          }
                          
                          $r = dbProc::updatePersonalTime($Id0[$day],
                                                                $proportion0[$day],
                                                                $baseTime0[$day],
                                                                $overTime0[$day]);
                         
                          if(!$r)
                          {
                              $oFormPop->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
                          }
                          else
                          {
                              $oFormPersonalTab -> setNewValue('baseTime['.$rowNum0[$day].']', number_format($baseTime0[$day],2,'.',''));
                              $oFormPersonalTab -> setNewValue('overTime['.$rowNum0[$day].']', number_format($overTime0[$day],2,'.',''));

                              $oFormPersonalTab->addSuccess(text::get('RECORD_WAS_SAVED'));
                          }
                      }
                      $totalTime[$day]['cvs'] = number_format($totalTime[$day]['cvs'], 2);
                      $totalTime[$day]['koef']  = number_format($totalTime[$day]['koef'] , 2);
                          
                    }
                  
                }
                // pārrēķinam summas
                dbProc::updatePersonalSumms($actId);
                // saglaba lietotaja darbiibu
                dbProc::setUserActionDate($userId, ACT_UPDATE);
            }
        }
      }
       $oFormPersonalTab -> makeHtml();
       include('f.akt.s.5.tpl');
    }



}
else
{
    RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}

?>