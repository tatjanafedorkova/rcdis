﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
     $sCriteria = reqVar::get('search');
     $sOrder = reqVar::get('sortOrder');
		// gel list of users
        $res = dbProc::getActFullList($sCriteria, $sOrder);
        $pamatstundas = 0;
        $virsstundas = 0;
        $summa = 0;
        if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
               $pamatstundas = $pamatstundas + $row['PAMATSTUNDAS'];
               $virsstundas = $virsstundas + $row['VIRSSTUNDAS'];
	               $laikscela = $laikscela + $row['LAIKSCELA'];
               $summa = $summa + $row['CENA_KOPA'];
			}
		}
        $pamatstundas = number_format($pamatstundas,2, '.', '');
        $virsstundas = number_format($virsstundas,2, '.', '');
                $laikscela = number_format($laikscela,2, '.', '');
        $summa = number_format($summa, 2, '.', '');

        $searchCr = array();
        $sCr = explode("^", $sCriteria);
        if(isset($sCr[0]) && $sCr[0] != -1  && isset($sCr[1]) && $sCr[1] != -1)
        {
           $searchCr[] = array('label' =>  text::get('YEAR'), 'value' => $sCr[0].'-'.$sCr[1]);
        }
        if(isset($sCr[2]) && $sCr[2] != -1  && isset($sCr[3]) && $sCr[3] != -1)
        {
           $searchCr[] = array('label' =>  text::get('MONTH'), 'value' => dtime::getMonthName($sCr[2]).'-'.dtime::getMonthName($sCr[3]));
        }
        if(isset($sCr[4]) && $sCr[4] != -1)
        {
           $searchCr[] = array('label' =>  text::get('RCD_REGION'), 'value' => $sCr[4]);
        }
        if(isset($sCr[5]) && $sCr[5] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGL_DV_AREA'), 'value' => dbProc::getDVAreaName($sCr[5]));
        }
        if(isset($sCr[6]) && $sCr[6] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ED_REGION'), 'value' => $sCr[6]);
        }
        if(isset($sCr[7]) && $sCr[7] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ED_IECIKNIS'), 'value' => dbProc::getEDAreaName($sCr[7]));
        }

        if(isset($sCr[8]) && $sCr[8] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_SOURCE_OF_FOUNDS'), 'value' => dbProc::getSourceOfFoundsName($sCr[8]));
        }
        if(isset($sCr[9]) && $sCr[9] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_VOLTAGE'), 'value' => dbProc::getVoltageName($sCr[9]));
        }
        if(isset($sCr[10]) && $sCr[10] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ACT_OUNER'), 'value' => dbProc::getUserName($sCr[10]));
        }
        if(isset($sCr[11]) && $sCr[11] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_ACT_TYPE'), 'value' => dbProc::getActTypeName($sCr[11]));
        }
        if(isset($sCr[12]) && $sCr[12] != -1)
        {
           $searchCr[] = array('label' =>  text::get('MMS_WORK_FINISHED'), 'value' => (($sCr[12] == 1) ? text::get('YES'): text::get('NO')));
        }
        $status = explode("*", $sCr[14]);
        $statusString = '';
        if($status[0] == -1)
        {
           $statusString = text::get('ALL_STATUS');
        }
        else
        {
          foreach($status as $s)
          {
              $statusInfo = dbProc::getKrfkInfoByName($s);
              $statusString .= $statusInfo['KRFK_NOZIME'].",";
          }
          $statusString  = substr($statusString, 0, -1);
        }
        $searchCr[] = array('label' =>  text::get('STATUS'), 'value' => $statusString);
        if(isset($sCr[16]) && $sCr[16] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ED_SECTION'), 'value' => urldecode($sCr[16]));
        }
        if(isset($sCr[17]) && $sCr[17] != -1)
        {
           $searchCr[] = array('label' =>  text::get('EPLA_ACT'), 'value' => (($sCr[17] == 1) ? text::get('YES'): text::get('NO')) );
        }
        
		include('f.rpt.p.1.tpl');
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
