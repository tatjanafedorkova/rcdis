﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
	$searchMode = reqVar::get('isSearch');
	// top frame
    if($searchMode)
	{
        $oLink=new urlQuery();
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.14');
    	$oLink->addPrm('isSearch', 1 );
    	// form inicialization
    	$oForm = new Form('frmMain','post',$oLink->getQuery());
    	unset($oLink);

        $oLink=new urlQuery();
	    $oLink->addPrm('isReturn', '1');
        $oLink->addPrm('isSearch', 1 );
	    $oLink->addPrm(FORM_ID, 'f.rpt.s.14');
	    $criteriaLink=$oLink ->getQuery();
	    unset($oLink);

        // if operation
    	if ($oForm -> isFormSubmitted())
    	{
            $oListLink=new urlQuery();
            $oListLink->addPrm(FORM_ID, 'f.rpt.s.14');
        }
        $searchTitle = text::toUpper(text::toUpper(text::get('REPORT_WORK_WITH_PLAN')));
        $searchName = 'REPORT_WORK_WITH_PLAN';
        include('f.akt.m.8.inc');
	}
	// bottom frale
	else
	{
        $sCriteria = reqVar::get('search');

        // URL  export
    	/*$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.e.14');
    	$oLink->addPrm('search', $sCriteria);
        $oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $exportUrl =  $oLink ->getQuery();
        unset($oLink);
        */
        // URL  print
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.p.14');
    	$oLink->addPrm('search', $sCriteria);
        //$oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $printUrl =  $oLink ->getQuery();
        unset($oLink);

		// gel list of users
		$res = dbProc::getPlanWorkList($sCriteria);
        /*echo "<pre>";
        print_r($res);
        echo "</pre>";*/

        $area = array();

        $pamatstundas = 0;
        $virsstundas = 0;
        $summa = 0;
        $km = 0;
        $stundas = 0;
        $darbaStundas = 0;

        if (is_array($res))
		{
       		foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]  = array (
                'section' => $row['KEDI_SECTION'],
                'totalMainTime' => (isset($area[$row['KEDI_SECTION']]['totalMainTime'])?$area[$row['KEDI_SECTION']]['totalMainTime']:0) + $row['PAMATSTUNDAS'],
                'totalOverTime' => (isset($area[$row['KEDI_SECTION']]['totalOverTime'])?$area[$row['KEDI_SECTION']]['totalOverTime']:0) + $row['VIRSSTUNDAS'],
                'totalKm' => (isset($area[$row['KEDI_SECTION']]['totalKm'])?$area[$row['KEDI_SECTION']]['totalKm']:0) + $row['KM'],
                'totalStundas' => (isset($area[$row['KEDI_SECTION']]['totalStundas'])?$area[$row['KEDI_SECTION']]['totalStundas']:0) + $row['STUNDAS'],
                'totalDarbaStundas' => (isset($area[$row['KEDI_SECTION']]['totalDarbaStundas'])?$area[$row['KEDI_SECTION']]['totalDarbaStundas']:0) + $row['DARBA_STUNDAS'],
                'totalCena' => (isset($area[$row['KEDI_SECTION']]['totalCena'])?$area[$row['KEDI_SECTION']]['totalCena']:0) + $row['CENA_KOPA'],
                'ed' => array()
               );

               $pamatstundas = $pamatstundas + $row['PAMATSTUNDAS'];
               $virsstundas = $virsstundas + $row['VIRSSTUNDAS'];
               $summa = $summa + $row['CENA_KOPA'];
               $km = $km + $row['KM'];
               $stundas = $stundas + $row['STUNDAS'];
               $darbaStundas = $darbaStundas + $row['DARBA_STUNDAS'];

            }
            foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]  = array (
                'code' => $row['KEDI_KODS'],
                'name' => $row['ED'],
                'totalMainTime' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalMainTime'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalMainTime']:0) + $row['PAMATSTUNDAS'],
                'totalOverTime' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalOverTime'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalOverTime']:0) + $row['VIRSSTUNDAS'],
                'totalKm' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalKm'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalKm']:0) + $row['KM'],
                'totalStundas' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalStundas'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalStundas']:0) + $row['STUNDAS'],
                'totalDarbaStundas' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalDarbaStundas'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalDarbaStundas']:0) + $row['DARBA_STUNDAS'],
                'totalCena' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalCena'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalCena']:0) + $row['CENA_KOPA'],
                'mms' => array()
               );



            }
            foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['RAKT_MMS_KODS']]  = array (
                'code' => $row['KMSD_KODS'],
                'cilvekstundas' => $row['KMSD_CILVEKSTUNDAS'],
                'materIzmaksas' => $row['KMSD_MATR_IZMAKSAS'],
                'totalMainTime' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['RAKT_MMS_KODS']]['totalMainTime'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['RAKT_MMS_KODS']]['totalMainTime']:0) + $row['PAMATSTUNDAS'],
                'totalOverTime' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['RAKT_MMS_KODS']]['totalOverTime'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['RAKT_MMS_KODS']]['totalOverTime']:0) + $row['VIRSSTUNDAS'],
                'totalKm' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['RAKT_MMS_KODS']]['totalKm'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['RAKT_MMS_KODS']]['totalKm']:0) + $row['KM'],
                'totalStundas' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['RAKT_MMS_KODS']]['totalStundas'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['RAKT_MMS_KODS']]['totalStundas']:0) + $row['STUNDAS'],
                'totalDarbaStundas' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['RAKT_MMS_KODS']]['totalDarbaStundas'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['RAKT_MMS_KODS']]['totalDarbaStundas']:0) + $row['DARBA_STUNDAS'],
                'totalCena' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['RAKT_MMS_KODS']]['totalCena'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['RAKT_MMS_KODS']]['totalCena']:0) + $row['CENA_KOPA'],
                'act' => array()
               );


            }
            foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['RAKT_MMS_KODS']]['act'][$row['RAKT_ID']]  = array(
                    'actNumber' => $row['NUMURS'],
                    'designation' => $row['RAKT_OPERATIVAS_APZIM'],
                    'isFinished' => $row['RAKT_IR_OBJEKTS_PABEIGTS'],
                    'comment' => $row['RAKT_PIEZIMES'],
                    'mainTime' => $row['PAMATSTUNDAS'],
                    'overTime' => $row['VIRSSTUNDAS'],
                    'km' => $row['KM'],
                    'stundas' => $row['STUNDAS'],
                    'darbaStundas' => $row['DARBA_STUNDAS'],
                    'cena' => $row['CENA_KOPA']
               );
            }
		}

        $pamatstundas = number_format($pamatstundas,2, '.', '');
        $virsstundas = number_format($virsstundas,2, '.', '');
        $summa = number_format($summa, 2, '.', '');
        $km = number_format($km,2, '.', '');
        $stundas = number_format($stundas,2, '.', '');
        $darbaStundas = number_format($darbaStundas, 2, '.', '');

        /*echo "<pre>";
        print_r($area);
        echo "</pre>";*/

		include('f.rpt.s.14.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
