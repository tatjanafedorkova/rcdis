﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
	$searchMode = reqVar::get('isSearch');
	// top frame
    if($searchMode)
	{
        $oLink=new urlQuery();
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.12');
    	$oLink->addPrm('isSearch', 1 );
    	// form inicialization
    	$oForm = new Form('frmMain','post',$oLink->getQuery());
    	unset($oLink);

        $oLink=new urlQuery();
	    $oLink->addPrm('isReturn', '1');
        $oLink->addPrm('isSearch', 1 );
	    $oLink->addPrm(FORM_ID, 'f.rpt.s.12');
	    $criteriaLink=$oLink ->getQuery();
	    unset($oLink);

        // if operation
    	if ($oForm -> isFormSubmitted())
    	{
            $oListLink=new urlQuery();
            $oListLink->addPrm(FORM_ID, 'f.rpt.s.12');
        }
        $searchTitle = text::toUpper(text::toUpper(text::get('REPORT_WORK_PLAN_AND_NOT_PLAN')));
        $searchName = 'REPORT_WORK_PLAN_AND_NOT_PLAN';
        $allType = true;
        include('f.akt.m.8.inc');
	}
	// bottom frale
	else
	{
        $sCriteria = reqVar::get('search');

        // URL  export
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.e.12');
    	$oLink->addPrm('search', $sCriteria);
        $oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $exportUrl =  $oLink ->getQuery();
        unset($oLink);

        // URL  print
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.p.12');
    	$oLink->addPrm('search', $sCriteria);
        //$oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $printUrl =  $oLink ->getQuery();
        unset($oLink);

		// gel mms list
        $mms = dbProc::getMMSPlanWorksList($sCriteria);
        $res = dbProc::getRealWorkList($sCriteria);
       // print_r($res);
        /*echo "<pre>";
        print_r($res);
        echo "</pre>";*/

        $area = array();

        if (is_array($mms))
		{
       		foreach ($mms as $i=>$row)
			{
              if(isset($row['KEDI_KODS']) && $row['KEDI_KODS'] != '')
              {
                $area[$row['KEDI_KODS']]  = array (
                'code' => $row['KEDI_KODS'],
                'name' => $row['ED'],
                'mmsCena' => $row['MMS_MATR_IZMAKSAS'],
                'mmsMainTime' => $row['MMS_CILVEKSTUNDAS'],
                'mmsOverTime' => '0'
                );
              }
            }
        }
        if (is_array($res))
		{
       		foreach ($res as $i=>$row)
			{
              $area[$row['KEDI_KODS']]  = array (
                'code' => $row['KEDI_KODS'],
                'name' => $row['ED'],
                'mmsCena' => (isset($area[$row['KEDI_KODS']]['mmsCena'])? $area[$row['KEDI_KODS']]['mmsCena'] : 0),
                'mmsMainTime' => (isset($area[$row['KEDI_KODS']]['mmsMainTime'])? $area[$row['KEDI_KODS']]['mmsMainTime'] : 0),
                'mmsOverTime' => '0',
                'planCena' => $row['CENA_KOPA_PLAN'],
                'planMainTime' => $row['PAMATSTUNDAS_PLAN'],
                'planOverTime' => $row['VIRSSTUNDAS_PLAN'],
                'difCena' => isset($area[$row['KEDI_KODS']]['mmsCena'])? number_format(($row['CENA_KOPA_PLAN'] - $area[$row['KEDI_KODS']]['mmsCena'] ),2,'.','') : '0.00',
                'difMainTime' => isset($area[$row['KEDI_KODS']]['mmsMainTime'])?number_format(($row['PAMATSTUNDAS_PLAN'] - $area[$row['KEDI_KODS']]['mmsMainTime'] ),2,'.',''):'0.00',
                'difOverTime' => isset($area[$row['KEDI_KODS']]['mmsOverTime'])?number_format(($row['VIRSSTUNDAS_PLAN'] - $area[$row['KEDI_KODS']]['mmsOverTime']),2,'.',''):'0.00',
                'damageCena' => $row['CENA_KOPA_DAMAGE'],
                'damageMainTime' => $row['PAMATSTUNDAS_DAMAGE'],
                'damageOverTime' => $row['VIRSSTUNDAS_DAMAGE'],
                'defectCena' => $row['CENA_KOPA_DEFECT'],
                'defectMainTime' => $row['PAMATSTUNDAS_DEFECT'],
                'defectOverTime' => $row['VIRSSTUNDAS_DEFECT'],
                'stihijaCena' => $row['CENA_KOPA_STIHIJA'],
                'stihijaMainTime' => $row['PAMATSTUNDAS_STIHIJA'],
                'stihijaOverTime' => $row['VIRSSTUNDAS_STIHIJA'],
                'notPlanCena' => number_format(($row['CENA_KOPA_DAMAGE']+$row['CENA_KOPA_DEFECT']+$row['CENA_KOPA_STIHIJA']),2,'.',''),
                'notPlanMainTime' => number_format(($row['PAMATSTUNDAS_DAMAGE']+$row['PAMATSTUNDAS_DEFECT']+$row['PAMATSTUNDAS_STIHIJA']),2,'.',''),
                'notPlanOverTime' => number_format(($row['VIRSSTUNDAS_DAMAGE']+$row['VIRSSTUNDAS_DEFECT']+$row['VIRSSTUNDAS_STIHIJA']),2,'.',''),
                'notPlanProcentCena' => (($row['CENA_KOPA_PLAN'] == 0 && ($row['CENA_KOPA_DAMAGE']+$row['CENA_KOPA_DEFECT']+$row['CENA_KOPA_STIHIJA']) == 0)? 0 : (($row['CENA_KOPA_PLAN'] == 0) ? 100 : number_format((($row['CENA_KOPA_DAMAGE']+$row['CENA_KOPA_DEFECT']+$row['CENA_KOPA_STIHIJA'])/$row['CENA_KOPA_PLAN'])*100,2,'.',''))),
                'notPlanProcentMainTime' => (($row['PAMATSTUNDAS_PLAN'] == 0 && ($row['PAMATSTUNDAS_DAMAGE']+$row['PAMATSTUNDAS_DEFECT']+$row['PAMATSTUNDAS_STIHIJA']) == 0) ? 0 : (($row['PAMATSTUNDAS_PLAN'] == 0) ? 100 : number_format((($row['PAMATSTUNDAS_DAMAGE']+$row['PAMATSTUNDAS_DEFECT']+$row['PAMATSTUNDAS_STIHIJA'])/$row['PAMATSTUNDAS_PLAN'])*100,2,'.',''))),
                'notPlanProcentOverTime' => (($row['VIRSSTUNDAS_PLAN'] == 0 && ($row['VIRSSTUNDAS_DAMAGE']+$row['VIRSSTUNDAS_DEFECT']+$row['VIRSSTUNDAS_STIHIJA']) == 0) ? 0 : (($row['VIRSSTUNDAS_PLAN'] == 0) ? 100 : number_format((($row['VIRSSTUNDAS_DAMAGE']+$row['VIRSSTUNDAS_DEFECT']+$row['VIRSSTUNDAS_STIHIJA'])/$row['VIRSSTUNDAS_PLAN'])*100,2,'.',''))),
                'totalCena' => number_format(($row['CENA_KOPA_PLAN']+$row['CENA_KOPA_DAMAGE']+$row['CENA_KOPA_DEFECT']+$row['CENA_KOPA_STIHIJA']),2,'.',''),
                'totalMainTime' => number_format(($row['PAMATSTUNDAS_PLAN']+$row['PAMATSTUNDAS_DAMAGE']+$row['PAMATSTUNDAS_DEFECT']+$row['PAMATSTUNDAS_STIHIJA']),2,'.',''),
                'totalOverTime' => number_format(($row['VIRSSTUNDAS_PLAN']+$row['VIRSSTUNDAS_DAMAGE']+$row['VIRSSTUNDAS_DEFECT']+$row['VIRSSTUNDAS_STIHIJA']),2,'.','')
              );
            }
        }
        ksort($area);

		include('f.rpt.s.12.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
