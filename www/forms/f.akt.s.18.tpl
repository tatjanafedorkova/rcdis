﻿<body class="frame_1">
<?=$oForm->getElementHtml('jsRefresh2');?>
<h1><?=text::toUpper(text::get('RFC_ACT'));?></h1>
<?= $oForm -> getFormHeader(); ?>
<table cellpadding="5" cellspacing="1" border="0" width="100%">
	<tr>
		<td align=center colspan="5"><?= $oForm -> getMessage(); ?></td>
	</tr>
	<tr>
		<td colspan="4" class="table_head" align="right">
            <?= $oForm -> getElementLabel('statusTxt'); ?>:
            <font color="red"><?=text::toUpper($oForm -> getElementHtml('statusTxt'));?></font></td>
        <td width="20%" rowspan="<?=(($actId === false)?'14':'15');?>" valign="bottom">
            <table cellpadding="5" cellspacing="0" border="0" align="center">
             <? if(!$isReadonly || !$isReadonlyExceptAdmin)  {?>
            	<tr><td><?= $oForm -> getElementHtml('save'); ?></td> </tr>
             <? } ?>
             <? if(!$isReadonly && ($actId != false))  {?>
                <tr><td><?=$oForm->getElementHtml('export');?></td></tr>
             <? } ?>
             <? if(!$isReadonlyExceptEconomist)  {?>
                <tr><td><?=$oForm->getElementHtml('return');?></td></tr>
             <? } ?>
             <? if($isAdmin && ($actId != false) && ($status == STAT_DELETE || $status == STAT_CLOSE))  {?>
                <tr><td><?=$oForm->getElementHtml('return');?></td></tr>
             <? } ?>
             <? if(!$isReadonlyExceptEconomist)  {?>
                <tr><td><?=$oForm->getElementHtml('accept');?></td></tr>
             <? } ?>
            
             <? if(!$isReadonly)  {?>
                <tr><td><?=$oForm->getElementHtml('clear');?></td></tr>
             <? } ?>
             <? if($isAllowEstimate)  {?>
                <tr><td><?=$oForm->getElementHtml('view');?></td></tr>
             <? } ?>
             <? if($isDelete)  {?>
                <tr><td><?=$oForm->getElementHtml('delete');?></td></tr>
             <? } ?>
             <? if($actId != false)  {?>
                <tr><td><?=$oForm->getElementHtml('back');?></td></tr>
             <? } ?>
            </table>
        </td>
	</tr>

    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('type'); ?>:</td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('type'); ?></td>
        <td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('actFullNumber'); ?>:</td>
        <? if($actId != false)  {?>
		    <td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('actFullNumber'); ?></td>
   	   <? } else { ?>
		    <td class="table_cell_2" width="24%"></td>
       <? }  ?>
   	</tr>
    <tr>

        <td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('ouner'); ?>:</td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('ouner'); ?></td>
        <td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('dvArea'); ?>:</td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('dvArea'); ?></td>
   	</tr>  
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('sourceOfFounds'); ?>:</td>
		<td class="table_cell_2" colspan="3"><?= $oForm -> getElementHtml('sourceOfFounds'); ?></td>
   	</tr>
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('proces_date'); ?>:<?=(($isReadonly && $isReadonlyExceptEconomist)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('proces_date'); ?></td>
      <td class="table_cell_c" width="16%">&nbsp;</td>
		<td class="table_cell_2" width="24%">&nbsp;</td>
   	</tr>

    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('description'); ?>:</td>
		<td class="table_cell_2" colspan="3"><?= $oForm -> getElementHtml('description'); ?></td>
   	</tr>

</table>
<a name="tab"></a>

<?= $oForm -> getFormBottom(); ?>
<?=$oForm->getElementHtml('jsButtonsControl');?>
<?=$oForm->getElementHtml('jsBackButtons');?>
</body>
