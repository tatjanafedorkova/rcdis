﻿<?



// Main form with XMLHTTP method load this code:
if (reqVar::get('xmlHttp') == 1)
{
	//if is set dvRegion
	if (reqVar::get('dvRegion') !== false)
	{
		// unset previouse values of dvArea
		?>
		for(i=document.all["dvArea"].length; i>=0; i--)
		{
			document.all["dvArea"].options[i] = null;
		}
		<?
		$res=dbProc::getDVAreaForRegion(reqVar::get('dvRegion'));
		if (is_array($res) && count($res)>0)
		{
			?>
			document.all["dvArea"].options[0] = new Option('<?=text::get('ALL');?>', '-1');
			<?
            $i = 1;
			foreach ($res as $row)
			{
				// feel options array
				?>
				document.all["dvArea"].options[<?=$i;?>] = new Option( '<?=$row['KDVI_REGIONS'].''.$row['KDVI_KODS'].' : '.$row['KDVI_NOSAUKUMS'];?>', '<?=$row['KDVI_ID'];?>');
                <?
                $i++;
			}
		}
	}
	exit;
}

        // get user info
      	$userInfo = dbProc::getUserInfo($userId);
        $isAdmin=userAuthorization::isAdmin();
        $isEdUser = dbProc::isUserInRole($userId, ROLE_ED_USER); 
      	if(count($userInfo) >0 )
      	{
      		$user = $userInfo[0];
       	}
         // get RCD region list
         $rcdRegion=array();
         if(isset($user['RLTT_REGIONS']) && isset($user['RLTT_ROLE']) && $user['RLTT_ROLE'] != ROLE_ED_USER )
         {
            $rcdRegion[$user['RLTT_REGIONS']]=$user['RLTT_REGIONS'];
         }
         else
         {
            $rcdRegion['-1']=text::get('ALL');
            $res=dbProc::getKrfkName(KRFK_REGION);
            if (is_array($res))
            {
           	    foreach ($res as $row)
           	    {
           		    $rcdRegion[$row['nosaukums']]=$row['nosaukums'];
           	    }
            }
         }
         unset($res);

         // get DV area list
         $dvarea = array();
         if(isset($user['RLTT_KDVI_ID']) )
         {
            $dvarea[$user['RLTT_KDVI_ID']] = dbProc::getDVAreaName($user['RLTT_KDVI_ID']);
         }
         elseif(isset($user['RLTT_REGIONS']) && isset($user['RLTT_ROLE']) && $user['RLTT_ROLE'] != ROLE_ED_USER )
         {
            $res=dbProc::getDVAreaForRegion($user['RLTT_REGIONS'], false);
			if (is_array($res))
			{
			    $dvarea['-1']=text::get('ALL');
				foreach ($res as $row)
				{
					$dvarea[$row['KDVI_ID']]=$row['KDVI_REGIONS'].$row['KDVI_KODS'].' : '.$row['KDVI_NOSAUKUMS'];
				}
			}
         }
         else
         {
            $res=dbProc::getDVAreaList(false,false,false,false,false,'ASC','`KDVI_REGIONS`, `KDVI_KODS`');
			if (is_array($res))
			{
			    $dvarea['-1']=text::get('ALL');
				foreach ($res as $row)
				{
					$dvarea[$row['KDVI_ID']]=$row['KDVI_REGIONS'].$row['KDVI_KODS'].' : '.$row['KDVI_NOSAUKUMS'];
				}
			}
         }

        // get transport location list
       $transportLocation=array();
       $transportLocation['-1']=text::get('ALL');
       $res=dbProc::getKrfkName(KRFK_REGION);
       if (is_array($res))
       {
       	foreach ($res as $row)
       	{
       		$transportLocation[$row['nosaukums']]=$row['nosaukums'];
       	}
       }
       $transportLocation['TN']=text::get('TRANSPORT_DEPARTMENT');
       unset($res);

        // get status list
       $status=array();
       $status['-1']=text::get('ALL_STATUS');
       $res=dbProc::getKrfkName(KRFK_STATUS);
       if (is_array($res))
       {
       	foreach ($res as $row)
       	{
            if($row['nosaukums'] != STAT_DELETE || ($row['nosaukums'] == STAT_DELETE && $isAdmin && !$isEdUser))
            {
                $status[$row['nosaukums']]=$row['nozime'];
            }
       	}
       }
       unset($res);

        $searcCriteria = array();

       // if operation
    	if ($oForm -> isFormSubmitted())
    	{
           $searcCriteria['yearFrom'] = $oForm->getValue('yearFrom');
           $searcCriteria['yearUntil'] = $oForm->getValue('yearUntil');
           $searcCriteria['monthFrom'] = $oForm->getValue('monthFrom');
           $searcCriteria['monthUntil'] = $oForm->getValue('monthUntil');
           $searcCriteria['rcdRegion'] = $oForm->getValue('rcdRegion');
           $searcCriteria['dvArea'] = $oForm->getValue('dvArea');
           $searcCriteria['trLocation'] = $oForm->getValue('trLocation');
           $searcCriteria['status']  = $oForm->getValue('status');
           $searcCriteria['status1']  = implode("*",$oForm->getValue('status'));

           dbProc::replaceSearchResult($searchName,$userId,
                                $searcCriteria['yearFrom'],
                                $searcCriteria['yearUntil'],
                                $searcCriteria['monthFrom'],
                                $searcCriteria['monthUntil'],
                                $searcCriteria['rcdRegion'],
                                $searcCriteria['dvArea'],
                                '','','','','','','',
                                $searcCriteria['status1'],
                                $searcCriteria['trLocation']
                                );

           $cr = '';
           foreach($searcCriteria as $s)
           {
             $cr .= $s.'^';
           }
           $cr = substr($cr, 0, -1);
           $oListLink->addPrm('search', $cr);
           $actListLink=$oListLink ->getQuery();
           $oForm->addElement('static','jsRefresh2','','<script>reloadFrame(2, "'.$actListLink.'");</script>');
           unset($oListLink);
    	}
        else
        {
             if (reqVar::get('isReturn') == 1)
            {
                $searchCritery = dbProc::getSearchCritery($userId);
           	    if(count($searchCritery) >0 )
      	        {
      		        $critery = $searchCritery[0];
       	        }
            }
           $searcCriteria['yearFrom'] = isset($critery['yearFrom'])? $critery['yearFrom'] :dtime::getCurrentYear();
           $searcCriteria['yearUntil'] = isset($critery['yearUntil'])? $critery['yearUntil'] :dtime::getCurrentYear();
           $searcCriteria['monthFrom'] = isset($critery['monthFrom'])? $critery['monthFrom'] :dtime::getCurrentMonth();
           $searcCriteria['monthUntil'] = isset($critery['monthUntil'])? $critery['monthUntil'] :dtime::getCurrentMonth();
           $searcCriteria['rcdRegion'] = isset($critery['rcdRegion'])? $critery['rcdRegion'] :$user['RLTT_REGIONS'];
           $searcCriteria['dvArea'] = isset($critery['dvArea'])? $critery['dvArea'] :$user['RLTT_KDVI_ID'];
           $searcCriteria['trLocation'] = isset($critery['trLocation'])? $critery['trLocation'] :'-1';
           $searcCriteria['status']  = isset($critery['status1'])? explode("*",$critery['status1']) : array(STAT_CLOSE);
        }

        $oForm -> addElement('label', 'year',  text::get('YEAR'), '');
        $oForm -> addElement('select', 'yearFrom',  text::get('FROM'), $searcCriteria['yearFrom'], 'style="width:50;"', '', '', dtime::getYearArray());
        $oForm -> addElement('select', 'yearUntil',  text::get('UNTIL'), $searcCriteria['yearUntil'], 'style="width:50;"', '', '', dtime::getYearArray());

        $oForm -> addElement('label', 'month',  text::get('MONTH'), '');
        $oForm -> addElement('select', 'monthFrom',  text::get('FROM'), $searcCriteria['monthFrom'], 'style="width:70;"', '', '', dtime::getMonthArray());
        $oForm -> addElement('select', 'monthUntil',  text::get('UNTIL'), $searcCriteria['monthUntil'], 'style="width:70;"', '', '', dtime::getMonthArray());
        // xmlHttp link
		$oLink=new urlQuery();
		$oLink->addPrm(DONT_USE_GLB_TPL, 1);
		$oLink->addPrm(FORM_ID, 'f.akt.m.2');
        $oForm -> addElement('select', 'rcdRegion',  text::get('RCD_REGION'), $searcCriteria['rcdRegion'], 'onChange="eval(xmlHttpGetValue(\''.$oLink->getQuery().'&xmlHttp=1&dvRegion=\'+document.all[\'rcdRegion\'].options[document.all[\'rcdRegion\'].selectedIndex].value));"', '', '', $rcdRegion);

        $oForm -> addElement('select', 'dvArea',  text::get('SINGL_DV_AREA'), $searcCriteria['dvArea'], '', '', '', $dvarea);

        $oForm -> addElement('select', 'trLocation',  text::get('TRANSPORT_LOCATION'), $searcCriteria['trLocation'], '', '', '', $transportLocation);

        $oForm -> addElement('multiple_select', 'status', text::get('STATUS'), $searcCriteria['status'], ' size="7" ', '', '', $status);

        // form buttons
       	$oForm -> addElement('submitImg', 'search', text::get('SEARCH'), 'img/btn_meklet.gif', 'width="70" height="20" onClick="setPosition1(\'loading\'); return true;"');

        $oForm -> makeHtml();
		include('f.akt.m.2.tpl');
?>