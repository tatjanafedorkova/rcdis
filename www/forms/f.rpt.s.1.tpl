﻿<script type="text/javascript">
    function doSort(column, order)
	{
    	eval(xmlHttpGetValue('<?= $searchLink; ?>&xml=1&sort_k=' + column + '&sort_o=' + order));
	    reloadFrame(2, '<?= $searchLink; ?>&order=' + o + '&kol=' + k);
    }
</script>

<body class="frame_2">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><h1><?= text::toUpper(text::get('REPORT_MAIN_ACT')); ?></h1></td>
		<td align="right">
            <img src="img/ico_print.gif" alt="" width="16" height="16" border="0">&nbsp;
            <a href="<?= $printUrl; ?>" target="new"><?=text::get('PRINT');?></a>&nbsp;&nbsp;
            <img src="img/ico_excel.gif" alt="" width="16" height="16">&nbsp;
            <a href="<?= $exportUrl; ?>" ><?=text::get('EXPORT');?></a>&nbsp;
        </td>
	</tr>
</table>

<table cellpadding="5" cellspacing="1" border="0" width="100%" >
	<thead>
		<tr class="table_head_2">
			<td width="6%"><?= text::get('ACT_NUMBER'); ?></td>
            <td width="6%"><?= text::get('ACT_NUM_POSTFIX'); ?></td>
			<td width="5%"><?= text::get('SINGLE_ACT_TYPE'); ?></td>
            <td width="5%"><?=text::get('STATUS');?></td>
            <td width="7%">
                <?=text::get('SINGLE_VOLTAGE');?><br />
                <a href="#" onclick="doSort('<?= COLUMN_VOLTAGE;?>', 'ASC')">
						<img src="img/sort_asc.gif" alt="A-Z" width="11" height="6" border="0"></a>
                <a href="#" onclick="doSort('<?= COLUMN_VOLTAGE;?>', 'DESC')">
						<img src="img/sort_desc.gif" alt="Z-A" width="11" height="6" border="0"></a>
            </td>
            <td width="8%">
                <?=text::get('SINGLE_SOURCE_OF_FOUNDS');?>
                <a href="#" onclick="doSort('<?= COLUMN_FINANSESANAS_AVOTS;?>', 'ASC')">
						<img src="img/sort_asc.gif" alt="A-Z" width="11" height="6" border="0"></a>
                <a href="#" onclick="doSort('<?= COLUMN_FINANSESANAS_AVOTS;?>', 'DESC')">
						<img src="img/sort_desc.gif" alt="Z-A" width="11" height="6" border="0"></a>
            </td>
            <td width="8%"><?= text::get('SINGLE_OBJECT'); ?></td>
			<td width="10%"><?= text::get('MAIN_DESIGNATION'); ?></td>
            <td width="5%"><?=text::get('MONTH');?></td>
            <td width="5%"><?= text::get('OBJECT_IS_FINISHED'); ?></td>
            <td width="5%"><?=text::get('BASE_TIME');?></td>
            <td width="5%"><?=text::get('OVER_TIME');?></td>
                        <td width="5%"><?=text::get('ROUT_TIME');?></td>
            <td width="5%"><?= text::get('REPORT_MATERIAL_PRICE_TOTAL'); ?></td>
            <td width="9%" ><?=text::get('SINGL_DV_AREA');?></td>
            <td width="9%" ><?=text::get('DATE');?></td>
 		</tr>
	</thead>
	<tbody>
<?
	if (is_array($res) && count($res) > 0)
	{
		foreach ($res as $row)
		{
?>
			<tr class="table_cell_3" >
			   	<td align="center"><?= $row['NUMURS']; ?></td>
                <td align="center"><?= $row['POSTFIX']; ?></td>
                <td align="left"><?= $row['TYPE']; ?></td>
                <td align="left"><?= $row['STATUS_NAME']; ?></td>
                <td align="left"><?= $row['KSRG_NOSAUKUMS']; ?></td>
                <td align="left"><?= $row['KFNA_NOSAUKUMS']; ?></td>
                <td align="left"><?= $row['KOBJ_NOSAUKUMS']; ?></td>
                <td align="left"><?= $row['RAKT_OPERATIVAS_APZIM']; ?></td>
                <td align="left"><?= dtime::getMonthName($row['RAKT_MENESIS']); ?></td>
                <td align="center"><?= (($row['RAKT_IR_OBJEKTS_PABEIGTS'] == 1) ? text::get('YES') : text::get('NO')); ?></td>
                <td align="center"><?= $row['PAMATSTUNDAS']; ?></td>
                <td align="center"><?= $row['VIRSSTUNDAS']; ?></td>
		<td align="center"><?= $row['LAIKSCELA']; ?></td>
                <td align="center"><?= $row['CENA_KOPA']; ?></td>
                <td align="center"><?= $row['DV']; ?></td>
                <td align="center"><?= $row['AAUD_DATUMS']; ?></td>
			</tr>
<?
		}
?>
        <tr>
             <td align="right" colspan="10"><b><?=text::get('TOTAL');?>:</b></td>
             <td align="center"><b><?= $pamatstundas; ?></b></td>
             <td align="center"><b><?= $virsstundas; ?></b></td>
	          <td align="center"><b><?= $laikscela; ?></b></td>
             <td align="center"><b><?= $summa; ?></b></td>
        </tr>
<?
	}
?>
	</tbody>
</table>

</body>