<?
// Created by Tatjana Fedorkova
if (userAuthorization::isAdmin())
{
  $oLink=new urlQuery();
  $oLink->addPrm(FORM_ID, 'f.adm.s.7');
  $oForm = new Form('frmMain','post',$oLink->getQuery());
  unset($oLink);

  // get info about system
  $systemInfo = dbProc::getSystemInfo();
  if(count($systemInfo)>0)
  {
  	$info = $systemInfo[0];
  }

  $oForm -> addElement('text', 'atlidziba',  text::get('INV_PRINT_ATLIDZIBA').' '.strtolower (text::get('Koeficents')), 
    isset($info['SNFO_REFOUND_COEF'])?$info['SNFO_REFOUND_COEF']:'', 'maxlength="6"');
  $oForm -> addRule('atlidziba', text::get('ERROR_REQUIRED_FIELD'), 'required');
  $oForm -> addRule('atlidziba', text::get('ERROR_DOUBLE_VALUE'), 'double', 4);

  $oForm -> addElement('text', 'soc',  text::get('INV_PRINT_SOC_APDR').' '.strtolower (text::get('Koeficents')), 
    isset($info['SNFO_SOC_COEF'])?$info['SNFO_SOC_COEF']:'', 'maxlength="6"');
  $oForm -> addRule('soc', text::get('ERROR_REQUIRED_FIELD'), 'required');
  $oForm -> addRule('soc', text::get('ERROR_DOUBLE_VALUE'), 'double', 4);
  
  $oForm -> addElement('submitImg', 'submit', text::get('SAVE'), 'img/btn_saglabat.gif', 'width="70" height="20"');

  if ($oForm -> isFormSubmitted())
  {

        $r = false;
     	  $r = dbProc::saveSystemInfo( '', '', '', $oForm->getValue('atlidziba'), $oForm->getValue('soc'));

	  	if (!$r)
	  	{
	  		$oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
	    }
  }
  $oForm -> makeHtml();
  include('f.adm.s.7.tpl');
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>