<?
   ini_set("memory_limit", "256M");
   $sCriteria = reqVar::get('search');
   $sOrder = reqVar::get('sortOrder');

   $searchCr = array();
        $sCr = explode("^", $sCriteria);
        if(isset($sCr[0]) && $sCr[0] != -1  && isset($sCr[1]) && $sCr[1] != -1)
        {
           $searchCr[] = text::get('YEAR').': '.$sCr[0].'-'.$sCr[1];
        }
        if(isset($sCr[2]) && $sCr[2] != -1  && isset($sCr[3]) && $sCr[3] != -1)
        {
           $searchCr[] = text::get('MONTH').': '.dtime::getMonthName($sCr[2]).'-'.dtime::getMonthName($sCr[3]);
        }
        if(isset($sCr[4]) && $sCr[4] != -1)
        {
           $searchCr[] = text::get('RCD_REGION').': '.$sCr[4];
        }
        if(isset($sCr[5]) && $sCr[5] != -1)
        {
           $searchCr[] = text::get('SINGL_DV_AREA').': '.dbProc::getDVAreaName($sCr[5]);
        }
        if(isset($sCr[6]) && $sCr[6] != -1)
        {
           $searchCr[] = text::get('ED_REGION').': '.$sCr[6];
        }
        if(isset($sCr[7]) && $sCr[7] != -1)
        {
           $searchCr[] = text::get('ED_IECIKNIS').': '.dbProc::getEDAreaName($sCr[7]);
        }

        if(isset($sCr[8]) && $sCr[8] != -1)
        {
           $searchCr[] = text::get('SINGLE_SOURCE_OF_FOUNDS').': '.dbProc::getSourceOfFoundsName($sCr[8]);
        }
        if(isset($sCr[9]) && $sCr[9] != -1)
        {
           $searchCr[] = text::get('SINGLE_VOLTAGE').': '.dbProc::getVoltageName($sCr[9]);
        }
        if(isset($sCr[10]) && $sCr[10] != -1)
        {
           $searchCr[] = text::get('ACT_OUNER').': '.dbProc::getUserName($sCr[10]);
        }
        if(isset($sCr[11]) && $sCr[11] != -1)
        {
           $searchCr[] = text::get('SINGLE_ACT_TYPE').': '.dbProc::getActTypeName($sCr[11]);
        }
        if(isset($sCr[12]) && $sCr[12] != -1)
        {
           $searchCr[] = text::get('MMS_WORK_FINISHED').': '.(($sCr[12] == 1) ? text::get('YES'): text::get('NO'));
        }
        $status = explode("*", $sCr[14]);
        $statusString = '';
        if($status[0] == -1)
        {
           $statusString = text::get('ALL_STATUS');
        }
        else
        {
          foreach($status as $s)
          {
              $statusInfo = dbProc::getKrfkInfoByName($s);
              $statusString .= $statusInfo['KRFK_NOZIME'].",";
          }
          $statusString  = substr($statusString, 0, -1);
        }
        $searchCr[] = text::get('STATUS').': '.$statusString;
	if(isset($sCr[16]) && $sCr[16] != -1)
        {
           $searchCr[] = text::get('ED_SECTION').': '.urldecode($sCr[16]);
        }
   $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
   $cacheSettings = array( ' memoryCacheSize ' => '8MB');
   PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

   // Create new PHPExcel object
   $objPHPExcel = new PHPExcel();

   $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
   $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

   $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(15);
   $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);

   $objPHPExcel->getActiveSheet()->setShowGridlines(true);

   $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
   $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(9);

   $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

   $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
   $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
   $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
   $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(35);
   $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(35);
   $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(35);
   $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(35);
   $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(35);
   $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(35);

   $objPHPExcel->getActiveSheet()->duplicateStyleArray(
            		array(
            			'font'    => array(
            				'bold'      => true,
                          'italic' => false ,
                          'size' => 12
            			),
            			'alignment' => array(
            				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            			)
                  ),  'A1:U1'   );
   $objPHPExcel->getActiveSheet()->mergeCells('A1:U1');
   $objPHPExcel->getActiveSheet()->setCellValue('A1', text::get('REPORT_MAIN_ACT'));
   $objPHPExcel->getActiveSheet()->mergeCells('A2:U2');
   $objPHPExcel->getActiveSheet()->setCellValue('A2', implode('; ',$searchCr));
   $objPHPExcel->getActiveSheet()->duplicateStyleArray(
            		array(
            			'font'    => array(
            			  'bold'      => true,
                          'italic' => true ,
                          'size' => 10
            			),
            			'alignment' => array(
            				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            			)
                  ),  'A3:U3'   );

        $objPHPExcel->getActiveSheet()->setCellValue('A3', text::get('ACT_NUMBER'));
        $objPHPExcel->getActiveSheet()->setCellValue('B3', text::get('SINGLE_ACT_TYPE'));
        $objPHPExcel->getActiveSheet()->setCellValue('C3', text::get('STATUS'));
        $objPHPExcel->getActiveSheet()->setCellValue('D3', text::get('SINGLE_VOLTAGE'));
        $objPHPExcel->getActiveSheet()->setCellValue('E3', text::get('SINGLE_SOURCE_OF_FOUNDS'));
        $objPHPExcel->getActiveSheet()->setCellValue('F3', text::get('SINGLE_OBJECT'));
        $objPHPExcel->getActiveSheet()->setCellValue('G3', text::get('MAIN_DESIGNATION'));
        $objPHPExcel->getActiveSheet()->setCellValue('H3', text::get('MONTH'));
        $objPHPExcel->getActiveSheet()->setCellValue('I3', text::get('OBJECT_IS_FINISHED'));
        $objPHPExcel->getActiveSheet()->setCellValue('J3', text::get('BASE_TIME'));
        $objPHPExcel->getActiveSheet()->setCellValue('K3', text::get('OVER_TIME'));
        $objPHPExcel->getActiveSheet()->setCellValue('L3', text::get('REPORT_MATERIAL_PRICE_TOTAL'));
        $objPHPExcel->getActiveSheet()->setCellValue('M3', text::get('OTHER_PAYMENTS'));
        $objPHPExcel->getActiveSheet()->setCellValue('N3', text::get('OVERTIME_KOEFICENT'));
        $objPHPExcel->getActiveSheet()->setCellValue('O3', text::get('COMMENT'));
        $objPHPExcel->getActiveSheet()->setCellValue('P3', text::get('ACT_OUNER'));
        $objPHPExcel->getActiveSheet()->setCellValue('Q3', text::get('RCD_REGION'));
        $objPHPExcel->getActiveSheet()->setCellValue('R3', text::get('SINGL_DV_AREA'));
        $objPHPExcel->getActiveSheet()->setCellValue('S3', text::get('ED_REGION'));
        $objPHPExcel->getActiveSheet()->setCellValue('T3', text::get('ED_IECIKNIS'));
	$objPHPExcel->getActiveSheet()->setCellValue('U3', text::get('ED_SECTION'));

        $res = dbProc::getActFullList($sCriteria, $sOrder);
        $pamatstundas = 0;
        $virsstundas = 0;
        $summa = 0;
        $currentRow = 4;
        if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
               $objPHPExcel->getActiveSheet()->setCellValue('A'.$currentRow, $row['NUMURS']);
               $objPHPExcel->getActiveSheet()->setCellValue('B'.$currentRow, $row['TYPE']);
               $objPHPExcel->getActiveSheet()->setCellValue('C'.$currentRow, $row['STATUS_NAME']);
               $objPHPExcel->getActiveSheet()->setCellValue('D'.$currentRow, $row['KSRG_NOSAUKUMS']);
               $objPHPExcel->getActiveSheet()->setCellValue('E'.$currentRow, $row['KFNA_NOSAUKUMS']);
               $objPHPExcel->getActiveSheet()->setCellValue('F'.$currentRow, $row['KOBJ_NOSAUKUMS']);
               $objPHPExcel->getActiveSheet()->setCellValue('G'.$currentRow, $row['RAKT_OPERATIVAS_APZIM']);
               $objPHPExcel->getActiveSheet()->setCellValue('H'.$currentRow, dtime::getMonthName($row['RAKT_MENESIS']));
               $objPHPExcel->getActiveSheet()->setCellValue('I'.$currentRow, $row['RAKT_IR_OBJEKTS_PABEIGTS']);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$currentRow, $row['PAMATSTUNDAS'],PHPExcel_Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$currentRow, $row['VIRSSTUNDAS'],PHPExcel_Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$currentRow, $row['CENA_KOPA'],PHPExcel_Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$currentRow, $row['RAKT_CITAS_IZMAKSAS'],PHPExcel_Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$currentRow, $row['RAKT_VIRSSTUNDU_KOEF'],PHPExcel_Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValue('O'.$currentRow, $row['RAKT_PIEZIMES']);
               $objPHPExcel->getActiveSheet()->setCellValue('P'.$currentRow, $row['AUTORS']);
               $objPHPExcel->getActiveSheet()->setCellValue('Q'.$currentRow, $row['KDVI_REGIONS']);
               $objPHPExcel->getActiveSheet()->setCellValue('R'.$currentRow, $row['DV']);
               $objPHPExcel->getActiveSheet()->setCellValue('S'.$currentRow, $row['KEDI_REGIONS']);
               $objPHPExcel->getActiveSheet()->setCellValue('T'.$currentRow, $row['ED']);
	       $objPHPExcel->getActiveSheet()->setCellValue('U'.$currentRow, $row['KEDI_SECTION']);

               $pamatstundas = $pamatstundas + $row['PAMATSTUNDAS'];
               $virsstundas = $virsstundas + $row['VIRSSTUNDAS'];
               $summa = $summa + $row['CENA_KOPA'];
               $currentRow ++;
			}
		}
        $objPHPExcel->getActiveSheet()->getStyle('I'.$currentRow)->applyFromArray(
                	array('font'=> array('bold' => true, 'italic' => true),
                    'alignment' => array( 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
                    ) );
        $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                	array('font'=> array('bold' => true, 'italic' => true),
                    'alignment' => array( 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                    ), 'J'.$currentRow.':L'.$currentRow    );

        $objPHPExcel->getActiveSheet()->setCellValue('I'.$currentRow, text::get('TOTAL').':');
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$currentRow, number_format($pamatstundas, 2, '.', ''),PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$currentRow, number_format($virsstundas, 2, '.', ''),PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$currentRow, number_format($summa, 2, '.', ''),PHPExcel_Cell_DataType::TYPE_NUMERIC);

        $objPHPExcel->getActiveSheet()->duplicateStyleArray(
            		array('borders' => array(
                        'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                        'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                        'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                        'bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		        	)
                 ),  'A2:U'.$currentRow   );

  	// redirect output to client browser
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="myfile.xlsx"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	//$objWriter->setUseDiskCaching(true, "/temp");
	$objWriter->save('php://output'); 


?>