﻿<body class="frame_2">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><h1><?= text::toUpper(text::get('REPORT_WORK_WITH_NO_PLAN')); ?></h1></td>
		<td align="right">
            <img src="img/ico_print.gif" alt="" width="16" height="16" border="0">&nbsp;
            <a href="<?= $printUrl; ?>" target="new"><?=text::get('PRINT');?></a>&nbsp;&nbsp;
            <img src="img/ico_excel.gif" alt="" width="16" height="16">&nbsp;
            <a href="<?= $exportUrl; ?>" ><?=text::get('EXPORT');?></a>&nbsp;
        </td>
	</tr>
</table>

<table cellpadding="5" cellspacing="1" border="0" width="100%" >
<?
	if (is_array($area) && count($area) > 0)
	{
		foreach ($area as $row)
		{
		  $actCount = 0;
          if (is_array($row['ed']) && count($row['ed']) > 0)
          {
            foreach ($row['ed'] as $rrS)
            {
?>
            <tr class="table_cell_3">
                <td colspan="2" align="left"><b><?=text::get('ED_IECIKNIS');?>:</b></td>
                <td colspan="2" align="left"><?= $rrS['name']; ?></td>
                <td colspan="7" align="left"><?= $rrS['code']; ?></td>
            </tr>
            <tr class="table_head_2">
    			<td width="8%" rowspan="2"><?= text::get('ACT_NUMBER'); ?></td>
                <td width="10%" rowspan="2"><?=text::get('MONTH');?></td>
                <td width="15%" rowspan="2"><?= text::get('MAIN_DESIGNATION'); ?></td>
                <td width="10%" rowspan="2"><?= text::get('SINGLE_ACT_TYPE'); ?></td>
                <td width="9%" rowspan="2"><?=text::get('MAN_HOUR_STANDART_TOTAL');?></td>
                <td width="48%" colspan="6"><?=text::get('REAL_WORK_PAYMENT');?></td>
            </tr>
            <tr class="table_head_2">
                <td width="8%"><?=text::get('TRANSPORT_KM');?><br /></td>
                <td width="8%"><?=text::get('TRANSPORT_AVTO_TIME');?></td>
                <td width="8%"><?= text::get('TRANSPORT_WORK_TIME'); ?></td>
                <td width="8%"><?=text::get('BASE_TIME');?></td>
                <td width="8%"><?=text::get('OVER_TIME');?></td>
                <td width="8%"><?= text::get('REPORT_MATERIAL_PRICE_TOTAL'); ?></td>
            </tr>
            <?
                if (is_array($rrS['act']) && count($rrS['act']) > 0)
                {
                   foreach ($rrS['act'] as $r)
                   {
                     ?>
                   <tr class="table_cell_3" >
      			      <td align="center"><?= $r['actNumber']; ?></td>
                      <td align="left"><?= $r['month']; ?></td>
                      <td align="left"><?= $r['designation']; ?></td>
                      <td align="left"><?= $r['type']; ?></td>
                      <td align="center"><?= $r['workTime']; ?></td>
                      <td align="center"><?= $r['km']; ?></td>
                      <td align="center"><?= $r['stundas']; ?></td>
                      <td align="center"><?= $r['darbaStundas']; ?></td>
                      <td align="center"><?= $r['mainTime']; ?></td>
                      <td align="center"><?= $r['overTime']; ?></td>
                      <td align="center"><?= $r['cena']; ?></td>
                  </tr>
                     <?
                     $actCount = $actCount + 1;
                   }
                }
            ?>
            <tr class="table_head_2">
             <td colspan="4" align="left"><b><?=$rrS['name'];?>: <?=count($rrS['act']);?> <?=text::get('ACTS1');?></b></td>
             <td align="center"><b><?=number_format($rrS['totalWorkTime'],2,'.','');?></b></td>
             <td align="center"><b><?=number_format($rrS['totalKm'],2,'.','');?></b></td>
             <td align="center"><b><?=number_format($rrS['totalStundas'],2,'.','');?></b></td>
             <td align="center"><b><?=number_format($rrS['totalDarbaStundas'],2,'.','');?></b></td>
             <td align="center"><b><?=number_format($rrS['totalMainTime'],2,'.','');?></b></td>
             <td align="center"><b><?=number_format($rrS['totalOverTime'],2,'.','');?></b></td>
             <td align="center"><b><?=number_format($rrS['totalCena'],2,'.','');?></b></td>
            </tr>
          <? }}?>
            <tr class="table_head_2">
             <td colspan="4" align="left"><b><?=$row['section'];?>: <?=$actCount;?> <?=text::get('ACTS1');?></b></td>
             <td align="center"><b><?=number_format($row['totalWorkTime'],2,'.','');?></b></td>
             <td align="center"><b><?=number_format($row['totalKm'],2,'.','');?></b></td>
             <td align="center"><b><?=number_format($row['totalStundas'],2,'.','');?></b></td>
             <td align="center"><b><?=number_format($row['totalDarbaStundas'],2,'.','');?></b></td>
             <td align="center"><b><?=number_format($row['totalMainTime'],2,'.','');?></b></td>
             <td align="center"><b><?=number_format($row['totalOverTime'],2,'.','');?></b></td>
             <td align="center"><b><?=number_format($row['totalCena'],2,'.','');?></b></td>
            </tr>

        <?
		}
        ?>
        <tr>
             <td colspan="4" align="right"><b><?=text::get('TOTAL');?>:</b></td>
             <td align="center"><b><?= $cilvekstundas; ?></b></td>
             <td align="center"><b><?= $km; ?></b></td>
             <td align="center"><b><?= $stundas; ?></b></td>
             <td align="center"><b><?= $darbaStundas; ?></b></td>
             <td align="center"><b><?= $pamatstundas; ?></b></td>
             <td align="center"><b><?= $virsstundas; ?></b></td>
             <td align="center"><b><?= $summa; ?></b></td>
        </tr>
<?
	}
?>
	</tbody>
</table>

</body>