﻿<body class="frame_2">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><h1><?= text::toUpper(text::get('REPORT_CALCULATION')); ?></h1></td>
		<td align="right">
            <img src="img/ico_print.gif" alt="" width="16" height="16" border="0">&nbsp;
            <a href="<?= $printUrl; ?>" target="new"><?=text::get('PRINT');?></a>&nbsp;&nbsp;
            <img src="img/ico_excel.gif" alt="" width="16" height="16">&nbsp;
            <a href="<?= $exportUrl; ?>" ><?=text::get('EXPORT');?></a>&nbsp;
        </td>
	</tr>
</table>

<table cellpadding="5" cellspacing="1" border="0" width="100%" >
	<thead>
		<tr class="table_head_2">
            <td width="10%" ><?= text::get('CHIPHER'); ?></td>
			<td width="30%" ><?= text::get('NAME'); ?></td>
            <td width="15%" ><?=text::get('UNIT_OF_MEASURE');?></td>
            <td width="15%" ><?=text::get('AMOUNT');?></td>
            <td width="15%" ><?=text::get('BASE_TIME');?></td>
            <td width="15%" ><?=text::get('OVER_TIME');?></td>

 		</tr>
	</thead>
	<tbody>
<?
	if (is_array($res) && count($res) > 0)
	{
		foreach ($res as $row)
		{
?>
			<tr class="table_cell_3" >
                <td align="center"><?= $row['DRBI_KKAL_SHIFRS']; ?></td>
                <td align="left" ><?= $row['DRBI_KKAL_NOSAUKUMS']; ?></td>
                <td align="center"><?= $row['DRBI_MERVIENIBA']; ?></td>
                <td align="center"><?= $row['DAUDZUMS']; ?></td>
                <td align="center"><?= $row['PAMATSTUNDAS']; ?></td>
                <td align="center"><?= $row['VIRSTUNDAS']; ?></td>

			</tr>
<?
		}
?>
        <tr>
             <td align="right"  colspan="4"><b><?=text::get('TOTAL');?></b>:</td>
             <td align="center"><b><?= $baseTime; ?></b></td>
             <td align="center"><b><?= $overTime; ?></b></td>
             <td>&nbsp;</td>
       </tr>
<?
	}
?>
	</tbody>
</table>

</body>