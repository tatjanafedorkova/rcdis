﻿<script type="text/javascript">
    function savedField (el, color) { 
        el.style.borderColor = color; 
        parent = el.parentNode;
        if(parent.firstChild.nodeName == 'SPAN') {
            parent.removeChild(sp1);
        }
        //parent.removeChild(parent.childNodes[0]); 
    }
    // succsess
    var sp1 = document.createElement('span');
    sp1.append('Saglabāts');
    sp1.style.color = 'green';
    // error
    var sp2 = document.createElement('span');
    sp2.append('<?=text::get('ERROR_DOUBLE_VALUE');?>');
    sp2.style.color = 'red';
</script>
<h1><?=text::toUpper(text::get(TAB_MATERIAL));?></h1>
<?= $oFormMaterialTab -> getFormHeader(); ?>
<table cellpadding="5" cellspacing="1" border="0" width="100%">
	<tr>
		<td align=center colspan="2"><?= $oFormMaterialTab -> getMessage(); ?></td>
	</tr>
    <tr>
		<td valign="top">
        <table cellpadding="5" cellspacing="1" border="0" width="100%">
		   <tr class="table_head_2">

				<th width="12%"><?=text::get('CODE');?></th>
				<th width="37%"><?=text::get('NAME');?></th>
				<th  width="12%"><?=text::get('UNIT_OF_MEASURE');?></th>
                <th  width="12%"><?=text::get('PRICE');?></th>
				<th  width="12%"><?=text::get('AMOUNT');?></th>
               <? if($tame && !$isInvestition) { ?>
                <th  width="12%"><?=text::get('AMOUNT_KOR');?></th>
               <? } ?>
				<th  width="12%"><?=text::get('PRICE_TOTAL');?></th>
			 	<th width="3%">&nbsp;</th>
			</tr>

	  <?
	   	foreach($aMaterials as $i=>$val)
		{
		?>
			<tr class="table_cell_3"  id="blank_mat<?= $i; ?>">

				<td>
                    <?= $oFormMaterialTab->getElementHtml('id['.$i.']'); ?>
                    <?= $oFormMaterialTab->getElementHtml('code['.$i.']'); ?><span style="width: 1px; color:#F0EEDF; font-size: 5;" ><?= $oFormMaterialTab->getElementHtml('is_manual['.$i.']'); ?></span>
                </td>
				<td><?= $oFormMaterialTab->getElementHtml('title['.$i.']'); ?></td>
				<td><?= $oFormMaterialTab->getElementHtml('unit['.$i.']'); ?></td>
                <td><?= $oFormMaterialTab->getElementHtml('price['.$i.']'); ?><span style="width: 1px; color:#F0EEDF; font-size: 5;" ><?= $oFormMaterialTab->getElementHtml('price_origin['.$i.']'); ?></span></td>
				
                <? if($tameMaterial && !$isInvestition) { ?>
					<td><?= $oFormMaterialTab->getElementHtml('amount_km['.$i.']'); ?></td>
					<td><?= $oFormMaterialTab->getElementHtml('amount_kor_mat['.$i.']'); ?></td>
				<? } else { ?>
					<td><?= $oFormMaterialTab->getElementHtml('amount_mat['.$i.']'); ?></td>
				<? } ?> 
				<td><?= $oFormMaterialTab->getElementHtml('total_mat['.$i.']'); ?><span style="width: 1px; color:#F0EEDF; font-size: 5;" ><?= $oFormMaterialTab->getElementHtml('total_mat_origin['.$i.']'); ?></span></td>
             	<td>
                    <? if(!$isReadonly)  {?>
                    <?= $oFormMaterialTab->getElementHtml('mat_del['.$i.']'); ?>
                    <? } else{ ?>
                    &nbsp;
                    <? } ?>
                </td>
			</tr>

		<?
		}
		?>
        <tr>
             <td align="right"  colspan="<?= (($tameMaterial || (reqVar::get('tame_mat') == 1)) && !$isInvestition) ? '6' : '5' ?> "><b><?=text::get('TOTAL');?>:</b></td>
             <td align="center"><b><?= $totalPrice; ?></b></td>
        </tr>
	    </table>
        </td>
        <td width="20%"  valign="top">
            <table cellpadding="5" cellspacing="1" border="0" align="center" width="100%">
            <? if(!$isReadonly)  {
              foreach($favorit as $i=>$val){?>
             <tr>
                <td bgcolor="white"><?= $oFormMaterialTab->getElementHtml('group['.$i.']'); ?></td>
             </tr>
            <? }} ?>
            </table>
            <br />
            <table cellpadding="5" cellspacing="0" border="0" align="center">
            <tr>
             <? if(!$isReadonly)  {?>
            	<td><?= $oFormMaterialTab -> getElementHtml('save'); ?></td>
             <? } ?>

             </tr>
            </table>
        </td>
	</tr>

</table>
<?= $oFormMaterialTab -> getFormBottom(); ?>

