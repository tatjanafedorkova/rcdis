﻿<body class="print">
<div class="print_header"><?= text::get('REPORT_MAIN_ACT'); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_landscape" >
<?
  foreach($searchCr as $i=>$s)
  {
    if($i%3 == 0)
    {
      ?>
        <tr>
      <?
    }
    ?>
      <td align="right" class="print_table_total" width="13%"><?=$s['label'];?>:</td>
      <td align="left" class="print_table_data" width="20%"><?=$s['value'];?></td>
    <?
  }
?>
</table>
<br />
<table cellpadding="0" cellspacing="0" class="print_table_landscape" >

		<tr>
            <td width="10%" class="print_table_header"><?= text::get('ACT_NUMBER'); ?></td>
			<td width="10%" class="print_table_header"><?= text::get('SINGLE_ACT_TYPE'); ?></td>
            <td width="10%" class="print_table_header"><?=text::get('STATUS');?></td>
            <td width="5%" class="print_table_header"><?=text::get('TRANSPORT_KM');?><br /></td>
            <td width="5%" class="print_table_header"><?=text::get('TRANSPORT_AVTO_TIME');?></td>
            <td width="5%" class="print_table_header"><?= text::get('TRANSPORT_WORK_TIME'); ?></td>
			<td width="15%" class="print_table_header"><?= text::get('SINGLE_MMS'); ?></td>
            <td width="5%" class="print_table_header"><?= text::get('OBJECT_IS_FINISHED'); ?></td>
            <td width="5%" class="print_table_header"><?=text::get('PERSONAL_COUNT');?></td>
            <td width="5%" class="print_table_header"><?=text::get('BASE_TIME');?></td>
            <td width="5%" class="print_table_header"><?=text::get('OVER_TIME');?></td>
            <td width="5%" class="print_table_header"><?= text::get('REPORT_MATERIAL_PRICE_TOTAL'); ?></td>
            <td width="15%" class="print_table_header"><?= text::get('SINGL_DV_AREA'); ?></td>
 		</tr>

<?
	if (is_array($res) && count($res) > 0)
	{
		foreach ($res as $row)
		{
?>
			<tr >
                <td align="center" class="print_table_data">&nbsp;<?= $row['NUMURS']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $row['TYPE']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $row['STATUS_NAME']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $row['KM']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $row['STUNDAS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $row['DARBA_STUNDAS']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $row['KMSD_KODS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= (($row['RAKT_IR_OBJEKTS_PABEIGTS'] == 1) ? text::get('YES') : text::get('NO')); ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $row['CILVEKI']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $row['PAMATSTUNDAS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $row['VIRSSTUNDAS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $row['CENA_KOPA']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $row['DV']; ?></td>
			</tr>
<?
		}
?>
        <tr>
             <td align="right" class="print_table_total" colspan="3"><?=text::get('TOTAL');?>:</td>
             <td align="center" class="print_table_total"><b><?= $km; ?></b></td>
             <td align="center" class="print_table_total"><b><?= $stundas; ?></b></td>
             <td align="center" class="print_table_total"><b><?= $darbaStundas; ?></b></td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td align="center"><b><?= $cilveki; ?></b></td>
             <td align="center" class="print_table_total"><?= $pamatstundas; ?></td>
             <td align="center" class="print_table_total"><?= $virsstundas; ?></td>
             <td align="center" class="print_table_total"><?= $summa; ?></td>
        </tr>
<?
	}
?>

</table>

</body>