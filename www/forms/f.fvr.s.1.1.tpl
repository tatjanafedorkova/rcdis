﻿<body class="frame_1">
<?=$oForm->getElementHtml('jsRefresh2');?> 
<?= $oForm -> getFormHeader(); ?>
<h1><?=text::toUpper(text::get('FAVORITES'));?></h1>
<div align=center><?= $oForm -> getMessage(); ?></div>

<table cellpadding="5" cellspacing="1" border="0" width="100%">

	<tr>
        <td class="table_cell_c" width="25%"><?= $oForm -> getElementLabel('favoritId'); ?>:<?=(($action==OP_INSERT || $action==OP_DELETE)?'<font color="red">*</font>':'');?></td>
		<td class="table_cell_2" colspan="3"><?= $oForm -> getElementHtml('favoritId'); ?></td>
   </tr>
   <?
		if (($action==OP_INSERT) || ($action==OP_UPDATE && is_numeric($favoritId)))
		{
			?>
            <tr>
                <td class="table_cell_c" width="25%"><?= $oForm -> getElementLabel('catalog'); ?>:<?=(($action==OP_INSERT)?'<font color="red">*</font>':'');?></td>
        		<td class="table_cell_2" width="25%"><?= $oForm -> getElementHtml('catalog'); ?></td>
                <td class="table_cell_c" width="25%"><?= $oForm -> getElementLabel('isDefault'); ?>:</td>
        		<td class="table_cell_2" width="25%"><?= $oForm -> getElementHtml('isDefault'); ?></td>
            </tr>
			<?
		}
		?>


</table>

<table cellpadding="5" cellspacing="0" border="0" align="center">
	<tr>
		<td>
			<?
			if(($action == OP_INSERT) || ($action == OP_UPDATE && $favoritId))
			{
				echo $oForm->getElementHtml('continue');
			}
			elseif($action == OP_DELETE)
			{
				echo $oForm->getElementHtml('delete');
			}
			?>
		</td>
		<td>
            <?
			if(($action == OP_INSERT) || $action == OP_DELETE)
			{
				echo $oForm->getElementHtml('clear');
			}

			?>
        </td>
	</tr>
</table>
<?= $oForm->getFormBottom(); ?>
</body>