﻿<body class="print">
<div class="print_header"><?= text::get('REPORT_CALCULATION_BY_GROUP'); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >
<?
  foreach($searchCr as $i=>$s)
  {
    if($i%3 == 0)
    {
      ?>
        <tr>
      <?
    }
    ?>
      <td align="right" class="print_table_total" width="13%"><?=$s['label'];?>:</td>
      <td align="left" class="print_table_data" width="20%"><?=$s['value'];?></td>
    <?
  }
?>
</table>
<br />
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >

		<tr>
            <td width="10%" class="print_table_header"><?= text::get('CHIPHER'); ?></td>
			<td width="30%" class="print_table_header"><?= text::get('NAME'); ?></td>
            <td width="15%" class="print_table_header"><?=text::get('UNIT_OF_MEASURE');?></td>
            <td width="15%" class="print_table_header"><?=text::get('AMOUNT');?></td>
            <td width="15%" class="print_table_header"><?=text::get('BASE_TIME');?></td>
            <td width="15%" class="print_table_header"><?=text::get('OVER_TIME');?> </td>

 		</tr>

<?
	if (is_array($res) && count($res) > 0)
	{
		foreach ($res as $row)
		{
?>
			<tr >
                <td align="center" class="print_table_data"><?= $row['DRBI_KKAL_SHIFRS']; ?></td>
                <td align="left" class="print_table_data"><?= $row['DRBI_KKAL_NOSAUKUMS']; ?></td>
                <td align="center" class="print_table_data"><?= $row['DRBI_MERVIENIBA']; ?></td>
                <td align="center" class="print_table_data"><?= $row['DAUDZUMS']; ?></td>
                <td align="center" class="print_table_data"><?= $row['PAMATSTUNDAS']; ?></td>
                <td align="center" class="print_table_data"><?= $row['VIRSTUNDAS']; ?></td>


			</tr>
<?
		}
?>
        <tr>
             <td align="right" class="print_table_total" colspan="4"><?=text::get('TOTAL');?>:</td>
             <td align="center" class="print_table_total"><?= $baseTime; ?></td>
             <td align="center" class="print_table_total"><?= $overTime; ?></td>
             <td>&nbsp;</td>
       </tr>
<?
	}
?>

</table>

</body>