<?
//created by Tatjana Fedorkova at 2004.12.02.
   
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();

$isValidDate = dtime::dateToInt(dtime::now()) >= 20111227;
$year = dtime::getSmartCurrentYear();


$isSystemUser = dbProc::isExistsUserRole($userId);
$isOldSystemUser = dbProc::isExistsUserRole($userId);
$isEditor = (( dbProc::isUserInRole($userId, ROLE_EDITOR) ||  dbProc::isUserInRole($userId, ROLE_EDITOR_VIEWER)));
$isEdUser = dbProc::isUserInRole($userId, ROLE_ED_USER);
$isAuditor = dbProc::isUserInRole($userId, ROLE_AUDIT_USER);
$isEconomist = dbProc::isUserInRole($userId, ROLE_ECONOMIST);
$isRight = true;
  // EXIT LINK
$oUrl=new urlQuery();
$oUrl->addPrm(FORM_ID,'exit');
$exitUrl=$oUrl->getQuery();
unset($oUrl);

// get user info
$userInfo = dbProc::getUserInfo($userId);
if(count($userInfo) >0 )
{
  $userName = $userInfo[0]['RLTT_VARDS'];
  $userSurName = $userInfo[0]['RLTT_UZVARDS'];
  $userAuthDate = $userInfo[0]['RLTT_DATUMS'];
}
else
{
    RequestHandle::showErrorAndDie(text::get('NO_RESULT_FOUND'));
}


$versionYear = $year.'.'.text::get('YEAR');

// jalietotajam nav lomas sist�m�, tad vinam nav tiesibas redzet si formu
if ( $isAdmin || $isSystemUser )
{


// menu USER_SETTINGS
	// change password
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.pwd.s.1');
	$changePassLink=$oLink ->getQuery();
	unset($oLink);

// menu ACT
	// EF act main form
	$oLink=new urlQuery();
	$oLink->addPrm('isEpla', '0');
	$oLink->addPrm(FORM_ID, 'f.akt.s.1');
	$efActLink=$oLink ->getQuery();
	unset($oLink);

	// EPLA act main form
	$oLink=new urlQuery();
	$oLink->addPrm('isEpla', '1');
	$oLink->addPrm(FORM_ID, 'f.akt.s.1');
	$eplaActLink=$oLink ->getQuery();
	unset($oLink);
	
	// RFC act main form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.akt.s.18');
	$rfcActLink=$oLink ->getQuery();
	unset($oLink);

    // act advanced search form
	$oLink=new urlQuery();
	$oLink->addPrm('isSearch', '1');
	$oLink->addPrm(FORM_ID, 'f.akt.s.9');
	$actSearchLink=$oLink ->getQuery();
	unset($oLink);
    // list of acts
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'blank');
	$actListLink=$oLink ->getQuery();
	unset($oLink);

    // act search by number form
	$oLink=new urlQuery();
	$oLink->addPrm('isSearch', '2');
	$oLink->addPrm(FORM_ID, 'f.akt.s.9');
	$actSearchByNumberLink=$oLink ->getQuery();
	unset($oLink);
    // list of acts
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'blank');
	$actListLink=$oLink ->getQuery();
	unset($oLink);

   

// menu CATALOGS

    // list of materials
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.1');
	$materialListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit materials form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.1');
	$oLink->addPrm('editMode', '1');
	$editMaterialLink = $oLink->getQuery();
	unset($oLink);
    
    // list of customers
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.2');
	$customerListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit customer form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.2');
	$oLink->addPrm('editMode', '1');
	$editCustomerLink = $oLink->getQuery();
	unset($oLink);

    // list of transports
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.3');
	$transportListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit transport form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.3');
	$oLink->addPrm('editMode', '1');
	$editTransportLink = $oLink->getQuery();
	unset($oLink);

 	// list of dv area
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.4');
	$dvAreaListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit dv area form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.4');
	$oLink->addPrm('editMode', '1');
	$editDvAreaLink = $oLink->getQuery();
	unset($oLink);

    // list of ed area
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.5');
	$edAreaListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit ed area form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.5');
	$oLink->addPrm('editMode', '1');
	$editEdAreaLink = $oLink->getQuery();
	unset($oLink);

    // list of voltage
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.6');
	$voltageListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit voltage form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.6');
	$oLink->addPrm('editMode', '1');
	$editVoltageLink = $oLink->getQuery();
	unset($oLink);

    // list of object
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.7');
	$objectListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit object form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.7');
	$oLink->addPrm('editMode', '1');
	$editObjectLink = $oLink->getQuery();
	unset($oLink);

    // list of source of founds
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.8');
	$sourceOfFoundsListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit source of founds form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.8');
	$oLink->addPrm('editMode', '1');
	$editSourceOfFoundsLink = $oLink->getQuery();
	unset($oLink);

    // list of work types
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.18');
	$workTypesListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit list of type form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.18');
	$oLink->addPrm('editMode', '1');
	$editWorkTypeLink = $oLink->getQuery();
	unset($oLink);

    // list of calculation group
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.9');
	$calculationGroupListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit calculation group form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.9');
	$oLink->addPrm('editMode', '1');
	$editCalculationGroupLink = $oLink->getQuery();
	unset($oLink);

    // list of calculation
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.10');
	$calculationListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit calculation form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.10');
	$oLink->addPrm('editMode', '1');
	$editCalculationLink = $oLink->getQuery();
	unset($oLink);

    // list of act types
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.11');
	$actTypeListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit act type form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.11');
	$oLink->addPrm('editMode', '1');
	$editActTypeLink = $oLink->getQuery();
	unset($oLink);

 	// list of users
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.12');
	$userListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit/delete user form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.12');
	$oLink->addPrm('editMode', '1');
	$editUserLink = $oLink->getQuery();
	unset($oLink);

    // list of mms
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.14');
	$mmsListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit/delete mms form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.14');
	$oLink->addPrm('editMode', '1');
	$editMMSLink = $oLink->getQuery();
	unset($oLink);

	// list of mms-calculation
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.20');
	$mmsCalculationListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit mms-calculation form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.20');
	$oLink->addPrm('editMode', '1');
	$editMmsCalculationLink = $oLink->getQuery();
	unset($oLink);

	// list of сalculation-material
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.21');
	$calculationMaterialListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit сalculation-material form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.21');
	$oLink->addPrm('editMode', '1');
	$editCalculationMaterialLink = $oLink->getQuery();
	unset($oLink);

	// list of investicijas prices
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.22');
	$investitionPricesListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit сalculation-material form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.22');
	$oLink->addPrm('editMode', '1');
	$editInvesticijasPriceLink = $oLink->getQuery();
	unset($oLink);

	// list of investicijas signatories
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.19');
	$investitionSignListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit сalculation-material form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.19');
	$oLink->addPrm('editMode', '1');
	$editInvesticijasSignLink = $oLink->getQuery();
	unset($oLink);

    // calalog imports
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.13');
	$catalogImportLink=$oLink ->getQuery();
	unset($oLink);

     // list of regions
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.15');
	$regionListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit region form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.15');
	$oLink->addPrm('editMode', '1');
	$editRegionLink = $oLink->getQuery();
	unset($oLink);

     // list of ef orders
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.16');
	$efOrderListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit ef orders form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.16');
	$oLink->addPrm('editMode', '1');
	$editefOrderLink = $oLink->getQuery();
	unset($oLink);

     // list of ef stock
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.ktl.s.17');
	$StockListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit ef stock form
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.ktl.s.17');
	$oLink->addPrm('editMode', '1');
	$editStockLink = $oLink->getQuery();
	unset($oLink);

// menu FAVORITS
	// total
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.fvr.s.3');

   	// add/edit/delete request
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.fvr.s.1');

	// add request
	$oLink->addPrm('action', OP_INSERT);
	$favoritAddLink=$oLink ->getQuery();
	// edit request
	$oLink->addPrm('action', OP_UPDATE);
	// add request
	$favoritEditLink=$oLink ->getQuery();
	// delete request
	$oLink->addPrm('action', OP_DELETE);
	$favoritDeleteLink=$oLink ->getQuery();

// Report links
	// Reports main
	$oLink = new urlQuery;
	$oLink -> addPrm(FORM_ID, 'f.rpt.s.0');
	$cRepMain = $oLink -> getQuery();
	unset($oLink);

// menu OPTIONS
	// warning
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.adm.s.1');
	$warningLink=$oLink ->getQuery();
	unset($oLink);

	// users actions
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.adm.s.2');
	$userActionsLink=$oLink ->getQuery();
	unset($oLink);

	// kvikStep
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.adm.s.3');
	$kvikStepLink=$oLink ->getQuery();
	unset($oLink);

	// calc start date
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.adm.s.5');
	$CalcStartDateLink=$oLink ->getQuery();
	unset($oLink);

	// ivestition coeffs
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.adm.s.7');
	$InvestitionCoefsLink=$oLink ->getQuery();
	unset($oLink);

	// list of jobs
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'f.adm.s.4');
	$jobListLink=$oLink ->getQuery();
	unset($oLink);
	// add/edit job
	$oLink=new urlQuery();
	$oLink->addPrm(FORM_ID, 'f.adm.s.4');
	$oLink->addPrm('editMode', '1');
	$editJobLink = $oLink->getQuery();
	unset($oLink);

	// event log
	$oLink=new urlQuery();
	$oLink->addPrm('isSearch', '1');
	$oLink->addPrm(FORM_ID, 'f.adm.s.6');
	$eventSearchLink=$oLink ->getQuery();
	unset($oLink);
    // list of acts
	$oLink=new urlQuery();
	$oLink->addPrm('isNew', '1');
	$oLink->addPrm(FORM_ID, 'blank');
	$eventListLink=$oLink ->getQuery();
	unset($oLink);
}
else
{
    $isRight = false;
    //RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
include('f.inf.s.1.tpl');

?>