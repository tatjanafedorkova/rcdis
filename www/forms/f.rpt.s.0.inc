<?
$projectId = reqVar::get('projectId');

$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isEdUser = dbProc::isUserInRole($userId, ROLE_ED_USER);
$isAuditor = dbProc::isUserInRole($userId, ROLE_AUDIT_USER);

// Report links
    // act summary
	$oLink=new urlQuery();
	$oLink->addPrm('isSearch', '1');
	$oLink->addPrm(FORM_ID, 'f.rpt.s.1');
	$actMainReportLink=$oLink ->getQuery();
	unset($oLink);

    // customer work time
	$oLink=new urlQuery();
	$oLink->addPrm('isSearch', '1');
	$oLink->addPrm(FORM_ID, 'f.rpt.s.2');
	$workTimeReportLink=$oLink ->getQuery();
	unset($oLink);

    // customer work time details
	$oLink=new urlQuery();
	$oLink->addPrm('isSearch', '1');
	$oLink->addPrm(FORM_ID, 'f.rpt.s.3');
	$workTimeDetailsReportLink=$oLink ->getQuery();
	unset($oLink);

    // materials
	$oLink=new urlQuery();
	$oLink->addPrm('isSearch', '1');
	$oLink->addPrm(FORM_ID, 'f.rpt.s.4');
	$materialReportLink=$oLink ->getQuery();
	unset($oLink);

    // tranport by group
	$oLink=new urlQuery();
	$oLink->addPrm('isSearch', '1');
	$oLink->addPrm(FORM_ID, 'f.rpt.s.5');
	$transportGroupReportLink=$oLink ->getQuery();
	unset($oLink);

    // tranport usage
	$oLink=new urlQuery();
	$oLink->addPrm('isSearch', '1');
	$oLink->addPrm(FORM_ID, 'f.rpt.s.6');
	$transportUsageReportLink=$oLink ->getQuery();
	unset($oLink);

    // calculation
	$oLink=new urlQuery();
	$oLink->addPrm('isSearch', '1');
	$oLink->addPrm(FORM_ID, 'f.rpt.s.7');
	$calculationReportLink=$oLink ->getQuery();
	unset($oLink);

   // calculation (plan/not plan)
	$oLink=new urlQuery();
	$oLink->addPrm('isSearch', '1');
	$oLink->addPrm(FORM_ID, 'f.rpt.s.19');
	$calculationPlanNplanReportLink=$oLink ->getQuery();
	unset($oLink);
	
	// apskatītie iekšējam aktam
	$oLink=new urlQuery();
	$oLink->addPrm('isSearch', '1');
	$oLink->addPrm(FORM_ID, 'f.rpt.s.20');
	$RfcActReportLink=$oLink ->getQuery();
	unset($oLink);


    if($isEdUser || $isAdmin || $isAuditor || (userAuthorization::getUserId() == 119))
    {
         // act summary
    	$oLink=new urlQuery();
    	$oLink->addPrm('isSearch', '1');
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.8');
    	$actEdMainReportLink=$oLink ->getQuery();
    	unset($oLink);

        // material by code
    	$oLink=new urlQuery();
    	$oLink->addPrm('isSearch', '1');
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.9');
    	$materialByCodeReportLink=$oLink ->getQuery();
    	unset($oLink);

        // material by group
    	$oLink=new urlQuery();
    	$oLink->addPrm('isSearch', '1');
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.10');
    	$materialByGroupReportLink=$oLink ->getQuery();
    	unset($oLink);

        // calculation by group
    	$oLink=new urlQuery();
    	$oLink->addPrm('isSearch', '1');
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.11');
    	$calculationByGroupReportLink=$oLink ->getQuery();
    	unset($oLink);

        // work with plan and not plan
    	$oLink=new urlQuery();
    	$oLink->addPrm('isSearch', '1');
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.12');
    	$workPlanNotPlanReportLink=$oLink ->getQuery();
    	unset($oLink);

        // work with plan and not plan
    	$oLink=new urlQuery();
    	$oLink->addPrm('isSearch', '1');
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.13');
    	$workPhisicalShowingReportLink=$oLink ->getQuery();
    	unset($oLink);

        // work with plan
    	$oLink=new urlQuery();
    	$oLink->addPrm('isSearch', '1');
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.14');
    	$workPlanReportLink=$oLink ->getQuery();
    	unset($oLink);

        // work no plan
        $oLink=new urlQuery();
    	$oLink->addPrm('isSearch', '1');
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.15');
    	$workNotPlanReportLink=$oLink ->getQuery();
    	unset($oLink);

        // not started work
        $oLink=new urlQuery();
    	$oLink->addPrm('isSearch', '1');
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.16');
    	$norStartedWorkReportLink=$oLink ->getQuery();
    	unset($oLink);

        // finished work
        $oLink=new urlQuery();
    	$oLink->addPrm('isSearch', '1');
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.17');
    	$finishedWorkReportLink=$oLink ->getQuery();
    	unset($oLink);

        // mms group
        $oLink=new urlQuery();
    	$oLink->addPrm('isSearch', '1');
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.18');
    	$mmsWorkReportLink=$oLink ->getQuery();
    	unset($oLink);
    }

include('f.rpt.s.0.tpl');
?>