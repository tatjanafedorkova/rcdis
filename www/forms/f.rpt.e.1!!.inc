<?
   $sCriteria = reqVar::get('search');
   $sOrder = reqVar::get('sortOrder');

   $searchCr = array();
        $sCr = explode("^", $sCriteria);
        if(isset($sCr[0]) && $sCr[0] != -1  && isset($sCr[1]) && $sCr[1] != -1)
        {
           $searchCr[] = text::get('YEAR').': '.$sCr[0].'-'.$sCr[1];
        }
        if(isset($sCr[2]) && $sCr[2] != -1  && isset($sCr[3]) && $sCr[3] != -1)
        {
           $searchCr[] = text::get('MONTH').': '.dtime::getMonthName($sCr[2]).'-'.dtime::getMonthName($sCr[3]);
        }
        if(isset($sCr[4]) && $sCr[4] != -1)
        {
           $searchCr[] = text::get('RCD_REGION').': '.$sCr[4];
        }
        if(isset($sCr[5]) && $sCr[5] != -1)
        {
           $searchCr[] = text::get('SINGL_DV_AREA').': '.dbProc::getDVAreaName($sCr[5]);
        }
        if(isset($sCr[6]) && $sCr[6] != -1)
        {
           $searchCr[] = text::get('ED_REGION').': '.$sCr[6];
        }
        if(isset($sCr[7]) && $sCr[7] != -1)
        {
           $searchCr[] = text::get('ED_IECIKNIS').': '.dbProc::getEDAreaName($sCr[7]);
        }

        if(isset($sCr[8]) && $sCr[8] != -1)
        {
           $searchCr[] = text::get('SINGLE_SOURCE_OF_FOUNDS').': '.dbProc::getSourceOfFoundsName($sCr[8]);
        }
        if(isset($sCr[9]) && $sCr[9] != -1)
        {
           $searchCr[] = text::get('SINGLE_VOLTAGE').': '.dbProc::getVoltageName($sCr[9]);
        }
        if(isset($sCr[10]) && $sCr[10] != -1)
        {
           $searchCr[] = text::get('ACT_OUNER').': '.dbProc::getUserName($sCr[10]);
        }
        if(isset($sCr[11]) && $sCr[11] != -1)
        {
           $searchCr[] = text::get('SINGLE_ACT_TYPE').': '.dbProc::getActTypeName($sCr[11]);
        }
        if(isset($sCr[12]) && $sCr[12] != -1)
        {
           $searchCr[] = text::get('MMS_WORK_FINISHED').': '.(($sCr[12] == 1) ? text::get('YES'): text::get('NO'));
        }
        $status = explode("*", $sCr[14]);
        $statusString = '';
        if($status[0] == -1)
        {
           $statusString = text::get('ALL_STATUS');
        }
        else
        {
          foreach($status as $s)
          {
              $statusInfo = dbProc::getKrfkInfoByName($s);
              $statusString .= $statusInfo['KRFK_NOZIME'].",";
          }
          $statusString  = substr($statusString, 0, -1);
        }
        $searchCr[] = text::get('STATUS').': '.$statusString;
        if(isset($sCr[16]) && $sCr[16] != -1)
        {
           $searchCr[] = text::get('ED_SECTION').': '.urldecode($sCr[16]);
        }
   $excel = '<head><meta http-equiv=Content-Type content="text/html; charset=utf-8"></head>';

   $excel .= '<table style="font-family:Arial; font-size:9pt;">';

   $excel .= '<tr><td colspan="20" style="font-size:12pt; font-weight:bold;">'.text::get('REPORT_MAIN_ACT').'</td></tr>';
   $excel .= '<tr><td colspan="20">'.implode('; ',$searchCr).'</td></tr>';
   $excel .= '<tr>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('ACT_NUMBER').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('SINGLE_ACT_TYPE').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('STATUS').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('SINGLE_VOLTAGE').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('SINGLE_SOURCE_OF_FOUNDS').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('SINGLE_OBJECT').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('MAIN_DESIGNATION').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('MONTH').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('OBJECT_IS_FINISHED').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('BASE_TIME').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('OVER_TIME').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('REPORT_MATERIAL_PRICE_TOTAL').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('OTHER_PAYMENTS').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('OVERTIME_KOEFICENT').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('COMMENT').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('ACT_OUNER').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('RCD_REGION').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('SINGL_DV_AREA').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('ED_REGION').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('ED_IECIKNIS').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('ED_SECTION').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('DATE').'</td>';
   $excel .= '</tr>';
        $res = dbProc::getActFullList($sCriteria, $sOrder);
        $pamatstundas = 0;
        $virsstundas = 0;
        $summa = 0;
        $currentRow = 4;
        if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
			  $excel .= '<tr>';
              $excel .= '<td style="border: 1px solid black;">'.sprintf(" %s",$row['NUMURS']).'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['TYPE'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['STATUS_NAME'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['KSRG_NOSAUKUMS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['KFNA_NOSAUKUMS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['KOBJ_NOSAUKUMS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['RAKT_OPERATIVAS_APZIM'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.dtime::getMonthName($row['RAKT_MENESIS']).'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['RAKT_IR_OBJEKTS_PABEIGTS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.number_format($row['PAMATSTUNDAS'],2,'.','').'</td>';
              $excel .= '<td style="border: 1px solid black;">'.number_format($row['VIRSSTUNDAS'],2,'.','').'</td>';
              $excel .= '<td style="border: 1px solid black;">'.number_format($row['CENA_KOPA'],2,'.','').'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['RAKT_CITAS_IZMAKSAS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.number_format($row['RAKT_VIRSSTUNDU_KOEF'],2,'.','').'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['RAKT_PIEZIMES'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['AUTORS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['KDVI_REGIONS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['DV'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['KEDI_REGIONS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['ED'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['KEDI_SECTION'].'</td>';
               $excel .= '<td style="border: 1px solid black;">'.$row['AAUD_DATUMS'].'</td>';  
              $excel .= '</tr>';
               $pamatstundas = $pamatstundas + $row['PAMATSTUNDAS'];
               $virsstundas = $virsstundas + $row['VIRSSTUNDAS'];
               $summa = $summa + $row['CENA_KOPA'];
               $currentRow ++;
			}
		}

        $excel .= '<tr>';
        $excel .= '<td colspan="9" style="font-weight:bold; text-align:right; font-style: italic">'.text::get('TOTAL').':'.'</td>';
        $excel .= '<td style="font-weight:bold; font-style: italic">'.number_format($pamatstundas, 2, '.', '').'</td>';
        $excel .= '<td style="font-weight:bold; font-style: italic">'.number_format($virsstundas, 2, '.', '').'</td>';
        $excel .= '<td style="font-weight:bold; font-style: italic">'.number_format($summa, 2, '.', '').'</td>';
        $excel .= '</tr>';

  // redirect output to client browser
  //header('Content-Type: application/vnd.ms-excel;');
  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;');
  header('Content-Disposition: attachment;filename=myfile.xlsx');
  header('Cache-Control: max-age=0');


  echo $excel;

?>