﻿<body class="print">
<div class="print_header"><?= text::get('REPORT_WORK_PLAN_AND_NOT_PLAN'); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >
<?
  foreach($searchCr as $i=>$s)
  {
    if($i%3 == 0)
    {
      ?>
        <tr>
      <?
    }
    ?>
      <td align="right" class="print_table_total" width="13%"><?=$s['label'];?>:</td>
      <td align="left" class="print_table_data" width="20%"><?=$s['value'];?></td>
    <?
  }
?>
</table>
<br />
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >
<?

	if (is_array($area) && count($area) > 0)
	{
		foreach ($area as $row)
		{
?>
            <tr>
                <td align="left" class="print_table_header2"><b><?=text::get('ED_IECIKNIS');?>:</b></td>
                <td colspan="2" align="left" class="print_table_header2"><b><?= $row['name']; ?></b></td>
                <td align="left" class="print_table_header2"><b><?= $row['code']; ?></b></td>
            </tr>

            <tr>
                <td width="30%"  class="print_table_header2">&nbsp;</td>
                <td width="20%"  class="print_table_header2"><?=text::get('BASE_TIME');?></td>
                <td width="20%"  class="print_table_header2"><?= text::get('REPORT_MATERIAL_PRICE_TOTAL'); ?></td>
                <td width="20%"  class="print_table_header2"><?=text::get('OVER_TIME');?></td>
            </tr>

            <tr>
                 <td align="right"><b><?=text::get('MMS_PLAN_WORKS');?>:</b></td>
                 <td align="center"><?= $row['mmsMainTime']; ?></td>
                 <td align="center"><?= $row['mmsCena']; ?></td>
                 <td align="center"><?= $row['mmsOverTime']; ?></td>
            </tr>
            <tr>
                 <td align="right"><b><?=text::get('REAL_PLAN_WORKS');?>:</b></td>
                 <td align="center"><?= $row['planMainTime']; ?></td>
                 <td align="center"><?= $row['planCena']; ?></td>
                 <td align="center"><?= $row['planOverTime']; ?></td>
            </tr>
            <tr>
                 <td align="right"><b><?=text::get('MMS_PLAN_MINUS_REAL_PLAN');?>:</b></td>
                 <td align="center"><?= $row['difMainTime']; ?></td>
                 <td align="center"><?= $row['difCena']; ?></td>
                 <td align="center"><?= $row['difOverTime']; ?></td>
            </tr>
            <tr>
                 <td align="right"><b><?=text::get('NOT_PLAN_WORK');?>:</b></td>
                 <td align="center"><?= $row['notPlanMainTime']; ?></td>
                 <td align="center"><?= $row['notPlanCena']; ?></td>
                 <td align="center"><?= $row['notPlanOverTime']; ?></td>
            </tr>
            <tr>
                 <td align="right"><b><?=text::get('DAMAGE_REAL_WORK');?>:</b></td>
                 <td align="center"><?= $row['damageMainTime']; ?></td>
                 <td align="center"><?= $row['damageCena']; ?></td>
                 <td align="center"><?= $row['damageOverTime']; ?></td>
            </tr>
            <tr>
                 <td align="right"><b><?=text::get('DEFECT_REAL_WORK');?>:</b></td>
                 <td align="center"><?= $row['defectMainTime']; ?></td>
                 <td align="center"><?= $row['defectCena']; ?></td>
                 <td align="center"><?= $row['defectOverTime']; ?></td>
            </tr>
            <tr>
                 <td align="right"><b><?=text::get('STIHIJA_REAL_WORK');?>:</b></td>
                 <td align="center"><?= $row['stihijaMainTime']; ?></td>
                 <td align="center"><?= $row['stihijaCena']; ?></td>
                 <td align="center"><?= $row['stihijaOverTime']; ?></td>
            </tr>
            <tr>
                 <td align="right"><b><?=text::get('NOT_PLAN_TO_PLAN');?>:</b></td>
                 <td align="center"><?= $row['notPlanProcentMainTime']; ?> %</td>
                 <td align="center"><?= $row['notPlanProcentCena']; ?> %</td>
                 <td align="center"><?= $row['notPlanProcentOverTime']; ?> %</td>
            </tr>
            <tr>
                 <td align="right"><b><?=text::get('NOT_PLAN_AND_PLAN_TOTAL');?>:</b></td>
                 <td align="center"><?= $row['totalMainTime']; ?></td>
                 <td align="center"><?= $row['totalCena']; ?></td>
                 <td align="center"><?= $row['totalOverTime']; ?></td>
            </tr>
            <tr><td colspan="4" class="print_table_data2">&nbsp;</td></tr>
        <?

		}
        ?>

<?
	}
?>

</table>
</body>