﻿<body class="frame_1">
<?=$oForm->getElementHtml('jsRefresh2');?>
<h1><?=$searchTitle;?></h1>
<?= $oForm -> getFormHeader(); ?>
<table cellpadding="5" cellspacing="1" border="0" width="100%">
	<tr>
		<td align=center colspan="4"><?= $oForm -> getMessage(); ?></td>
	</tr>
   <tr>
		<td class="table_cell_c" width="10%"><?= $oForm -> getElementLabel('year'); ?>:</td>
		<td class="table_cell_2" width="20%">
            <?= $oForm -> getElementLabel('yearFrom'); ?>&nbsp;
            <?= $oForm -> getElementHtml('yearFrom'); ?>&nbsp;&nbsp;
            <?= $oForm -> getElementLabel('yearUntil'); ?>&nbsp;
            <?= $oForm -> getElementHtml('yearUntil'); ?>
        </td>
        <td class="table_cell_c" width="10%"><?= $oForm -> getElementLabel('month'); ?>:</td>
		<td class="table_cell_2" width="24%">
            <?= $oForm -> getElementLabel('monthFrom'); ?>
            <?= $oForm -> getElementHtml('monthFrom'); ?>&nbsp;
            <?= $oForm -> getElementLabel('monthUntil'); ?>
            <?= $oForm -> getElementHtml('monthUntil'); ?>
        </td>
        <td class="table_cell_c" width="10%"><?= $oForm -> getElementLabel('trLocation'); ?>:</td>
        <td class="table_cell_2" width="23%"><?= $oForm -> getElementHtml('trLocation'); ?></td>
   </tr>
   <tr>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('rcdRegion'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('rcdRegion'); ?></td>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('dvArea'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('dvArea'); ?></td>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('status'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('status'); ?></td>
   	</tr>
    	<tr>
        <td colspan="2">&nbsp;</td>
		<td colspan="2" align="right">
        <span id="loading" style="position:absolute; width:32; height:32; margin-left:30px; display: none; ">
        <img src="./img/loading.gif" widht="32" height="32" border="0" />
        </span><?=$oForm->getElementHtml('search');?></td>
        <td colspan="2"><a href="#" onClick="reloadFrame(1,'<?= $criteriaLink; ?>');reloadFrame(23,'');"><?=text::get('RETURN_TO_SEARCH_CRRITERIA');?></a></td>
    </tr>
</table>

<?= $oForm -> getFormBottom(); ?>
</body>
