<?
// Created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);
$isEditor = ( dbProc::isUserInRole($userId, ROLE_EDITOR) ||  dbProc::isUserInRole($userId, ROLE_EDITOR_VIEWER));

// act ID
$actId  = reqVar::get('actId');
// catalog
$catalog  = reqVar::get('catalog');
// calculation group code
$calcGroup  = reqVar::get('calcGroup');
// favorīta identifikators
$favoritId  = reqVar::get('favoritId');
// region
$region  = reqVar::get('region');
// all personal
$all  = reqVar::get('all');
// search form
$isSearch  = reqVar::get('isSearch');
// search act by number
$searchNum  = reqVar::get('searchNum');
// EPLA act
$isEpla  = reqVar::get('isEpla');
$tame  = reqVar::get('tame');
$estimate  = reqVar::get('estimate');

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
  
    $oLink=new urlQuery();
    $oLink->addPrm(FORM_ID, 'f.akt.s.8');
    $oFormPop = new Form('frmMainTab','post',$oLink->getQuery());
    unset($oLink);

    if($actId !== false && $catalog !== false &&
       ( ($catalog == KL_CALCULALATION && ($calcGroup !== false || $favoritId !== false || $estimate == 1)) ||
         ($catalog == KL_MATERIALS && ($favoritId !== false || $estimate == 1)) ||
         ($catalog == KL_TRANSPORT && ($favoritId !== false  || $all !== false || $estimate == 1)) ||
         ($catalog == KL_CUSTOMERS && ($favoritId !== false  || $all !== false )) 
              ))
    {
      // get info about act
      $actInfo = dbProc::getActInfo($actId);
      //print_r($actInfo);
      if(count($actInfo)>0)
      {
      	$act = $actInfo[0];
        $status = $act['RAKT_STATUS'];
        $isAuto = $act['RAKT_IS_AUTO'];
      }

      $isReadonly   = true;
      // Ir pieejams mainīšanai, ja  AKTS:LIETOTĀJS = ’Tekošais lietotājs’ un lietotāja loma ir ‘Ievadītais’ un AKTS:STATUS = ‘Izveide’ vai ‘Atgriezts’ vai ‘Tāme’.
      // un aktām nav uzlikta atzīme par automatīsku materiāla ielādi
      // vai lietotājs ir administrātops
      if(
        (
          $act['RAKT_RLTT_ID'] == $userId && $isEditor && 
          ($status == STAT_INSERT || $status == STAT_RETURN || $status == STAT_ESTIMATE)
        ) || $isAdmin
      )
      {
        $isReadonly   = false;
      }

       // get act works total time
      $totalTime = dbProc::getActTotalWorkTime($actId);
      $days = array();
      if(is_array($totalTime)){
        foreach ($totalTime as $day => $val){
          $d = explode('-', $day );
          if(is_array($d) && count($d) > 1){
            $days[$day] = $d[0].' / '.$d[1].'. '.text::get('BRIGADE');
          }
        }
      }
      
      // brigade
      $brigade = array();
      for ($i = 1; $i<16; $i++){
        $brigade[$i] = $i;
      }
      $brigade_from_work = array();
      $work_brigade = dbProc::getActWorkBrigades($actId);
      foreach ($work_brigade as $key => $val){
        $brigade_from_work[$val['DRBI_BRIGADE']] = $val['DRBI_BRIGADE'];
      }

      $aScope=array();
      $aScope['-1']='';
      $serchText = '';

      // get info about system
      $systemInfo = dbProc::getSystemInfo();
      if(count($systemInfo)>0)
      {
        $info = $systemInfo[0];
      }

      if ($oFormPop -> isFormSubmitted() && $oFormPop->getValue('action')==OP_SEARCH )
      {
	        $serchText = $oFormPop->getValue('searchCode');
          unset($aScope);
	        $aScope=array();
      	  $aScope['-1']='';

      }
      switch ($catalog)
      {
        case  KL_CALCULALATION:
          if($estimate == 1) {
            // set list title
            $title = strtoupper(text::get(TAB_ESTIMATE)). ' ' .text::get(TAB_WORKS_T);
            // get calculation from estimate
            $res = dbProc::getEstimatedCalculation($actId);
            if (is_array($res))
            {
                foreach ($res as $row)
                 {
                   $aScope[$row['KKAL_ID']] = $row['KKAL_SHIFRS'] .' : '. $row['KKAL_NOSAUKUMS'] .' ('.$row['KKAL_NORMATIVS'].')';
                }
            }
            unset($res);
            $oFormPop -> addElement('hidden', 'estimate', '', $estimate);
          }
          elseif($calcGroup !== false)
          {
              // get info about calculation group
              $calcGrInfo = dbProc::getCalculationGroupInfo($calcGroup);
              if(count($calcGrInfo)>0)
              {
                $group = $calcGrInfo[0];
                  $groupTitle = $group['KKLG_NOSAUKUMS'];
              }
              // set list title
              $title = text::toUpper($groupTitle);
              // get calculation by calculation group code
              $res = dbProc::getFreeCalculationList($calcGroup, $actId, $serchText, $isEpla);
              if (is_array($res))
              {
                  foreach ($res as $row)
                {
                      $aScope[$row['KKAL_ID']] = $row['KKAL_SHIFRS'] .' : '. $row['KKAL_NOSAUKUMS'] .' ('.$row['KKAL_NORMATIVS'].')';
                  }
              }
              unset($res);
              $oFormPop -> addElement('hidden', 'calcGroup', '', $calcGroup);
            }
            
            elseif($favoritId !== false)
            {
              // get info about favorit
              $favoritInfo = dbProc::getFavoritDefinition($favoritId);
              if(count($favoritInfo)>0)
              {
              	  $favorit = $favoritInfo[0];
                  $favoritTitle = $favorit['LTFV_NOSAUKUMS'];
              }
              // set list title
              $title = text::toUpper($favoritTitle);
              // get calculation by favorit ID
              $res = dbProc::getFreeCalculationFavoritList($favoritId, $actId);
              if (is_array($res))
              {
                  foreach ($res as $row)
               	  {
                	   $aScope[$row['KKAL_ID']] = $row['KKAL_SHIFRS'] .' : '. $row['KKAL_NOSAUKUMS'] .' ('.$row['KKAL_NORMATIVS'].')';
                  }
              }
              unset($res);
              $oFormPop -> addElement('hidden', 'favoritId', '', $favoritId);
            }

            break;
        case  KL_MATERIALS:
          
          if($estimate == 1) {
            
            // set list title
            $title = strtoupper(text::get(TAB_ESTIMATE)). ' ' .text::get(TAB_MATERIAL_T);
            // get material from estimate
            $res = dbProc::getActEstimateMaterialList($actId);
            if (is_array($res))
            {
                foreach ($res as $row)
                 {
                  $aScope[$row['KMAT_ID']] = $row['MATR_KODS'] .' : '. $row['MATR_NOSAUKUMS'].' : '. $row['MATR_DAUDZUMS_KOR'].' '.$row['MATR_MERVIENIBA'] .' : '.$row['MATR_CENA'].' EUR';
                }
            }
            unset($res);
            $oFormPop -> addElement('hidden', 'estimate', '', $estimate);
          }
          else {
            // get info about calculation group
            $favoritInfo = dbProc::getFavoritDefinition($favoritId);
            if(count($favoritInfo)>0)
            {
            	$favorit = $favoritInfo[0];
                $favoritTitle = $favorit['LTFV_NOSAUKUMS'];
            }
            // set list title
            $title = text::toUpper($favoritTitle);
            // get material by favorit ID
            $res = dbProc::getFreeMatFavoritList($favoritId, $actId);
            if (is_array($res))
            {
                foreach ($res as $row)
             	{
              	    $aScope[$row['KMAT_ID']] = $row['KMAT_KODS'] .' : '. $row['KMAT_NOSAUKUMS'].' : '. $row['KMAT_DAUDZUMS'].' '.$row['KMAT_MERVIENIBA'] .' : '.$row['KMAT_CENA'].' EUR';
                }
            }
            unset($res);
            $oFormPop -> addElement('hidden', 'favoritId', '', $favoritId);
          }
          break;
        
        case  KL_TRANSPORT:
            if($favoritId !== false)
            {
              // get info about favorit
              $favoritInfo = dbProc::getFavoritDefinition($favoritId);
              if(count($favoritInfo)>0)
              {
              	$favorit = $favoritInfo[0];
                  $favoritTitle = $favorit['LTFV_NOSAUKUMS'];
              }
              // set list title
              $title = text::toUpper($favoritTitle);
              // get transport by favorit ID
              $res = dbProc::getFreeTransFavoritList($favoritId, $actId, $tame);
              if (is_array($res))
              {
                  foreach ($res as $row)
               	  {
                	    $aScope[$row['KMEH_ID']] = $row['KMEH_VALSTS_NUMURS'] .' : '. $row['KMEH_GRUPAS_NOSAUKUMS'].' : '. $row['KMEH_APAKSGRUPAS_NOSAUKUMS'];
                  }
              }
              unset($res);
              $oFormPop -> addElement('hidden', 'favoritId', '', $favoritId);
            }
            elseif($region !== false)
            {
              // set list title
              $title = text::toUpper(text::get('REGION').' '.$region);
              // get transport by region
              $res = dbProc::getFreeTransRegionList($region, $actId, $tame);
              if (is_array($res))
              {
                  foreach ($res as $row)
               	  {
                	    $aScope[$row['KMEH_ID']] = $row['KMEH_VALSTS_NUMURS'] .' : '. $row['KMEH_GRUPAS_NOSAUKUMS'].' : '. $row['KMEH_APAKSGRUPAS_NOSAUKUMS'];
                  }
              }
              unset($res);
              $oFormPop -> addElement('hidden', 'region', '', $favoritId);
            }
            elseif($all !== false)
            {
              // set list title
              $title = text::toUpper(text::get('ALL_VALUES'));
              // get all transport
              $res = dbProc::getFreeTransList($actId, $tame);
              if (is_array($res))
              {
                  foreach ($res as $row)
               	  {
                	    $aScope[$row['KMEH_ID']] = $row['KMEH_VALSTS_NUMURS'] .' : '. $row['KMEH_GRUPAS_NOSAUKUMS'].' : '. $row['KMEH_APAKSGRUPAS_NOSAUKUMS'];
                  }
              }
              unset($res);
              $oFormPop -> addElement('hidden', 'all', '', $all);
            }

            break;
            case  KL_CUSTOMERS:
            if($favoritId !== false)
            {
              // get info about favorit
              $favoritInfo = dbProc::getFavoritDefinition($favoritId);
              if(count($favoritInfo)>0)
              {
              	$favorit = $favoritInfo[0];
                  $favoritTitle = $favorit['LTFV_NOSAUKUMS'];
              }
              // set list title
              $title = text::toUpper($favoritTitle);
              // get personal by favorit ID
              $res = dbProc::getFreePersFavoritList($favoritId, $actId);
              if (is_array($res))
              {
                  foreach ($res as $row)
               	  {
                	    $aScope[$row['KPRF_ID']] = $row['KPRF_KODS'] .' : '. $row['KPRF_VARDS'].' '. $row['KPRF_UZVARDS'].' : '.$row['KPRF_DARBA_VIETA'];
                  }
              }
              unset($res);
              $oFormPop -> addElement('hidden', 'favoritId', '', $favoritId);
            }
            elseif($region !== false)
            {
              // set list title
              $title = text::toUpper(text::get('REGION').' '.$region);
              // get personal by region
              $res = dbProc::getFreePersRegionList($region, $actId);
              if (is_array($res))
              {
                  foreach ($res as $row)
               	  {
                	    $aScope[$row['KPRF_ID']] = $row['KPRF_KODS'] .' : '. $row['KPRF_VARDS'].' '. $row['KPRF_UZVARDS'].' : '.$row['KPRF_DARBA_VIETA'];
                  }
              }
              unset($res);
              $oFormPop -> addElement('hidden', 'region', '', $favoritId);
            }
            elseif($all !== false)
            {
              // set list title
              $title = text::toUpper(text::get('ALL_VALUES'));
              // get all personal
              $res = dbProc::getFreePersList($actId);
              if (is_array($res))
              {
                  foreach ($res as $row)
               	  {
                	    $aScope[$row['KPRF_ID']] = $row['KPRF_KODS'] .' : '. $row['KPRF_VARDS'].' '. $row['KPRF_UZVARDS'].' : '.$row['KPRF_DARBA_VIETA'];
                  }
              }
              unset($res);
              $oFormPop -> addElement('hidden', 'all', '', $all);
            }
            break;
      }

      // form elements
      $oFormPop -> addElement('hidden', 'action', '');
      $oFormPop -> addElement('hidden', 'actId', '', $actId);
      $oFormPop -> addElement('hidden', 'catalog', '', $catalog);
      $oFormPop -> addElement('hidden', 'isSearch', '', $isSearch);
      $oFormPop -> addElement('hidden', 'searchNum', '', $searchNum);
      $oFormPop -> addElement('hidden', 'isEpla', '', $isEpla);
      $oFormPop -> addElement('hidden', 'isAuto', '', $isAuto);
      $oFormPop -> addElement('hidden', 'tame', '', $tame);
      $oFormPop -> addElement('hidden', 'calc_period_date', '', $info['SNFO_CALC_START_DATE']);
      
      $oFormPop -> addElement('multiple_select', 'scope', '', '', (($isReadonly)?' disabled ':'').' size="19" ', '', '', $aScope);
       if($catalog == KL_CALCULALATION && $tame == 0)
       {
         //datums
        $oFormPop -> addElement('date', 'plan_date',  text::get('FINISHING_DATE'), dtime::now(), (($isReadonly)?' disabled ':''));
        $oFormPop -> addRule('plan_date', text::get('ERROR_REQUIRED_FIELD'), 'required');
        $oFormPop -> addRule('plan_date', text::get('ERROR_INCORRECT_DATE'),'validatedate');
        // check Amaunt field
        $v1=&$oFormPop->createValidation('calc_period_validation', array('plan_date','calc_period_date' ), 'ifonefilled');
        // ja Cilv�kstundas normat�vs ir defin�ts un > 100000
        $r1=&$oFormPop->createRule(array('plan_date', 'calc_period_date'), sprintf(text::get('ERROR_CALC_PERIOD_DATE'), $info['SNFO_CALC_START_DATE']), 'comparedate','>');
        $oFormPop->addRule(array($r1), 'groupRuleDate', 'group', $v1);

        // brigade
        $oFormPop -> addElement('select', 'brigade',  text::get('BRIGADE'), '', (($isReadonly || $tame)?' disabled ':'').' ', '','', $brigade);
        $oFormPop -> addRule('brigade', text::get('ERROR_REQUIRED_FIELD'), 'required');

      /*
       	$oFormPop->addElement('text', 'searchCode', text::get('TITLE'), $serchText, (($isReadonly)?' disabled ':'').'maxlength="50"');
	      // form buttons
     	  if (!$isReadonly)
      	{
        		$oFormPop -> addElement('submitImg', 'search', text::get('SEARCH'),'img/btn_meklet.gif', 'width="70" height="20" onclick="setValue(\'action\',\''.OP_SEARCH.'\');"');
        }*/
      }
      else if($tame == 0){
        $oFormPop -> addElement('select', 'plan_date',  text::get('FINISHING_DATE').'/'.text::get('BRIGADE'), '', (($isReadonly)?' disabled ':'').' ', '','', $days);
        $oFormPop -> addRule('plan_date', text::get('ERROR_REQUIRED_FIELD'), 'required');

       
      }

      

      // form buttons
      if (!$isReadonly)
      {
        $oFormPop -> addElement('submitImg', 'save', text::get('SAVE'), 'img/btn_saglabat.gif', 'width="70" height="20" onclick="if (!isEmptyFormField(\'frmMainTab\',\'actId\')) {setValue(\'action\',\''.OP_UPDATE.'\');} else {return false;}"');
      }
      $oFormPop -> addElement('button', 'close', '', text::get('CLOSE'), 'class="btn70" onclick="javascript:window.close();return false;"; ');

      if ($oFormPop -> isFormSubmitted() AND $oFormPop->getValue('action')==OP_UPDATE)
      {
            $checkRequiredFields = false;
            $r = true;
            $aListScope = $oFormPop->getValue('scope');
            if (is_array($aListScope))
			      {
              if($catalog ==  KL_CUSTOMERS)
               {
                  // get act works total time
                  $totalTime = dbProc::getActTotalWorkTime($oFormPop->getValue('actId'));
                  // get act virsstundu koef
                  $actInfo = dbProc::getActInfo($oFormPop->getValue('actId'));
                  if(count($actInfo)>0)
                  {
                      $koef = $actInfo[0]['RAKT_VIRSSTUNDU_KOEF'];
                  }
               }
                foreach ($aListScope as $cValue)
                {
                  if ($cValue>0)
                  {
                                switch ($catalog)
                                {
                                  case  KL_CALCULALATION:
                                    $r = dbProc::saveActWorkRecord($oFormPop->getValue('actId'), $cValue, $tame, $oFormPop->getValue('plan_date'), $oFormPop->getValue('brigade'));
                                    break;
                                  case  KL_MATERIALS:
                                    $r = dbProc::saveActMaterialRecord($oFormPop->getValue('actId'), $cValue, false, $tame);
                                    break;
                                  case  KL_TRANSPORT:
                                    $r = dbProc::saveActTransportRecord($oFormPop->getValue('actId'), $cValue, $tame,);
                                    break;
                                  case  KL_CUSTOMERS:
                                    $d = explode('-', $oFormPop->getValue('plan_date') );
                                    $r = dbProc::saveActPersonalRecord($oFormPop->getValue('actId'), $cValue, $d[0], $d[1]);
                                    break;
                                }
                                if(!$r)
                                {
                                  $oFormPop->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
                                  break 1;
                                }
                                $checkRequiredFields = true;
                  }
                }
                if(!$checkRequiredFields)
                {
                    $oFormPop->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
                }
                else
                {
                   if($r)
                    {
                      $oRedirectLink=new urlQuery();
                      $oRedirectLink->addPrm(FORM_ID, 'f.akt.s.1');
                      $oRedirectLink->addPrm('actId', $oFormPop->getValue('actId'));
                      $oRedirectLink->addPrm('isSearch', $oFormPop->getValue('isSearch'));
                      $oRedirectLink->addPrm('searchNum', $oFormPop->getValue('searchNum'));
                      $oRedirectLink->addPrm('isEpla', $oFormPop->getValue('isEpla'));
                      $oRedirectLink->addPrm('tame', $tame);
                      $oRedirectLink->addPrm('tame_mat', $tame);
                      $oRedirectLink->addPrm('tame_trans', $tame);

                      switch ($catalog)
                      {
                          case  KL_CALCULALATION:                            
			   if($tame) {
                            	$oRedirectLink->addPrm('tametab', TAB_WORKS_T);
			    } else  {
				$oRedirectLink->addPrm('tab', TAB_WORKS);	
			    }
                            break;
                          case  KL_MATERIALS:
			    if($tame) {
                            	$oRedirectLink->addPrm('tametab', TAB_MATERIAL_T);
			    } else  {
				$oRedirectLink->addPrm('tab', TAB_MATERIAL);	
			    }
                            break;
                          case  KL_TRANSPORT:
                            if($tame) {
                            	$oRedirectLink->addPrm('tametab', TAB_TRANSPORT_T);
			    } else  {
				$oRedirectLink->addPrm('tab', TAB_TRANSPORT);	
			    }
                            break;
                          case  KL_CUSTOMERS:
                            $oRedirectLink->addPrm('tab', TAB_PERONAL);
                            break;
                      }

                      $oRedirectLink->addPrm('isNew', RequestHandler::getMicroTime());

                      // recalculate personal vertibas
                      if($catalog ==  KL_CUSTOMERS)
                      {
                          dbProc::recalculateWorkTime($actId) ;
                      }
                      requestHandler::makeParentReplace($oRedirectLink->getQuery().'#tametab');

                      unset($oRedirectLink);
                   }
                }
			}
            else
            {
              $oFormPop->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
            }

      }

      $oFormPop -> makeHtml();
      include('f.akt.s.8.tpl');
    }
    else
	{
		$oFormPop->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
	}
}
else
{
    RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}

?>