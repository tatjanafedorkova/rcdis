<?
   $sCriteria = reqVar::get('search');
   $searchCr = array();
        $sCr = explode("^", $sCriteria);
        if(isset($sCr[0]) && $sCr[0] != -1  && isset($sCr[1]) && $sCr[1] != -1)
        {
           $searchCr[] = text::get('YEAR').': '.$sCr[0].'-'.$sCr[1];
        }
        if(isset($sCr[2]) && $sCr[2] != -1  && isset($sCr[3]) && $sCr[3] != -1)
        {
           $searchCr[] = text::get('MONTH').': '.dtime::getMonthName($sCr[2]).'-'.dtime::getMonthName($sCr[3]);
        }
        if(isset($sCr[4]) && $sCr[4] != -1)
        {
           $searchCr[] = text::get('RCD_REGION').': '.$sCr[4];
        }
        if(isset($sCr[5]) && $sCr[5] != -1)
        {
           $searchCr[] = text::get('SINGL_DV_AREA').': '.dbProc::getDVAreaName($sCr[5]);
        }
        if(isset($sCr[6]) && $sCr[6] != -1)
        {
           $searchCr[] = text::get('ED_REGION').': '.$sCr[6];
        }
        if(isset($sCr[7]) && $sCr[7] != -1)
        {
           $searchCr[] = text::get('ED_IECIKNIS').': '.dbProc::getEDAreaName($sCr[7]);
        }

        if(isset($sCr[8]) && $sCr[8] != -1)
        {
           $searchCr[] = text::get('SINGLE_SOURCE_OF_FOUNDS').': '.dbProc::getSourceOfFoundsName($sCr[8]);
        }
        if(isset($sCr[9]) && $sCr[9] != -1)
        {
           $searchCr[] = text::get('SINGLE_VOLTAGE').': '.dbProc::getVoltageName($sCr[9]);
        }
        if(isset($sCr[10]) && $sCr[10] != -1)
        {
           $searchCr[] = text::get('ACT_OUNER').': '.dbProc::getUserName($sCr[10]);
        }
        $type = explode("*", $sCr[18]);
        $typeString = '';
        if($type[0] == -1)
        {
           $typeString = text::get('ALL');
        }
        else
        {
          $typeKls = classif::getValueArray(KL_ACT_TYPE);
          foreach($type as $t)
          {
              $typeString .= $typeKls[$t].",";
          }
          $typeString  = substr($typeString, 0, -1);
        }
        $searchCr[] = text::get('SINGLE_ACT_TYPE').': '.$typeString;
        if(isset($sCr[12]) && $sCr[12] != -1)
        {
           $searchCr[] = text::get('MMS_WORK_FINISHED').': '.(($sCr[12] == 1) ? text::get('YES'): text::get('NO'));
        }
        $status = explode("*", $sCr[14]);
        $statusString = '';
        if($status[0] == -1)
        {
           $statusString = text::get('ALL_STATUS');
        }
        else
        {
          foreach($status as $s)
          {
              $statusInfo = dbProc::getKrfkInfoByName($s);
              $statusString .= $statusInfo['KRFK_NOZIME'].",";
          }
          $statusString  = substr($statusString, 0, -1);
        }
        $searchCr[] = text::get('STATUS').': '.$statusString;
         if(isset($sCr[15]) && $sCr[15] != -1)
        {
           $searchCr[] = text::get('SINGLE_OBJECT').': '.dbProc::getObjectName($sCr[15]);
        }
        if(isset($sCr[16]) && $sCr[16] != -1)
        {
           $searchCr[] = text::get('SINGLE_OTHER_PAYMENTS').': '.(($sCr[16] == 1) ? text::get('YES'): text::get('NO'));
        }
        if(isset($sCr[17]) && $sCr[17] != -1)
        {
          // get info about calculation group
            $favoritInfo = dbProc::getFavoritDefinition($sCr[17]);
            if(count($favoritInfo)>0)
            {
            	$favorit = $favoritInfo[0];
                $favoritTitle = $favorit['LTFV_NOSAUKUMS'];
            }
           $searchCr[] = text::get('CALCULATION_GROUP').': '.$favoritTitle;
        }

       if(isset($sCr[19]) && $sCr[19] != -1)
        {
           $searchCr[] = text::get('ED_SECTION').': '.urldecode($sCr[19]);
        }
   $excel = '<head><meta http-equiv=Content-Type content="text/html; charset=utf-8"></head>';

   $excel .= '<table style="font-family:Arial; font-size:9pt;">';

   $excel .= '<tr><td colspan="11" style="font-size:12pt; font-weight:bold;">'.text::get('REPORT_CALCULATION_BY_GROUP').'</td></tr>';
   $excel .= '<tr><td colspan="11">'.implode('; ',$searchCr).'</td></tr>';
   $excel .= '<tr>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('CHIPHER').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('NAME').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('UNIT_OF_MEASURE').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('ACT_NUMBER').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('AMOUNT').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('BASE_TIME').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('OVER_TIME').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('RCD_REGION').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('SINGL_DV_AREA').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('MONTH').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('STATUS').'</td>';
   $excel .= '</tr>';

        $res = dbProc::getCalculationByGroupList($sCriteria, false);

        $currentRow = 4;
        $baseTime = 0;
        $overTime = 0;
        if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
              $excel .= '<tr>';
              $excel .= '<td style="border: 1px solid black;">'.$row['DRBI_KKAL_SHIFRS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['DRBI_KKAL_NOSAUKUMS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['DRBI_MERVIENIBA'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['NUMURS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.number_format($row['DAUDZUMS'],2,'.','').'</td>';
              $excel .= '<td style="border: 1px solid black;">'.number_format($row['PAMATSTUNDAS'],2,'.','').'</td>';
              $excel .= '<td style="border: 1px solid black;">'.number_format($row['VIRSTUNDAS'],2,'.','').'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['KDVI_REGIONS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['DV'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.dtime::getMonthName($row['RAKT_MENESIS']).'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['STATUS_NAME'].'</td>';
              $excel .= '</tr>';

               $currentRow ++;
               $baseTime = $baseTime + $row['PAMATSTUNDAS'];
               $overTime = $overTime + $row['VIRSTUNDAS'];
			}
		}

        $excel .= '<tr>';
        $excel .= '<td colspan="5" style="font-weight:bold; text-align:right; font-style: italic">'.text::get('TOTAL').':'.'</td>';
        $excel .= '<td style="font-weight:bold; font-style: italic">'.number_format($baseTime, 2, '.', '').'</td>';
        $excel .= '<td style="font-weight:bold; font-style: italic">'.number_format($overTime, 2, '.', '').'</td>';
        $excel .= '</tr>';

// redirect output to client browser

  header('Content-Type: application/vnd.ms-excel;');
  header('Content-Disposition: attachment;filename="myfile.xls"');
  header('Cache-Control: max-age=0');

  echo $excel;

?>