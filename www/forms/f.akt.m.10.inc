﻿<?
// Main form with XMLHTTP method load this code:
if (reqVar::get('xmlHttp') == 1)
{
	//if is set edRegion
	if (reqVar::get('edRegion') !== false)
	{
		// unset previouse values of edArea
		?>
		for(i=document.all["edArea"].length; i>=0; i--)
		{
			document.all["edArea"].options[i] = null;
		}
		<?
		$res=dbProc::getEDAreaListForRegion(reqVar::get('edRegion'));
		if (is_array($res) && count($res)>0)
		{
			?>
			document.all["edArea"].options[0] = new Option('<?=text::get('ALL');?>', '-1');
			<?
            $i = 1;
			foreach ($res as $row)
			{
				// feel options array
				?>
				document.all["edArea"].options[<?=$i;?>] = new Option( '<?=$row['KEDI_REGIONS'].' : '.$row['KEDI_KODS'].' : '.$row['KEDI_NOSAUKUMS'];?>', '<?=$row['KEDI_ID'];?>');
                <?
                $i++;
			}
		}
        // unset previouse values of section
		?>
		for(i=document.all["section"].length; i>=0; i--)
		{
			document.all["section"].options[i] = null;
		}
		<?
		$res=dbProc::getEDSectionListForRegion(reqVar::get('edRegion'));
		if (is_array($res) && count($res)>0)
		{
			?>
			document.all["section"].options[0] = new Option('<?=text::get('ALL');?>', '-1');
			<?
            $i = 1;
			foreach ($res as $row)
			{
				// feel options array
				?>
				document.all["section"].options[<?=$i;?>] = new Option( '<?=$row['KEDI_SECTION'];?>', '<?=$row['KEDI_SECTION'];?>');
                <?
                $i++;
			}
		}
	}

	exit;
}

        // get user info
      	$userInfo = dbProc::getUserInfo($userId);
        $isAdmin=userAuthorization::isAdmin();
        $isEdUser = dbProc::isUserInRole($userId, ROLE_ED_USER);
      	if(count($userInfo) >0 )
      	{
      		$user = $userInfo[0];
       	}
         // get ED region list
         $edRegion=array();
         if(isset($user['RLTT_REGIONS']) && isset($user['RLTT_ROLE']) && $user['RLTT_ROLE'] == ROLE_ED_USER )
         {
            $edRegion[$user['RLTT_REGIONS']]=$user['RLTT_REGIONS'];
         }
         else
         {
           $edRegion['-1']=text::get('ALL');
           $res=dbProc::getEDRegionList();
           if (is_array($res))
           {
           	foreach ($res as $row)
           	{
           		$edRegion[$row['nosaukums']]=$row['nosaukums'];
           	}
           }
         }
         unset($res);

         // get ED area list
        $edarea=array();
        if(isset($user['RLTT_REGIONS']) && isset($user['RLTT_ROLE']) && $user['RLTT_ROLE'] == ROLE_ED_USER )
        {
            $res=dbProc::getEDAreaListForRegion($user['RLTT_REGIONS'], false);
			if (is_array($res))
			{
			    $edarea['-1']=text::get('ALL');
				foreach ($res as $row)
				{
					$edarea[$row['KEDI_ID']]=$row['KEDI_REGIONS'].' : '.$row['KEDI_KODS'].' : '.$row['KEDI_NOSAUKUMS'];
				}
			}
         }
         else
         {
            $res=dbProc::getEDAreaList(false,false,false,false,false,'ASC','`KEDI_REGIONS`, `KEDI_KODS`');
            if (is_array($res))
            {
                $edarea['-1']=text::get('ALL');
             	foreach ($res as $row)
             	{
              	    $edarea[$row['KEDI_ID']]=$row['KEDI_REGIONS'].' : '.$row['KEDI_KODS'].' : '.$row['KEDI_NOSAUKUMS'];
                }
            }
         }
        unset($res);

        // get ED section list
        $section=array();
        if(isset($user['RLTT_REGIONS']) && isset($user['RLTT_ROLE']) && $user['RLTT_ROLE'] == ROLE_ED_USER )
        {
            $res=dbProc::getEDSectionListForRegion($user['RLTT_REGIONS']);
			if (is_array($res))
			{
			    $section['-1']=text::get('ALL');
				foreach ($res as $row)
				{
					$section[$row['KEDI_SECTION']]=$row['KEDI_SECTION'];
				}
			}
         }
         else
         {
            $res=dbProc::getEDSectionListForRegion(-1);
            if (is_array($res))
            {
                $section['-1']=text::get('ALL');
             	foreach ($res as $row)
             	{
              	    $section[$row['KEDI_SECTION']]=$row['KEDI_SECTION'];
                }
            }
         }
        unset($res);

        $searcCriteria = array();

       // if operation
    	if ($oForm -> isFormSubmitted())
    	{
           $searcCriteria['yearFrom'] = $oForm->getValue('yearFrom');
           $searcCriteria['yearUntil'] = $oForm->getValue('yearUntil');
           $searcCriteria['quarterFrom'] = $oForm->getValue('quarterFrom');
           $searcCriteria['quarterUntil'] = $oForm->getValue('quarterUntil');
           $searcCriteria['edRegion'] = $oForm->getValue('edRegion');
           $searcCriteria['edArea'] = $oForm->getValue('edArea');
           $searcCriteria['section'] = urlencode($oForm->getValue('section'));
           dbProc::replaceSearchResult($searchName,$userId,
                                $searcCriteria['yearFrom'],
                                $searcCriteria['yearUntil'],
                                $searcCriteria['quarterFrom'],
                                $searcCriteria['quarterUntil'],
                                '','',
                                $searcCriteria['edRegion'],
                                $searcCriteria['edArea'],
                                '','', '', '', '', '', '', '',
                                '', '', '', '', '', '', '', '',
                                $searcCriteria['section']
                                );

           $cr = '';
           foreach($searcCriteria as $s)
           {
             $cr .= $s.'^';
           }
           $cr = substr($cr, 0, -1);
           $oListLink->addPrm('search', $cr);
           $actListLink=$oListLink ->getQuery();
           $oForm->addElement('static','jsRefresh2','','<script>reloadFrame(2, "'.$actListLink.'");</script>');
           unset($oListLink);
    	}
        else
        {
           if (reqVar::get('isReturn') == 1)
            {
                $searchCritery = dbProc::getSearchCritery($userId);
           	    if(count($searchCritery) >0 )
      	        {
      		        $critery = $searchCritery[0];
       	        }
            }
           $searcCriteria['yearFrom'] = isset($critery['yearFrom'])? $critery['yearFrom'] :dtime::getCurrentYear();
           $searcCriteria['yearUntil'] = isset($critery['yearUntil'])? $critery['yearUntil'] :dtime::getCurrentYear();
           $searcCriteria['quarterFrom'] = isset($critery['quarterFrom'])? $critery['quarterFrom'] :1;
           $searcCriteria['quarterUntil'] = isset($critery['quarterUntil'])? $critery['quarterUntil'] :4;
           $searcCriteria['edRegion'] = isset($critery['edRegion'])? $critery['edRegion'] :'-1';
           $searcCriteria['edArea'] = isset($critery['edArea'])? $critery['edArea'] :'-1';
           $searcCriteria['section']  = isset($critery['section'])? urldecode($critery['section']) : '';
        }

        $oForm -> addElement('label', 'year',  text::get('YEAR'), '');
        $oForm -> addElement('select', 'yearFrom',  text::get('FROM'), $searcCriteria['yearFrom'], 'style="width:50;"', '', '', dtime::getYearArray());
        $oForm -> addElement('select', 'yearUntil',  text::get('UNTIL'), $searcCriteria['yearUntil'], 'style="width:50;"', '', '', dtime::getYearArray());

        $oForm -> addElement('label', 'quarter',  text::get('QUARTER'), '');
        $oForm -> addElement('select', 'quarterFrom',  text::get('FROM'), $searcCriteria['quarterFrom'], 'style="width:70;"', '', '', dtime::getQuarterArray());
        $oForm -> addElement('select', 'quarterUntil',  text::get('UNTIL'), $searcCriteria['quarterUntil'], 'style="width:70;"', '', '', dtime::getQuarterArray());
        // xmlHttp link
		$oLink=new urlQuery();
		$oLink->addPrm(DONT_USE_GLB_TPL, 1);
		$oLink->addPrm(FORM_ID, 'f.akt.m.1');

		$oForm -> addElement('select', 'edRegion',  text::get('ED_REGION'), $searcCriteria['edRegion'], 'onChange="eval(xmlHttpGetValue(\''.$oLink->getQuery().'&xmlHttp=1&edRegion=\'+document.all[\'edRegion\'].options[document.all[\'edRegion\'].selectedIndex].value));"', '', '', $edRegion);
        unset($oLink);

        $oForm -> addElement('select', 'edArea',  text::get('ED_IECIKNIS'), $searcCriteria['edArea'], '', '', '', $edarea);

        $oForm -> addElement('select', 'section',  text::get('ED_SECTION'), $searcCriteria['section'], '', '', '', $section);

               	// form buttons
       	$oForm -> addElement('submitImg', 'search', text::get('SEARCH'), 'img/btn_meklet.gif', 'width="70" height="20" onClick="setPosition1(\'loading\'); return true;"');

        $oForm -> makeHtml();
		include('f.akt.m.10.tpl');
?>