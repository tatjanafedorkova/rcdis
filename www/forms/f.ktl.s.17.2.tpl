﻿
<body class="frame_2">
<?=$oForm->getElementHtml('jsRefresh2');?>
<h1><?=text::toUpper(text::toUpper(text::get('STOCK_INFO')));?></h1>
<?= $oForm -> getFormHeader(); ?>
<table cellpadding="5" cellspacing="1" border="0" width="100%">
	<tr>
		<td align=center colspan="6"><?= $oForm -> getMessage(); ?></td>
	</tr>
	<tr>
		<td colspan="6" class="table_separator">&nbsp;</td>
	</tr>
	<tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('section'); ?>:<font color="red">*</font></td>
		<td class="table_cell_2" width="16%"><?= $oForm -> getElementHtml('section'); ?></td>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('cost_center'); ?>:<font color="red">*</font></td>
		<td class="table_cell_2" width="16%"><?= $oForm -> getElementHtml('cost_center'); ?></td>
        <td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('stock_code'); ?>:<font color="red">*</font></td>
		<td class="table_cell_2" width="16%"><?= $oForm -> getElementHtml('stock_code'); ?></td>
	</tr>
	<tr>
		<td class="table_cell_c"><?= $oForm -> getElementLabel('place_code'); ?>:<font color="red">*</font></td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('place_code'); ?></td>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('isActive'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('isActive'); ?></td>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('isAuto'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('isAuto'); ?></td>
	 </tr>
	 <tr>
		<td class="table_cell_c">&nbsp;</td>
		<td class="table_cell_2">&nbsp;</td>
        <td class="table_cell_c">&nbsp;</td>
		<td class="table_cell_2">&nbsp;</td>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('auto_stock_code'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('auto_stock_code'); ?></td>
 	</tr>
</table>

<table cellpadding="5" cellspacing="0" border="0" align="center">
	<tr>
		<td><?=$oForm->getElementHtml('add');?></td>
		<td><?=$oForm->getElementHtml('save');?></td>
		<td><?=$oForm->getElementHtml('clear');?></td>

	</tr>
</table>
<?= $oForm -> getFormBottom(); ?>
<?=$oForm->getElementHtml('jsButtonsControl');?>
</body>
