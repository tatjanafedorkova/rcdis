﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isEdUser = (dbProc::isUserInRole($userId, ROLE_ED_USER) || dbProc::isUserInRole($userId, ROLE_AUDIT_USER));
if (userAuthorization::isAdmin() || $isEdUser )
{
	$editMode = reqVar::get('editMode');
	// bottom frame
	// if insert/update

     if($editMode)
	{
		$efOrderId = reqVar::get('efOrderId');

		$oLink=new urlQuery();
		$oLink->addPrm(FORM_ID, 'f.ktl.s.16');
		$oLink->addPrm('editMode', 1 );
		// form inicialization
		$oForm = new Form('frmMain','post',$oLink->getQuery());
		unset($oLink);

		// if operation is success, show success message and redirect top frame
		if (reqVar::get('successMessage'))
		{
			$oForm->addSuccess(reqVar::get('successMessage'));
			$oForm->addElement('static','jsRefresh2','','<script>refreshFrame(1);</script>');
		}

		// inicial state: efOrderId=0;
		// if efOrderId>0 ==> voltage selected from the list
		if($efOrderId != false)
		{
			// get info about voltage
			$efOrderInfo = dbProc::getEfOrderList($efOrderId);
			//print_r($voltageInfo);
			if(count($efOrderInfo)>0)
			{
				$order = $efOrderInfo[0];
			}

			// get user info
			$userInfo = dbProc::getUserInfo($order['KEFP_CREATOR']);
			if(count($userInfo) >0 )
			{
				$creator = $userInfo[0]['RLTT_VARDS']. ' ' .$userInfo[0]['RLTT_UZVARDS'];
			}
			$userInfo1 = dbProc::getUserInfo($order['KEFP_EDITOR']);
			if(count($userInfo1) >0 )
			{
				$editor = $userInfo1[0]['RLTT_VARDS']. ' ' .$userInfo1[0]['RLTT_UZVARDS'];
			}
        }

        //print_r($efOrderInfo);

		// form elements
		$oForm -> addElement('hidden', 'action', '');
		$oForm -> addElement('hidden', 'efOrderId', null, isset($order['KEFP_ID'])? $order['KEFP_ID']:'');

        $oForm -> addElement('select', 'month',  text::get('MONTH'), isset($order['KEFP_MENESIS'])?$order['KEFP_MENESIS']:'', ''.(( $isEdUser)?' disabled' : ''), '', '', dtime::getMonthNumArray());
        $oForm -> addRule('month', text::get('ERROR_REQUIRED_FIELD'), 'required');

        $oForm -> addElement('select', 'year',  text::get('YEAR'), isset($order['KEFP_GADS'])?$order['KEFP_GADS']:'', ''.(( $isEdUser)?' disabled' : ''), '', '', dtime::getYearArray());
        $oForm -> addRule('year', text::get('ERROR_REQUIRED_FIELD'), 'required');


        $oForm -> addElement('text', 'department',  text::get('EF_DEPARTMENT'), isset($order['KEFP_EF_DEPARTMENT'])?$order['KEFP_EF_DEPARTMENT']:'', 'maxlength="100"' . (( $isEdUser)?' disabled' : ''));
		$oForm -> addRule('department', text::get('ERROR_REQUIRED_FIELD'), 'required');

        $oForm -> addElement('text', 'section',  text::get('EF_SECTION'), isset($order['KEFP_EF_SECTION'])?$order['KEFP_EF_SECTION']:'', 'maxlength="100"' . (( $isEdUser)?' disabled' : ''));
		$oForm -> addRule('section', text::get('ERROR_REQUIRED_FIELD'), 'required');

        $oForm -> addElement('text', 'cvh',  text::get('CVH'), isset($order['KEFP_CVH'])?$order['KEFP_CVH']:'', 'maxlength="6"' . (( $isEdUser)?' disabled' : ''));
		$oForm -> addRule('cvh', text::get('ERROR_REQUIRED_FIELD'), 'required');
        $oForm -> addRule('cvh', text::get('ERROR_NUMERIC_VALUE'), 'numeric');
 
		// importa dati
		$oForm -> addElement('text', 'created',  text::get('CREATED'), isset($order['KEFP_CREATED'])?$order['KEFP_CREATED']:dtime::now(), 'tabindex=21 maxlength="20" disabled');
		$oForm -> addElement('text', 'creator',  text::get('CREATOR'), isset($creator)?$creator:'', 'tabindex=21 maxlength="20" disabled');
		$oForm -> addElement('text', 'edited',  text::get('EDITED'), isset($order['KEFP_EDITED'])?$order['KEFP_EDITED']:dtime::now(), 'tabindex=21 maxlength="20" disabled');
		$oForm -> addElement('text', 'editor',  text::get('EDITOR'), isset($order)?$editor:'', 'tabindex=21 maxlength="20" disabled');


		// form buttons
        if(!$isEdUser)
        {
    		$oForm -> addElement('submitImg', 'add', text::get('ADD'), 'img/btn_pievienot.gif', 'width="70" height="20" onclick="setValue(\'action\',\''.OP_INSERT.'\');"');
    		$oForm -> addElement('submitImg', 'save', text::get('SAVE'), 'img/btn_saglabat.gif', 'width="70" height="20" onclick="if (!isEmptyField(\'efOrderId\')) {setValue(\'action\',\''.OP_UPDATE.'\');} else {return false;}"');
    		$oForm -> addElement('clearImg', 'clear', text::get('CLEAR'), 'img/btn_attirit.gif', 'width="70" height="20"','','',array('onClick'=>'refreshButton(\'efOrderId\');parent.frame_1.unActiveRow();'));

         	$oForm -> addElement('static', 'jsButtonsControl', '', '
    			<script>
    				function refreshButton(name)
    				{
    					if (!isEmptyField(name))
    					{
    						document.all["save"].src="img/btn_saglabat.gif";

    					}
    					else
    					{
    						document.all["save"].src="img/btn_saglabat_disabled.gif";

    					}
    				}
    				refreshButton("efOrderId");
    			</script>
    		');
        }
		// if operation
		if ($oForm -> isFormSubmitted())
		{
			$check = true;
			$r = false;
			$checkRequiredFields = false;
			if ($oForm -> getValue('month') && $oForm -> getValue('year') && $oForm -> getValue('department') && $oForm -> getValue('section') && $oForm -> getValue('cvh'))
			{
				$checkRequiredFields = true;
			}
			if ($checkRequiredFields)
			{
				// save
				if ($oForm->getValue('action')==OP_INSERT || ($oForm->getValue('action')==OP_UPDATE  && is_numeric($oForm->getValue('efOrderId'))))
				{

					// if all is ok, do operation
					if($check)
					{
						$r=dbProc::saveEfOrder(($oForm->getValue('action')==OP_UPDATE?$oForm->getValue('efOrderId'):false),
                        $oForm->getValue('year'),
                        $oForm->getValue('month'),
						$oForm->getValue('department'),
                        $oForm->getValue('section'),
						$oForm->getValue('cvh'),
						$userId
						);

						if (!$r)
						{
							$oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
						}
					}
				}


				// if operation was compleated succefully, show success mesage and redirect current frame
				if ($r)
				{
					$oLink=new urlQuery();
					$oLink->addPrm(FORM_ID, 'f.ktl.s.16');
					$oLink->addPrm('editMode', '1');
					switch($oForm->getValue('action'))
					{
						case OP_INSERT:
							$oLink->addPrm('successMessage', text::get('RECORD_WAS_ADDED'));
							break;
						case OP_UPDATE:
							$oLink->addPrm('successMessage', text::get('RECORD_WAS_SAVED'));
							break;

					}
					RequestHandler::makeRedirect($oLink->getQuery());
				}
			}
			else
			{
				$oForm->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
			}
		}

       	$oForm -> makeHtml();
		include('f.ktl.s.16.2.tpl');
	}
	// top frale
	else
	{
        // Check if this form was requested via xmlHttpRequest and search
    	if (isset($_GET['xml']) && isset($_GET['search_q']) && isset($_GET['search_c']))
    	{
    		ob_clean();
    		$q = $_GET['search_q'];
            if (isset($q) || $q == 0)
    		{
    			echo 'q = "'.trim($q).'";';
    		}
            else
            {
    			echo 'q = "";';
    		}
            $c = $_GET['search_c'];
            if ($c)
    		{
    			echo 'c = "'.$c.'";';
    		}
            else
            {
    			echo 'c = "";';
    		}
		    exit;
    	}
        if (isset($_GET['xml']) && isset($_GET['sort_k']) && isset($_GET['sort_o']))
    	{
    		ob_clean();
    		$o = $_GET['sort_o'];
            if (isset($o))
    		{
    			echo 'o = "'.$o.'";';
    		}
            else
            {
    			echo 'o = "";';
    		}
            $k = $_GET['sort_k'];
            if ($k)
    		{
    			echo 'k = "'.$k.'";';
    		}
            else
            {
    			echo 'k = "";';
    		}
		    exit;
    	}
        // set list title
        $title = text::toUpper(text::get('EF_ORDER_TITLE'));
        // get search column list
        $columns=dbProc::getKlklName(KL_EF_ORDER);
        $searchText = ((reqVar::get('search') && reqVar::get('search') != '') || reqVar::get('search') == 0) ? reqVar::get('search') : false;
        $searchColumn = (reqVar::get('column') && reqVar::get('column') != '') ? reqVar::get('column') : false;
        $sortOrder = (reqVar::get('order') && reqVar::get('order') != '') ? reqVar::get('order') : false;
        $orderColumn = (reqVar::get('kol') && reqVar::get('kol') != '') ? reqVar::get('kol') : false;


        // default sort column
        $defaultColumn = dbProc::getKlklDefaultColumn(KL_EF_ORDER);

        // URL to search Zinojums by number.
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.ktl.s.16');
    	$oLink->addPrm('isNew', '1');

        // add search/order params to listing
        $oLink->addPrm('search', reqVar::get('search'));
        $oLink->addPrm('column', reqVar::get('column'));
        $oLink->addPrm('order', reqVar::get('order'));
        $oLink->addPrm('kol', reqVar::get('kol'));
        $searchLink = $oLink -> getQuery();

		// inicialising of listing object
		$amount=dbProc::getEfOrderCount($searchText,
                (($searchColumn === false) ? $defaultColumn : $searchColumn));
		$listing=new listing();
		$listing->setAmount($amount);

		$listingHtml=$listing->getHtml($oLink ->getQuery());
		unset($oLink);

		//link for frame2
		$oLink=new urlQuery();
		$oLink->addPrm(FORM_ID, 'f.ktl.s.16');
		$oLink->addPrm('editMode', 1);

		// gel list of users
		$res = dbProc::getEfOrderList( false,
                                        $listing->getStartPosition(),
                                        $listing->getAmountOnPage(),
                                        $searchText,
                                        (($searchColumn === false) ? $defaultColumn : $searchColumn),
                                        $sortOrder,
                                        $orderColumn);

		if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
				// add dv area Id to each link
				$oLink->addPrm('efOrderId', $row['KEFP_ID']);
				$res[$i]['URL']=$oLink ->getQuery();
			}
		}
		unset($oLink);

		// inicial state of users number
		$number=$listing->getStartPosition()+1;

		include('f.ktl.s.x.1.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
