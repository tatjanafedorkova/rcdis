<?
   $sCriteria = reqVar::get('search');
   $sOrder = reqVar::get('sortOrder');

   $sCriteria = reqVar::get('search');
   $searchCr = array();
        $sCr = explode("^", $sCriteria);
        if(isset($sCr[0]) && $sCr[0] != -1  && isset($sCr[1]) && $sCr[1] != -1)
        {
           $searchCr[] = text::get('YEAR').': '.$sCr[0].'-'.$sCr[1];
        }
        if(isset($sCr[2]) && $sCr[2] != -1  && isset($sCr[3]) && $sCr[3] != -1)
        {
           $searchCr[] = text::get('MONTH').': '.dtime::getMonthName($sCr[2]).'-'.dtime::getMonthName($sCr[3]);
        }
        if(isset($sCr[4]) && $sCr[4] != -1)
        {
           $searchCr[] = text::get('RCD_REGION').': '.$sCr[4];
        }
        if(isset($sCr[5]) && $sCr[5] != -1)
        {
           $searchCr[] = text::get('SINGL_DV_AREA').': '.dbProc::getDVAreaName($sCr[5]);
        }
        if(isset($sCr[6]) && $sCr[6] != -1)
        {
           $searchCr[] = text::get('TRANSPORT_LOCATION').': '.$sCr[6];
        }
        $status = explode("*", $sCr[8]);
        $statusString = '';
        if($status[0] == -1)
        {
           $statusString = text::get('ALL_STATUS');
        }
        else
        {
          foreach($status as $s)
          {
              $statusInfo = dbProc::getKrfkInfoByName($s);
              $statusString .= $statusInfo['KRFK_NOZIME'].",";
          }
          $statusString  = substr($statusString, 0, -1);
        }
        $searchCr[] = text::get('STATUS').': '.$statusString;

   $excel = '<head><meta http-equiv=Content-Type content="text/html; charset=utf-8"></head>';

   $excel .= '<table style="font-family:Arial; font-size:9pt;">';

   $excel .= '<tr><td colspan="9" style="font-size:12pt; font-weight:bold;">'.text::get('REPORT_TRANSPORT_USAGE').'</td></tr>';
   $excel .= '<tr><td colspan="9">'.implode('; ',$searchCr).'</td></tr>';
   $excel .= '<tr>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('GROUP_TITLE').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('SUBGROUP_CODE').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('SUBGROUP_TITLE').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('TRANSPORT_KM').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('TRANSPORT_AVTO_TIME').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('TRANSPORT_WORK_TIME').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('SINGL_DV_AREA').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('TRANSPORT_LOCATION').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('NATION_NUMBER').'</td>';
   $excel .= '</tr>';

        $res = dbProc::getTransportByNumberList($sCriteria, $sOrder);
        $km = 0;
        $stundas = 0;
        $darbaStundas = 0;
        $currentRow = 4;
        if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
              $excel .= '<tr>';
              $excel .= '<td style="border: 1px solid black;">'.$row['TRNS_GRUPAS_NOSAUKUMS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['TRNS_APAKSGRUPAS_KODS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['TRNS_APAKSGRUPAS_NOSAUKUMS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.number_format($row['KM'],2,'.','').'</td>';
              $excel .= '<td style="border: 1px solid black;">'.number_format($row['STUNDAS'],2,'.','').'</td>';
              $excel .= '<td style="border: 1px solid black;">'.number_format($row['DARBA_STUNDAS'],2,'.','').'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['DV_KODS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['TRNS_PIEDERIBA'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['TRNS_VALSTS_NUMURS'].'</td>';
              $excel .= '</tr>';

               $km = $km + $row['KM'];
               $stundas = $stundas + $row['STUNDAS'];
               $darbaStundas = $darbaStundas + $row['DARBA_STUNDAS'];
               $currentRow ++;
			}
		}
        $excel .= '<tr>';
        $excel .= '<td colspan="3" style="font-weight:bold; text-align:right; font-style: italic">'.text::get('TOTAL').':'.'</td>';
        $excel .= '<td style="font-weight:bold; font-style: italic">'.number_format($km, 2, '.', '').'</td>';
        $excel .= '<td style="font-weight:bold; font-style: italic">'.number_format($stundas, 2, '.', '').'</td>';
        $excel .= '<td style="font-weight:bold; font-style: italic">'.number_format($darbaStundas, 2, '.', '').'</td>';
        $excel .= '</tr>';

 // redirect output to client browser

  header('Content-Type: application/vnd.ms-excel;');
  header('Content-Disposition: attachment;filename="myfile.xls"');
  header('Cache-Control: max-age=0');

  echo $excel;

?>