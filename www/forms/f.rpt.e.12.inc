<?
   $sCriteria = reqVar::get('search');
   $searchCr = array();
        $sCr = explode("^", $sCriteria);
        if(isset($sCr[0]) && $sCr[0] != -1  && isset($sCr[1]) && $sCr[1] != -1)
        {
           $searchCr[] = text::get('YEAR').': '.$sCr[0].'-'.$sCr[1];
        }
        if(isset($sCr[2]) && $sCr[2] != -1  && isset($sCr[3]) && $sCr[3] != -1)
        {
           $searchCr[] = text::get('MONTH').': '.dtime::getMonthName($sCr[2]).'-'.dtime::getMonthName($sCr[3]);
        }
        if(isset($sCr[4]) && $sCr[4] != -1)
        {
           $searchCr[] = text::get('RCD_REGION').': '.$sCr[4];
        }
        if(isset($sCr[5]) && $sCr[5] != -1)
        {
           $searchCr[] = text::get('SINGL_DV_AREA').': '.dbProc::getDVAreaName($sCr[5]);
        }
        if(isset($sCr[6]) && $sCr[6] != -1)
        {
           $searchCr[] = text::get('ED_REGION').': '.$sCr[6];
        }
        if(isset($sCr[7]) && $sCr[7] != -1)
        {
           $searchCr[] = text::get('ED_IECIKNIS').': '.dbProc::getEDAreaName($sCr[7]);
        }

        if(isset($sCr[8]) && $sCr[8] != -1)
        {
           $searchCr[] = text::get('SINGLE_SOURCE_OF_FOUNDS').': '.dbProc::getSourceOfFoundsName($sCr[8]);
        }
        if(isset($sCr[9]) && $sCr[9] != -1)
        {
           $searchCr[] = text::get('SINGLE_VOLTAGE').': '.dbProc::getVoltageName($sCr[9]);
        }
        if(isset($sCr[10]) && $sCr[10] != -1)
        {
           $searchCr[] = text::get('ACT_OUNER').': '.dbProc::getUserName($sCr[10]);
        }
        $type = explode("*", $sCr[18]);
        $typeString = '';
        if($type[0] == -1)
        {
           $typeString = text::get('ALL');
        }
        else
        {
          $typeKls = classif::getValueArray(KL_ACT_TYPE);
          foreach($type as $t)
          {
              $typeString .= $typeKls[$t].",";
          }
          $typeString  = substr($typeString, 0, -1);
        }
        $searchCr[] = text::get('SINGLE_ACT_TYPE').': '.$typeString;
        if(isset($sCr[12]) && $sCr[12] != -1)
        {
           $searchCr[] = text::get('MMS_WORK_FINISHED').': '.(($sCr[12] == 1) ? text::get('YES'): text::get('NO'));
        }
        $status = explode("*", $sCr[14]);
        $statusString = '';
        if($status[0] == -1)
        {
           $statusString = text::get('ALL_STATUS');
        }
        else
        {
          foreach($status as $s)
          {
              $statusInfo = dbProc::getKrfkInfoByName($s);
              $statusString .= $statusInfo['KRFK_NOZIME'].",";
          }
          $statusString  = substr($statusString, 0, -1);
        }
        $searchCr[] = text::get('STATUS').': '.$statusString;
         if(isset($sCr[15]) && $sCr[15] != -1)
        {
           $searchCr[] = text::get('SINGLE_OBJECT').': '.dbProc::getObjectName($sCr[15]);
        }
        if(isset($sCr[16]) && $sCr[16] != -1)
        {
           $searchCr[] = text::get('SINGLE_OTHER_PAYMENTS').': '.(($sCr[16] == 1) ? text::get('YES'): text::get('NO'));
        }
        if(isset($sCr[17]) && $sCr[17] != -1)
        {
          // get info about calculation group
            $favoritInfo = dbProc::getFavoritDefinition($sCr[17]);
            if(count($favoritInfo)>0)
            {
            	$favorit = $favoritInfo[0];
                $favoritTitle = $favorit['LTFV_NOSAUKUMS'];
            }
           $searchCr[] = text::get('CALCULATION_GROUP').': '.$favoritTitle;
        }

       if(isset($sCr[19]) && $sCr[19] != -1)
        {
           $searchCr[] = text::get('ED_SECTION').': '.urldecode($sCr[19]);
        }
   $excel = '<head><meta http-equiv=Content-Type content="text/html; charset=utf-8"></head>';

   $excel .= '<table style="font-family:Arial; font-size:9pt;">';

   $excel .= '<tr><td colspan="4" style="font-size:12pt; font-weight:bold;">'.text::get('REPORT_CALCULATION_BY_GROUP').'</td></tr>';
   $excel .= '<tr><td colspan="4">'.implode('; ',$searchCr).'</td></tr>';

   // gel mms list
   $mms = dbProc::getMMSPlanWorksList($sCriteria);
   $res = dbProc::getRealWorkList($sCriteria);
   $area = array();

        if (is_array($mms))
		{
       		foreach ($mms as $i=>$row)
			{
              if(isset($row['KEDI_KODS']) && $row['KEDI_KODS'] != '')
              {
                $area[$row['KEDI_KODS']]  = array (
                'code' => $row['KEDI_KODS'],
                'name' => $row['ED'],
                'mmsCena' => $row['MMS_MATR_IZMAKSAS'],
                'mmsMainTime' => $row['MMS_CILVEKSTUNDAS'],
                'mmsOverTime' => '0'
                );
              }
            }
        }
        if (is_array($res))
		{
       		foreach ($res as $i=>$row)
			{
              $area[$row['KEDI_KODS']]  = array (
                'code' => $row['KEDI_KODS'],
                'name' => $row['ED'],
                'mmsCena' => (isset($area[$row['KEDI_KODS']]['mmsCena'])? $area[$row['KEDI_KODS']]['mmsCena'] : 0),
                'mmsMainTime' => (isset($area[$row['KEDI_KODS']]['mmsMainTime'])? $area[$row['KEDI_KODS']]['mmsMainTime'] : 0),
                'mmsOverTime' => '0',
                'planCena' => $row['CENA_KOPA_PLAN'],
                'planMainTime' => $row['PAMATSTUNDAS_PLAN'],
                'planOverTime' => $row['VIRSSTUNDAS_PLAN'],
                'difCena' => isset($area[$row['KEDI_KODS']]['mmsCena'])? number_format(($row['CENA_KOPA_PLAN'] - $area[$row['KEDI_KODS']]['mmsCena'] ),2,'.','') : '0.00',
                'difMainTime' => isset($area[$row['KEDI_KODS']]['mmsMainTime'])?number_format(($row['PAMATSTUNDAS_PLAN'] - $area[$row['KEDI_KODS']]['mmsMainTime'] ),2,'.',''):'0.00',
                'difOverTime' => isset($area[$row['KEDI_KODS']]['mmsOverTime'])?number_format(($row['VIRSSTUNDAS_PLAN'] - $area[$row['KEDI_KODS']]['mmsOverTime']),2,'.',''):'0.00',
                'damageCena' => $row['CENA_KOPA_DAMAGE'],
                'damageMainTime' => $row['PAMATSTUNDAS_DAMAGE'],
                'damageOverTime' => $row['VIRSSTUNDAS_DAMAGE'],
                'defectCena' => $row['CENA_KOPA_DEFECT'],
                'defectMainTime' => $row['PAMATSTUNDAS_DEFECT'],
                'defectOverTime' => $row['VIRSSTUNDAS_DEFECT'],
                'stihijaCena' => $row['CENA_KOPA_STIHIJA'],
                'stihijaMainTime' => $row['PAMATSTUNDAS_STIHIJA'],
                'stihijaOverTime' => $row['VIRSSTUNDAS_STIHIJA'],
                'notPlanCena' => number_format(($row['CENA_KOPA_DAMAGE']+$row['CENA_KOPA_DEFECT']+$row['CENA_KOPA_STIHIJA']),2,'.',''),
                'notPlanMainTime' => number_format(($row['PAMATSTUNDAS_DAMAGE']+$row['PAMATSTUNDAS_DEFECT']+$row['PAMATSTUNDAS_STIHIJA']),2,'.',''),
                'notPlanOverTime' => number_format(($row['VIRSSTUNDAS_DAMAGE']+$row['VIRSSTUNDAS_DEFECT']+$row['VIRSSTUNDAS_STIHIJA']),2,'.',''),
                'notPlanProcentCena' => (($row['CENA_KOPA_PLAN'] == 0 && ($row['CENA_KOPA_DAMAGE']+$row['CENA_KOPA_DEFECT']+$row['CENA_KOPA_STIHIJA']) == 0)? 0 : (($row['CENA_KOPA_PLAN'] == 0) ? 100 : number_format((($row['CENA_KOPA_DAMAGE']+$row['CENA_KOPA_DEFECT']+$row['CENA_KOPA_STIHIJA'])/$row['CENA_KOPA_PLAN'])*100,2,'.',''))),
                'notPlanProcentMainTime' => (($row['PAMATSTUNDAS_PLAN'] == 0 && ($row['PAMATSTUNDAS_DAMAGE']+$row['PAMATSTUNDAS_DEFECT']+$row['PAMATSTUNDAS_STIHIJA']) == 0) ? 0 : (($row['PAMATSTUNDAS_PLAN'] == 0) ? 100 : number_format((($row['PAMATSTUNDAS_DAMAGE']+$row['PAMATSTUNDAS_DEFECT']+$row['PAMATSTUNDAS_STIHIJA'])/$row['PAMATSTUNDAS_PLAN'])*100,2,'.',''))),
                'notPlanProcentOverTime' => (($row['VIRSSTUNDAS_PLAN'] == 0 && ($row['VIRSSTUNDAS_DAMAGE']+$row['VIRSSTUNDAS_DEFECT']+$row['VIRSSTUNDAS_STIHIJA']) == 0) ? 0 : (($row['VIRSSTUNDAS_PLAN'] == 0) ? 100 : number_format((($row['VIRSSTUNDAS_DAMAGE']+$row['VIRSSTUNDAS_DEFECT']+$row['VIRSSTUNDAS_STIHIJA'])/$row['VIRSSTUNDAS_PLAN'])*100,2,'.',''))),
                'totalCena' => number_format(($row['CENA_KOPA_PLAN']+$row['CENA_KOPA_DAMAGE']+$row['CENA_KOPA_DEFECT']+$row['CENA_KOPA_STIHIJA']),2,'.',''),
                'totalMainTime' => number_format(($row['PAMATSTUNDAS_PLAN']+$row['PAMATSTUNDAS_DAMAGE']+$row['PAMATSTUNDAS_DEFECT']+$row['PAMATSTUNDAS_STIHIJA']),2,'.',''),
                'totalOverTime' => number_format(($row['VIRSSTUNDAS_PLAN']+$row['VIRSSTUNDAS_DAMAGE']+$row['VIRSSTUNDAS_DEFECT']+$row['VIRSSTUNDAS_STIHIJA']),2,'.','')
              );
            }
        }
        ksort($area);

   if (is_array($area) && count($area) > 0)
	{
		foreach ($area as $row)
		{
            $excel .= '<tr>';
                $excel .= '<td align="left"><b>'.text::get('ED_IECIKNIS').':</b><td>';
                $excel .= '<td colspan="2" align="left"><b>'. $row['name'].'</b><td>';
                $excel .= '<td align="left"><b>'. $row['code'].'</b><td>';
            $excel .= '</tr>';

            $excel .= '<tr>';
                $excel .= '<td width="30%">&nbsp;<td>';
                $excel .= '<td width="20%">'.text::get('BASE_TIME').'<td>';
                $excel .= '<td width="20%">'. text::get('REPORT_MATERIAL_PRICE_TOTAL').'<td>';
                $excel .= '<td width="20%">'.text::get('OVER_TIME').'<td>';
            $excel .= '</tr>';

            $excel .= '<tr>';
                 $excel .= '<td align="left"><b>'.text::get('MMS_PLAN_WORKS').':</b><td>';
                 $excel .= '<td align="center">'. $row['mmsMainTime'].'<td>';
                 $excel .= '<td align="center">'. $row['mmsCena'].'<td>';
                 $excel .= '<td align="center">'. $row['mmsOverTime'].'<td>';
            $excel .= '</tr>';
            $excel .= '<tr>';
                 $excel .= '<td align="left"><b>'.text::get('REAL_PLAN_WORKS').':</b><td>';
                 $excel .= '<td align="center">'. $row['planMainTime'].'<td>';
                 $excel .= '<td align="center">'. $row['planCena'].'<td>';
                 $excel .= '<td align="center">'. $row['planOverTime'].'<td>';
            $excel .= '</tr>';
            $excel .= '<tr>';
                 $excel .= '<td align="left"><b>'.text::get('MMS_PLAN_MINUS_REAL_PLAN').':</b><td>';
                 $excel .= '<td align="center">'. $row['difMainTime'].'<td>';
                 $excel .= '<td align="center">'. $row['difCena'].'<td>';
                 $excel .= '<td align="center">'. $row['difOverTime'].'<td>';
            $excel .= '</tr>';
            $excel .= '<tr>';
                 $excel .= '<td align="left"><b>'.text::get('NOT_PLAN_WORK').':</b><td>';
                 $excel .= '<td align="center">'. $row['notPlanMainTime'].'<td>';
                 $excel .= '<td align="center">'. $row['notPlanCena'].'<td>';
                 $excel .= '<td align="center">'. $row['notPlanOverTime'].'<td>';
            $excel .= '</tr>';
            $excel .= '<tr>';
                 $excel .= '<td align="left"><b>'.text::get('DAMAGE_REAL_WORK').':</b><td>';
                 $excel .= '<td align="center">'. $row['damageMainTime'].'<td>';
                 $excel .= '<td align="center">'. $row['damageCena'].'<td>';
                 $excel .= '<td align="center">'. $row['damageOverTime'].'<td>';
            $excel .= '</tr>';
            $excel .= '<tr>';
                 $excel .= '<td align="left"><b>'.text::get('DEFECT_REAL_WORK').':</b><td>';
                 $excel .= '<td align="center">'. $row['defectMainTime'].'<td>';
                 $excel .= '<td align="center">'. $row['defectCena'].'<td>';
                 $excel .= '<td align="center">'. $row['defectOverTime'].'<td>';
            $excel .= '</tr>';
            $excel .= '<tr>';
                 $excel .= '<td align="left"><b>'.text::get('STIHIJA_REAL_WORK').':</b><td>';
                 $excel .= '<td align="center">'. $row['stihijaMainTime'].'<td>';
                 $excel .= '<td align="center">'. $row['stihijaCena'].'<td>';
                 $excel .= '<td align="center">'. $row['stihijaOverTime'].'<td>';
            $excel .= '</tr>';
            $excel .= '<tr>';
                 $excel .= '<td align="left"><b>'.text::get('NOT_PLAN_TO_PLAN').':</b><td>';
                 $excel .= '<td align="center">'. $row['notPlanProcentMainTime'].' %<td>';
                 $excel .= '<td align="center">'. $row['notPlanProcentCena'].' %<td>';
                 $excel .= '<td align="center">'. $row['notPlanProcentOverTime'].' %<td>';
            $excel .= '</tr>';
            $excel .= '<tr>';
                 $excel .= '<td align="left"><b>'.text::get('NOT_PLAN_AND_PLAN_TOTAL').':</b><td>';
                 $excel .= '<td align="center">'. $row['totalMainTime'].'<td>';
                 $excel .= '<td align="center">'. $row['totalCena'].'<td>';
                 $excel .= '<td align="center">'. $row['totalOverTime'].'<td>';
            $excel .= '</tr>';
            $excel .= '<tr>';
                $excel .= '<td colspan="4">&nbsp;<td>';
            $excel .= '</tr>';
			}
		}


// redirect output to client browser

  header('Content-Type: application/vnd.ms-excel;');
  header('Content-Disposition: attachment;filename="myfile.xls"');
  header('Cache-Control: max-age=0');

  echo $excel;
?>