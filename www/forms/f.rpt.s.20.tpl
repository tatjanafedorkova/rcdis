﻿<body class="frame_2">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><h1><?= text::toUpper(text::get('REPORT_RFC_INNER_ACT')); ?></h1></td>
		<td align="right">
            <img src="img/ico_excel.gif" alt="" width="16" height="16">&nbsp;
            <a href="<?= $exportUrl; ?>" ><?=text::get('EXPORT');?></a>&nbsp;
        </td>
	</tr>
</table>

<table cellpadding="5" cellspacing="1" border="0" width="100%" >
	<thead>
		<tr class="table_head_2">
            <td width="8%" ><?= text::get('ACT_NUMBER'); ?></td>
			<td width="5%" ><?= text::get('MONTH'); ?></td>
            <td width="5%" ><?=text::get('YEAR');?></td>
            <td width="12%" ><?=text::get('ED_IECIKNIS');?></td>
            <td width="12%" ><?=text::get('USER_NAME');?> <?=text::get('USER_SURNAME');?></td>
            <td width="5%" ><?=text::get('RCD_KODS');?></td>
			<td width="5%" ><?=text::get('TRANSPORT_WORK_TIME');?></td>
			<td width="8%" ><?=text::get('DATE');?> <?=text::get('FROM');?></td>
			<td width="8%" ><?=text::get('DATE');?> <?=text::get('UNTIL');?></td>
			<td width="12%" ><?=text::get('WORK_TYPE');?></td>
			<td width="20%" ><?=text::get('WORK_DESCRIPTION');?></td>
 		</tr>
	</thead>
	<tbody>
<?
	if (is_array($res) && count($res) > 0)
	{
		foreach ($res as $row)
		{
?>
			<tr class="table_cell_3" >
                <td align="center"><?= $row['RAKT_FULL_NUMBER']; ?></td>
                <td align="left" ><?= $row['RAKT_MENESIS']; ?></td>
                <td align="center"><?= $row['RAKT_GADS']; ?></td>
                <td align="center"><?= $row['STRUCTURVIENIBA']; ?></td>
                <td align="center"><?= $row['RCFD_KPRF_VARDS_UZVARDS']; ?></td>
                <td align="center"><?= $row['RCFD_KPRF_KODS']; ?></td>
		<td align="center"><?= number_format($row['RCFD_STUNDAS'],2, '.', ''); ?></td>
		<td align="center"><?= $row['RCFD_DATUMS_NO']; ?></td>
		<td align="center"><?= $row['RCFD_DATUMS_LIDZ']; ?></td>
		<td align="center"><?= $row['WORK_TITLE']; ?></td>
		<td align="center"><?= $row['RCFD_WORK_DESCRIPTION']; ?></td>

			</tr>
<?
		}
	}
?>
	</tbody>
</table>

</body>