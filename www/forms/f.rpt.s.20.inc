﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
	$searchMode = reqVar::get('isSearch');
	// top frame
    if($searchMode)
	{
        $oLink=new urlQuery();
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.20');
    	$oLink->addPrm('isSearch', 1 );
    	// form inicialization
    	$oForm = new Form('frmMain','post',$oLink->getQuery());
    	unset($oLink);

        $oLink=new urlQuery();
	    $oLink->addPrm('isReturn', '1');
        $oLink->addPrm('isSearch', 1 );
	    $oLink->addPrm(FORM_ID, 'f.rpt.s.20');
	    $criteriaLink=$oLink ->getQuery();
	    unset($oLink);

        // if operation
    	if ($oForm -> isFormSubmitted())
    	{
            $oListLink=new urlQuery();
            $oListLink->addPrm(FORM_ID, 'f.rpt.s.20');
        }
        $searchTitle = text::toUpper(text::toUpper(text::get('REPORT_RFC_INNER_ACT')));
        $searchName = 'REPORT_RFC_INNER_ACT';
        include('f.akt.m.13.inc');
	}
	// bottom frale
	else
	{
	    $sCriteria = reqVar::get('search');

        // URL  export
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.e.20');
    	$oLink->addPrm('search', $sCriteria);
        $oLink->addPrm(DONT_USE_GLB_TPL, '1');

        // gel list of acts
		$res = dbProc::getRfcActReportList($sCriteria, false);
        if(count($res) > MAX_EXCEL_ROW_COUNT)
        {
           $exportUrl = "javascript:alert('".text::get('TOO_MATCH_SEARCH_RESULTS')."');" ;
        }
        else
        {
          $exportUrl =  $oLink ->getQuery();
        }

        unset($oLink);

        // gel list of acts
		$res = dbProc::getRfcActReportList($sCriteria);
     

		include('f.rpt.s.20.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
