<?
// Created by Tatjana Fedorkova
if (userAuthorization::isAdmin())
{
  $oLink=new urlQuery();
  $oLink->addPrm(FORM_ID, 'f.adm.s.5');
  $oForm = new Form('frmMain','post',$oLink->getQuery());
  unset($oLink);

  // get info about system
  $systemInfo = dbProc::getSystemInfo();
  if(count($systemInfo)>0)
  {
  	$info = $systemInfo[0];
  }

  $oForm -> addElement('date', 'date',  text::get('CALC_PERIOD_START_DATE'), isset($info['SNFO_CALC_START_DATE'])?$info['SNFO_CALC_START_DATE']:dtime::now());
  $oForm -> addRule('date', text::get('ERROR_REQUIRED_FIELD'), 'required');
  $oForm -> addRule('date', text::get('ERROR_INCORRECT_DATE'),'validatedate');
  
  $oForm -> addElement('submitImg', 'submit', text::get('SAVE'), 'img/btn_saglabat.gif', 'width="70" height="20"');

  if ($oForm -> isFormSubmitted())
  {

        $r = false;
     	  $r = dbProc::saveSystemInfo( '', '', $oForm->getValue('date'));

	  	if (!$r)
	  	{
	  		$oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
	    }
  }
  $oForm -> makeHtml();
  include('f.adm.s.5.tpl');
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>