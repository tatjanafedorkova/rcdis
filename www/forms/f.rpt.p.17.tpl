﻿<body class="print">
<div class="print_header"><?= text::get('REPORT_FINISHED_WORK'); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >
<?
  foreach($searchCr as $i=>$s)
  {
    if($i%3 == 0)
    {
      ?>
        <tr>
      <?
    }
    ?>
      <td align="right" class="print_table_total" width="13%"><?=$s['label'];?>:</td>
      <td align="left" class="print_table_data" width="20%"><?=$s['value'];?></td>
    <?
  }
?>
</table>
<br />
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >
<?
   if (is_array($area) && count($area) > 0)
	{
		foreach ($area as $row)
		{

          if (is_array($row['ed']) && count($row['ed']) > 0)
          {
            foreach ($row['ed'] as $rrS)
            {
?>
            <tr>
                <td  align="left" class="print_table_header2"><b><?=text::get('ED_IECIKNIS');?>:</b></td>
                <td colspan="3" align="left" class="print_table_header2"><?= $rrS['name']; ?></td>
                <td colspan="2" align="left" class="print_table_header2"><?= $rrS['code']; ?></td>
            </tr>
            <tr>
                <td width="40%" class="print_table_header2"><?= text::get('SINGLE_MMS'); ?></td>
                <td width="12%" class="print_table_header2"><?=text::get('QUARTER');?></td>
                <td width="12%" class="print_table_header2"><?= text::get('MAN_HOUR_STANDART_TOTAL'); ?></td>
                <td width="12%" class="print_table_header2"><?= text::get('MATERIAL_PRICE'); ?></td>
                <td width="12%" class="print_table_header2"><?=text::get('PRIORITY');?></td>
                <td width="12%" class="print_table_header2"><?=text::get('ACT_NUMBER');?></td>
            </tr>

            <?
                if (is_array($rrS['mms']) && count($rrS['mms']) > 0)
                {
                   foreach ($rrS['mms'] as $r)
                   {
                     ?>
                   <tr>
                      <td align="left"><?= $r['code']; ?></td>
                      <td align="center"><?= $r['quarter']; ?></td>
                      <td align="center"><?= $r['stundas']; ?></td>
                      <td align="center"><?= $r['cena']; ?></td>
                      <td align="center"><?= $r['priority']; ?></td>
                      <td align="center"><?= $r['act']; ?></td>
                  </tr>

                     <?

                   }
                }
            ?>
            <tr>
             <td colspan="2" align="left" class="print_table_header1"><b><?=$rrS['name'];?>: </b></td>
             <td align="center" class="print_table_header1"><b><?=number_format($rrS['totalStundas'],2,'.','');?></b></td>
             <td align="center" class="print_table_header1"><b><?=number_format($rrS['totalCena'],2,'.','');?></b></td>
             <td class="print_table_header1">&nbsp;</td>
             <td class="print_table_header1">&nbsp;</td>
            </tr>
            <tr><td colspan="6">&nbsp;</td></tr>
           <?}}?>
            <tr>
             <td colspan="2" align="left" class="print_table_header"><b><?=$row['section'];?>:</b></td>
             <td align="center" class="print_table_header"><b><?=number_format($row['totalStundas'],2,'.','');?></b></td>
             <td align="center" class="print_table_header"><b><?=number_format($row['totalCena'],2,'.','');?></b></td>
             <td class="print_table_header">&nbsp;</td>
             <td class="print_table_header">&nbsp;</td>
            </tr>
            <tr><td colspan="6">&nbsp;</td></tr>
        <?
		}
        ?>
        <tr>
             <td colspan="2" align="right" class="print_table_total"><b><?=text::get('TOTAL');?>:</b></td>
             <td align="center" class="print_table_total"><b><?= $stundas; ?></b></td>
             <td align="center" class="print_table_total"><b><?= $summa; ?></b></td>
             <td align="center" class="print_table_total">&nbsp;</td>
             <td align="center" class="print_table_total">&nbsp;</td>
        </tr>
<?
	}
?>

</table>

</body>