﻿<?
// Main form with XMLHTTP method load this code:
if (reqVar::get('xmlHttp') == 1)
{
	//if is set edRegion
	if (reqVar::get('edRegion') !== false)
	{
		// unset previouse values of edArea
		?>
		for(i=document.all["edArea"].length; i>=0; i--)
		{
			document.all["edArea"].options[i] = null;
		}
		<?
		$res=dbProc::getEDAreaListForRegion(reqVar::get('edRegion'));
		if (is_array($res) && count($res)>0)
		{
			?>
			document.all["edArea"].options[0] = new Option('<?=text::get('ALL');?>', '-1');
			<?
            $i = 1;
			foreach ($res as $row)
			{
				// feel options array
				?>
				document.all["edArea"].options[<?=$i;?>] = new Option( '<?=$row['KEDI_REGIONS'].' : '.$row['KEDI_KODS'].' : '.$row['KEDI_NOSAUKUMS'];?>', '<?=$row['KEDI_ID'];?>');
                <?
                $i++;
			}
		}
        // unset previouse values of section
		?>
		for(i=document.all["section"].length; i>=0; i--)
		{
			document.all["section"].options[i] = null;
		}
		<?
		$res=dbProc::getEDSectionListForRegion(reqVar::get('edRegion'));
		if (is_array($res) && count($res)>0)
		{
			?>
			document.all["section"].options[0] = new Option('<?=text::get('ALL');?>', '-1');
			<?
            $i = 1;
			foreach ($res as $row)
			{
				// feel options array
				?>
				document.all["section"].options[<?=$i;?>] = new Option( '<?=$row['KEDI_SECTION'];?>', '<?=$row['KEDI_SECTION'];?>');
                <?
                $i++;
			}
		}
	}
    	//if is set dvRegion
	if (reqVar::get('dvRegion') !== false)
	{
		// unset previouse values of dvArea
		?>
		for(i=document.all["dvArea"].length; i>=0; i--)
		{
			document.all["dvArea"].options[i] = null;
		}
		<?
		$res=dbProc::getDVAreaForRegion(reqVar::get('dvRegion'));
		if (is_array($res) && count($res)>0)
		{
			?>
			document.all["dvArea"].options[0] = new Option('<?=text::get('ALL');?>', '-1');
			<?
            $i = 1;
			foreach ($res as $row)
			{
				// feel options array
				?>
				document.all["dvArea"].options[<?=$i;?>] = new Option( '<?=$row['KDVI_REGIONS'].''.$row['KDVI_KODS'].' : '.$row['KDVI_NOSAUKUMS'];?>', '<?=$row['KDVI_ID'];?>');
                <?
                $i++;
			}
		}
	}
	exit;
}
// ED pasūtītie darbi
define ('ED_SOURCE_OF_FOUNDS',10);
        // get user info
      	$userInfo = dbProc::getUserInfo($userId);
        $isEconomist = dbProc::isUserInRole($userId, ROLE_ECONOMIST);
        $isAdmin=userAuthorization::isAdmin();
        $isEdUser = dbProc::isUserInRole($userId, ROLE_ED_USER);
      	if(count($userInfo) >0 )
      	{
      		$user = $userInfo[0];
       	}
         // get RCD region list
         $rcdRegion=array();
         if(isset($user['RLTT_REGIONS']) && isset($user['RLTT_ROLE']) && $user['RLTT_ROLE'] != ROLE_ED_USER )
         {
            $rcdRegion[$user['RLTT_REGIONS']]=$user['RLTT_REGIONS'];
         }
         else
         {
            $rcdRegion['-1']=text::get('ALL');
            $res=dbProc::getKrfkName(KRFK_REGION);
            if (is_array($res))
            {
           	    foreach ($res as $row)
           	    {
           		    $rcdRegion[$row['nosaukums']]=$row['nosaukums'];
           	    }
            }
         }
         unset($res);

         // get DV area list
         $dvarea = array();
         if(isset($user['RLTT_KDVI_ID']) )
         {
            $dvarea[$user['RLTT_KDVI_ID']] = dbProc::getDVAreaName($user['RLTT_KDVI_ID']);
         }
         elseif(isset($user['RLTT_REGIONS']) && isset($user['RLTT_ROLE']) && $user['RLTT_ROLE'] != ROLE_ED_USER )
         {
            $res=dbProc::getDVAreaForRegion($user['RLTT_REGIONS'], false);
			if (is_array($res))
			{
			    $dvarea['-1']=text::get('ALL');
				foreach ($res as $row)
				{
					$dvarea[$row['KDVI_ID']]=$row['KDVI_REGIONS'].$row['KDVI_KODS'].' : '.$row['KDVI_NOSAUKUMS'];
				}
			}
         }
         else
         {
            $res=dbProc::getDVAreaList(false,false,false,false,false,'ASC','`KDVI_REGIONS`, `KDVI_KODS`');
			if (is_array($res))
			{
			    $dvarea['-1']=text::get('ALL');
				foreach ($res as $row)
				{
					$dvarea[$row['KDVI_ID']]=$row['KDVI_REGIONS'].$row['KDVI_KODS'].' : '.$row['KDVI_NOSAUKUMS'];
				}
			}
         }

         // get ED region list
         $edRegion=array();
         if(isset($user['RLTT_REGIONS']) && isset($user['RLTT_ROLE']) && $user['RLTT_ROLE'] == ROLE_ED_USER )
         {
            $edRegion[$user['RLTT_REGIONS']]=$user['RLTT_REGIONS'];
         }
         else
         {
           $edRegion['-1']=text::get('ALL');
           $res=dbProc::getEDRegionList();
           if (is_array($res))
           {
           	foreach ($res as $row)
           	{
           		$edRegion[$row['nosaukums']]=$row['nosaukums'];
           	}
           }
         }
         unset($res);

         // get ED area list
        $edarea=array();
        if(isset($user['RLTT_REGIONS']) && isset($user['RLTT_ROLE']) && $user['RLTT_ROLE'] == ROLE_ED_USER )
        {
            $res=dbProc::getEDAreaListForRegion($user['RLTT_REGIONS'], false);
			if (is_array($res))
			{
			    $edarea['-1']=text::get('ALL');
				foreach ($res as $row)
				{
					$edarea[$row['KEDI_ID']]=$row['KEDI_REGIONS'].' : '.$row['KEDI_KODS'].' : '.$row['KEDI_NOSAUKUMS'];
				}
			}
         }
         else
         {
            $res=dbProc::getEDAreaList(false,false,false,false,false,'ASC','`KEDI_REGIONS`, `KEDI_KODS`');
            if (is_array($res))
            {
                $edarea['-1']=text::get('ALL');
             	foreach ($res as $row)
             	{
              	    $edarea[$row['KEDI_ID']]=$row['KEDI_REGIONS'].' : '.$row['KEDI_KODS'].' : '.$row['KEDI_NOSAUKUMS'];
                }
            }
         }
        unset($res);

        // get ED section list
        $section=array();
        if(isset($user['RLTT_REGIONS']) && isset($user['RLTT_ROLE']) && $user['RLTT_ROLE'] == ROLE_ED_USER )
        {
            $res=dbProc::getEDSectionListForRegion($user['RLTT_REGIONS']);
			if (is_array($res))
			{
			    $section['-1']=text::get('ALL');
				foreach ($res as $row)
				{
					$section[$row['KEDI_SECTION']]=$row['KEDI_SECTION'];
				}
			}
         }
         else
         {
            $res=dbProc::getEDSectionListForRegion(-1);
            if (is_array($res))
            {
                $section['-1']=text::get('ALL');
             	foreach ($res as $row)
             	{
              	    $section[$row['KEDI_SECTION']]=$row['KEDI_SECTION'];
                }
            }
         }
        unset($res);

        // get users list
         $usersList = array();
         if(isset($user['RLTT_KDVI_ID'])  && !$isEdUser)
         {
            $res=dbProc::getUserDVAreaList($user['RLTT_KDVI_ID'], false);
			if (is_array($res))
			{
			  $usersList['-1']=text::get('ALL');
			  foreach ($res as $row)
			  {
			  	$usersList[$row['RLTT_ID']]=$row['RLTT_VARDS'].' '.$row['RLTT_UZVARDS'];
			  }
			}
         }
         elseif(isset($user['RLTT_REGIONS'])  && !$isEdUser)
         {
            $res=dbProc::getUserRegionList($user['RLTT_REGIONS'], false);
			if (is_array($res))
			{
			  $usersList['-1']=text::get('ALL');
			  foreach ($res as $row)
			  {
			  	$usersList[$row['RLTT_ID']]=$row['RLTT_VARDS'].' '.$row['RLTT_UZVARDS'];
			  }
			}
         }
         else
         {
            $res=dbProc::getUsersList(false,false,false,false,false,'ASC','RLTT_VARDS, RLTT_UZVARDS');
			if (is_array($res))
			{
			  $usersList['-1']=text::get('ALL');
			  foreach ($res as $row)
			  {
			  	$usersList[$row['RLTT_ID']]=$row['RLTT_VARDS'].' '.$row['RLTT_UZVARDS'];
			  }
			}
         }

        // get is finished list
        $isFinished = array();
        $isFinished['-1']=text::get('ALL');
        $isFinished['1']=text::get('YES');
        $isFinished['2']=text::get('NO');

        // get other payment list
        $isOtherPayment = array();
        $isOtherPayment['-1']=text::get('ALL');
        $isOtherPayment['1']=text::get('YES');
        $isOtherPayment['2']=text::get('NO');


       // get status list
       $status=array();
       $status['-1']=text::get('ALL_STATUS');
       $res=dbProc::getKrfkName(KRFK_STATUS);
       if (is_array($res))
       {
       	foreach ($res as $row)
       	{
            if($row['nosaukums'] != STAT_DELETE || ($row['nosaukums'] == STAT_DELETE && $isAdmin && !$isEdUser))
            {
                $status[$row['nosaukums']]=$row['nozime'];
            }
       	}
       }
       unset($res);

       // get type list
       $type=array();
       $type['-1']=text::get('ALL');
       $res=classif::getValueArray(KL_ACT_TYPE);
       if (is_array($res))
       {
       	foreach ($res as $i=>$v)
       	{
            $type[$i]=$v;
       	}
       }
       unset($res);

       $searcCriteria = array();

       // if operation
    	if ($oForm -> isFormSubmitted())
    	{
           $searcCriteria['yearFrom'] = $oForm->getValue('yearFrom');
           $searcCriteria['yearUntil'] = $oForm->getValue('yearUntil');
           $searcCriteria['monthFrom'] = $oForm->getValue('monthFrom');
           $searcCriteria['monthUntil'] = $oForm->getValue('monthUntil');
           $searcCriteria['rcdRegion'] = $oForm->getValue('rcdRegion');
           $searcCriteria['dvArea'] = $oForm->getValue('dvArea');
           $searcCriteria['edRegion'] = $oForm->getValue('edRegion');
           $searcCriteria['edArea'] = $oForm->getValue('edArea');
           $searcCriteria['sourceOfFounds'] = $oForm->getValue('sourceOfFounds');
           $searcCriteria['voltage'] = $oForm->getValue('voltage');
           $searcCriteria['ouner'] = $oForm->getValue('ouner');
           $searcCriteria['type'] = $oForm->getValue('type');
           $searcCriteria['isFinished'] = $oForm->getValue('isFinished');
           $searcCriteria['status']  = $oForm->getValue('status');
           $searcCriteria['status1']  = implode("*",$oForm->getValue('status'));
           $searcCriteria['object']  = $oForm->getValue('object');
           $searcCriteria['isOtherPayment']  = $oForm->getValue('isOtherPayment');
           $searcCriteria['code']  = $oForm->getValue('code');
           $searcCriteria['type1']  = implode("*",$oForm->getValue('type'));
           $searcCriteria['section'] = urlencode($oForm->getValue('section'));
           dbProc::replaceSearchResult($searchName,$userId,
                                $searcCriteria['yearFrom'],
                                $searcCriteria['yearUntil'],
                                $searcCriteria['monthFrom'],
                                $searcCriteria['monthUntil'],
                                $searcCriteria['rcdRegion'],
                                $searcCriteria['dvArea'],
                                $searcCriteria['edRegion'],
                                $searcCriteria['edArea'],
                                $searcCriteria['sourceOfFounds'],
                                $searcCriteria['voltage'],
                                $searcCriteria['ouner'],
                                $searcCriteria['type1'],
                                $searcCriteria['isFinished'],
                                $searcCriteria['status1'],
                                '',
                                $searcCriteria['object'],
                                $searcCriteria['isOtherPayment'],
                                '',
                                $searcCriteria['code'],
                                '','','','','',
                                $searcCriteria['section']
                                );

           $cr = '';
           foreach($searcCriteria as $s)
           {
             $cr .= $s.'^';
           }
           $cr = substr($cr, 0, -1);
           $oListLink->addPrm('search', $cr);
           $actListLink=$oListLink ->getQuery();
           $oForm->addElement('static','jsRefresh2','','<script>reloadFrame(2, "'.$actListLink.'");</script>');
           unset($oListLink);
    	}
        else
        {
           if (reqVar::get('isReturn') == 1)
            {
                $searchCritery = dbProc::getSearchCritery($userId);
           	    if(count($searchCritery) >0 )
      	        {
      		        $critery = $searchCritery[0];
       	        }
            }

           $searcCriteria['yearFrom'] = isset($critery['yearFrom'])? $critery['yearFrom'] :dtime::getCurrentYear();
           $searcCriteria['yearUntil'] = isset($critery['yearUntil'])? $critery['yearUntil'] :dtime::getCurrentYear();
           $searcCriteria['monthFrom'] = isset($critery['monthFrom'])? $critery['monthFrom'] :dtime::getCurrentMonth();
           $searcCriteria['monthUntil'] = isset($critery['monthUntil'])? $critery['monthUntil'] :dtime::getCurrentMonth();
           $searcCriteria['rcdRegion'] = isset($critery['rcdRegion'])? $critery['rcdRegion'] :$user['RLTT_REGIONS'];
           $searcCriteria['dvArea'] = isset($critery['dvArea'])? $critery['dvArea'] :$user['RLTT_KDVI_ID'];
           $searcCriteria['edRegion'] = isset($critery['edRegion'])? $critery['edRegion'] :'-1';
           $searcCriteria['edArea'] = isset($critery['edArea'])? $critery['edArea'] :'-1';
           $searcCriteria['sourceOfFounds'] = isset($critery['sourceOfFounds'])? $critery['sourceOfFounds'] :(($isEdUser)? dbProc::getSourceOfFoundsId(ED_SOURCE_OF_FOUNDS) :'-1');
           $searcCriteria['voltage'] = isset($critery['voltage'])? $critery['voltage'] :'-1';
           $searcCriteria['ouner'] = isset($critery['ouner'])? $critery['ouner'] :(($isEconomist || $isEdUser) ? -1 : $userId);
           $searcCriteria['type'] = isset($critery['type'])? explode("*",$critery['type']) : array('-1');
           $searcCriteria['isFinished'] = isset($critery['isFinished'])? $critery['isFinished'] :'-1';
           $searcCriteria['status']  = isset($critery['status1'])? explode("*",$critery['status1']) : array(STAT_CLOSE);
           $searcCriteria['object'] = isset($critery['object'])? $critery['object'] :'-1';
           $searcCriteria['isOtherPayment'] = isset($critery['isOtherPayment'])? $critery['isOtherPayment'] :'-1';
           $searcCriteria['code'] = isset($critery['code'])? $critery['code'] :'';
           $searcCriteria['section']  = isset($critery['section'])? urldecode($critery['section']) : '';
        }

        $oForm -> addElement('label', 'year',  text::get('YEAR'), '');
        $oForm -> addElement('select', 'yearFrom',  text::get('FROM'), $searcCriteria['yearFrom'], 'style="width:50;"', '', '', dtime::getYearArray());
        $oForm -> addElement('select', 'yearUntil',  text::get('UNTIL'), $searcCriteria['yearUntil'], 'style="width:50;"', '', '', dtime::getYearArray());

        $oForm -> addElement('label', 'month',  text::get('MONTH'), '');
        $oForm -> addElement('select', 'monthFrom',  text::get('FROM'), $searcCriteria['monthFrom'], 'style="width:70;"', '', '', dtime::getMonthArray());
        $oForm -> addElement('select', 'monthUntil',  text::get('UNTIL'), $searcCriteria['monthUntil'], 'style="width:70;"', '', '', dtime::getMonthArray());
        // xmlHttp link
		$oLink=new urlQuery();
		$oLink->addPrm(DONT_USE_GLB_TPL, 1);
		$oLink->addPrm(FORM_ID, 'f.akt.m.1');
        $oForm -> addElement('select', 'rcdRegion',  text::get('RCD_REGION'), $searcCriteria['rcdRegion'], 'onChange="eval(xmlHttpGetValue(\''.$oLink->getQuery().'&xmlHttp=1&dvRegion=\'+document.all[\'rcdRegion\'].options[document.all[\'rcdRegion\'].selectedIndex].value));"', '', '', $rcdRegion);

        $oForm -> addElement('select', 'dvArea',  text::get('SINGL_DV_AREA'), $searcCriteria['dvArea'], '', '', '', $dvarea);


		$oForm -> addElement('select', 'edRegion',  text::get('ED_REGION'), $searcCriteria['edRegion'], 'onChange="eval(xmlHttpGetValue(\''.$oLink->getQuery().'&xmlHttp=1&edRegion=\'+document.all[\'edRegion\'].options[document.all[\'edRegion\'].selectedIndex].value));"', '', '', $edRegion);
        unset($oLink);

        $oForm -> addElement('select', 'edArea',  text::get('ED_IECIKNIS'), $searcCriteria['edArea'], '', '', '', $edarea);

        $oForm -> addElement('select', 'section',  text::get('ED_SECTION'), $searcCriteria['section'], '', '', '', $section);

        $oForm -> addElement('kls2', 'sourceOfFounds',  text::get('SINGLE_SOURCE_OF_FOUNDS'), array('classifName'=>KL_SOURCE_OF_FOUNDS,'value'=>$searcCriteria['sourceOfFounds'],'readonly'=>false), 'maxlength="2000"');

        $oForm -> addElement('kls2', 'voltage',  text::get('SINGLE_VOLTAGE'), array('classifName'=>KL_VOLTAGE,'value'=>$searcCriteria['voltage'],'readonly'=>false), 'maxlength="2000"');

        $oForm -> addElement('select', 'ouner',  text::get('ACT_OUNER'), $searcCriteria['ouner'], '', '', '', $usersList);

        $oForm -> addElement('multiple_select', 'type', text::get('SINGLE_ACT_TYPE'), $searcCriteria['type'], ' size="5" ', '', '', $type);

        $oForm -> addElement('select', 'isFinished',  text::get('_WORK_FINISHED'), $searcCriteria['isFinished'], '', '', '', $isFinished);

        $oForm -> addElement('multiple_select', 'status', text::get('STATUS'), $searcCriteria['status'], ' size="7" ', '', '', $status);

        $oForm -> addElement('kls2', 'object',  text::get('SINGLE_OBJECT'), array('classifName'=>KL_OBJECTS,'value'=>$searcCriteria['object'],'readonly'=>false), 'maxlength="2000"');

        $oForm -> addElement('select', 'isOtherPayment',  text::get('SINGLE_OTHER_PAYMENTS'), $searcCriteria['isOtherPayment'], '', '', '', $isOtherPayment);

        $oForm -> addElement('text', 'code',  text::get('REPORT_MATERIAL_CODE'), $searcCriteria['code']);

       	// form buttons
       	$oForm -> addElement('submitImg', 'search', text::get('SEARCH'), 'img/btn_meklet.gif', 'width="70" height="20" onClick="setPosition1(\'loading\'); return true;"');

        $oForm -> makeHtml();
		include('f.akt.m.5.tpl');
?>