﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
     $sCriteria = reqVar::get('search');

        // gel mms list
        $mms = dbProc::getMMSPlanWorksList($sCriteria);
        $res = dbProc::getRealWorkList($sCriteria);
       // print_r($res);
       /* echo "<pre>";
        print_r($mms);
        echo "</pre>";
       */
        $area = array();

        if (is_array($mms))
		{
       		foreach ($mms as $i=>$row)
			{
			  if(isset($row['KEDI_KODS']) && $row['KEDI_KODS'] != '')
              {
                $area[$row['KEDI_KODS']]  = array (
                'code' => $row['KEDI_KODS'],
                'name' => $row['ED'],
                'mmsCena' => $row['MMS_MATR_IZMAKSAS'],
                'mmsMainTime' => $row['MMS_CILVEKSTUNDAS'],
                'mmsOverTime' => '0'
                );
              }
            }
        }
        if (is_array($res))
		{
       		foreach ($res as $i=>$row)
			{
              $area[$row['KEDI_KODS']]  = array (
                'code' => $row['KEDI_KODS'],
                'name' => $row['ED'],
                'mmsCena' => (isset($area[$row['KEDI_KODS']]['mmsCena'])? $area[$row['KEDI_KODS']]['mmsCena'] : 0),
                'mmsMainTime' => (isset($area[$row['KEDI_KODS']]['mmsMainTime'])? $area[$row['KEDI_KODS']]['mmsMainTime'] : 0),
                'mmsOverTime' => '0',
                'planCena' => $row['CENA_KOPA_PLAN'],
                'planMainTime' => $row['PAMATSTUNDAS_PLAN'],
                'planOverTime' => $row['VIRSSTUNDAS_PLAN'],
                'difCena' => isset($area[$row['KEDI_KODS']]['mmsCena'])? number_format(($row['CENA_KOPA_PLAN'] - $area[$row['KEDI_KODS']]['mmsCena'] ),2,'.','') : '0.00',
                'difMainTime' => isset($area[$row['KEDI_KODS']]['mmsMainTime'])?number_format(($row['PAMATSTUNDAS_PLAN'] - $area[$row['KEDI_KODS']]['mmsMainTime'] ),2,'.',''):'0.00',
                'difOverTime' => isset($area[$row['KEDI_KODS']]['mmsOverTime'])?number_format(($row['VIRSSTUNDAS_PLAN'] - $area[$row['KEDI_KODS']]['mmsOverTime']),2,'.',''):'0.00',
                'damageCena' => $row['CENA_KOPA_DAMAGE'],
                'damageMainTime' => $row['PAMATSTUNDAS_DAMAGE'],
                'damageOverTime' => $row['VIRSSTUNDAS_DAMAGE'],
                'defectCena' => $row['CENA_KOPA_DEFECT'],
                'defectMainTime' => $row['PAMATSTUNDAS_DEFECT'],
                'defectOverTime' => $row['VIRSSTUNDAS_DEFECT'],
                'stihijaCena' => $row['CENA_KOPA_STIHIJA'],
                'stihijaMainTime' => $row['PAMATSTUNDAS_STIHIJA'],
                'stihijaOverTime' => $row['VIRSSTUNDAS_STIHIJA'],
                'notPlanCena' => number_format(($row['CENA_KOPA_DAMAGE']+$row['CENA_KOPA_DEFECT']+$row['CENA_KOPA_STIHIJA']),2,'.',''),
                'notPlanMainTime' => number_format(($row['PAMATSTUNDAS_DAMAGE']+$row['PAMATSTUNDAS_DEFECT']+$row['PAMATSTUNDAS_STIHIJA']),2,'.',''),
                'notPlanOverTime' => number_format(($row['VIRSSTUNDAS_DAMAGE']+$row['VIRSSTUNDAS_DEFECT']+$row['VIRSSTUNDAS_STIHIJA']),2,'.',''),
                'notPlanProcentCena' => (($row['CENA_KOPA_PLAN'] == 0 && ($row['CENA_KOPA_DAMAGE']+$row['CENA_KOPA_DEFECT']+$row['CENA_KOPA_STIHIJA']) == 0)? 0 : (($row['CENA_KOPA_PLAN'] == 0) ? 100 : number_format((($row['CENA_KOPA_DAMAGE']+$row['CENA_KOPA_DEFECT']+$row['CENA_KOPA_STIHIJA'])/$row['CENA_KOPA_PLAN'])*100,2,'.',''))),
                'notPlanProcentMainTime' => (($row['PAMATSTUNDAS_PLAN'] == 0 && ($row['PAMATSTUNDAS_DAMAGE']+$row['PAMATSTUNDAS_DEFECT']+$row['PAMATSTUNDAS_STIHIJA']) == 0) ? 0 : (($row['PAMATSTUNDAS_PLAN'] == 0) ? 100 : number_format((($row['PAMATSTUNDAS_DAMAGE']+$row['PAMATSTUNDAS_DEFECT']+$row['PAMATSTUNDAS_STIHIJA'])/$row['PAMATSTUNDAS_PLAN'])*100,2,'.',''))),
                'notPlanProcentOverTime' => (($row['VIRSSTUNDAS_PLAN'] == 0 && ($row['VIRSSTUNDAS_DAMAGE']+$row['VIRSSTUNDAS_DEFECT']+$row['VIRSSTUNDAS_STIHIJA']) == 0) ? 0 : (($row['VIRSSTUNDAS_PLAN'] == 0) ? 100 : number_format((($row['VIRSSTUNDAS_DAMAGE']+$row['VIRSSTUNDAS_DEFECT']+$row['VIRSSTUNDAS_STIHIJA'])/$row['VIRSSTUNDAS_PLAN'])*100,2,'.',''))),
                'totalCena' => number_format(($row['CENA_KOPA_PLAN']+$row['CENA_KOPA_DAMAGE']+$row['CENA_KOPA_DEFECT']+$row['CENA_KOPA_STIHIJA']),2,'.',''),
                'totalMainTime' => number_format(($row['PAMATSTUNDAS_PLAN']+$row['PAMATSTUNDAS_DAMAGE']+$row['PAMATSTUNDAS_DEFECT']+$row['PAMATSTUNDAS_STIHIJA']),2,'.',''),
                'totalOverTime' => number_format(($row['VIRSSTUNDAS_PLAN']+$row['VIRSSTUNDAS_DAMAGE']+$row['VIRSSTUNDAS_DEFECT']+$row['VIRSSTUNDAS_STIHIJA']),2,'.','')
              );
            }
        }
        ksort($area);

        $searchCr = array();
        $sCr = explode("^", $sCriteria);
        if(isset($sCr[0]) && $sCr[0] != -1  && isset($sCr[1]) && $sCr[1] != -1)
        {
           $searchCr[] = array('label' =>  text::get('YEAR'), 'value' => $sCr[0].'-'.$sCr[1]);
        }
        if(isset($sCr[2]) && $sCr[2] != -1  && isset($sCr[3]) && $sCr[3] != -1)
        {
           $searchCr[] = array('label' =>  text::get('MONTH'), 'value' => dtime::getMonthName($sCr[2]).'-'.dtime::getMonthName($sCr[3]));
        }
        if(isset($sCr[4]) && $sCr[4] != -1)
        {
           $searchCr[] = array('label' =>  text::get('RCD_REGION'), 'value' => $sCr[4]);
        }
        if(isset($sCr[5]) && $sCr[5] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGL_DV_AREA'), 'value' => dbProc::getDVAreaName($sCr[5]));
        }
        if(isset($sCr[6]) && $sCr[6] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ED_REGION'), 'value' => $sCr[6]);
        }
        if(isset($sCr[7]) && $sCr[7] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ED_IECIKNIS'), 'value' => dbProc::getEDAreaName($sCr[7]));
        }

        if(isset($sCr[8]) && $sCr[8] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_SOURCE_OF_FOUNDS'), 'value' => dbProc::getSourceOfFoundsName($sCr[8]));
        }
        if(isset($sCr[9]) && $sCr[9] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_VOLTAGE'), 'value' => dbProc::getVoltageName($sCr[9]));
        }
        if(isset($sCr[10]) && $sCr[10] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ACT_OUNER'), 'value' => dbProc::getUserName($sCr[10]));
        }
        if(isset($sCr[11]) && $sCr[11] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_ACT_TYPE'), 'value' => dbProc::getActTypeName($sCr[11]));
        }
        if(isset($sCr[12]) && $sCr[12] != -1)
        {
           $searchCr[] = array('label' =>  text::get('MMS_WORK_FINISHED'), 'value' => (($sCr[12] == 1) ? text::get('YES'): text::get('NO')));
        }
        $status = explode("*", $sCr[14]);
        $statusString = '';
        if($status[0] == -1)
        {
           $statusString = text::get('ALL_STATUS');
        }
        else
        {
          foreach($status as $s)
          {
              $statusInfo = dbProc::getKrfkInfoByName($s);
              $statusString .= $statusInfo['KRFK_NOZIME'].",";
          }
          $statusString  = substr($statusString, 0, -1);
        }
        $searchCr[] = array('label' =>  text::get('STATUS'), 'value' => $statusString);
        if(isset($sCr[15]) && $sCr[15] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_OBJECT'), 'value' => dbProc::getObjectName($sCr[15]));
        }
        if(isset($sCr[16]) && $sCr[16] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_OTHER_PAYMENTS'), 'value' => (($sCr[16] == 1) ? text::get('YES'): text::get('NO')));
        }
        if(isset($sCr[17]) && $sCr[17] != -1)
        {
           $searchCr[] = array('label' =>  text::get('OVER_TIME'), 'value' => (($sCr[17] == 1) ? text::get('REPORTS_WITH_OWER_TIME'): text::get('REPORTS_WITHOUT_OWER_TIME')));
        }
        if(isset($sCr[18]) && $sCr[18] != -1)
        {
           $searchCr[] = array('label' =>  text::get('PROBLEM_OBJECT'), 'value' => (($sCr[18] == 1) ? text::get('PRIORITY_PLAN_OBJECT'): text::get('NORMAL_PLAN_OBJECT')));
        }
        if(isset($sCr[19]) && $sCr[19] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ED_SECTION'), 'value' => urldecode($sCr[19]));
        }
		include('f.rpt.p.12.tpl');
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
