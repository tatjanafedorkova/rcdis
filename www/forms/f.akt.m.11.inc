<?


        // get user info
      	$userInfo = dbProc::getUserInfo($userId);
        $isAdmin=userAuthorization::isAdmin();
        $isEdUser = dbProc::isUserInRole($userId, ROLE_ED_USER);
      	if(count($userInfo) >0 )
      	{
      		$user = $userInfo[0];
       	}

         // get favorit
        $favorit=array();
        $krfk = dbProc::getKrfkInfo(KRFK_MMS_ALL_ID);
        if($krfk !== false)
        {

          $res=dbProc::getFavoritTypeDefinitionList($userId, $krfk['KRFK_VERTIBA']);
          if (is_array($res))
          {
              foreach ($res as $row)
         	    {
         		    $favorit[$row['LTFV_ID']]=$row['LTFV_NOSAUKUMS'];
         	    }
          }

        }
        unset($oPopLink);
        unset($res);

        $searcCriteria = array();

       // if operation
    	if ($oForm -> isFormSubmitted())
    	{
           $searcCriteria['yearFrom'] = $oForm->getValue('yearFrom');
           $searcCriteria['yearUntil'] = $oForm->getValue('yearUntil');
           $searcCriteria['monthFrom'] = $oForm->getValue('monthFrom');
           $searcCriteria['monthUntil'] = $oForm->getValue('monthUntil');
           $searcCriteria['favorit']  = $oForm->getValue('favorit');
           dbProc::replaceSearchResult($searchName,$userId,
                                $searcCriteria['yearFrom'],
                                $searcCriteria['yearUntil'],
                                $searcCriteria['monthFrom'],
                                $searcCriteria['monthUntil'],
                                '','','','','','', '', '', '', '', '', '','', '', '',
                                $searcCriteria['favorit'] ,
                                '', '', '', '', ''
                                );

           $cr = '';
           foreach($searcCriteria as $s)
           {
             $cr .= $s.'^';
           }
           $cr = substr($cr, 0, -1);
           $oListLink->addPrm('search', $cr);
           $actListLink=$oListLink ->getQuery();
           $oForm->addElement('static','jsRefresh2','','<script>reloadFrame(2, "'.$actListLink.'");</script>');
           unset($oListLink);
    	}
        else
        {
           if (reqVar::get('isReturn') == 1)
            {
                $searchCritery = dbProc::getSearchCritery($userId);
           	    if(count($searchCritery) >0 )
      	        {
      		        $critery = $searchCritery[0];
       	        }
            }
           $searcCriteria['yearFrom'] = isset($critery['yearFrom'])? $critery['yearFrom'] :dtime::getCurrentYear();
           $searcCriteria['yearUntil'] = isset($critery['yearUntil'])? $critery['yearUntil'] :dtime::getCurrentYear();
           $searcCriteria['monthFrom'] = isset($critery['monthFrom'])? $critery['monthFrom'] :dtime::getCurrentMonth();
           $searcCriteria['monthUntil'] = isset($critery['monthUntil'])? $critery['monthUntil'] :dtime::getCurrentMonth();
           $searcCriteria['favorit'] = isset($critery['favoritMaterial'])? $critery['favoritMaterial'] :'';
        }

        $oForm -> addElement('label', 'year',  text::get('YEAR'), '');
        $oForm -> addElement('select', 'yearFrom',  text::get('FROM'), $searcCriteria['yearFrom'], 'style="width:50;"', '', '', dtime::getYearArray());
        $oForm -> addElement('select', 'yearUntil',  text::get('UNTIL'), $searcCriteria['yearUntil'], 'style="width:50;"', '', '', dtime::getYearArray());

        $oForm -> addElement('label', 'month',  text::get('MONTH'), '');
        $oForm -> addElement('select', 'monthFrom',  text::get('FROM'), $searcCriteria['monthFrom'], 'style="width:70;"', '', '', dtime::getMonthArray());
        $oForm -> addElement('select', 'monthUntil',  text::get('UNTIL'), $searcCriteria['monthUntil'], 'style="width:70;"', '', '', dtime::getMonthArray());
        $oForm -> addElement('select', 'favorit',  text::get('SINGLE_MMS'), $searcCriteria['favorit'], '', '', '', $favorit);
               	// form buttons
       	$oForm -> addElement('submitImg', 'search', text::get('SEARCH'), 'img/btn_meklet.gif', 'width="70" height="20" onClick="setPosition1(\'loading\'); return true;"');

        $oForm -> makeHtml();
		include('f.akt.m.11.tpl');
?>