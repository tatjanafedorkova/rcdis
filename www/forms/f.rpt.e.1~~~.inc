<?
   $sCriteria = reqVar::get('search');
   $sOrder = reqVar::get('sortOrder');

   $searchCr = array();
        $sCr = explode("^", $sCriteria);
        if(isset($sCr[0]) && $sCr[0] != -1  && isset($sCr[1]) && $sCr[1] != -1)
        {
           $searchCr[] = text::get('YEAR').': '.$sCr[0].'-'.$sCr[1];
        }
        if(isset($sCr[2]) && $sCr[2] != -1  && isset($sCr[3]) && $sCr[3] != -1)
        {
           $searchCr[] = text::get('MONTH').': '.dtime::getMonthName($sCr[2]).'-'.dtime::getMonthName($sCr[3]);
        }
        if(isset($sCr[4]) && $sCr[4] != -1)
        {
           $searchCr[] = text::get('RCD_REGION').': '.$sCr[4];
        }
        if(isset($sCr[5]) && $sCr[5] != -1)
        {
           $searchCr[] = text::get('SINGL_DV_AREA').': '.dbProc::getDVAreaName($sCr[5]);
        }
        if(isset($sCr[6]) && $sCr[6] != -1)
        {
           $searchCr[] = text::get('ED_REGION').': '.$sCr[6];
        }
        if(isset($sCr[7]) && $sCr[7] != -1)
        {
           $searchCr[] = text::get('ED_IECIKNIS').': '.dbProc::getEDAreaName($sCr[7]);
        }

        if(isset($sCr[8]) && $sCr[8] != -1)
        {
           $searchCr[] = text::get('SINGLE_SOURCE_OF_FOUNDS').': '.dbProc::getSourceOfFoundsName($sCr[8]);
        }
        if(isset($sCr[9]) && $sCr[9] != -1)
        {
           $searchCr[] = text::get('SINGLE_VOLTAGE').': '.dbProc::getVoltageName($sCr[9]);
        }
        if(isset($sCr[10]) && $sCr[10] != -1)
        {
           $searchCr[] = text::get('ACT_OUNER').': '.dbProc::getUserName($sCr[10]);
        }
        if(isset($sCr[11]) && $sCr[11] != -1)
        {
           $searchCr[] = text::get('SINGLE_ACT_TYPE').': '.dbProc::getActTypeName($sCr[11]);
        }
        if(isset($sCr[12]) && $sCr[12] != -1)
        {
           $searchCr[] = text::get('MMS_WORK_FINISHED').': '.(($sCr[12] == 1) ? text::get('YES'): text::get('NO'));
        }
        $status = explode("*", $sCr[14]);
        $statusString = '';
        if($status[0] == -1)
        {
           $statusString = text::get('ALL_STATUS');
        }
        else
        {
          foreach($status as $s)
          {
              $statusInfo = dbProc::getKrfkInfoByName($s);
              $statusString .= $statusInfo['KRFK_NOZIME'].",";
          }
          $statusString  = substr($statusString, 0, -1);
        }
        $searchCr[] = text::get('STATUS').': '.$statusString;

   $excel = '';

   $excel .= text::get('REPORT_MAIN_ACT').';';
   $excel .= implode('| ',$searchCr).';';

   $excel .= text::get('ACT_NUMBER').',';
   $excel .= text::get('SINGLE_ACT_TYPE').',';
   $excel .= text::get('STATUS').'</td>';
   $excel .= text::get('SINGLE_VOLTAGE').'</td>';
   $excel .= text::get('SINGLE_SOURCE_OF_FOUNDS').'</td>';
   $excel .= text::get('SINGLE_OBJECT').'</td>';
   $excel .= text::get('MAIN_DESIGNATION').',';
   $excel .= text::get('MONTH').',';
   $excel .= text::get('OBJECT_IS_FINISHED').',';
   $excel .= text::get('BASE_TIME').',';
   $excel .= text::get('OVER_TIME').',';
   $excel .= text::get('REPORT_MATERIAL_PRICE_TOTAL').',';
   $excel .= text::get('OTHER_PAYMENTS').',';
   $excel .= text::get('OVERTIME_KOEFICENT').',';
   $excel .= text::get('COMMENT').',';
   $excel .= text::get('ACT_OUNER').',';
   $excel .= text::get('RCD_REGION').',';
   $excel .= text::get('SINGL_DV_AREA').',';
   $excel .= text::get('ED_REGION').',';
   $excel .= text::get('ED_IECIKNIS').'\r\n';

        $res = dbProc::getActFullList($sCriteria, $sOrder);
        $pamatstundas = 0;
        $virsstundas = 0;
        $summa = 0;
        $currentRow = 4;
        if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{

              $excel .= $row['NUMURS'].',';
              $excel .= $row['TYPE'].',';
              $excel .= $row['STATUS_NAME'].',';
              $excel .= $row['KSRG_NOSAUKUMS'].',';
              $excel .= $row['KFNA_NOSAUKUMS'].',';
              $excel .= $row['KOBJ_NOSAUKUMS'].',';
              $excel .= $row['RAKT_OPERATIVAS_APZIM'].',';
              $excel .= dtime::getMonthName($row['RAKT_MENESIS']).',';
              $excel .= $row['RAKT_IR_OBJEKTS_PABEIGTS'].',';
              $excel .= $row['PAMATSTUNDAS'].',';
              $excel .= $row['VIRSSTUNDAS'].',';
              $excel .= $row['CENA_KOPA'].',';
              $excel .= $row['RAKT_CITAS_IZMAKSAS'].',';
              $excel .= $row['RAKT_VIRSSTUNDU_KOEF'].',';
              $excel .= $row['RAKT_PIEZIMES'].',';
              $excel .= $row['AUTORS'].',';
              $excel .= $row['KDVI_REGIONS'].',';
              $excel .= $row['DV'].',';
              $excel .= $row['KEDI_REGIONS'].',';
              $excel .= $row['ED'].'\r\n';

               $pamatstundas = $pamatstundas + $row['PAMATSTUNDAS'];
               $virsstundas = $virsstundas + $row['VIRSSTUNDAS'];
               $summa = $summa + $row['CENA_KOPA'];
               $currentRow ++;
			}
		}


        $excel .= ',,,,,,,,,'.text::get('TOTAL').':'.',';
        $excel .= number_format($pamatstundas, 2, '.', '').',';
        $excel .= number_format($virsstundas, 2, '.', '').',';
        $excel .= number_format($summa, 2, '.', '').'\r\n';

  // redirect output to client browser

  header('Content-Type: application/csv;');
  header('Content-Disposition: attachment;filename="atskaite.csv"');
  header('Cache-Control: max-age=0');

  echo $excel;

?>