﻿<body class="print">
<h4 align="right"><?= text::toUpper(text::get('PROJECT')); ?></h4>
<div class="print_header"><?= text::get('EXPORT_TITLE').' '.$act['NUMURS'].' '.(isset($act['RAKT_NUM_POSTFIX'])? text::get('SINGLE_MMS').': '.$act['MMS'] : ''); ?><br />
<?= text::get('EXPORT_ED_IECIRKNIS').' '.$act['STRUKTURVIENIBA']; ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >
    <tr>
        <td width="15%" ><b><i><?= text::get('ACT_NUMBER').': '; ?></i></b></td>
        <td width="35%" ><?= ''.$act['NUMURS']; ?></td>
        <td width="15%" ><b><i><?= text::get('SINGLE_OBJECT').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['OBJECT']; ?></td>
    </tr>
    <tr>
        <td width="15%" ><b><i><?= text::get('SINGLE_ACT_TYPE').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['TYPE']; ?></td>
        <td width="15%" ><b><i><?= text::get('SINGLE_SOURCE_OF_FOUNDS').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['FOUNDS']; ?></td>
    </tr>
    <tr>
        <td width="15%" ><b><i><?= text::get('EXPORT_MAIN_DESIGNATION').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['RAKT_OPERATIVAS_APZIM']; ?></td>
        <td width="15%" ></td>
        <td width="35%" ></td>
    </tr>
    <tr>
        <td width="15%" ><b><i><?= text::get('SINGLE_VOLTAGE').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['SPRIEGUMS']; ?></td>
        <td width="15%" ><b><i><?= text::get('EXPORT_OVERTIME_KOEFICENT').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['RAKT_VIRSSTUNDU_KOEF']; ?></td>
    </tr>
    <tr>
        <td width="15%" ><b><i><?= text::get('OTHER_PAYMENTS').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['RAKT_CITAS_IZMAKSAS']; ?></td>
        <td width="15%" ><b><i><?= text::get('KOEFICENT').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['RAKT_KOEFICENT']; ?></td>
    </tr>
    <tr>
        <td width="15%" ><b><i><?= text::get('EXPORT_OTHER_PAYMENT_DESCRIPTION').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['RAKT_CITAS_IZMAKSAS_PIEZIMES']; ?></td>
        <td width="15%" ><b><i><?= text::get('MONTH').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.dtime::getMonthName($act['RAKT_MENESIS']); ?></td>
    </tr>
    <tr>
        <td width="15%" ><b><i><?= text::get('ACT_NUMBER').': '; ?></i></b></td>
        <td width="35%" ><?= ''.$act['NUMURS']; ?></td>
        <td width="15%" ><b><i><?= text::get('SINGLE_OBJECT').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['OBJECT']; ?></td>
    </tr>
        <tr>
        <td width="15%" >&nbsp;</td>
        <td width="35%" >&nbsp;</td>
        <td width="15%" ><b><i><?= text::get('TO_ID').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['RAKT_TO_ID']; ?></td>        
    </tr>
    <tr>
     <td colspan="4">&nbsp;</td>
  </tr>
  <tr>
     <td colspan="4"><?= $act['WORK_TITLE']; ?></td>
  </tr>
  <tr>
     <td colspan="4">&nbsp;</td>
  </tr>

</table>
<br />
<div class="print_header"><?= text::get('TAB_WORKS'); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >

		<tr>
            <td width="12%" class="print_table_header"><?= text::get('CHIPHER'); ?></td>
			<td width="40%" class="print_table_header"><?= text::get('EXPORT_CALCULATION_NAME'); ?></td>
            <td width="12%" class="print_table_header"><?=text::get('UNIT_OF_MEASURE');?></td>
            <td width="12%" class="print_table_header"><?=text::get('AMOUNT');?></td>
            <td width="12%" class="print_table_header"><?=text::get('EXPORT_MAN_HOUR_STANDART');?></td>

            <td width="12%" class="print_table_header"><?= text::get('EXPORT_MAN_HOUR_STANDART_TOTAL'); ?></td>
 		</tr>

<?
	if (is_array($aWorkDb) && count($aWorkDb) > 0)
	{
		foreach ($aWorkDb as $i => $work)
		{
?>
			<tr >
                <td align="center" class="print_table_data">&nbsp;<?= $work['DRBI_KKAL_SHIFRS']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $work['DRBI_KKAL_NOSAUKUMS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $work['DRBI_MERVIENIBA']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $work['DRBI_DAUDZUMS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $work['DRBI_KKAL_NORMATIVS']; ?></td>

                <td align="center" class="print_table_data">&nbsp;<?= $work['DRBI_CILVEKSTUNDAS']; ?></td>
			</tr>
<?
		}
?>
        <tr>
             <td align="right" class="print_table_total" colspan="5"><?=text::get('TOTAL');?>:</td>
             <td align="center" class="print_table_total"><?= $workTotal; ?></td>
        </tr>
<?
	}
?>
</table>
<br />
<? if($isEpla == 0) { ?>
<div class="print_header"><?= text::get(TAB_MATERIAL); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >

		<tr>
            <td width="12%" class="print_table_header"><?= text::get('CODE'); ?></td>
			<td width="40%" class="print_table_header"><?= text::get('NAME'); ?></td>
            <td width="12%" class="print_table_header"><?=text::get('UNIT_OF_MEASURE');?></td>
            <td width="12%" class="print_table_header"><?=text::get('PRICE');?></td>
            <td width="12%" class="print_table_header"><?=text::get('AMOUNT');?></td>
            <td width="12%" class="print_table_header"><?= text::get('PRICE_TOTAL'); ?></td>
 		</tr>

<?
	if (is_array($aMaterialDb) && count($aMaterialDb) > 0)
	{
		foreach ($aMaterialDb as $i => $material)
		{
?>
			<tr >
                <td align="center" class="print_table_data">&nbsp;<?= $material['MATR_KODS']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $material['MATR_NOSAUKUMS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $material['MATR_MERVIENIBA']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $material['MATR_CENA']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $material['MATR_DAUDZUMS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $material['MATR_CENA_KOPA']; ?></td>
			</tr>
<?
		}
?>
        <tr>
             <td align="right" class="print_table_total" colspan="5"><?=text::get('TOTAL');?>:</td>
             <td align="center" class="print_table_total"><?= $materialTotal; ?></td>
        </tr>
<?
	}
?>
</table>
<br />
<? } ?>
<? if(true/*(isset($_SESSION[CURRENT_YEAR])  && $_SESSION[CURRENT_YEAR] == 2010) || date('Y') == 2010*/)  {?>
<div class="print_header"><?= text::get(TAB_TRANSPORT); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >

		<tr>
            <td width="10%" class="print_table_header"><?= text::get('NATION_NUMBER'); ?></td>
			<td width="30%" class="print_table_header"><?= text::get('GROUP_TITLE'); ?></td>
            <td width="30%" class="print_table_header"><?=text::get('SUBGROUP_TITLE');?></td>
            <td width="10%" class="print_table_header"><?=text::get('TRANSPORT_KM');?></td>
            <td width="10%" class="print_table_header"><?=text::get('TRANSPORT_AVTO_TIME');?></td>
            <td width="10%" class="print_table_header"><?= text::get('TRANSPORT_WORK_TIME'); ?></td>
 		</tr>

<?
	if (is_array($aTransportDb) && count($aTransportDb) > 0)
	{
		foreach ($aTransportDb as $i => $transport)
		{
?>
			<tr >
                <td align="left" class="print_table_data">&nbsp;<?= $transport['TRNS_VALSTS_NUMURS']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $transport['TRNS_GRUPAS_NOSAUKUMS']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $transport['TRNS_APAKSGRUPAS_NOSAUKUMS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $transport['TRNS_KM']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $transport['TRNS_STUNDAS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $transport['TRNS_DARBA_STUNDAS']; ?></td>
			</tr>
<?
		}
?>
        <tr>
             <td align="right" class="print_table_total" colspan="3"><?=text::get('TOTAL');?>:</td>
             <td align="center" class="print_table_total"><?= $transportKm; ?></td>
             <td align="center" class="print_table_total"><?= $transportStundas; ?></td>
             <td align="center" class="print_table_total"><?= $transportTotal; ?></td>
        </tr>
<?
	}
?>
</table>
<br />
<div class="print_header"><?= text::get(TAB_PERONAL); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >

		<tr>
            <td width="15%" class="print_table_header"><?= text::get('RCD_KODS'); ?></td>
			<td width="40%" class="print_table_header"><?= text::get('USER_NAME').' '.text::get('USER_SURNAME'); ?></td>
            <td width="15%" class="print_table_header"><?=text::get('PROPORTION');?></td>
            <td width="15%" class="print_table_header"><?=text::get('BASE_TIME');?></td>
            <td width="15%" class="print_table_header"><?=text::get('OVER_TIME');?></td>
 		</tr>

<?
	if (is_array($aCustomerDb) && count($aCustomerDb) > 0)
	{
		foreach ($aCustomerDb as $i => $customer)
		{
?>
			<tr >
                <td align="center" class="print_table_data">&nbsp;<?= $customer['PRSN_RCD_KODS']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $customer['PRSN_VARDS_UZVARDS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $customer['PRSN_PROPORCIJA']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $customer['PRSN_PAMATSTUNDAS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $customer['PRSN_VIRSSTUNDAS']; ?></td>
			</tr>
<?
		}
?>
        <tr>
             <td align="right" class="print_table_total" colspan="3"><?=text::get('TOTAL');?>:</td>
             <td align="center" class="print_table_total"><?= $customerTotalBase; ?></td>
             <td align="center" class="print_table_total"><?= $customerTotalOver; ?></td>
        </tr>
<?
	}
?>
</table>
<br />
<?}?>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >
    <tr>
        <td><b><i><?= text::get('EXPORT_ACT_AUTHOR').': '.$act['AUTORS']; ?></i></b></td>
    </tr>
    <tr>
        <td><b><i><?= text::get('DATE').': '.date('d.m.Y H:i:s'); ?></i></b></td>
    </tr>
    <tr>
        <td><b><i><?= text::get('EXPORT_FOOTER'); ?></i></b></td>
    </tr>
</table>
</body>