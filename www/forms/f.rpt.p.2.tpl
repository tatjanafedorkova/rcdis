﻿<body class="print">
<div class="print_header"><?= text::get('REPORT_CUSTOMER_WORK_TIME'); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >
<?
  foreach($searchCr as $i=>$s)
  {
    if($i%3 == 0)
    {
      ?>
        <tr>
      <?
    }
    ?>
      <td align="right" class="print_table_total" width="13%"><?=$s['label'];?>:</td>
      <td align="left" class="print_table_data" width="20%"><?=$s['value'];?></td>
    <?
  }
?>
</table>
<br />
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >

		<tr>
			<td width="15%" class="print_table_header"><?= text::get('WORKING_PLACE_NUMBER'); ?></td>
			<td width="15%" class="print_table_header"><?= text::get('RCD_KODS'); ?></td>
            <td width="40%" class="print_table_header"><?=text::get('USER_SURNAME');?> <?=text::get('USER_NAME');?></td>
            <td width="10%" class="print_table_header"><?=text::get('BASE_TIME');?></td>
            <td width="10%" class="print_table_header"><?=text::get('OVER_TIME');?></td>
	        <td width="10%" class="print_table_header"><?=text::get('ROUT_TIME');?></td>

 		</tr>

<?
	if (is_array($res) && count($res) > 0)
	{
		foreach ($res as $row)
		{
?>
			<tr >
			   	<td align="center" class="print_table_data"><?= $row['PRSN_DARBA_VIETA']; ?></td>
                <td align="center" class="print_table_data"><?= $row['PRSN_RCD_KODS']; ?></td>
                <td align="left" class="print_table_data"><?= $row['PRSN_VARDS_UZVARDS']; ?></td>
                <td align="center" class="print_table_data"><?= $row['PAMATSTUNDAS']; ?></td>
                <td align="center" class="print_table_data"><?= $row['VIRSSTUNDAS']; ?></td>
		<td align="center" class="print_table_data"><?= $row['LAIKSCELA']; ?></td>


			</tr>
<?
		}
?>
        <tr>
             <td align="right" class="print_table_total" colspan="3"><?=text::get('TOTAL');?>:</td>
             <td align="center" class="print_table_total"><?= $pamatstundas; ?></td>
             <td align="center" class="print_table_total"><?= $virsstundas; ?></td>
		<td align="center" class="print_table_total"><?= $laikscela; ?></td>

        </tr>
<?
	}
?>

</table>

</body>