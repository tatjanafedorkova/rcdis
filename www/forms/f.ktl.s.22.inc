<?
//created by Tatjana Fedorkova
// Main form with XMLHTTP method load this code:
if (reqVar::get('xmlHttp') == 1)
{
	// unset previouse values of region
	?>
	for(i=document.all["subgroup"].length; i>=0; i--)
	{
		document.all["subgroup"].options[i] = null;
	}
	<?
	//if is set edRegion
	if (reqVar::get('group') !== false && reqVar::get('group') != '')
	{

		$res=dbProc::getTransportSubGroupList(reqVar::get('group'));
		if(iconv('UTF-8', 'ISO-8859-1//IGNORE', reqVar::get('group')) == iconv('UTF-8', 'ISO-8859-1//IGNORE', INV_GROUP_WORK))
		{ 
		?>
			document.all["subgroup"].options[0] = new Option('<?=text::get('EMPTY_SELECT_OPTION');?>', '...');
		<?
		} else {
		?>
			document.all["subgroup"].options[0] = new Option('<?=text::get('EMPTY_SELECT_OPTION');?>', '');
		<?
		}
		if (is_array($res) && count($res)>0)
		{			
			$i = 1;
			foreach ($res as $row)
			{
				// feel options array
				?>
				document.all["subgroup"].options[<?=$i;?>] = new Option( '<?=$row['SUBGROUP'];?>', '<?=$row['CODE'];?>');
				<?
				$i++;
			}
		}
	}
	
	exit;
}
$userId = userAuthorization::getUserId();
$isEdUser = (dbProc::isUserInRole($userId, ROLE_ED_USER) || dbProc::isUserInRole($userId, ROLE_AUDIT_USER));
if (userAuthorization::isAdmin() || $isEdUser )
{
	$editMode = reqVar::get('editMode');
	// bottom frame
	// if insert/update

    if($editMode)
	{
		$investicijasId = reqVar::get('investicijasId');

		$oLink=new urlQuery();
		$oLink->addPrm(FORM_ID, 'f.ktl.s.22');
		$oLink->addPrm('editMode', 1 );
		// form inicialization
		$oForm = new Form('frmMain','post',$oLink->getQuery());
		unset($oLink);

		// if operation is success, show success message and redirect top frame
		if (reqVar::get('successMessage'))
		{
			$oForm->addSuccess(reqVar::get('successMessage'));
			$oForm->addElement('static','jsRefresh2','','<script>refreshFrame(1);</script>');
		}
		$subgroup=array();
		// get grupas list
		$group=array();
		$group['']=text::get('EMPTY_SELECT_OPTION');
		$group[text::get('INVESTICIJAS_PRICES_WORK_GROUP')]= text::get('INVESTICIJAS_PRICES_WORK_GROUP');
		$res=dbProc::getTransportGroupList();
		if (is_array($res))
		{
			foreach ($res as $row)
			{
				$group[$row['GROUP']]=$row['GROUP'];
			}
		}
		unset($res);
		
		if($investicijasId != false)
		{
			// get info about voltage
			$investicijasPricesInfo = dbProc::getInvesticijasPricesList($investicijasId);
			
			if(count($investicijasPricesInfo)>0)
			{
				$investicija = $investicijasPricesInfo[0];
			}

			// get transporta apakškategorijas			
			if(iconv('UTF-8', 'ISO-8859-1//IGNORE', $investicija['INVC_GRUPA']) == iconv('UTF-8', 'ISO-8859-1//IGNORE', INV_GROUP_WORK))
			{
				$subgroup['...']=text::get('EMPTY_SELECT_OPTION');
			} else {
				$subgroup['']=text::get('EMPTY_SELECT_OPTION');
			}
			$res=dbProc::getTransportSubGroupList($investicija['INVC_GRUPA']);
			if (is_array($res))
			{
				foreach ($res as $row)
				{
					$subgroup[$row['CODE']]=$row['SUBGROUP'];
				}
			}
			unset($res);
			
        }

		// form elements
		$oForm -> addElement('hidden', 'action', '');
		$oForm -> addElement('hidden', 'investicijasId', null, isset($investicija['INVC_ID'])? $investicija['INVC_ID']:'');
		// xmlHttp link
		$oLink=new urlQuery();
		$oLink->addPrm(DONT_USE_GLB_TPL, 1);
		$oLink->addPrm(FORM_ID, 'f.ktl.s.22');
   		$oLink->addPrm('editMode', 1);
		$oForm -> addElement('select', 'group',  text::get('GROUP_TITLE'), isset($investicija['INVC_GRUPA'])?$investicija['INVC_GRUPA']:'', 'onChange="eval(xmlHttpGetValue(\''.$oLink->getQuery().'&xmlHttp=1&group=\'+this.value));" tabindex=1'.(($userId == false || $isEdUser)? ' disabled ': ''), '', '', $group);
		$oForm -> addRule('group', text::get('ERROR_REQUIRED_FIELD'), 'required');
		unset($oLink);
		$oForm -> addElement('select', 'subgroup',  text::get('SUBGROUP_TITLE'), isset($investicija['INVC_APAKSGRUPAS_KODS'])?$investicija['INVC_APAKSGRUPAS_KODS']:'', 'tabindex=2'.(($userId == false || $isEdUser)? ' disabled ': ''), '', '', $subgroup);
		$oForm -> addRule('group', text::get('ERROR_REQUIRED_FIELD'), 'required');

		$oForm -> addElement('text', 'title',  text::get('TITLE'), isset($investicija['INVC_NOSAUKUMS'])?$investicija['INVC_NOSAUKUMS']:'', 'tabindex=4 maxlength="500"' . (($isEdUser)?' disabled' : ''));
		$oForm -> addRule('title', text::get('ERROR_REQUIRED_FIELD'), 'required');

		$oForm -> addElement('text', 'measure',  text::get('UNIT_OF_MEASURE'), isset($investicija['INVC_MERVIENIBA'])?$investicija['INVC_MERVIENIBA']:'EUR', 'tabindex=5 maxlength="3"' . (($isEdUser)?' disabled' : ''));
		$oForm -> addRule('measure', text::get('ERROR_REQUIRED_FIELD'), 'required');

		$oForm -> addElement('text', 'km_price',  text::get('INVESTITION_KM'), isset($investicija['INVC_CENA_KM'])?$investicija['INVC_CENA_KM']:'0.000', 'tabindex=6 maxlength="6"'.(($isEdUser)?' disabled' : ''));
		$oForm -> addRule('km_price', text::get('ERROR_REQUIRED_FIELD'), 'required');
		$oForm -> addRule('km_price', text::get('ERROR_DOUBLE_VALUE'), 'double', 3);

		$oForm -> addElement('text', 'motor_price',  text::get('INVESTITION_MOTOR'), isset($investicija['INVC_CENA_MOTORST'])?$investicija['INVC_CENA_MOTORST']:'0.000', 'tabindex=7 maxlength="6"'.(($isEdUser)?' disabled' : ''));
		$oForm -> addRule('motor_price', text::get('ERROR_REQUIRED_FIELD'), 'required');
		$oForm -> addRule('motor_price', text::get('ERROR_DOUBLE_VALUE'), 'double', 3);

		$oForm -> addElement('text', 'work_price',  text::get('INVESTITION_WORK'), isset($investicija['INVC_CENA_DARBAST'])?$investicija['INVC_CENA_DARBAST']:'0.000', 'tabindex=8 maxlength="6"'.(($isEdUser)?' disabled' : ''));
		$oForm -> addRule('work_price', text::get('ERROR_REQUIRED_FIELD'), 'required');
		$oForm -> addRule('work_price', text::get('ERROR_DOUBLE_VALUE'), 'double', 3);

        $oForm -> addElement('checkbox', 'isActive',  text::get('IS_ACTIVE'), isset($investicija['INVC_IR_AKTIVS'])?$investicija['INVC_IR_AKTIVS']:1, 'tabindex=9'.(($isEdUser)?' disabled' : ''));

		// form buttons
        if(!$isEdUser)
        {
    		$oForm -> addElement('submitImg', 'add', text::get('ADD'), 'img/btn_pievienot.gif', 'width="70" height="20" onclick="setValue(\'action\',\''.OP_INSERT.'\');"');
    		$oForm -> addElement('submitImg', 'save', text::get('SAVE'), 'img/btn_saglabat.gif', 'width="70" height="20" onclick=" if (!isEmptyField(\'investicijasId\')) {setValue(\'action\',\''.OP_UPDATE.'\');} else {return false;}"');
    		$oForm -> addElement('clearImg', 'clear', text::get('CLEAR'), 'img/btn_attirit.gif', 'width="70" height="20"','','',array('onClick'=>'refreshButton(\'investicijasId\');parent.frame_1.unActiveRow();'));
            
         	$oForm -> addElement('static', 'jsButtonsControl', '', '
    			<script>
    				function refreshButton(name)
    				{
    					if (!isEmptyField(name))
    					{
    						document.all["save"].src="img/btn_saglabat.gif";							
    					}
    					else
    					{
							document.all["save"].src="img/btn_saglabat_disabled.gif";
							document.all["isActive"].checked=true;	
							for(i=document.all["subgroup"].length; i>=0; i--)
							{
								document.all["subgroup"].options[i] = null;
							}
    					}
    				}
    				refreshButton("investicijasId");
    			</script>
    		');
        }
		// if operation
		if ($oForm -> isFormSubmitted())
		{
			$check = true;
			$r = false;
			$checkRequiredFields = false;
		   
            // save
			if ($oForm->getValue('action')==OP_INSERT || ($oForm->getValue('action')==OP_UPDATE  && 
															is_numeric($oForm->getValue('investicijasId'))))
			{
								
    			if ($oForm -> getValue('group') && $oForm -> getValue('title') &&
					$oForm -> getValue('measure') && $oForm -> getValue('km_price') &&
					$oForm -> getValue('motor_price') && $oForm -> getValue('work_price') )
    			{
    				$checkRequiredFields = true;
    			}
    			if ($checkRequiredFields)
    			{

					// chech that mms-calculation with same requirement, chipper and trase not set
					if (dbProc::investicijasPricesGroupExists(
															$oForm->getValue('group'),
															$oForm->getValue('subgroup'),
                                                            ($oForm->getValue('action')==OP_UPDATE)?$oForm->getValue('investicijasId'):false)
                                                            )
					{
						$oForm->addError(text::get('ERROR_EXISTS_INVESTITION_CODE'));
						$check = false;
					}

					
					// if all is ok, do operation
					if($check)
					{


						$r=dbProc::saveInvesticijasPrices(($oForm->getValue('action')==OP_UPDATE?$oForm->getValue('investicijasId'):false),
										$oForm->getValue('group'),
										$oForm->getValue('subgroup'),
										$oForm->getValue('title'),
										$oForm->getValue('measure'),
										$oForm->getValue('km_price'),
										$oForm->getValue('motor_price'),
										$oForm->getValue('work_price'),
										$oForm->getValue('isActive', 0)
						);

						if (!$r)
						{
							$oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
						}
					}
				}
				else
				{
					$oForm->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
				}
			}
			else
			{
				$oForm->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
			}
            // if operation was compleated succefully, show success mesage and redirect current frame
    		if ($r)
    		{
    			$oLink=new urlQuery();
    			$oLink->addPrm(FORM_ID, 'f.ktl.s.22');
    			$oLink->addPrm('editMode', '1');
    			switch($oForm->getValue('action'))
    			{
    				case OP_INSERT:
    					$oLink->addPrm('successMessage', text::get('RECORD_WAS_ADDED'));
    					break;
    				case OP_UPDATE:
    					$oLink->addPrm('successMessage', text::get('RECORD_WAS_SAVED'));
    					break;
                    case OP_DELETE:
    					$oLink->addPrm('successMessage', text::get('FAVORITS_IS_CLENED'));
    					break;
    			}
    		   	RequestHandler::makeRedirect($oLink->getQuery());
    		}
		}

       	$oForm -> makeHtml();
		include('f.ktl.s.22.2.tpl');
	}
	// top frale
	else
	{
        // Check if this form was requested via xmlHttpRequest and search
    	if (isset($_GET['xml']) && isset($_GET['search_q']) && isset($_GET['search_c']))
    	{
    		ob_clean();
    		$q = $_GET['search_q'];
            if (isset($q) || $q == 0)
    		{
    			echo 'q = "'.trim($q).'";';
    		}
            else
            {
    			echo 'q = "";';
    		}
            $c = $_GET['search_c'];
            if ($c)
    		{
    			echo 'c = "'.$c.'";';
    		}
            else
            {
    			echo 'c = "";';
    		}
		    exit;
    	}
        if (isset($_GET['xml']) && isset($_GET['sort_k']) && isset($_GET['sort_o']))
    	{
    		ob_clean();
    		$o = $_GET['sort_o'];
            if (isset($o))
    		{
    			echo 'o = "'.$o.'";';
    		}
            else
            {
    			echo 'o = "";';
    		}
            $k = $_GET['sort_k'];
            if ($k)
    		{
    			echo 'k = "'.$k.'";';
    		}
            else
            {
    			echo 'k = "";';
    		}
		    exit;
    	}
        // set list title
        $title = text::toUpper(text::get('INVESTITION_PRICES'));
        // get search column list
        $columns=dbProc::getKlklName(KL_INVESTITION_PRICES);
        $searchText = ((reqVar::get('search') && reqVar::get('search') != '') || reqVar::get('search') == 0) ? reqVar::get('search') : false;
        $searchColumn = (reqVar::get('column') && reqVar::get('column') != '') ? reqVar::get('column') : false;
        $sortOrder = (reqVar::get('order') && reqVar::get('order') != '') ? reqVar::get('order') : false;
        $orderColumn = (reqVar::get('kol') && reqVar::get('kol') != '') ? reqVar::get('kol') : false;


        // default sort column
        $defaultColumn = dbProc::getKlklDefaultColumn(KL_INVESTITION_PRICES);
        
        // URL to search Zinojums by number.
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.ktl.s.22');
    	$oLink->addPrm('isNew', '1');

        // add search/order params to listing
        $oLink->addPrm('search', reqVar::get('search'));
        $oLink->addPrm('column', reqVar::get('column'));
        $oLink->addPrm('order', reqVar::get('order'));
        $oLink->addPrm('kol', reqVar::get('kol'));
        $searchLink = $oLink -> getQuery();

		// inicialising of listing object
		$amount=dbProc::getInvesticijasPricesCount($searchText,
                (($searchColumn === false) ? $defaultColumn : $searchColumn));
		$listing=new listing();
		$listing->setAmount($amount);

		$listingHtml=$listing->getHtml($oLink ->getQuery());
		unset($oLink);

		//link for frame2
		$oLink=new urlQuery();
		$oLink->addPrm(FORM_ID, 'f.ktl.s.22');
		$oLink->addPrm('editMode', 1);

		// gel list of users
		$res = dbProc::getInvesticijasPricesList( false,
                                        $listing->getStartPosition(),
                                        $listing->getAmountOnPage(),
                                        $searchText,
                                        (($searchColumn === false) ? $defaultColumn : $searchColumn),
                                        $sortOrder,
                                        $orderColumn);

		if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
				// add dv area Id to each link
				$oLink->addPrm('investicijasId', $row['INVC_ID']);
				$res[$i]['URL']=$oLink ->getQuery();
			}
		}
		unset($oLink);

		// inicial state of users number
		$number=$listing->getStartPosition()+1;

		include('f.ktl.s.x.1.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
