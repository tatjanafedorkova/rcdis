﻿<body class="print">
<div class="print_header"><?= text::get('REPORT_MAIN_ACT'); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_landscape" >
<?
  foreach($searchCr as $i=>$s)
  {
    if($i%3 == 0)
    {
      ?>
        <tr>
      <?
    }
    ?>
      <td align="right" class="print_table_total" width="13%"><?=$s['label'];?>:</td>
      <td align="left" class="print_table_data" width="20%"><?=$s['value'];?></td>
    <?
  }
?>
</table>
<br />
<table cellpadding="0" cellspacing="0" class="print_table_landscape" >

		<tr>
            <td width="6%" class="print_table_header"><?= text::get('ACT_NUMBER'); ?></td>
            <td width="6%" class="print_table_header"><?= text::get('ACT_NUM_POSTFIX'); ?></td>
			<td width="5%" class="print_table_header"><?= text::get('SINGLE_ACT_TYPE'); ?></td>
            <td width="5%" class="print_table_header"><?=text::get('STATUS');?></td>
            <td width="7%" class="print_table_header"><?=text::get('SINGLE_VOLTAGE');?></td>
            <td width="8%" class="print_table_header"><?=text::get('SINGLE_SOURCE_OF_FOUNDS');?></td>
            <td width="8%" class="print_table_header"><?= text::get('SINGLE_OBJECT'); ?></td>
			<td width="10%" class="print_table_header"><?= text::get('MAIN_DESIGNATION'); ?></td>
            <td width="5%" class="print_table_header"><?=text::get('MONTH');?></td>
            <td width="5%" class="print_table_header"><?= text::get('OBJECT_IS_FINISHED'); ?></td>
            <td width="5%" class="print_table_header"><?=text::get('BASE_TIME');?></td>
            <td width="5%" class="print_table_header"><?=text::get('OVER_TIME');?></td>
	        <td width="5%" class="print_table_header"><?=text::get('ROUT_TIME');?></td>
            <td width="5%" class="print_table_header"><?= text::get('REPORT_MATERIAL_PRICE_TOTAL'); ?></td>
            <td width="9%" class="print_table_header"><?= text::get('SINGL_DV_AREA'); ?></td>
            <td width="9%" class="print_table_header"><?= text::get('DATE'); ?></td>
 		</tr>

<?
	if (is_array($res) && count($res) > 0)
	{
		foreach ($res as $row)
		{
?>
			<tr >
                <td align="center" class="print_table_data">&nbsp;<?= $row['NUMURS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $row['POSTFIX']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $row['TYPE']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $row['STATUS_NAME']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $row['KSRG_NOSAUKUMS']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $row['KFNA_NOSAUKUMS']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $row['KOBJ_NOSAUKUMS']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $row['RAKT_OPERATIVAS_APZIM']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= dtime::getMonthName($row['RAKT_MENESIS']); ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= (($row['RAKT_IR_OBJEKTS_PABEIGTS'] == 1) ? text::get('YES') : text::get('NO')); ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $row['PAMATSTUNDAS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $row['VIRSSTUNDAS']; ?></td>
		<td align="center" class="print_table_data">&nbsp;<?= $row['LAIKSCELA']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $row['CENA_KOPA']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $row['DV']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $row['AAUD_DATUMS']; ?></td>
			</tr>
<?
		}
?>
        <tr>
             <td align="right" class="print_table_total" colspan="10"><?=text::get('TOTAL');?>:</td>
             <td align="center" class="print_table_total"><?= $pamatstundas; ?></td>
             <td align="center" class="print_table_total"><?= $virsstundas; ?></td>
	          <td align="center" class="print_table_total"><?= $laikscela; ?></td>
             <td align="center" class="print_table_total"><?= $summa; ?></td>
        </tr>
<?
	}
?>

</table>

</body>