﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
// query ID
$favoritId  = reqVar::get('favoritId');

if($favoritId !== false)
{
	// tikai  lietotajam ar lomu EDITOR projektaa ir pieeja!
	if (( dbProc::isUserInRole($userId, ROLE_EDITOR) ||
            dbProc::isUserInRole($userId, ROLE_EDITOR_VIEWER) ||
            dbProc::isUserInRole($userId, ROLE_ED_USER)))
	{
            // get info about favorit definition
            $favoritDefinitionInfo = dbProc::getFavoritDefinition($favoritId);
			if(count($favoritDefinitionInfo)>0)
      		{
      		    $favorit = $favoritDefinitionInfo[0];
      		}
            $catalog = $favorit['LTFV_TIPS'];
            // set list title
            $title = text::toUpper(sprintf(text::get('FAVORIT_VALUES'), $favorit['LTFV_NOSAUKUMS']));

            switch ($catalog)
            {
              case 'MATERIAL' :

                // get search column list
                $columns=dbProc::getKlklName(KL_MATERIALS);
                // default sort column
                $defaultColumn = dbProc::getKlklDefaultColumn(KL_MATERIALS);
                // gel list
                $res = dbProc::getMaterialFavoritList($favoritId);
                break;
                case 'PERSONAL' :

                // get search column list
                $columns=dbProc::getKlklName(KL_CUSTOMERS);
                // default sort column
                $defaultColumn = dbProc::getKlklDefaultColumn(KL_CUSTOMERS);
		        // gel list
                $res = dbProc::getCustomerFavoritList($favoritId);

                break;
                case 'TRANSPORT' :

                // get search column list
                $columns=dbProc::getKlklName(KL_TRANSPORT);
                // default sort column
                $defaultColumn = dbProc::getKlklDefaultColumn(KL_TRANSPORT);
		        // gel list
                $res = dbProc::getTransportFavoritList($favoritId);

                break;
                case 'ED_AREA' :

                // get search column list
                $columns=dbProc::getKlklName(KL_ED_AREA);
                // default sort column
                $defaultColumn = dbProc::getKlklDefaultColumn(KL_ED_AREA);
		        // gel list
                $res = dbProc::getEDAreaFavoritList($favoritId);

                break;
                case 'MMS_WORKS' :

                // get search column list
                $columns=dbProc::getKlklName(KL_MMS_WORKS);
                // default sort column
                $defaultColumn = dbProc::getKlklDefaultColumn(KL_MMS_WORKS);
		        // gel list
                $res = dbProc::getMMSWorkFavoritList($favoritId);

                break;
                 case 'MMS_WORKS_ALL' :

                // get search column list
                $columns=dbProc::getKlklName(KL_MMS_WORKS);
                // default sort column
                $defaultColumn = dbProc::getKlklDefaultColumn(KL_MMS_WORKS);
		        // gel list
                $res = dbProc::getMMSWorkFavoritList($favoritId);

                break;
                case 'CALKULATION' :

                // get search column list
                $columns=dbProc::getKlklName(KL_CALCULALATION);
                // default sort column
                $defaultColumn = dbProc::getKlklDefaultColumn(KL_CALCULALATION);
		        // gel list
                $res = dbProc::getCalculationFavoritList($favoritId);

                break;
            }
            include('f.fvr.s.2.1.tpl');

	}
	else
	{
		RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NO_ALL_INPUT_DATES'));
}
?>
