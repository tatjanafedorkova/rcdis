﻿<script type="text/javascript">
    function doSort(column, order)
	{
    	eval(xmlHttpGetValue('<?= $searchLink; ?>&xml=1&sort_k=' + column + '&sort_o=' + order));
	    reloadFrame(2, '<?= $searchLink; ?>&order=' + o + '&kol=' + k);
    }
</script>

<body class="frame_2">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><h1><?= text::toUpper(text::get('REPORT_TRANSPORT_USAGE')); ?></h1></td>
		<td align="right">
            <img src="img/ico_print.gif" alt="" width="16" height="16" border="0">&nbsp;
            <a href="<?= $printUrl; ?>" target="new"><?=text::get('PRINT');?></a>&nbsp;&nbsp;
            <img src="img/ico_excel.gif" alt="" width="16" height="16">&nbsp;
            <a href="<?= $exportUrl; ?>" ><?=text::get('EXPORT');?></a>&nbsp;
        </td>
	</tr>
</table>

<table cellpadding="5" cellspacing="1" border="0" width="100%" >
	<thead>
		<tr class="table_head_2">
			<td width="20%"><?= text::get('SINGL_DV_AREA'); ?></td>
            <td width="20%">
                <?=text::get('NATION_NUMBER');?><br />
                <a href="#" onclick="doSort('<?= COLUMN_NATION_NUMBER;?>', 'ASC')">
						<img src="img/sort_asc.gif" alt="A-Z" width="11" height="6" border="0"></a>
                <a href="#" onclick="doSort('<?= COLUMN_NATION_NUMBER;?>', 'DESC')">
						<img src="img/sort_desc.gif" alt="Z-A" width="11" height="6" border="0"></a>
            </td>
			<td width="20%"><?=text::get('TRANSPORT_KM');?></td>
            <td width="20%"><?= text::get('TRANSPORT_AVTO_TIME'); ?></td>
			<td width="20%"><?= text::get('TRANSPORT_WORK_TIME'); ?></td>

 		</tr>
	</thead>
	<tbody>
<?
	if (is_array($res) && count($res) > 0)
	{
		foreach ($res as $row)
		{
?>
			<tr class="table_cell_3" >
                <td align="center"><?= $row['DV_KODS']; ?></td>
                <td align="left"><?= $row['TRNS_VALSTS_NUMURS']; ?></td>
                <td align="center"><?= $row['KM']; ?></td>
                <td align="center"><?= $row['STUNDAS']; ?></td>
                <td align="center"><?= $row['DARBA_STUNDAS']; ?></td>

			</tr>
<?
		}
?>
        <tr>
             <td align="right" colspan="2"><b><?=text::get('TOTAL');?>:</b></td>
             <td align="center"><b><?= $km; ?></b></td>
             <td align="center"><b><?= $stundas; ?></b></td>
             <td align="center"><b><?= $darbaStundas; ?></b></td>
        </tr>
<?
	}
?>
	</tbody>
</table>

</body>