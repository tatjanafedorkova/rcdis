﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
     $sCriteria = reqVar::get('search');

		// gel list of users
        $res = dbProc::getNotPlanWorkList($sCriteria);
        $area = array();

        $pamatstundas = 0;
        $virsstundas = 0;
        $summa = 0;
        $km = 0;
        $stundas = 0;
        $darbaStundas = 0;
        $cilvekstundas = 0;

        if (is_array($res))
		{
            foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]  = array (
                'section' => $row['KEDI_SECTION'],
                'totalWorkTime' => (isset($area[$row['KEDI_SECTION']]['totalWorkTime'])?$area[$row['KEDI_SECTION']]['totalWorkTime']:0) + $row['CILVEKSTUNDAS'],
                'totalMainTime' => (isset($area[$row['KEDI_SECTION']]['totalMainTime'])?$area[$row['KEDI_SECTION']]['totalMainTime']:0) + $row['PAMATSTUNDAS'],
                'totalOverTime' => (isset($area[$row['KEDI_SECTION']]['totalOverTime'])?$area[$row['KEDI_SECTION']]['totalOverTime']:0) + $row['VIRSSTUNDAS'],
                'totalKm' => (isset($area[$row['KEDI_SECTION']]['totalKm'])?$area[$row['KEDI_SECTION']]['totalKm']:0) + $row['KM'],
                'totalStundas' => (isset($area[$row['KEDI_SECTION']]['totalStundas'])?$area[$row['KEDI_SECTION']]['totalStundas']:0) + $row['STUNDAS'],
                'totalDarbaStundas' => (isset($area[$row['KEDI_SECTION']]['totalDarbaStundas'])?$area[$row['KEDI_SECTION']]['totalDarbaStundas']:0) + $row['DARBA_STUNDAS'],
                'totalCena' => (isset($area[$row['KEDI_SECTION']]['totalCena'])?$area[$row['KEDI_SECTION']]['totalCena']:0) + $row['CENA_KOPA'],
                'ed' => array()
               );

               $pamatstundas = $pamatstundas + $row['PAMATSTUNDAS'];
               $virsstundas = $virsstundas + $row['VIRSSTUNDAS'];
               $summa = $summa + $row['CENA_KOPA'];
               $km = $km + $row['KM'];
               $stundas = $stundas + $row['STUNDAS'];
               $darbaStundas = $darbaStundas + $row['DARBA_STUNDAS'];
               $cilvekstundas = $cilvekstundas + $row['CILVEKSTUNDAS'];
            }
            foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]  = array (
                'code' => $row['KEDI_KODS'],
                'name' => $row['ED'],
                'totalWorkTime' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalWorkTime'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalWorkTime']:0) + $row['CILVEKSTUNDAS'],
                'totalMainTime' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalMainTime'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalMainTime']:0) + $row['PAMATSTUNDAS'],
                'totalOverTime' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalOverTime'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalOverTime']:0) + $row['VIRSSTUNDAS'],
                'totalKm' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalKm'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalKm']:0) + $row['KM'],
                'totalStundas' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalStundas'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalStundas']:0) + $row['STUNDAS'],
                'totalDarbaStundas' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalDarbaStundas'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalDarbaStundas']:0) + $row['DARBA_STUNDAS'],
                'totalCena' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalCena'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalCena']:0) + $row['CENA_KOPA'],
                'act' => array()
               );


            }
            foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['act'][$row['RAKT_ID']]  = array(
                    'actNumber' => $row['NUMURS'],
                    'designation' => $row['RAKT_OPERATIVAS_APZIM'],
                    'month' => dtime::getMonthName($row['RAKT_MENESIS']),
                    'type' => $row['TYPE'],
                    'workTime' => $row['CILVEKSTUNDAS'],
                    'mainTime' => $row['PAMATSTUNDAS'],
                    'overTime' => $row['VIRSSTUNDAS'],
                    'km' => $row['KM'],
                    'stundas' => $row['STUNDAS'],
                    'darbaStundas' => $row['DARBA_STUNDAS'],
                    'cena' => $row['CENA_KOPA']
               );
            }
		}

        $pamatstundas = number_format($pamatstundas,2, '.', '');
        $virsstundas = number_format($virsstundas,2, '.', '');
        $summa = number_format($summa, 2, '.', '');
        $km = number_format($km,2, '.', '');
        $stundas = number_format($stundas,2, '.', '');
        $darbaStundas = number_format($darbaStundas, 2, '.', '');
        $cilvekstundas = number_format($cilvekstundas,2, '.', '');

        $searchCr = array();
        $sCr = explode("^", $sCriteria);
        if(isset($sCr[0]) && $sCr[0] != -1  && isset($sCr[1]) && $sCr[1] != -1)
        {
           $searchCr[] = array('label' =>  text::get('YEAR'), 'value' => $sCr[0].'-'.$sCr[1]);
        }
        if(isset($sCr[2]) && $sCr[2] != -1  && isset($sCr[3]) && $sCr[3] != -1)
        {
           $searchCr[] = array('label' =>  text::get('MONTH'), 'value' => dtime::getMonthName($sCr[2]).'-'.dtime::getMonthName($sCr[3]));
        }
        if(isset($sCr[4]) && $sCr[4] != -1)
        {
           $searchCr[] = array('label' =>  text::get('RCD_REGION'), 'value' => $sCr[4]);
        }
        if(isset($sCr[5]) && $sCr[5] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGL_DV_AREA'), 'value' => dbProc::getDVAreaName($sCr[5]));
        }
        if(isset($sCr[6]) && $sCr[6] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ED_REGION'), 'value' => $sCr[6]);
        }
        if(isset($sCr[7]) && $sCr[7] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ED_IECIKNIS'), 'value' => dbProc::getEDAreaName($sCr[7]));
        }

        if(isset($sCr[8]) && $sCr[8] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_SOURCE_OF_FOUNDS'), 'value' => dbProc::getSourceOfFoundsName($sCr[8]));
        }
        if(isset($sCr[9]) && $sCr[9] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_VOLTAGE'), 'value' => dbProc::getVoltageName($sCr[9]));
        }
        if(isset($sCr[10]) && $sCr[10] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ACT_OUNER'), 'value' => dbProc::getUserName($sCr[10]));
        }
        $type = explode("*", $sCr[18]);
        $typeString = '';
        if($type[0] == -1)
        {
           $typeString = text::get('ALL');
        }
        else
        {
          $typeKls = classif::getValueArray(KL_ACT_TYPE);
          foreach($type as $t)
          {
              $typeString .= $typeKls[$t].",";
          }
          $typeString  = substr($typeString, 0, -1);
        }
        $searchCr[] = array('label' =>  text::get('SINGLE_ACT_TYPE'), 'value' => $typeString);
        if(isset($sCr[12]) && $sCr[12] != -1)
        {
           $searchCr[] = array('label' =>  text::get('MMS_WORK_FINISHED'), 'value' => (($sCr[12] == 1) ? text::get('YES'): text::get('NO')));
        }
        $status = explode("*", $sCr[14]);
        $statusString = '';
        if($status[0] == -1)
        {
           $statusString = text::get('ALL_STATUS');
        }
        else
        {
          foreach($status as $s)
          {
              $statusInfo = dbProc::getKrfkInfoByName($s);
              $statusString .= $statusInfo['KRFK_NOZIME'].",";
          }
          $statusString  = substr($statusString, 0, -1);
        }
        $searchCr[] = array('label' =>  text::get('STATUS'), 'value' => $statusString);
        if(isset($sCr[15]) && $sCr[15] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_OBJECT'), 'value' => dbProc::getObjectName($sCr[15]));
        }
        if(isset($sCr[16]) && $sCr[16] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_OTHER_PAYMENTS'), 'value' => (($sCr[16] == 1) ? text::get('YES'): text::get('NO')));
        }
        if(isset($sCr[17]) && $sCr[17] != -1)
        {
           $searchCr[] = array('label' =>  text::get('OVER_TIME'), 'value' => (($sCr[17] == 1) ? text::get('REPORTS_WITH_OWER_TIME'): text::get('REPORTS_WITHOUT_OWER_TIME')));
        }
        if(isset($sCr[19]) && $sCr[19] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ED_SECTION'), 'value' => urldecode($sCr[19]));
        }
		include('f.rpt.p.15.tpl');
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
