﻿<?
// Main form with XMLHTTP method load this code:
if (reqVar::get('xmlHttp') == 1)
{
	//if is set edRegion
	if (reqVar::get('edRegion') !== false)
	{
		// unset previouse values of edArea
		?>
		for(i=document.all["edArea"].length; i>=0; i--)
		{
			document.all["edArea"].options[i] = null;
		}
		<?
		$res=dbProc::getEDAreaListForRegion(reqVar::get('edRegion'));
		if (is_array($res) && count($res)>0)
		{
			?>
			document.all["edArea"].options[0] = new Option('<?=text::get('ALL');?>', '-1');
			<?
            $i = 1;
			foreach ($res as $row)
			{
				// feel options array
				?>
				document.all["edArea"].options[<?=$i;?>] = new Option( '<?=$row['KEDI_REGIONS'].' : '.$row['KEDI_KODS'].' : '.$row['KEDI_NOSAUKUMS'];?>', '<?=$row['KEDI_ID'];?>');
                <?
                $i++;
			}
		}

        // unset previouse values of section
		?>
		for(i=document.all["section"].length; i>=0; i--)
		{
			document.all["section"].options[i] = null;
		}
		<?
		$res=dbProc::getEDSectionListForRegion(reqVar::get('edRegion'));
		if (is_array($res) && count($res)>0)
		{
			?>
			document.all["section"].options[0] = new Option('<?=text::get('ALL');?>', '-1');
			<?
            $i = 1;
			foreach ($res as $row)
			{
				// feel options array
				?>
				document.all["section"].options[<?=$i;?>] = new Option( '<?=$row['KEDI_SECTION'];?>', '<?=$row['KEDI_SECTION'];?>');
                <?
                $i++;
			}
		}
	}
    	//if is set dvRegion
	if (reqVar::get('dvRegion') !== false)
	{
		// unset previouse values of dvArea
		?>
		for(i=document.all["dvArea"].length; i>=0; i--)
		{
			document.all["dvArea"].options[i] = null;
		}
		<?
		$res=dbProc::getDVAreaForRegion(reqVar::get('dvRegion'));
		if (is_array($res) && count($res)>0)
		{
			?>
			document.all["dvArea"].options[0] = new Option('<?=text::get('ALL');?>', '-1');
			<?
            $i = 1;
			foreach ($res as $row)
			{
				// feel options array
				?>
				document.all["dvArea"].options[<?=$i;?>] = new Option( '<?=$row['KDVI_REGIONS'].''.$row['KDVI_KODS'].' : '.$row['KDVI_NOSAUKUMS'];?>', '<?=$row['KDVI_ID'];?>');
                <?
                $i++;
			}
		}
	}
	exit;
}
        $isSearchForm = isset($isSearchForm)? $isSearchForm : false;
        // get user info
      	$userInfo = dbProc::getUserInfo($userId);
        $isEconomist = dbProc::isUserInRole($userId, ROLE_ECONOMIST);
        $isAdmin=userAuthorization::isAdmin();
        $isEdUser = dbProc::isUserInRole($userId, ROLE_ED_USER);
        // if came back from act
        $selectedId = reqVar::get('selectedId');
      	if(count($userInfo) >0 )
      	{
      		$user = $userInfo[0];
       	}


         // get DV area list
         $dvarea = array();
         if(isset($user['RLTT_KDVI_ID']) )
         {
            $dvarea[$user['RLTT_KDVI_ID']] = dbProc::getDVAreaName($user['RLTT_KDVI_ID']);
         }
         elseif(isset($user['RLTT_REGIONS']) && isset($user['RLTT_ROLE']) && $user['RLTT_ROLE'] != ROLE_ED_USER )
         {
            $res=dbProc::getDVAreaForRegion($user['RLTT_REGIONS'], false);
			if (is_array($res))
			{
			    $dvarea['-1']=text::get('ALL');
				foreach ($res as $row)
				{
					$dvarea[$row['KDVI_ID']]=$row['KDVI_REGIONS'].$row['KDVI_KODS'].' : '.$row['KDVI_NOSAUKUMS'];
				}
			}
         }
         else
         {
            $res=dbProc::getDVAreaList(false,false,false,false,false,'ASC','`KDVI_REGIONS`, `KDVI_KODS`');
			if (is_array($res))
			{
			    $dvarea['-1']=text::get('ALL');
				foreach ($res as $row)
				{
					$dvarea[$row['KDVI_ID']]=$row['KDVI_REGIONS'].$row['KDVI_KODS'].' : '.$row['KDVI_NOSAUKUMS'];
				}
			}
         }        

        // get users list
         $usersList = array();
         if(isset($user['RLTT_KDVI_ID']) && !$isEdUser)
         {
            $res=dbProc::getUserDVAreaList($user['RLTT_KDVI_ID'], false);
			if (is_array($res))
			{
			  $usersList['-1']=text::get('ALL');
			  foreach ($res as $row)
			  {
			  	$usersList[$row['RLTT_ID']]=$row['RLTT_VARDS'].' '.$row['RLTT_UZVARDS'];
			  }
			}
         }
         elseif(isset($user['RLTT_REGIONS']) && !$isEdUser)
         {
            $res=dbProc::getUserRegionList($user['RLTT_REGIONS'], false);
			if (is_array($res))
			{
			  $usersList['-1']=text::get('ALL');
			  foreach ($res as $row)
			  {
			  	$usersList[$row['RLTT_ID']]=$row['RLTT_VARDS'].' '.$row['RLTT_UZVARDS'];
			  }
			}
         }
         else
         {
            $res=dbProc::getUsersList(false,false,false,false,false,'ASC','RLTT_VARDS, RLTT_UZVARDS');
			if (is_array($res))
			{
			  $usersList['-1']=text::get('ALL');
			  foreach ($res as $row)
			  {
			  	$usersList[$row['RLTT_ID']]=$row['RLTT_VARDS'].' '.$row['RLTT_UZVARDS'];
			  }
			}
         }

        // get is finished list
        $isFinished = array();
        $isFinished['-1']=text::get('ALL');
        $isFinished['1']=text::get('YES');
        $isFinished['2']=text::get('NO');

        // get status list
       $status=array();
       $status['-1']=text::get('ALL_STATUS');
       $res=dbProc::getKrfkName(KRFK_STATUS);
       if (is_array($res))
       {
       	foreach ($res as $row)
       	{
       	  if($row['nosaukums'] != STAT_DELETE || ($row['nosaukums'] == STAT_DELETE && $isAdmin && !$isEdUser))
          {
            $status[$row['nosaukums']]=$row['nozime'];
          }
       	}
       }
       unset($res);

       $searcCriteria = array();

       // if operation
    	if ($oForm -> isFormSubmitted())
    	{
           $searcCriteria['yearFrom'] = $oForm->getValue('yearFrom');
           $searcCriteria['yearUntil'] = $oForm->getValue('yearUntil');
           $searcCriteria['monthFrom'] = $oForm->getValue('monthFrom');
           $searcCriteria['monthUntil'] = $oForm->getValue('monthUntil');
           $searcCriteria['dvArea'] = $oForm->getValue('dvArea');
           $searcCriteria['ouner'] = $oForm->getValue('ouner');
           $searcCriteria['status']  = $oForm->getValue('status');
           $searcCriteria['status1']  = implode("*",$oForm->getValue('status'));
           dbProc::replaceSearchResult($searchName, $userId,
                                $searcCriteria['yearFrom'],
                                $searcCriteria['yearUntil'],
                                $searcCriteria['monthFrom'],
                                $searcCriteria['monthUntil'],
                                '',
                                $searcCriteria['dvArea'],
                                '',
                                '',
                                '',
                                '',
                                $searcCriteria['ouner'],
                                '',
                                '',
                                $searcCriteria['status1'],
                                '',
                                '',
                                '','','','','','','',
                                '',
                                ''
                                );

           $cr = '';
           foreach($searcCriteria as $s)
           {
             $cr .= $s.'^';
           }
           $cr = substr($cr, 0, -1);
           $oListLink->addPrm('search', $cr);
           $actListLink=$oListLink ->getQuery();
           $oForm->addElement('static','jsRefresh2','','<script>reloadFrame(2, "'.$actListLink.'");</script>');
           unset($oListLink);
    	}
        else
        {
            if (reqVar::get('isReturn') == 1 || $selectedId)
            {
                $searchCritery = dbProc::getSearchCritery($userId);
           	    if(count($searchCritery) >0 )
      	        {
      		        $critery = $searchCritery[0];
       	        }
            }

           $searcCriteria['yearFrom'] = isset($critery['yearFrom'])? $critery['yearFrom'] :dtime::getCurrentYear();
           $searcCriteria['yearUntil'] = isset($critery['yearUntil'])? $critery['yearUntil'] :dtime::getCurrentYear();
           $searcCriteria['monthFrom'] = isset($critery['monthFrom'])? $critery['monthFrom'] :dtime::getCurrentMonth();
           $searcCriteria['monthUntil'] = isset($critery['monthUntil'])? $critery['monthUntil'] :dtime::getCurrentMonth();
           $searcCriteria['dvArea'] = isset($critery['dvArea'])? $critery['dvArea'] :$user['RLTT_KDVI_ID'];
           $searcCriteria['ouner'] = isset($critery['ouner'])? $critery['ouner'] :(($isEconomist || $isEdUser) ? -1 : $userId);
           $searcCriteria['status']  = isset($critery['status1'])? explode("*",$critery['status1']) : array(STAT_CLOSE);
           
        }

        $oForm -> addElement('label', 'year',  text::get('YEAR'), '');
        $oForm -> addElement('select', 'yearFrom',  text::get('FROM'), $searcCriteria['yearFrom'], 'style="width:50;"', '', '', dtime::getYearArray());
        $oForm -> addElement('select', 'yearUntil',  text::get('UNTIL'), $searcCriteria['yearUntil'], 'style="width:50;"', '', '', dtime::getYearArray());

        $oForm -> addElement('label', 'month',  text::get('MONTH'), '');
        $oForm -> addElement('select', 'monthFrom',  text::get('FROM'), $searcCriteria['monthFrom'], 'style="width:70;"', '', '', dtime::getMonthArray());
        $oForm -> addElement('select', 'monthUntil',  text::get('UNTIL'), $searcCriteria['monthUntil'], 'style="width:70;"', '', '', dtime::getMonthArray());
        // xmlHttp link
		$oLink=new urlQuery();
		$oLink->addPrm(DONT_USE_GLB_TPL, 1);
		$oLink->addPrm(FORM_ID, 'f.akt.m.13');
        
        $oForm -> addElement('select', 'dvArea',  text::get('SINGL_DV_AREA'), $searcCriteria['dvArea'], '', '', '', $dvarea);

        $oForm -> addElement('select', 'ouner',  text::get('ACT_OUNER'), $searcCriteria['ouner'], '', '', '', $usersList);
		$oForm -> addElement('multiple_select', 'status', text::get('STATUS'), $searcCriteria['status'], ' size="7"', '', '', $status);
       	// form buttons
       	$oForm -> addElement('submitImg', 'search', text::get('SEARCH'), 'img/btn_meklet.gif', 'width="70" height="20" onClick="setPosition1(\'loading\'); return true;"');



        $oForm -> makeHtml();
		include('f.akt.m.13.tpl');
?>