﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isEdUser = (dbProc::isUserInRole($userId, ROLE_ED_USER) || dbProc::isUserInRole($userId, ROLE_AUDIT_USER));
if (userAuthorization::isAdmin() || $isEdUser )
{
	$editMode = reqVar::get('editMode');
	// bottom frame
	// if insert/update



	if($editMode)
	{
		$stockId = reqVar::get('stockId');

		$oLink=new urlQuery();
		$oLink->addPrm(FORM_ID, 'f.ktl.s.17');
		$oLink->addPrm('editMode', 1 );
		// form inicialization
		$oForm = new Form('frmMain','post',$oLink->getQuery());
		unset($oLink);

		// if operation is success, show success message and redirect top frame
		if (reqVar::get('successMessage'))
		{
			$oForm->addSuccess(reqVar::get('successMessage'));
			$oForm->addElement('static','jsRefresh2','','<script>refreshFrame(1);</script>');
		}

		// inicial state: stockId=0;
		// if stockId>0 ==> stock selected from the list
		if($stockId != false)
		{
			// get info about material stock
			$stockInfo = dbProc::getStockList($stockId);
			//print_r($dvAreaInfo);
			if(count($stockInfo)>0)
			{
				$stock = $stockInfo[0];
			}
        }

		// form elements
		$oForm -> addElement('hidden', 'action', '');
		$oForm -> addElement('hidden', 'stockId', null, isset($stock['MSTK_ID'])? $stock['MSTK_ID']:'');

        $oForm -> addElement('kls', 'section',  text::get('ED_SECTION'), array('classifName'=>KL_ED_SECTION,'value'=>isset($stock['MSTK_KEDI_SECTION'])?$stock['MSTK_KEDI_SECTION']:'','readonly'=>false), (($isEdUser)?' disabled ':'').' tabindex=1 maxlength="2000"');
		$oForm -> addRule('section', text::get('ERROR_REQUIRED_FIELD'), 'required');

        $oForm -> addElement('text', 'cost_center',  text::get('COST_CENTER'), isset($stock['MSTK_COST_CENTER'])?$stock['MSTK_COST_CENTER']:'', ' tabindex=2 maxlength="3"'.(($isEdUser)?' disabled' : ''));
		$oForm -> addRule('cost_center', text::get('ERROR_REQUIRED_FIELD'), 'required');

        $oForm -> addElement('text', 'stock_code',  text::get('STOCK_CODE'), isset($stock['MSTK_STOCK_CODE'])?$stock['MSTK_STOCK_CODE']:'', ' tabindex=3 maxlength="12"'.(($isEdUser)?' disabled' : ''));
		$oForm -> addRule('stock_code', text::get('ERROR_REQUIRED_FIELD'), 'required');

        $oForm -> addElement('text', 'place_code',  text::get('PLACE_CODE'), isset($stock['MSTK_PLACE_CODE'])?$stock['MSTK_PLACE_CODE']:'', ' tabindex=4 maxlength="30"'.(($isEdUser)?' disabled' : ''));
		$oForm -> addRule('place_code', text::get('ERROR_REQUIRED_FIELD'), 'required');

		$oForm -> addElement('checkbox', 'isActive',  text::get('IS_ACTIVE'), isset($stock['MSTK_IR_AKTIVS'])?$stock['MSTK_IR_AKTIVS']:1, 'tabindex=5'.(($isEdUser)?' disabled' : ''));

		$oForm -> addElement('checkbox', 'isAuto',  text::get('IS_AUTO'), isset($stock['MSTK_IR_AUTO'])?$stock['MSTK_IR_AUTO']:0, 'tabindex=6'.(($isEdUser)?' disabled' : ''));
		$oForm -> addElement('text', 'auto_stock_code',  text::get('MSTK_AUTO_STOCK_CODE'), isset($stock['MSTK_AUTO_STOCK_CODE'])?$stock['MSTK_AUTO_STOCK_CODE']:'', ' tabindex=7 maxlength="20"'.(($isEdUser)?' disabled' : ''));
		// form buttons
        if(!$isEdUser)
        {
    		$oForm -> addElement('submitImg', 'add', text::get('ADD'), 'img/btn_pievienot.gif', 'width="70" height="20" onclick="setValue(\'action\',\''.OP_INSERT.'\');"');
    		$oForm -> addElement('submitImg', 'save', text::get('SAVE'), 'img/btn_saglabat.gif', 'width="70" height="20" onclick="if (!isEmptyField(\'stockId\')) {setValue(\'action\',\''.OP_UPDATE.'\');} else {return false;}"');
    		$oForm -> addElement('clearImg', 'clear', text::get('CLEAR'), 'img/btn_attirit.gif', 'width="70" height="20"','','',array('onClick'=>'refreshButton(\'stockId\');parent.frame_1.unActiveRow();'));

         	$oForm -> addElement('static', 'jsButtonsControl', '', '
    			<script>
    				function refreshButton(name)
    				{
    					if (!isEmptyField(name))
    					{
    						document.all["save"].src="img/btn_saglabat.gif";

    					}
    					else
    					{
    						document.all["save"].src="img/btn_saglabat_disabled.gif";

    					}
    				}
    				refreshButton("stockId");
    			</script>
    		');
         }
		// if operation
		if ($oForm -> isFormSubmitted())
		{
			$check = true;
			$r = false;
			$checkRequiredFields = false;
			if ($oForm -> getValue('section') && $oForm -> getValue('cost_center') && $oForm -> getValue('stock_code') && $oForm -> getValue('place_code') )
			{
				$checkRequiredFields = true;
			}
			if($oForm->getValue('isAuto')  && !$oForm -> getValue('auto_stock_code'))
			{
				$oForm -> addError(text::get('MSTK_AUTO_STOCK_CODE_REQUIRED'));
				$checkRequiredFields = false;
			}
			if ($checkRequiredFields)
			{
				// save
				if ($oForm->getValue('action')==OP_INSERT || ($oForm->getValue('action')==OP_UPDATE  && is_numeric($oForm->getValue('stockId'))))
				{
					/*
					if (dbProc::stockWithSectionExists(
                                                            $oForm->getValue('place_code'),
                                                            ($oForm->getValue('action')==OP_UPDATE)?$oForm->getValue('stockId'):false)
                                                            )
					{
						$oForm->addError(text::get('ERROR_EXISTS_STOCK'));
						$check = false;
					}
					*/
					// if all is ok, do operation
					if($check)
					{
						$r=dbProc::saveStock(($oForm->getValue('action')==OP_UPDATE?$oForm->getValue('stockId'):false),
						$oForm->getValue('section'),
						$oForm->getValue('cost_center'),
						$oForm->getValue('stock_code'),
                        $oForm->getValue('place_code'),
						$oForm->getValue('isActive',0),
						$oForm->getValue('isAuto',0),
						$oForm->getValue('auto_stock_code')
						);

						if (!$r)
						{
							$oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
						}
					}
				}


				// if operation was compleated succefully, show success mesage and redirect current frame
				if ($r)
				{
					$oLink=new urlQuery();
					$oLink->addPrm(FORM_ID, 'f.ktl.s.17');
					$oLink->addPrm('editMode', '1');
					switch($oForm->getValue('action'))
					{
						case OP_INSERT:
							$oLink->addPrm('successMessage', text::get('RECORD_WAS_ADDED'));
							break;
						case OP_UPDATE:
							$oLink->addPrm('successMessage', text::get('RECORD_WAS_SAVED'));
							break;

					}
					RequestHandler::makeRedirect($oLink->getQuery());
				}
			}
			else
			{
				$oForm->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
			}
		}

       	$oForm -> makeHtml();
		include('f.ktl.s.17.2.tpl');
	}
	// top frale
	else
	{
        // Check if this form was requested via xmlHttpRequest and search
    	if (isset($_GET['xml']) && isset($_GET['search_q']) && isset($_GET['search_c']))
    	{
    		ob_clean();
    		$q = $_GET['search_q'];
            if (isset($q) || $q == 0)
    		{
    			echo 'q = "'.trim($q).'";';
    		}
            else
            {
    			echo 'q = "";';
    		}
            $c = $_GET['search_c'];
            if ($c)
    		{
    			echo 'c = "'.$c.'";';
    		}
            else
            {
    			echo 'c = "";';
    		}
		    exit;
    	}
        if (isset($_GET['xml']) && isset($_GET['sort_k']) && isset($_GET['sort_o']))
    	{
    		ob_clean();
    		$o = $_GET['sort_o'];
            if (isset($o))
    		{
    			echo 'o = "'.$o.'";';
    		}
            else
            {
    			echo 'o = "";';
    		}
            $k = $_GET['sort_k'];
            if ($k)
    		{
    			echo 'k = "'.$k.'";';
    		}
            else
            {
    			echo 'k = "";';
    		}
		    exit;
    	}
        // set list title
        $title = text::toUpper(text::get('STOCK_TITLE'));
        // get search column list
        $columns=dbProc::getKlklName(KL_STOCK);
        $searchText = ((reqVar::get('search') && reqVar::get('search') != '') || reqVar::get('search') == 0) ? reqVar::get('search') : false;
        $searchColumn = (reqVar::get('column') && reqVar::get('column') != '') ? reqVar::get('column') : false;
        $sortOrder = (reqVar::get('order') && reqVar::get('order') != '') ? reqVar::get('order') : false;
        $orderColumn = (reqVar::get('kol') && reqVar::get('kol') != '') ? reqVar::get('kol') : false;


        // default sort column
        $defaultColumn = dbProc::getKlklDefaultColumn(KL_STOCK);

        // URL to search Zinojums by number.
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.ktl.s.17');
    	$oLink->addPrm('isNew', '1');

        // add search/order params to listing
        $oLink->addPrm('search', reqVar::get('search'));
        $oLink->addPrm('column', reqVar::get('column'));
        $oLink->addPrm('order', reqVar::get('order'));
        $oLink->addPrm('kol', reqVar::get('kol'));
        $searchLink = $oLink -> getQuery();

		// inicialising of listing object
		$amount=dbProc::getStockCount($searchText,
                (($searchColumn === false) ? $defaultColumn : $searchColumn));
		$listing=new listing();
		$listing->setAmount($amount);

		$listingHtml=$listing->getHtml($oLink ->getQuery());
		unset($oLink);

		//link for frame2
		$oLink=new urlQuery();
		$oLink->addPrm(FORM_ID, 'f.ktl.s.17');
		$oLink->addPrm('editMode', 1);

		// gel list of users
		$res = dbProc::getStockList( false,
                                        $listing->getStartPosition(),
                                        $listing->getAmountOnPage(),
                                        $searchText,
                                        (($searchColumn === false) ? $defaultColumn : $searchColumn),
                                        $sortOrder,
                                        $orderColumn);

		if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
				// add dv area Id to each link
				$oLink->addPrm('stockId', $row['MSTK_ID']);
				$res[$i]['URL']=$oLink ->getQuery();
			}
		}
		unset($oLink);

		// inicial state of users number
		$number=$listing->getStartPosition()+1;

		include('f.ktl.s.x.1.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
