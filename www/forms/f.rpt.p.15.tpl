﻿<body class="print">
<div class="print_header"><?= text::get('REPORT_WORK_WITH_NO_PLAN'); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_landscape" >
<?
  foreach($searchCr as $i=>$s)
  {
    if($i%3 == 0)
    {
      ?>
        <tr>
      <?
    }
    ?>
      <td align="right" class="print_table_total" width="13%"><?=$s['label'];?>:</td>
      <td align="left" class="print_table_data" width="20%"><?=$s['value'];?></td>
    <?
  }
?>
</table>
<br />
<table cellpadding="0" cellspacing="0" class="print_table_landscape" >
<?
   if (is_array($area) && count($area) > 0)
	{
		foreach ($area as $row)
		{
		  $actCount = 0;
          if (is_array($row['ed']) && count($row['ed']) > 0)
          {
            foreach ($row['ed'] as $rrS)
            {
?>
            <tr>
                <td colspan="2" align="left" class="print_table_header2"><b><?=text::get('ED_IECIKNIS');?>:</b></td>
                <td colspan="2" align="left" class="print_table_header2"><?= $rrS['name']; ?></td>
                <td colspan="7" align="left" class="print_table_header2"><?= $rrS['code']; ?></td>
            </tr>
            <tr>
    			<td width="8%" rowspan="2" class="print_table_header2"><?= text::get('ACT_NUMBER'); ?></td>
                <td width="10%" rowspan="2" class="print_table_header2"><?=text::get('MONTH');?></td>
                <td width="15%" rowspan="2" class="print_table_header2"><?= text::get('MAIN_DESIGNATION'); ?></td>
                <td width="10%" rowspan="2" class="print_table_header2"><?= text::get('SINGLE_ACT_TYPE'); ?></td>
                <td width="9%" rowspan="2" class="print_table_header2"><?=text::get('MAN_HOUR_STANDART_TOTAL');?></td>
                <td width="48%" colspan="6" class="print_table_header2"><?=text::get('REAL_WORK_PAYMENT');?></td>
            </tr>
            <tr>
                <td width="8%" class="print_table_header2"><?=text::get('TRANSPORT_KM');?><br /></td>
                <td width="8%" class="print_table_header2"><?=text::get('TRANSPORT_AVTO_TIME');?></td>
                <td width="8%" class="print_table_header2"><?= text::get('TRANSPORT_WORK_TIME'); ?></td>
                <td width="8%" class="print_table_header2"><?=text::get('BASE_TIME');?></td>
                <td width="8%" class="print_table_header2"><?=text::get('OVER_TIME');?></td>
                <td width="8%" class="print_table_header2"><?= text::get('REPORT_MATERIAL_PRICE_TOTAL'); ?></td>
            </tr>
            <?
                if (is_array($rrS['act']) && count($rrS['act']) > 0)
                {
                   foreach ($rrS['act'] as $r)
                   {
                     ?>
                   <tr>
      			      <td align="center"><?= $r['actNumber']; ?></td>
                      <td align="left"><?= $r['month']; ?></td>
                      <td align="left"><?= $r['designation']; ?></td>
                      <td align="left"><?= $r['type']; ?></td>
                      <td align="center"><?= $r['workTime']; ?></td>
                      <td align="center"><?= $r['km']; ?></td>
                      <td align="center"><?= $r['stundas']; ?></td>
                      <td align="center"><?= $r['darbaStundas']; ?></td>
                      <td align="center"><?= $r['mainTime']; ?></td>
                      <td align="center"><?= $r['overTime']; ?></td>
                      <td align="center"><?= $r['cena']; ?></td>
                  </tr>

                     <?
                     $actCount = $actCount + 1;
                   }
                }
            ?>
            <tr>
             <td colspan="4" align="left" class="print_table_header1"><b><?=$rrS['name'];?>: <?=count($rrS['act']);?> <?=text::get('ACTS1');?></b></td>
             <td align="center" class="print_table_header1"><b><?=number_format($rrS['totalWorkTime'],2,'.','');?></b></td>
             <td align="center" class="print_table_header1"><b><?=number_format($rrS['totalKm'],2,'.','');?></b></td>
             <td align="center" class="print_table_header1"><b><?=number_format($rrS['totalStundas'],2,'.','');?></b></td>
             <td align="center" class="print_table_header1"><b><?=number_format($rrS['totalDarbaStundas'],2,'.','');?></b></td>
             <td align="center" class="print_table_header1"><b><?=number_format($rrS['totalMainTime'],2,'.','');?></b></td>
             <td align="center" class="print_table_header1"><b><?=number_format($rrS['totalOverTime'],2,'.','');?></b></td>
             <td align="center" class="print_table_header1"><b><?=number_format($rrS['totalCena'],2,'.','');?></b></td>
            </tr>
            <tr><td colspan="11">&nbsp;</td></tr>
           <?}}?>
            <tr>
             <td colspan="4" align="right" class="print_table_header"><b><?=$row['section'];?>: <?=$actCount;?> <?=text::get('ACTS1');?></b></td>
             <td align="center" class="print_table_header"><b><?=number_format($row['totalWorkTime'],2,'.','');?></b></td>
             <td align="center" class="print_table_header"><b><?=number_format($row['totalKm'],2,'.','');?></b></td>
             <td align="center" class="print_table_header"><b><?=number_format($row['totalStundas'],2,'.','');?></b></td>
             <td align="center" class="print_table_header"><b><?=number_format($row['totalDarbaStundas'],2,'.','');?></b></td>
             <td align="center" class="print_table_header"><b><?=number_format($row['totalMainTime'],2,'.','');?></b></td>
             <td align="center" class="print_table_header"><b><?=number_format($row['totalOverTime'],2,'.','');?></b></td>
             <td align="center" class="print_table_header"><b><?=number_format($row['totalCena'],2,'.','');?></b></td>
            </tr>
            <tr><td colspan="11">&nbsp;</td></tr>
        <?
		}
        ?>
        <tr>
             <td colspan="4" align="right" class="print_table_total"><b><?=text::get('TOTAL');?>:</b></td>
             <td align="center" class="print_table_total"><b><?= $cilvekstundas; ?></b></td>
             <td align="center" class="print_table_total"><b><?= $km; ?></b></td>
             <td align="center" class="print_table_total"><b><?= $stundas; ?></b></td>
             <td align="center" class="print_table_total"><b><?= $darbaStundas; ?></b></td>
             <td align="center" class="print_table_total"><b><?= $pamatstundas; ?></b></td>
             <td align="center" class="print_table_total"><b><?= $virsstundas; ?></b></td>
             <td align="center" class="print_table_total"><b><?= $summa; ?></b></td>
        </tr>
<?
	}
?>

</table>

</body>