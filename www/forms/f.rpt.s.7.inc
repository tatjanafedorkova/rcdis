﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
	$searchMode = reqVar::get('isSearch');
	// top frame
    if($searchMode)
	{
        $oLink=new urlQuery();
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.7');
    	$oLink->addPrm('isSearch', 1 );
    	// form inicialization
    	$oForm = new Form('frmMain','post',$oLink->getQuery());
    	unset($oLink);

        $oLink=new urlQuery();
	    $oLink->addPrm('isReturn', '1');
        $oLink->addPrm('isSearch', 1 );
	    $oLink->addPrm(FORM_ID, 'f.rpt.s.7');
	    $criteriaLink=$oLink ->getQuery();
	    unset($oLink);

        // if operation
    	if ($oForm -> isFormSubmitted())
    	{
            $oListLink=new urlQuery();
            $oListLink->addPrm(FORM_ID, 'f.rpt.s.7');
        }
        $searchTitle = text::toUpper(text::toUpper(text::get('REPORT_CALCULATION')));
        $searchName = 'REPORT_CALCULATION';
        include('f.akt.m.1.inc');
	}
	// bottom frale
	else
	{
	    $sCriteria = reqVar::get('search');

        // URL  export
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.e.7');
    	$oLink->addPrm('search', $sCriteria);
        $oLink->addPrm(DONT_USE_GLB_TPL, '1');

        // gel list of calculation
		$res = dbProc::getActCalculationList($sCriteria, false);
        if(count($res) > MAX_EXCEL_ROW_COUNT)
        {
           $exportUrl = "javascript:alert('".text::get('TOO_MATCH_SEARCH_RESULTS')."');" ;
        }
        else
        {
          $exportUrl =  $oLink ->getQuery();
        }

        unset($oLink);

        // URL  print
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.p.7');
    	$oLink->addPrm('search', $sCriteria);
        //$oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $printUrl =  $oLink ->getQuery();
        unset($oLink);

        // gel list of users
		$res = dbProc::getActCalculationList($sCriteria);

        $baseTime = 0;
        $overTime = 0;
        if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
               $baseTime = $baseTime + $row['PAMATSTUNDAS'];
               $overTime = $overTime + $row['VIRSTUNDAS'];
			}
		}
        $baseTime = number_format($baseTime,2, '.', '');
        $overTime = number_format($overTime,2, '.', '');

		include('f.rpt.s.7.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
