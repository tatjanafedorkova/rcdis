﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
     $sCriteria = reqVar::get('search');

        	// gel list of users
		$res = dbProc::getMMSNotStartedWorksList($sCriteria);
        $area = array();

        $summa = 0;
        $stundas = 0;

        if (is_array($res))
		{
            foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]  = array (
                'section' => $row['KEDI_SECTION'],
                'totalStundas' => (isset($area[$row['KEDI_SECTION']]['totalStundas'])?$area[$row['KEDI_SECTION']]['totalStundas']:0) + $row['MMS_CILVEKSTUNDAS'],
                'totalCena' => (isset($area[$row['KEDI_SECTION']]['totalCena'])?$area[$row['KEDI_SECTION']]['totalCena']:0) + $row['MMS_MATR_IZMAKSAS'],
                'ed' => array()
               );

               $summa = $summa + $row['MMS_MATR_IZMAKSAS'];
               $stundas = $stundas + $row['MMS_CILVEKSTUNDAS'];

            }
            foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]  = array (
                'code' => $row['KEDI_KODS'],
                'name' => $row['ED'],
                'totalStundas' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalStundas'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalStundas']:0) + $row['MMS_CILVEKSTUNDAS'],
                'totalCena' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalCena'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalCena']:0) + $row['MMS_MATR_IZMAKSAS'],
                'mms' => array()
               );


            }
            foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['KMSD_KODS']]  = array(
                    'code' => $row['KMSD_KODS'],
                    'quarter' => $row['KMSD_CETUKSNIS'],
                    'stundas' => $row['MMS_CILVEKSTUNDAS'],
                    'cena' => $row['MMS_MATR_IZMAKSAS'],
                    'priority' => $row['KMSD_PRIORITATE']
               );
            }
		}

        $summa = number_format($summa, 2, '.', '');
        $stundas = number_format($stundas,2, '.', '');


        $searchCr = array();
        $sCr = explode("^", $sCriteria);
        if(isset($sCr[0]) && $sCr[0] != -1  && isset($sCr[1]) && $sCr[1] != -1)
        {
           $searchCr[] = array('label' =>  text::get('YEAR'), 'value' => $sCr[0].'-'.$sCr[1]);
        }
        if(isset($sCr[2]) && $sCr[2] != -1  && isset($sCr[3]) && $sCr[3] != -1)
        {
           $searchCr[] = array('label' =>  text::get('QUARTER'), 'value' => $sCr[2].'-'.$sCr[3]);
        }

        if(isset($sCr[4]) && $sCr[4] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ED_REGION'), 'value' => $sCr[4]);
        }
        if(isset($sCr[5]) && $sCr[5] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ED_IECIKNIS'), 'value' => dbProc::getEDAreaName($sCr[5]));
        }

        if(isset($sCr[6]) && $sCr[6] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ED_SECTION'), 'value' => urldecode($sCr[6]));
        }
		include('f.rpt.p.16.tpl');
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
