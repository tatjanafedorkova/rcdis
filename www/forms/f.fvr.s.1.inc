<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
// action ID
$action = reqVar::get('action');
// query ID
$favoritId  = reqVar::get('favoritId');
// pageId
$pageId  = reqVar::get('pageId');

//if page not set => is first page
if(!$pageId)
{
	$pageId = 1;
}
// Main form with XMLHTTP method load this code:
if (reqVar::get('xmlHttp') == 1)
{
    // if a isDefault is checked
	if (reqVar::get('isDefault') == 1)
	{
	  // check is another user favorit marked as default with the same catalog
      $fvr=dbProc::isUserCatalogFavoritExist($userId, reqVar::get('catalog'), reqVar::get('favoritId'));
      if($fvr !== false)
      {
	  ?>
        if(confirm("<?= sprintf(text::get('CONFIRM_FAVORIT_IS_DEFAULT'), $fvr['LTFV_NOSAUKUMS']);?> ") )
        {
        }
        else
        {
          document.all['isDefault'].checked = false;
        }
	  <?
      }
    }

    exit;
}

if( ( ($action !==false) && ($pageId==1)) ||
	(($favoritId !== false) && ($pageId==2)) )
{
	// tikai  lietotajam ar lomu EDITOR projektaa ir pieeja!
	if (( dbProc::isUserInRole($userId, ROLE_EDITOR) ||
            dbProc::isUserInRole($userId, ROLE_EDITOR_VIEWER) ||
            dbProc::isUserInRole($userId, ROLE_ED_USER)))
	{
		// first pafe, first steap
		if($pageId == 1)
		{
			$oLink=new urlQuery();
			$oLink->addPrm(FORM_ID, 'f.fvr.s.1');
			$oLink->addPrm('action', $action);
			$oLink->addPrm('pageId', 1);
			// form inicialization
			$oForm = new Form('frmMain1','post',$oLink->getQuery());
			unset($oLink);

			if($action != OP_INSERT)
			{
				// get query names
				$favoritName=array();
				$favoritName['']=text::get('EMPTY_SELECT_OPTION');
				$res=dbProc::getFavoritDefinitionList($userId);
				if (is_array($res))
				{
					foreach ($res as $row)
					{
						$favoritName[$row['LTFV_ID']]=$row['LTFV_NOSAUKUMS'];
					}
				}
				unset($res);
			}
            // get catalog import list
            $catalog=array();
            $catalog['']=text::get('EMPTY_SELECT_OPTION');
            $res=dbProc::getKrfkName(KRFK_FAVORIT);
            if (is_array($res))
            {
               	foreach ($res as $row)
              	{
              		$catalog[$row['nosaukums']]=$row['nozime'];
               	}
            }
            unset($res);

			// if operation is success, show success message
			if (reqVar::get('successMessage'))
			{
				$oForm->addSuccess(reqVar::get('successMessage'));
			}
			// form elements
			$oForm -> addElement('hidden', 'action', '', $action);

			// query name field
			if($action == OP_INSERT)
			{
				// if it is insert, drow twxt field
				$oForm -> addElement('text', 'favoritId',  text::get('NAME'),'', 'maxlength="30"');
				$oForm -> addRule('favoritId', text::get('ERROR_REQUIRED_FIELD'), 'required');

                $oForm -> addElement('select', 'catalog',  text::get('CATALOG'),'', 'onChange="document.all[\'isDefault\'].disabled=false;"', '', '', $catalog);
                $oForm -> addRule('catalog', text::get('ERROR_REQUIRED_FIELD'), 'required');

                $oLink=new urlQuery();
		        $oLink->addPrm(DONT_USE_GLB_TPL, 1);
		        $oLink->addPrm(FORM_ID, 'f.fvr.s.1');
		        $oLink->addPrm('favoritId', $favoritId);
				$oForm -> addElement('checkbox', 'isDefault',  text::get('FAVORIT_IS_DEFAULT'), '' ,'disabled onclick="if(this.checked){eval(xmlHttpGetValue(\''.$oLink->getQuery().'&xmlHttp=1&catalog=\'+document.all[\'catalog\'].value + \'&isDefault=1\'));}"');
                unset($oLink);

			}
			// id it is update, user should first select the query
			elseif($action == OP_UPDATE)
			{
				$oLink=new urlQuery();
				$oLink->addPrm(FORM_ID, 'f.fvr.s.1');
				$oLink->addPrm('action', $action);
				$oLink->addPrm('pageId', 1);
				$redirectUrl = $oLink->getQuery();
				$oForm -> addElement('select', 'favoritId',  text::get('NAME'), ($favoritId)?$favoritId:'', 'onChange="document.location.replace(\''.$redirectUrl.'&favoritId=\'+document.all[\'favoritId\'].value)"', '', '', $favoritName);

				if ($favoritId)
				{
                  // get info about favorit definition
                  $favoritDefinitionInfo = dbProc::getFavoritDefinition($favoritId);
			      //print_r($favoritDefinitionInfo);
                  if(count($favoritDefinitionInfo)>0)
      			  {
      				$favorit = $favoritDefinitionInfo[0];
      			  }

                    $oForm -> addElement('select', 'catalog',  text::get('CATALOG'),isset($favorit['LTFV_TIPS'])?$favorit['LTFV_TIPS']:'', ' disabled ', '', '', $catalog);
                    $oLink=new urlQuery();
		            $oLink->addPrm(DONT_USE_GLB_TPL, 1);
		            $oLink->addPrm(FORM_ID, 'f.fvr.s.1');
		            $oLink->addPrm('favoritId', $favoritId);
					$oForm -> addElement('checkbox', 'isDefault',  text::get('FAVORIT_IS_DEFAULT'), isset($favorit['LTFV_IR_DEFAULT'])?$favorit['LTFV_IR_DEFAULT']:'', 'onclick="if(this.checked){eval(xmlHttpGetValue(\''.$oLink->getQuery().'&xmlHttp=1&catalog=\'+document.all[\'catalog\'].value + \'&isDefault=1\'));}"');
                    unset($oLink);
				}

			}
			else
			{
				$oForm -> addElement('select', 'favoritId',  text::get('NAME'), ($favoritId)?$favoritId:'', '', '', '', $favoritName);
                $oForm -> addRule('favoritId', text::get('ERROR_REQUIRED_FIELD'), 'required');
			}

		   	// form buttons
			if(($action == OP_INSERT) || ($action == OP_UPDATE && $favoritId))
			{

				$oForm -> addElement('submitImg', 'continue', text::get('CONTINUE'), 'img/btn_turpinat.gif', 'width="70" height="20" ');

			}
			elseif($action == OP_DELETE)
			{
				$oForm -> addElement('submitImg', 'delete', text::get('DELETE'), 'img/btn_dzest.gif', 'width="70" height="20" onclick="if (isEmptyField(\'favoritId\') ) {} else if( !isDelete()) {return false;}"');
			}
            if(($action == OP_INSERT) || $action == OP_DELETE)
            {
			    $oForm -> addElement('resetImg', 'clear', text::get('CLEAR'), 'img/btn_attirit.gif', 'width="70" height="20"');
            }

			// if operation
			if ($oForm -> isFormSubmitted())
			{
				$r = false;
				$r1 = false;
				$checkRequiredFields = false;

				// check that all required fields are set
				//insert operation
				if ($oForm->getValue('action')==OP_INSERT || $oForm->getValue('action')==OP_UPDATE)
				{
					// query name should be defined
					if (($oForm->getValue('action')==OP_INSERT && $oForm->getValue('catalog')  && $oForm->getValue('favoritId')) ||
						($oForm->getValue('action')==OP_UPDATE && $oForm->getValue('catalog')  && $favoritId))
					{

						$checkRequiredFields = true;
					}
				}

				// delete operation
				if($oForm->getValue('action')==OP_DELETE)
				{
					// query name should be defined
					if($oForm->getValue('favoritId'))
					{
						$checkRequiredFields = true;
					}
				}

				// all required fields are set
				if ($checkRequiredFields)
				{
					// INSERT
					if($oForm->getValue('action')==OP_INSERT)
					{
                         // check is another user favorit marked as default with the same catalog
                        $fvr=dbProc::isUserCatalogFavoritExist($userId, $oForm->getValue('catalog'), false);
                        if($fvr !== false && $oForm->getValue('isDefault', 0) == 1)
                        {
                            $r=dbProc::saveFavoritDefinition($fvr['LTFV_ID'],
                                            false,
                                            false,
                                            false,
                                            0 );
                        }
						// save favorit header
                        $r=dbProc::saveFavoritDefinition(false, $userId,
                                            $oForm->getValue('favoritId'),
                                            $oForm->getValue('catalog'),
                                            $oForm->getValue('isDefault', 0)
                                            );
                        $favoritId=dbLayer::getInsertId();
                        dbProc::setUserActionDate($userId, FAVORIT_INSERT);
					}

					// update
					if($oForm->getValue('action')==OP_UPDATE)
					{
                         // check is another user favorit marked as default with the same catalog
                        $fvr=dbProc::isUserCatalogFavoritExist($userId, $oForm->getValue('catalog'), $oForm->getValue('favoritId'));
                        if($fvr !== false && $oForm->getValue('isDefault', 0) == 1)
                        {
                            $r=dbProc::saveFavoritDefinition($fvr['LTFV_ID'],
                                            false,
                                            false,
                                            false,
                                            0 );
                        }
                        // save favorit header
                        $r=dbProc::saveFavoritDefinition($oForm->getValue('favoritId'),
                                            false,
                                            false,
                                            false,
                                            $oForm->getValue('isDefault', 0)
                                            );
                        dbProc::setUserActionDate($userId, FAVORIT_UPDATE);
                  	}

					// delete
					if($oForm->getValue('action')==OP_DELETE)
					{

						// delete favoriita veertibas
						dbProc::deleteFavorits($oForm->getValue('favoritId'));
						// delete favoriita definiiciju
						$r=dbProc::deleteFavoritDefinition($oForm->getValue('favoritId'));
                        dbProc::setUserActionDate($userId, FAVORIT_DELETE);
					}

					if(!$r)
					{
						// show error
						$oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
					}
					else
					{
						$oLink=new urlQuery();
						$oLink->addPrm(FORM_ID, 'f.fvr.s.1');
						$oLink->addPrm('action', $action);
						$oLink->addPrm('successMessage', text::get('INFO_WAS_SAVED'));
						if($action == OP_DELETE)
						{
							$oLink->addPrm('pageId', 1);
							$oLink->addPrm('successMessage', text::get('QUERY_WAS_DELETED'));
						}
						else
						{
							$oLink->addPrm('favoritId', $favoritId);
							$oLink->addPrm('pageId', 2);
						}
                        $listLink=$oLink ->getQuery();
                        unset($oLink);

                        //link for frame2
		                $oLink=new urlQuery();
		                $oLink->addPrm(FORM_ID, 'f.fvr.s.2');
		                $oLink->addPrm('favoritId', $favoritId);
                        $resultLink=$oLink ->getQuery();
                        unset($oLink);

                        // delete
    					if($oForm->getValue('action')==OP_DELETE)
    					{
                            RequestHandler::makeRedirect($listLink);
                        }
                        else
                        {
                             $oForm->addElement('static','jsRefresh2','','
                              <script>
                                  parent["frameTop"].enableFrameControl();
								  window.top.enableResize();
								  window.top.min(0);
                                  window.top.normal();
                                  reloadFrame(1,"'.$listLink.'");
                                  reloadFrame(2,"'.$resultLink.'");
                                  reloadFrame(3,"");
                             </script>
                       ');  
                       }
					}
				}
				else
				{
					$oForm->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
				}
			}
			$oForm -> makeHtml();
			include('f.fvr.s.1.1.tpl');
		}
		##################
		##################
		// second page, second steap
		if($pageId == 2)
		{
            // Check if this form was requested via xmlHttpRequest and search
        	if (isset($_GET['xml']) && isset($_GET['search_q']) && isset($_GET['search_c']))
        	{
        		ob_clean();
        		$q = $_GET['search_q'];
                if (isset($q) || $q == 0)
        		{
        			echo 'q = "'.trim($q).'";';
        		}
                else
                {
        			echo 'q = "";';
        		}
                $c = $_GET['search_c'];
                if ($c)
        		{
        			echo 'c = "'.$c.'";';
        		}
                else
                {
        			echo 'c = "";';
        		}
    		    exit;
        	}
            if (isset($_GET['xml']) && isset($_GET['sort_k']) && isset($_GET['sort_o']))
        	{
        		ob_clean();
        		$o = $_GET['sort_o'];
                if (isset($o))
        		{
        			echo 'o = "'.$o.'";';
        		}
                else
                {
        			echo 'o = "";';
        		}
                $k = $_GET['sort_k'];
                if ($k)
        		{
        			echo 'k = "'.$k.'";';
        		}
                else
                {
        			echo 'k = "";';
        		}
    		    exit;
        	}
            // if a kataloga ID set
        	if (isset($_GET['xml']) && isset($_GET['code']) && isset($_GET['op']))
        	{
                ob_clean();
        		$o = $_GET['op'];
                $cd = $_GET['code'];
                if (isset($o) && $o == OP_INSERT)
        		{
        			$r = dbProc::saveFavoritValue($favoritId, $cd);
                    if($r == "NOTYEAR")
                    {        // alert("Gads nav aktu�ls.");  
                      ?>

                      <?
                    }
        		}
                else
                {
                   dbProc::deleteFavoritValue($favoritId, $cd);
                }
                exit;
            }
            $searchText = ((reqVar::get('search') && reqVar::get('search') != '') || reqVar::get('search') == 0) ? reqVar::get('search') : false;
            $searchColumn = (reqVar::get('column') && reqVar::get('column') != '') ? reqVar::get('column') : false;
            $sortOrder = (reqVar::get('order') && reqVar::get('order') != '') ? reqVar::get('order') : false;
            $orderColumn = (reqVar::get('kol') && reqVar::get('kol') != '') ? reqVar::get('kol') : false;

            // URL to search .
        	$oLink = new urlQuery;
        	$oLink -> addPrm(FORM_ID, 'f.fvr.s.1');
        	$oLink->addPrm('favoritId', $favoritId);
            $oLink->addPrm('pageId', 2);

            // add search/order params to listing
            $oLink->addPrm('search', reqVar::get('search'));
            $oLink->addPrm('column', reqVar::get('column'));
            $oLink->addPrm('order', reqVar::get('order'));
            $oLink->addPrm('kol', reqVar::get('kol'));
            $searchLink = $oLink -> getQuery();
            $listingLink = $oLink -> getQuery();
            unset($oLink);
            
            // get info about favorit definition
            $favoritDefinitionInfo = dbProc::getFavoritDefinition($favoritId);
			if(count($favoritDefinitionInfo)>0)
      		{
      		    $favorit = $favoritDefinitionInfo[0];
      		}
            $catalog = $favorit['LTFV_TIPS'];
            // set list title
            $title = text::toUpper(sprintf(text::get('EDIT_FAVORIT'), $favorit['LTFV_NOSAUKUMS']));
            // inicialising of listing object
            $listing=new listing();

            switch ($catalog)
            {
              case 'MATERIAL' :

                // get search column list
                $columns=dbProc::getKlklName(KL_MATERIALS);
                // default sort column
                $defaultColumn = dbProc::getKlklDefaultColumn(KL_MATERIALS);
		        $amount=dbProc::getMaterialCount($searchText,
                (($searchColumn === false) ? $defaultColumn : $searchColumn), true);

                $listing->setAmount($amount);
                $listingHtml=$listing->getHtml($listingLink);
                // gel list
		        $res = dbProc::getAllMaterialWithFavoritList( $favoritId,
                                        $listing->getStartPosition(),
                                        $listing->getAmountOnPage(),
                                        $searchText,
                                        (($searchColumn === false) ? $defaultColumn : $searchColumn),
                                        $sortOrder,
                                        $orderColumn);

                if (is_array($res))
        		{
        			foreach ($res as $i=>$row)
        			{
        				$res[$i]['CODE']=$row['KMAT_KODS'];
                        $res[$i]['CHECKED']= (isset($row['FAVR_ID']) ? 'checked' : '');
        			}
        		}
                break;
                case 'PERSONAL' :

                // get search column list
                $columns=dbProc::getKlklName(KL_CUSTOMERS);
                // default sort column
                $defaultColumn = dbProc::getKlklDefaultColumn(KL_CUSTOMERS);
		        $amount=dbProc::getCustomerCount($searchText,
                (($searchColumn === false) ? $defaultColumn : $searchColumn), true);
                $listing->setAmount($amount);
                $listingHtml=$listing->getHtml($listingLink);
                // gel list
		        $res = dbProc::getAllCustomerWithFavoritList($favoritId,
                                        $listing->getStartPosition(),
                                        $listing->getAmountOnPage(),
                                        $searchText,
                                        (($searchColumn === false) ? $defaultColumn : $searchColumn),
                                        $sortOrder,
                                        $orderColumn);
                if (is_array($res))
        		{
        			foreach ($res as $i=>$row)
        			{
        				$res[$i]['CODE']=$row['KPRF_KODS'];
                        $res[$i]['CHECKED']= (isset($row['FAVR_ID']) ? 'checked' : '');
        			}
        		}

                break;
                case 'TRANSPORT' :

                // get search column list
                $columns=dbProc::getKlklName(KL_TRANSPORT);
                // default sort column
                $defaultColumn = dbProc::getKlklDefaultColumn(KL_TRANSPORT);
		        $amount=dbProc::getTransportCount($searchText,
                (($searchColumn === false) ? $defaultColumn : $searchColumn), true);
                $listing->setAmount($amount);
                $listingHtml=$listing->getHtml($listingLink);
                // gel list
		        $res = dbProc::getAllTransportWithFavoritList( $favoritId,
                                        $listing->getStartPosition(),
                                        $listing->getAmountOnPage(),
                                        $searchText,
                                        (($searchColumn === false) ? $defaultColumn : $searchColumn),
                                        $sortOrder,
                                        $orderColumn);
                if (is_array($res))
        		{
        			foreach ($res as $i=>$row)
        			{
        				$res[$i]['CODE']=$row['KMEH_VALSTS_NUMURS'];
                        $res[$i]['CHECKED']= (isset($row['FAVR_ID']) ? 'checked' : '');
        			}
        		}
                break;
                case 'ED_AREA' :

                // get search column list
                $columns=dbProc::getKlklName(KL_ED_AREA);
                // default sort column
                $defaultColumn = dbProc::getKlklDefaultColumn(KL_ED_AREA);
		        $amount=dbProc::getEDAreaCount($searchText,
                (($searchColumn === false) ? $defaultColumn : $searchColumn), true);
                $listing->setAmount($amount);
                $listingHtml=$listing->getHtml($listingLink);
                // gel list
		        $res = dbProc::getAllEDAreaWithFavoritList( $favoritId,
                                        $listing->getStartPosition(),
                                        $listing->getAmountOnPage(),
                                        $searchText,
                                        (($searchColumn === false) ? $defaultColumn : $searchColumn),
                                        $sortOrder,
                                        $orderColumn);
                if (is_array($res))
        		{
        			foreach ($res as $i=>$row)
        			{
        				$res[$i]['CODE']=$row['KEDI_KODS'];
                        $res[$i]['CHECKED']= (isset($row['FAVR_ID']) ? 'checked' : '');
        			}
        		}
                break;
                case 'MMS_WORKS' :

                // get search column list
                $columns=dbProc::getKlklName(KL_MMS_WORKS);
                // default sort column
                $defaultColumn = dbProc::getKlklDefaultColumn(KL_MMS_WORKS);
		        $amount=dbProc::getMMSWorkCount($searchText,
                (($searchColumn === false) ? $defaultColumn : $searchColumn), true);
                $listing->setAmount($amount);
                $listingHtml=$listing->getHtml($listingLink);
                // gel list
		        $res = dbProc::getAllMMSWorkWithFavoritList( $favoritId,
                                        $listing->getStartPosition(),
                                        $listing->getAmountOnPage(),
                                        $searchText,
                                        (($searchColumn === false) ? $defaultColumn : $searchColumn),
                                        $sortOrder,
                                        $orderColumn);
                if (is_array($res))
        		{
        			foreach ($res as $i=>$row)
        			{
        				$res[$i]['CODE']=$row['KMSD_ID'];
                        $res[$i]['CHECKED']= (isset($row['FAVR_ID']) ? 'checked' : '');
        			}
        		}
                break;
                case 'MMS_WORKS_ALL' :

                // get search column list
                $columns=dbProc::getKlklName(KL_MMS_WORKS);
                // default sort column
                $defaultColumn = dbProc::getKlklDefaultColumn(KL_MMS_WORKS);
		        $amount=dbProc::getMMSWorkCount($searchText,
                (($searchColumn === false) ? $defaultColumn : $searchColumn), false);
                $listing->setAmount($amount);
                $listingHtml=$listing->getHtml($listingLink);
                // gel list
		        $res = dbProc::getAllMMSWorkWithFavoritList( $favoritId,
                                        $listing->getStartPosition(),
                                        $listing->getAmountOnPage(),
                                        $searchText,
                                        (($searchColumn === false) ? $defaultColumn : $searchColumn),
                                        $sortOrder,
                                        $orderColumn,
                                        true);
                if (is_array($res))
        		{
        			foreach ($res as $i=>$row)
        			{
        				$res[$i]['CODE']=$row['KMSD_ID'];
                        $res[$i]['CHECKED']= (isset($row['FAVR_ID']) ? 'checked' : '');
        			}
        		}
                break;
                case 'CALKULATION' :

                // get search column list
                $columns=dbProc::getKlklName(KL_CALCULALATION);
                // default sort column
                $defaultColumn = dbProc::getKlklDefaultColumn(KL_CALCULALATION);
		        $amount=dbProc::getCalculationCount($searchText,
                (($searchColumn === false) ? $defaultColumn : $searchColumn), true);
                $listing->setAmount($amount);
                $listingHtml=$listing->getHtml($listingLink);
                // gel list
		        $res = dbProc::getAllCalculationWithFavoritList( $favoritId,
                                        $listing->getStartPosition(),
                                        $listing->getAmountOnPage(),
                                        $searchText,
                                        (($searchColumn === false) ? $defaultColumn : $searchColumn),
                                        $sortOrder,
                                        $orderColumn);
                if (is_array($res))
        		{
        			foreach ($res as $i=>$row)
        			{
        				$res[$i]['CODE']=$row['KKAL_SHIFRS'];
                        $res[$i]['CHECKED']= (isset($row['FAVR_ID']) ? 'checked' : '');
        			}
        		}
                break;
            }

            //link for frame2
		    $oLink=new urlQuery();
		    $oLink->addPrm(FORM_ID, 'f.fvr.s.2');
		    $oLink->addPrm('favoritId', $favoritId);
            $resultLink=$oLink ->getQuery();
             unset($oLink);

           	include('f.fvr.s.1.2.tpl');
		}
	}
	else
	{
		RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NO_ALL_INPUT_DATES'));
}
?>
