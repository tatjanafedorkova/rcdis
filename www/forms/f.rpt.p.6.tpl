﻿<body class="print">
<div class="print_header"><?= text::get('REPORT_TRANSPORT_USAGE'); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >
<?
  foreach($searchCr as $i=>$s)
  {
    if($i%3 == 0)
    {
      ?>
        <tr>
      <?
    }
    ?>
      <td align="right" class="print_table_total" width="13%"><?=$s['label'];?>:</td>
      <td align="left" class="print_table_data" width="20%"><?=$s['value'];?></td>
    <?
  }
?>
</table>
<br />
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >

		<tr>
			<td width="20%" class="print_table_header"><?= text::get('SINGL_DV_AREA'); ?></td>
			<td width="20%" class="print_table_header"><?= text::get('NATION_NUMBER'); ?></td>
            <td width="20%" class="print_table_header"><?=text::get('TRANSPORT_KM');?></td>
            <td width="20%" class="print_table_header"><?=text::get('TRANSPORT_AVTO_TIME');?></td>
            <td width="20%" class="print_table_header"><?=text::get('TRANSPORT_WORK_TIME');?></td>
 		</tr>

<?
	if (is_array($res) && count($res) > 0)
	{
		foreach ($res as $row)
		{
?>
			<tr >
			   	<td align="center" class="print_table_data"><?= $row['DV_KODS']; ?></td>
                <td align="left" class="print_table_data"><?= $row['TRNS_VALSTS_NUMURS']; ?></td>
                <td align="center" class="print_table_data"><?= $row['KM']; ?></td>
                <td align="center" class="print_table_data"><?= $row['STUNDAS']; ?></td>
                <td align="center" class="print_table_data"><?= $row['DARBA_STUNDAS']; ?></td>

			</tr>
<?
		}
?>
        <tr>
             <td align="right" class="print_table_total" colspan="2"><?=text::get('TOTAL');?>:</td>
             <td align="center" class="print_table_total"><?= $km; ?></td>
             <td align="center" class="print_table_total"><?= $stundas; ?></td>
             <td align="center" class="print_table_total"><?= $darbaStundas; ?></td>
       </tr>
<?
	}
?>

</table>

</body>