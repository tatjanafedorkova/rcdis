﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
	$searchMode = reqVar::get('isSearch');
	// top frame
    if($searchMode)
	{
        $oLink=new urlQuery();
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.16');
    	$oLink->addPrm('isSearch', 1 );
    	// form inicialization
    	$oForm = new Form('frmMain','post',$oLink->getQuery());
    	unset($oLink);

        $oLink=new urlQuery();
	    $oLink->addPrm('isReturn', '1');
        $oLink->addPrm('isSearch', 1 );
	    $oLink->addPrm(FORM_ID, 'f.rpt.s.16');
	    $criteriaLink=$oLink ->getQuery();
	    unset($oLink);

        // if operation
    	if ($oForm -> isFormSubmitted())
    	{
            $oListLink=new urlQuery();
            $oListLink->addPrm(FORM_ID, 'f.rpt.s.16');
        }
        $searchTitle = text::toUpper(text::toUpper(text::get('REPORT_NOT_STARTED_WORK')));
        $searchName = 'REPORT_NOT_STARTED_WORK';
        $notPlan = true;
        include('f.akt.m.10.inc');
	}
	// bottom frale
	else
	{
        $sCriteria = reqVar::get('search');

        /*// URL  export
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.e.16');
    	$oLink->addPrm('search', $sCriteria);
        $oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $exportUrl =  $oLink ->getQuery();
        unset($oLink); */

        // URL  print
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.p.16');
    	$oLink->addPrm('search', $sCriteria);
        //$oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $printUrl =  $oLink ->getQuery();
        unset($oLink);

		// gel list of users
		$res = dbProc::getMMSNotStartedWorksList($sCriteria);
        $area = array();

        $summa = 0;
        $stundas = 0;

        if (is_array($res))
		{
            foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]  = array (
                'section' => $row['KEDI_SECTION'],
                'totalStundas' => (isset($area[$row['KEDI_SECTION']]['totalStundas'])?$area[$row['KEDI_SECTION']]['totalStundas']:0) + $row['MMS_CILVEKSTUNDAS'],
                'totalCena' => (isset($area[$row['KEDI_SECTION']]['totalCena'])?$area[$row['KEDI_SECTION']]['totalCena']:0) + $row['MMS_MATR_IZMAKSAS'],
                'ed' => array()
               );

               $summa = $summa + $row['MMS_MATR_IZMAKSAS'];
               $stundas = $stundas + $row['MMS_CILVEKSTUNDAS'];

            }
            foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]  = array (
                'code' => $row['KEDI_KODS'],
                'name' => $row['ED'],
                'totalStundas' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalStundas'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalStundas']:0) + $row['MMS_CILVEKSTUNDAS'],
                'totalCena' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalCena'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalCena']:0) + $row['MMS_MATR_IZMAKSAS'],
                'mms' => array()
               );


            }
            foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['KMSD_KODS']]  = array(
                    'code' => $row['KMSD_KODS'],
                    'quarter' => $row['KMSD_CETUKSNIS'],
                    'stundas' => $row['MMS_CILVEKSTUNDAS'],
                    'cena' => $row['MMS_MATR_IZMAKSAS'],
                    'priority' => $row['KMSD_PRIORITATE']
               );
            }
		}

        $summa = number_format($summa, 2, '.', '');
        $stundas = number_format($stundas,2, '.', '');



		include('f.rpt.s.16.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
