﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);

define ('COLUMN_VOLTAGE','s.KSRG_NOSAUKUMS');
define ('COLUMN_FINANSESANAS_AVOTS','f.KFNA_NOSAUKUMS');

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
	$searchMode = reqVar::get('isSearch');
	// top frame
    if($searchMode)
	{
        $oLink=new urlQuery();
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.8');
    	$oLink->addPrm('isSearch', 1 );
    	// form inicialization
    	$oForm = new Form('frmMain','post',$oLink->getQuery());
    	unset($oLink);

        $oLink=new urlQuery();
	    $oLink->addPrm('isReturn', '1');
        $oLink->addPrm('isSearch', 1 );
	    $oLink->addPrm(FORM_ID, 'f.rpt.s.8');
	    $criteriaLink=$oLink ->getQuery();
	    unset($oLink);

        // if operation
    	if ($oForm -> isFormSubmitted())
    	{
            $oListLink=new urlQuery();
            $oListLink->addPrm(FORM_ID, 'f.rpt.s.8');
        }
        $searchTitle = text::toUpper(text::toUpper(text::get('REPORT_MAIN_ACT')));
        $searchName = 'REPORT_MAIN_ACT_ED';
        include('f.akt.m.4.inc');
	}
	// bottom frale
	else
	{
        $sCriteria = reqVar::get('search');

        // URL  export
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.e.8');
    	$oLink->addPrm('search', $sCriteria);
        $oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $exportUrl =  $oLink ->getQuery();
        unset($oLink);

        // URL  print
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.p.8');
    	$oLink->addPrm('search', $sCriteria);
        //$oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $printUrl =  $oLink ->getQuery();
        unset($oLink);

		// gel list of users
		$res = dbProc::getEDActFullList($sCriteria);
        $pamatstundas = 0;
        $virsstundas = 0;
        $summa = 0;
        $km = 0;
        $stundas = 0;
        $darbaStundas = 0;
        $cilveki = 0;
        if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
               $pamatstundas = $pamatstundas + $row['PAMATSTUNDAS'];
               $virsstundas = $virsstundas + $row['VIRSSTUNDAS'];
               $summa = $summa + $row['CENA_KOPA'];
               $km = $km + $row['KM'];
               $stundas = $stundas + $row['STUNDAS'];
               $darbaStundas = $darbaStundas + $row['DARBA_STUNDAS'];
               $cilveki = $cilveki + $row['CILVEKI'];
			}
		}
        $pamatstundas = number_format($pamatstundas,2, '.', '');
        $virsstundas = number_format($virsstundas,2, '.', '');
        $summa = number_format($summa, 2, '.', '');
        $km = number_format($km,2, '.', '');
        $stundas = number_format($stundas,2, '.', '');
        $darbaStundas = number_format($darbaStundas, 2, '.', '');

		include('f.rpt.s.8.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
