﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAuditor =  dbProc::isUserInRole($userId, ROLE_AUDIT_USER);
// tikai administratoram ir pieeja!
if (userAuthorization::isAdmin() || $isAuditor)
{
	$editMode = reqVar::get('editMode');
	// bottom frame
	// if insert/update

     if($editMode)
	{
        // get region list
       $region=array();
       $region['']=text::get('EMPTY_SELECT_OPTION');
       $res=dbProc::getKrfkName(KRFK_REGION);
       if (is_array($res))
       {
       	foreach ($res as $row)
       	{
       		$region[$row['nosaukums']]=$row['nosaukums'];
       	}
       }
       $region['TN']=text::get('TRANSPORT_DEPARTMENT');
       unset($res);

		$transportId = reqVar::get('transportId');

		$oLink=new urlQuery();
		$oLink->addPrm(FORM_ID, 'f.ktl.s.3');
		$oLink->addPrm('editMode', 1 );
		// form inicialization
		$oForm = new Form('frmMain','post',$oLink->getQuery());
		unset($oLink);

		// if operation is success, show success message and redirect top frame
		if (reqVar::get('successMessage'))
		{
			$oForm->addSuccess(reqVar::get('successMessage'));
			$oForm->addElement('static','jsRefresh2','','<script>refreshFrame(1);</script>');
		}

		// inicial state: transportId=0;
		// if transportId>0 ==> voltage selected from the list
		if($transportId != false)
		{
			// get info about voltage
			$transportInfo = dbProc::getTransportList($transportId);
			//print_r($transportInfo);
			if(count($transportInfo)>0)
			{
				$transport = $transportInfo[0];
			}

			// get user info
			$userInfo = dbProc::getUserInfo($transport['KMEH_CREATOR']);
			if(count($userInfo) >0 )
			{
				$creator = $userInfo[0]['RLTT_VARDS']. ' ' .$userInfo[0]['RLTT_UZVARDS'];
			}
			$userInfo1 = dbProc::getUserInfo($transport['KMEH_EDITOR']);
			if(count($userInfo1) >0 )
			{
				$editor = $userInfo1[0]['RLTT_VARDS']. ' ' .$userInfo1[0]['RLTT_UZVARDS'];
			}
        }

		// form elements
		$oForm -> addElement('hidden', 'action', '');
		$oForm -> addElement('hidden', 'transportId', null, isset($transport['KMEH_ID'])? $transport['KMEH_ID']:'');

        $oForm -> addElement('text', 'number',  text::get('NATION_NUMBER'), isset($transport['KMEH_VALSTS_NUMURS'])?$transport['KMEH_VALSTS_NUMURS']:'', ' tabindex=1 maxlength="10"' . (($transportId || $isAuditor)?' disabled' : ''));
		$oForm -> addRule('number', text::get('ERROR_REQUIRED_FIELD'), 'required');
        $oForm -> addRule('number', text::get('ERROR_ALPHANUMERICONLY_VALUE'), 'alphanumericonly');

        $oForm -> addElement('text', 'group',  text::get('GROUP_TITLE'), isset($transport['KMEH_GRUPAS_NOSAUKUMS'])?$transport['KMEH_GRUPAS_NOSAUKUMS']:'', ' tabindex=3 maxlength="30"'.(($isAuditor)?' disabled' : ''));
		$oForm -> addRule('group', text::get('ERROR_REQUIRED_FIELD'), 'required');

        $oForm -> addElement('text', 'subgroupCode',  text::get('SUBGROUP_CODE'), isset($transport['KMEH_APAKSGRUPAS_KODS'])?$transport['KMEH_APAKSGRUPAS_KODS']:'', ' tabindex=2 maxlength="3"'.(($isAuditor)?' disabled' : ''));
		$oForm -> addRule('subgroupCode', text::get('ERROR_REQUIRED_FIELD'), 'required');
        $oForm -> addRule('subgroupCode', text::get('ERROR_NUMERIC_VALUE'), 'numeric');

        $oForm -> addElement('text', 'subgroup',  text::get('SUBGROUP_TITLE'), isset($transport['KMEH_APAKSGRUPAS_NOSAUKUMS'])?$transport['KMEH_APAKSGRUPAS_NOSAUKUMS']:'', 'tabindex=4 bmaxlength="30"'.(($isAuditor)?' disabled' : ''));
        $oForm -> addRule('subgroup', text::get('ERROR_REQUIRED_FIELD'), 'required');

        $oForm -> addElement('select', 'region',  text::get('TRANSPORT_LOCATION'), isset($transport['KMEH_PIEDERIBA'])?$transport['KMEH_PIEDERIBA']:'', 'tabindex=5'.(($isAuditor)?' disabled' : ''), '', '', $region);
        //$oForm -> addRule('region', text::get('ERROR_REQUIRED_FIELD'), 'required');

		$oForm -> addElement('checkbox', 'isActive',  text::get('IS_ACTIVE'), isset($transport['KMEH_IR_AKTIVS'])?$transport['KMEH_IR_AKTIVS']:1,'tabindex=6'.(($isAuditor)?' disabled' : ''));

		// importa dati
		$oForm -> addElement('text', 'created',  text::get('CREATED'), isset($transport['KMEH_CREATED'])?$transport['KMEH_CREATED']:dtime::now(), 'tabindex=21 maxlength="20" disabled');
		$oForm -> addElement('text', 'creator',  text::get('CREATOR'), isset($creator)?$creator:'', 'tabindex=21 maxlength="20" disabled');
		$oForm -> addElement('text', 'edited',  text::get('EDITED'), isset($transport['KMEH_EDITED'])?$transport['KMEH_EDITED']:dtime::now(), 'tabindex=21 maxlength="20" disabled');
		$oForm -> addElement('text', 'editor',  text::get('EDITOR'), isset($transport)?$editor:'', 'tabindex=21 maxlength="20" disabled');


		// form buttons
        if(!$isAuditor)
        {
		$oForm -> addElement('submitImg', 'add', text::get('ADD'), 'img/btn_pievienot.gif', 'width="70" height="20" onclick="setValue(\'action\',\''.OP_INSERT.'\');"');
		$oForm -> addElement('submitImg', 'save', text::get('SAVE'), 'img/btn_saglabat.gif', 'width="70" height="20" onclick="if (!isEmptyField(\'transportId\')) {setValue(\'action\',\''.OP_UPDATE.'\');} else {return false;}"');
		$oForm -> addElement('clearImg', 'clear', text::get('CLEAR'), 'img/btn_attirit.gif', 'width="70" height="20"','','',array('onClick'=>'refreshButton(\'transportId\');parent.frame_1.unActiveRow();'));
        $oForm -> addElement('buttonImg', 'deleteFavorit', text::get('CLEAN_FAVORITS'), 'img/btn_iztirit_favoritus.gif', 'width="90" height="20" onclick="setValue(\'action\',\''.OP_DELETE.'\'); document.all[\'frmMain\'].submit();"');
     	$oForm -> addElement('static', 'jsButtonsControl', '', '
			<script>
				function refreshButton(name)
				{
					if (!isEmptyField(name))
					{
						document.all["save"].src="img/btn_saglabat.gif";

					}
					else
					{
						document.all["save"].src="img/btn_saglabat_disabled.gif";
						document.all["isActive"].checked=true;
					}
				}
				refreshButton("transportId");
			</script>
		');
         }
		// if operation
		if ($oForm -> isFormSubmitted())
		{
			$check = true;
			$r = false;
			$checkRequiredFields = false;
            // delete favorits
            if ($oForm->getValue('action')== OP_DELETE )
            {
               $favorit = dbProc::getKrfkInfo(22);

               $r = dbProc::deleteNotActivFromUserFavorit($favorit['KRFK_VERTIBA']) ;

            }
            // save
			elseif ($oForm->getValue('action')==OP_INSERT || ($oForm->getValue('action')==OP_UPDATE  && is_numeric($oForm->getValue('transportId'))))
			{
    			if ($oForm -> getValue('number') && $oForm -> getValue('group') &&
                    $oForm -> getValue('subgroupCode') && /*$oForm -> getValue('region') &&*/
                    $oForm -> getValue('subgroup') )
    			{
    				$checkRequiredFields = true;
    			}
    			if ($checkRequiredFields)
    			{

					// chech that ED area with same region and code not set
					if (dbProc::transportWithNationalNumberExists(
                                                            $oForm->getValue('number'),
                                                            ($oForm->getValue('action')==OP_UPDATE)?$oForm->getValue('transportId'):false)
                                                            )
					{
						$oForm->addError(text::get('ERROR_EXISTS_TRANSPORT_CODE'));
						$check = false;
					}

					// if all is ok, do operation
					if($check)
					{
						$r=dbProc::saveTransport(($oForm->getValue('action')==OP_UPDATE?$oForm->getValue('transportId'):false),
						$oForm->getValue('number'),
                        $oForm->getValue('group'),
                        $oForm->getValue('subgroupCode'),
                        $oForm->getValue('subgroup'),
                        $oForm->getValue('region'),
						$oForm->getValue('isActive',0),
						$userId
						);
                        echo $r;
						if (!$r)
						{
							$oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
						}
					}
				}

			}
			else
			{
				$oForm->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
			}
            // if operation was compleated succefully, show success mesage and redirect current frame
    		if ($r)
    		{
    			$oLink=new urlQuery();
    			$oLink->addPrm(FORM_ID, 'f.ktl.s.3');
    			$oLink->addPrm('editMode', '1');
    			switch($oForm->getValue('action'))
    			{
    				case OP_INSERT:
    					$oLink->addPrm('successMessage', text::get('RECORD_WAS_ADDED'));
    					break;
    				case OP_UPDATE:
    					$oLink->addPrm('successMessage', text::get('RECORD_WAS_SAVED'));
    					break;
                          case OP_DELETE:
          					$oLink->addPrm('successMessage', text::get('FAVORITS_IS_CLENED'));
          					break;
    			}
    			RequestHandler::makeRedirect($oLink->getQuery());
    		}
		}

       	$oForm -> makeHtml();
		include('f.ktl.s.3.2.tpl');
	}
	// top frale
	else
	{
        // Check if this form was requested via xmlHttpRequest and search
    	if (isset($_GET['xml']) && isset($_GET['search_q']) && isset($_GET['search_c']))
    	{
    		ob_clean();
    		$q = $_GET['search_q'];
            if (isset($q) || $q == 0)
    		{
    			echo 'q = "'.trim($q).'";';
    		}
            else
            {
    			echo 'q = "";';
    		}
            $c = $_GET['search_c'];
            if ($c)
    		{
    			echo 'c = "'.$c.'";';
    		}
            else
            {
    			echo 'c = "";';
    		}
		    exit;
    	}
        if (isset($_GET['xml']) && isset($_GET['sort_k']) && isset($_GET['sort_o']))
    	{
    		ob_clean();
    		$o = $_GET['sort_o'];
            if (isset($o))
    		{
    			echo 'o = "'.$o.'";';
    		}
            else
            {
    			echo 'o = "";';
    		}
            $k = $_GET['sort_k'];
            if ($k)
    		{
    			echo 'k = "'.$k.'";';
    		}
            else
            {
    			echo 'k = "";';
    		}
		    exit;
    	}
        // set list title
        $title = text::toUpper(text::get('TRANSPORT'));
        // get search column list
        $columns=dbProc::getKlklName(KL_TRANSPORT);
        $searchText = ((reqVar::get('search') && reqVar::get('search') != '') || reqVar::get('search') == 0) ? reqVar::get('search') : false;
        $searchColumn = (reqVar::get('column') && reqVar::get('column') != '') ? reqVar::get('column') : false;
        $sortOrder = (reqVar::get('order') && reqVar::get('order') != '') ? reqVar::get('order') : false;
        $orderColumn = (reqVar::get('kol') && reqVar::get('kol') != '') ? reqVar::get('kol') : false;


        // default sort column
        $defaultColumn = dbProc::getKlklDefaultColumn(KL_TRANSPORT);

        // URL to search Zinojums by number.
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.ktl.s.3');
    	$oLink->addPrm('isNew', '1');

        // add search/order params to listing
        $oLink->addPrm('search', reqVar::get('search'));
        $oLink->addPrm('column', reqVar::get('column'));
        $oLink->addPrm('order', reqVar::get('order'));
        $oLink->addPrm('kol', reqVar::get('kol'));
        $searchLink = $oLink -> getQuery();

		// inicialising of listing object
		$amount=dbProc::getTransportCount($searchText,
                (($searchColumn === false) ? $defaultColumn : $searchColumn));
		$listing=new listing();
		$listing->setAmount($amount);

		$listingHtml=$listing->getHtml($oLink ->getQuery());
		unset($oLink);

		//link for frame2
		$oLink=new urlQuery();
		$oLink->addPrm(FORM_ID, 'f.ktl.s.3');
		$oLink->addPrm('editMode', 1);

		// gel list of users
		$res = dbProc::getTransportList( false,
                                        $listing->getStartPosition(),
                                        $listing->getAmountOnPage(),
                                        $searchText,
                                        (($searchColumn === false) ? $defaultColumn : $searchColumn),
                                        $sortOrder,
                                        $orderColumn);

		if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
				// add dv area Id to each link
				$oLink->addPrm('transportId', $row['KMEH_ID']);
				$res[$i]['URL']=$oLink ->getQuery();
			}
		}
		unset($oLink);

		// inicial state of users number
		$number=$listing->getStartPosition()+1;

		include('f.ktl.s.x.1.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
