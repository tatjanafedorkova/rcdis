﻿<body class="frame_1">
<?= $oFormPop -> getFormHeader(); ?>
<h1><?=$title;?></h1>
<div align=center><?= $oFormPop -> getMessage(); ?></div>

<table cellpadding="5" cellspacing="1" border="0" width="100%">
<tr>
    <td class="table_cell_c" width="20%"><?= $oFormPop -> getElementLabel('work'); ?>:<font color="red">*</font></td>
    <td class="table_cell_2" width="80%" colspan="3"><?= $oFormPop -> getElementHtml('work'); ?></td>
</tr>
<tr>
    <td class="table_cell_c" width="20%"><?= $oFormPop -> getElementLabel('description'); ?></td>
    <td class="table_cell_2" width="80%" colspan="3"><?= $oFormPop -> getElementHtml('description'); ?></td>
</tr>
<tr>
    <td class="table_cell_c" width="20%"><?= $oFormPop -> getElementLabel('start_date'); ?>:<font color="red">*</font></td>
    <td class="table_cell_2" width="30%"><?= $oFormPop -> getElementHtml('start_date'); ?></td>
    <td class="table_cell_c" width="20%"><?= $oFormPop -> getElementLabel('end_date'); ?>:<font color="red">*</font></td>
    <td class="table_cell_2" width="30%"><?= $oFormPop -> getElementHtml('end_date'); ?></td>
</tr>
<tr>
    <td class="table_cell_c" width="20%"><?= $oFormPop -> getElementLabel('scope'); ?>:<font color="red">*</font></td>
    <td class="table_cell_2"  colspan="3"><?= $oFormPop -> getElementHtml('scope'); ?></td>
</tr>

</table>

<table cellpadding="5" cellspacing="0" border="0" align="center">
    <tr>
      <? if(!$isReadonly)  {?>
        <td><?= $oFormPop -> getElementHtml('save'); ?></td>
      <? } ?>
        <td><?=$oFormPop->getElementHtml('close');?></td>
    </tr>
</table>
<?= $oFormPop->getFormBottom(); ?>
</body>