﻿<body class="print">
<div class="print_header"><?= text::get('REPORT_WORK_WITH_PLAN'); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_landscape" >
<?
  foreach($searchCr as $i=>$s)
  {
    if($i%3 == 0)
    {
      ?>
        <tr>
      <?
    }
    ?>
      <td align="right" class="print_table_total" width="13%"><?=$s['label'];?>:</td>
      <td align="left" class="print_table_data" width="20%"><?=$s['value'];?></td>
    <?
  }
?>
</table>
<br />
<table cellpadding="0" cellspacing="0" class="print_table_landscape" >
<?
    $cilvekstundasRegion = 0;
    $materIzmaksasRegion = 0;
	if (is_array($area) && count($area) > 0)
	{
		foreach ($area as $row)
		{
          $cilvekstundasSection = 0;
          $materIzmaksasSection = 0;
          if (is_array($row['ed']) && count($row['ed']) > 0)
          {
            foreach ($row['ed'] as $rrS)
            {
?>
            <tr >
                <td colspan="2" align="left" class="print_table_header"><b><?=text::get('ED_IECIKNIS');?>:</b></td>
                <td colspan="2" align="left" class="print_table_header"><b><?= $rrS['name']; ?></b></td>
                <td colspan="6" align="left" class="print_table_header"><b><?= $rrS['code']; ?></b></td>
            </tr>
            <?
            $cilvekstundasArea = 0;
            $materIzmaksasArea = 0;
            if (is_array($rrS['mms']) && count($rrS['mms']) > 0)
                {
                   foreach ($rrS['mms'] as $rr)
                   {
            ?>
                    <tr>
                        <td align="left"><b><?=text::get('SINGLE_MMS');?>:</b></td>
                        <td colspan="9" align="left"><b><u><?= $rr['code']; ?></u></b></td>
                    </tr>
                    <tr>
            			<td width="10%" class="print_table_header3"><?= text::get('ACT_NUMBER'); ?></td>
                        <td width="8%" class="print_table_header3"><?=text::get('BASE_TIME');?></td>
                        <td width="10%" class="print_table_header3"><?= text::get('REPORT_MATERIAL_PRICE_TOTAL'); ?></td>
                        <td width="8%" class="print_table_header3"><?=text::get('TRANSPORT_KM');?><br /></td>
                        <td width="8%" class="print_table_header3"><?=text::get('TRANSPORT_AVTO_TIME');?></td>
                        <td width="8%" class="print_table_header3"><?= text::get('TRANSPORT_WORK_TIME'); ?></td>
                        <td width="8%" class="print_table_header3"><?=text::get('OVER_TIME');?></td>
                        <td width="15%"class="print_table_header3"><?= text::get('MAIN_DESIGNATION'); ?></td>
                        <td width="10%"class="print_table_header3"><?= text::get('OBJECT_IS_FINISHED'); ?></td>
                        <td width="15%" class="print_table_header3"><?= text::get('COMMENT'); ?></td>
                    </tr>
                    <?
                        if (is_array($rr['act']) && count($rr['act']) > 0)
                        {
                           foreach ($rr['act'] as $r)
                           {
                             ?>
                           <tr>
              			      <td align="center"><?= $r['actNumber']; ?></td>
                              <td align="center"><?= $r['mainTime']; ?></td>
                              <td align="center"><?= $r['cena']; ?></td>
                              <td align="center"><?= $r['km']; ?></td>
                              <td align="center"><?= $r['stundas']; ?></td>
                              <td align="center"><?= $r['darbaStundas']; ?></td>
                              <td align="center"><?= $r['overTime']; ?></td>
                              <td align="left"><?= $r['designation']; ?></td>
                              <td align="left"><?= (($r['isFinished'] == 1)?text::get('YES'):text::get('NO')); ?></td>
                              <td align="left"><?= $r['comment']; ?></td>
                          </tr>
                             <?
                           }
                        }
                        ?>
                          <tr>
                            <td align="left" class="print_table_data2"><b><?=text::get('TOTAL_EXECUTION');?>:</b></td>
                            <td align="center" class="print_table_data2"><?=number_format($rr['totalMainTime'],2,'.','');?></td>
                            <td align="center" class="print_table_data2"><?=number_format($rr['totalCena'],2,'.','');?></td>
                            <td align="center" class="print_table_data2"><?=number_format($rr['totalKm'],2,'.','');?></td>
                            <td align="center" class="print_table_data2"><?=number_format($rr['totalStundas'],2,'.','');?></td>
                            <td align="center" class="print_table_data2"><?=number_format($rr['totalDarbaStundas'],2,'.','');?></td>
                            <td align="center" class="print_table_data2"><?=number_format($rr['totalOverTime'],2,'.','');?></td>
                            <td colspan="3" class="print_table_data2">&nbsp;</td>
                         </tr>
                         <tr>
                            <td align="left" ><b><?=text::get('PLAN');?>:</b></td>
                            <td align="center"><?=number_format($rr['cilvekstundas'],2,'.','');?></td>
                            <td align="center"><?=number_format($rr['materIzmaksas'],2,'.','');?></td>
                            <td colspan="4" align="left"><b><?= $rr['code']; ?></b></td>
                            <td colspan="3">&nbsp;</td>
                         </tr>
                         <tr>
                            <td align="left"><b><?=text::get('DIFFERENCE');?>:</b></td>
                            <td align="center"><?=number_format(($rr['cilvekstundas'] - $rr['totalMainTime']),2,'.','');?></td>
                            <td align="center"><?=number_format(($rr['materIzmaksas'] - $rr['totalCena']),2,'.','');?></td>
                            <td colspan="7">&nbsp;</td>
                         </tr>
                         <tr><td colspan="10" class="print_table_data2">&nbsp;</td></tr>
                        <?
                        $cilvekstundasArea = $cilvekstundasArea + $rr['cilvekstundas'];
                        $materIzmaksasArea = $materIzmaksasArea + $rr['materIzmaksas'];
                    }
            }
            ?>
                        <tr>
                            <td align="left" class="print_table_total"><i><?=text::get('TOTAL_EXECUTION');?>:</i></td>
                            <td align="center" class="print_table_total"><?=number_format($rrS['totalMainTime'],2,'.','');?></td>
                            <td align="center" class="print_table_total"><?=number_format($rrS['totalCena'],2,'.','');?></td>
                            <td align="center" class="print_table_total"><?=number_format($rrS['totalKm'],2,'.','');?></td>
                            <td align="center" class="print_table_total"><?=number_format($rrS['totalStundas'],2,'.','');?></td>
                            <td align="center" class="print_table_total"><?=number_format($rrS['totalDarbaStundas'],2,'.','');?></td>
                            <td align="center" class="print_table_total"><?=number_format($rrS['totalOverTime'],2,'.','');?></td>
                            <td colspan="3" align="left" class="print_table_total"><i><?= $rrS['name']; ?></i></td>
                         </tr>
                         <tr>
                            <td align="left" class="print_table_total"><i><?=text::get('PLAN');?>:</i></td>
                            <td align="center" class="print_table_total"><?=number_format($cilvekstundasArea,2,'.','');?></td>
                            <td align="center" class="print_table_total"><?=number_format($materIzmaksasArea,2,'.','');?></td>
                            <td colspan="7" class="print_table_total">&nbsp;</td>
                         </tr>
                         <tr>
                            <td align="left" class="print_table_total"><i><?=text::get('DIFFERENCE');?>:</i></td>
                            <td align="center" class="print_table_total"><?=number_format(($cilvekstundasArea - $rrS['totalMainTime']),2,'.','');?></td>
                            <td align="center" class="print_table_total"><?=number_format(($materIzmaksasArea - $rrS['totalCena']),2,'.','');?></td>
                            <td colspan="7" class="print_table_total">&nbsp;</td>
                         </tr>
                         <tr><td colspan="10" class="print_table_data2">&nbsp;</td></tr>
        <?
        $cilvekstundasSection = $cilvekstundasSection + $cilvekstundasArea;
        $materIzmaksasSection = $materIzmaksasSection + $materIzmaksasArea;
		}
        }
        ?>
          <tr>
          <td align="left" class="print_table_total"><b><?=text::get('TOTAL_EXECUTION');?>:</b></td>
          <td align="center" class="print_table_total"><?=number_format($row['totalMainTime'],2,'.','');?></td>
          <td align="center" class="print_table_total"><?=number_format($row['totalCena'],2,'.','');?></td>
          <td align="center" class="print_table_total"><?=number_format($row['totalKm'],2,'.','');?></td>
          <td align="center" class="print_table_total"><?=number_format($row['totalStundas'],2,'.','');?></td>
          <td align="center" class="print_table_total"><?=number_format($row['totalDarbaStundas'],2,'.','');?></td>
          <td align="center" class="print_table_total"><?=number_format($row['totalOverTime'],2,'.','');?></td>
          <td colspan="3" align="left" class="print_table_total"><b><?= $row['section']; ?></b></td>
       </tr>
       <tr>
          <td align="left" class="print_table_total"><b><?=text::get('PLAN');?>:</b></td>
          <td align="center" class="print_table_total"><?=number_format($cilvekstundasSection,2,'.','');?></td>
          <td align="center" class="print_table_total"><?=number_format($materIzmaksasSection,2,'.','');?></td>
          <td colspan="7" class="print_table_total">&nbsp;</td>
       </tr>
       <tr>
          <td align="left" class="print_table_total"><b><?=text::get('DIFFERENCE');?>:</b></td>
          <td align="center" class="print_table_total"><?=number_format(($cilvekstundasSection - $row['totalMainTime']),2,'.','');?></td>
          <td align="center" class="print_table_total"><?=number_format(($materIzmaksasSection - $row['totalCena']),2,'.','');?></td>
          <td colspan="7" class="print_table_total">&nbsp;</td>
       </tr>
       <tr><td colspan="10" class="print_table_data2">&nbsp;</td></tr> 
        <?

        $cilvekstundasRegion = $cilvekstundasRegion + $cilvekstundasSection;
        $materIzmaksasRegion = $materIzmaksasRegion + $materIzmaksasSection;
		}
        ?>
                        <tr><td colspan="10" class="print_table_data2">&nbsp;</td></tr>
                        <tr>
                            <td align="left" class="print_table_total"><?=text::get('TOTAL_EXECUTION');?>:</td>
                            <td align="center" class="print_table_total"><?=$pamatstundas;?></td>
                            <td align="center" class="print_table_total"><?=$summa;?></td>
                            <td align="center" class="print_table_total"><?=$km;?></td>
                            <td align="center" class="print_table_total"><?=$stundas;?></td>
                            <td align="center" class="print_table_total"><?=$darbaStundas;?></td>
                            <td align="center" class="print_table_total"><?=$virsstundas;?></td>
                            <td colspan="3" align="left" class="print_table_total"><?=text::get('TOTAL');?></td>
                         </tr>
                         <tr>
                            <td align="left" class="print_table_total"><?=text::get('PLAN');?>:</td>
                            <td align="center" class="print_table_total"><?=number_format($cilvekstundasRegion, 2,'.','');?></td>
                            <td align="center" class="print_table_total"><?=number_format($materIzmaksasRegion, 2,'.','');?></td>
                            <td colspan="7" class="print_table_total">&nbsp;</td>
                         </tr>
                         <tr>
                            <td align="left" class="print_table_total"><?=text::get('DIFFERENCE');?>:</td>
                            <td align="center" class="print_table_total"><?=number_format(($cilvekstundasRegion - $pamatstundas),2,'.','');?></td>
                            <td align="center" class="print_table_total"><?=number_format(($materIzmaksasRegion - $summa),2,'.','');?></td>
                            <td colspan="7" class="print_table_total">&nbsp;</td>
                         </tr>
<?
	}
?>
</table>
</body>