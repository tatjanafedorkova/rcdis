<?
// Created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);
$isEditor = ( dbProc::isUserInRole($userId, ROLE_EDITOR) ||  dbProc::isUserInRole($userId, ROLE_EDITOR_VIEWER));

// act ID
$actId  = reqVar::get('actId');
// search form
$isSearch  = reqVar::get('isSearch');
// search act by number
$searchNum  = reqVar::get('searchNum');

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
    $oLink=new urlQuery();
    $oLink->addPrm(FORM_ID, 'f.akt.s.20');
    $oFormPop = new Form('frmMainTab','post',$oLink->getQuery());
    unset($oLink);

    if($actId !== false )
    {
      // get info about act
      $actInfo = dbProc::getActInfo($actId);
      //print_r($actInfo);
      if(count($actInfo)>0)
      {
      	$act = $actInfo[0];
        $status = $act['RAKT_STATUS'];
      }

      $isReadonly   = true;
      // Ir pieejams mainīšanai, ja  AKTS:LIETOTĀJS = ’Tekošais lietotājs’ un lietotāja loma ir ‘Ievadītais’ un AKTS:STATUS = ‘Izveide’ vai ‘Atgriezts’ vai ‘Tāme’.
      if(($act['RAKT_RLTT_ID'] == $userId && ($isEditor || $isAdmin) && ($status == STAT_INSERT || $status == STAT_RETURN || $status == STAT_ESTIMATE)) )
      {
        $isReadonly   = false;
      }

      $aScope=array();
      $aScope['-1']='';
      $serchText = '';

      $title = text::toUpper(text::get('ADD'));
      // get all personal
      $res = dbProc::getFreePersList($actId);
      if (is_array($res))
      {
          foreach ($res as $row)
       	  {
        	    $aScope[$row['KPRF_ID']] = $row['KPRF_KODS'] .' : '. $row['KPRF_VARDS'].' '. $row['KPRF_UZVARDS'];
          }
      }
      unset($res);

      // form elements
      $oFormPop -> addElement('hidden', 'action', '');
      $oFormPop -> addElement('hidden', 'actId', '', $actId);
      $oFormPop -> addElement('kls', 'work',  text::get('WORK_TYPE'), array('classifName'=>KL_WORK_TYPES,'value'=>'','readonly'=>false), (($isReadonly)?' disabled ':''));
      $oFormPop -> addRule('work', text::get('ERROR_REQUIRED_FIELD'), 'required');
      $oFormPop -> addElement('text', 'description',  text::get('WORK_DESCRIPTION'), '', 'maxlength="150"'.(($isReadonly)?' disabled':''));
      $oFormPop -> addElement('date', 'start_date',  text::get('DATE').' '.text::get('FROM'), '', 'maxlength="10" '.(($isReadonly)?' disabled':'').' onchange= " setValue(\'end_date\', this.value); ";');
      $oFormPop -> addRule('start_date', text::get('ERROR_INCORRECT_DATE'),'validatedate');
      $oFormPop -> addRule('start_date', text::get('ERROR_REQUIRED_FIELD'), 'required');
      $oFormPop -> addElement('date', 'end_date',  text::get('DATE').' '.text::get('UNTIL'), '', 'maxlength="10" '.(($isReadonly)?' disabled':''));
      $oFormPop -> addRule('end_date', text::get('ERROR_INCORRECT_DATE'),'validatedate');
      $oFormPop -> addRule('end_date', text::get('ERROR_REQUIRED_FIELD'), 'required');
      $oFormPop -> addElement('multiple_select', 'scope', text::get('CUSTOMERS'), '', (($isReadonly)?' disabled ':'').' size="10" ', '', '', $aScope);
      $oFormPop -> addRule('work', text::get('ERROR_REQUIRED_FIELD'), 'required');

      // form buttons
      if (!$isReadonly)
      {
        $oFormPop -> addElement('submitImg', 'save', text::get('SAVE'), 'img/btn_saglabat.gif', 'width="70" height="20" onclick="if (!isEmptyFormField(\'frmMainTab\',\'actId\')) {setValue(\'action\',\''.OP_UPDATE.'\');} else {return false;}"');
      }
      $oFormPop -> addElement('button', 'close', '', text::get('CLOSE'), 'class="btn70" onclick="javascript:window.close();return false;"; ');

      if ($oFormPop -> isFormSubmitted() AND $oFormPop->getValue('action')==OP_UPDATE)
      {
            $checkRequiredFields = false;
            $r = true;
            $aListScope = $oFormPop->getValue('scope');
            if (is_array($aListScope) && $oFormPop->getValue('work') && $oFormPop->getValue('start_date') && $oFormPop->getValue('end_date'))
			{
			  if( ($oFormPop->getValue('work') == KDBV_LT_REMONT_ID) && !$oFormPop->getValue('description') )
              {
                  $oFormPop->addError(text::get('ERROR_NOT_WORK_DESCRIPTION'));
                  $checkRequiredFields = true;
              }
              else
              {
				foreach ($aListScope as $cValue)
			    {
					if ($cValue>0)
					{
                        $r = dbProc::saveActRcfWorkRecord($oFormPop->getValue('actId'),
                                                    $oFormPop->getValue('work'),
                                                    $oFormPop->getValue('description'),
                                                    $oFormPop->getValue('start_date'),
                                                    $oFormPop->getValue('end_date'),
                                                    $cValue);
                        if(!$r)
                        {
                          $oFormPop->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
                          break 1;
                        }
                        $checkRequiredFields = true;
					}
				}
              }
              if(!$checkRequiredFields)
              {
                  $oFormPop->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
              }
              else
              {
                 if($r)
                  {


                    $oRedirectLink=new urlQuery();
                    $oRedirectLink->addPrm(FORM_ID, 'f.akt.s.18');
                    $oRedirectLink->addPrm('actId', $oFormPop->getValue('actId'));
                    $oRedirectLink->addPrm('isSearch', $oFormPop->getValue('isSearch'));
                    $oRedirectLink->addPrm('searchNum', $oFormPop->getValue('searchNum'));
                    $oRedirectLink->addPrm('tab', TAB_WORKS);

                    //$oRedirectLink->addPrm('isNew', RequestHandler::getMicroTime());
                    requestHandler::makeParentReplace($oRedirectLink->getQuery().'#tab');
                    unset($oRedirectLink);
                 }
              }
	      }
          else
          {
            $oFormPop->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
          }

      }

      $oFormPop -> makeHtml();
      include('f.akt.s.20.tpl');
    }
    else
	{
		$oFormPop->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
	}
}
else
{
    RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}

?>