<?
// Created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);
$isEditor = ( dbProc::isUserInRole($userId, ROLE_EDITOR) ||  dbProc::isUserInRole($userId, ROLE_EDITOR_VIEWER));

// act ID
$actId  = reqVar::get('actId');

// Main form with XMLHTTP method load this code:
if (reqVar::get('xmlHttp') == 1)
{
    // if operation is DELETE , delete record from DB
    if (reqVar::get('deleteId')!='')
	{
	   dbProc::deleteActWorkRcfRecord(reqVar::get('deleteId'));
       ?>
	    rowNode = this.parentNode.parentNode;
		rowNode.parentNode.removeChild(rowNode);
		<?
        $totalTime = dbProc::getRcfWorkTimeSum(reqVar::get('actId'));
        ?>
         setFormValue("frmWorkTab","totalTime", <?=number_format($totalTime, 2, '.', '');?>  );
        <?

    }
    exit();
}


// tikai  sist�mas lietotajam vai administr�toram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
    if($actId !== false )
    {
      $oLink=new urlQuery();
      $oLink->addPrm(FORM_ID, 'f.akt.s.18');
      $oLink->addPrm('actId', $actId);
      $oLink->addPrm('tab', TAB_WORKS);
      $oLink->addPrm('isSearch', $isSearch);
      $oLink->addPrm('searchNum', $searchNum);
      $oFormWorkTab = new Form('frmWorkTab','post',$oLink->getQuery().'#tab');
      unset($oLink);

      // if operation is success, show success message and redirect top frame
      if (reqVar::get('successMessageTab') && $isNew === false)
      {
      	$oFormWorkTab->addSuccess(reqVar::get('successMessageTab'));

      }

      $isReadonly   = true;
      // Ir pieejams main��anai, ja  AKTS:LIETOT�JS = �Teko�ais lietot�js� un lietot�ja loma ir �Ievad�tais� un AKTS:STATUS = �Izveide� vai �Atgriezts� vai �T�me�.
      if((($act['RAKT_RLTT_ID'] == $userId && $isEditor) || $isAdmin) && ($status == STAT_INSERT || $status == STAT_RETURN || $status == STAT_ESTIMATE) )
      {
        $isReadonly   = false;
      }

      //Popup saites sagatave
      $oPopLink=new urlQuery;
      $oPopLink->addPrm(FORM_ID, 'f.akt.s.20');
      $oPopLink->addPrm('actId',$actId);
      $oPopLink->addPrm('isSearch', $isSearch);
      $oPopLink->addPrm('searchNum', $searchNum);
      
       // get favorit
      $favorit=array();
      $krfk = dbProc::getKrfkInfo(KRFK_CUSTOMER_ID);
      if($krfk !== false)
      {
	 $res=dbProc::getFavoritTypeDefinitionList($userId, $krfk['KRFK_VERTIBA']);
       	 if (is_array($res))
        	{
         		foreach ($res as $i => $row)
         		{
             		 $favorit[$i]['TITLE'] = $row['LTFV_NOSAUKUMS'];
          	 		 $favorit[$i]['URL'] = RequestHandler::popupHref($oPopLink -> getQuery().'&favoritId='.$row['LTFV_ID']);
           		 }
        	}
       	 unset($res);
      }
      
      $next =  count($favorit);
      $favorit[$next]['TITLE'] = text::get('ALL_VALUES');   ;
      $favorit[$next]['URL'] = RequestHandler::popupHref($oPopLink -> getQuery().'&all=1');
       // get region list
       $region=array();
       $res=dbProc::getKrfkName(KRFK_REGION);
       if (is_array($res))
       {
       	foreach ($res as $i => $row)
       	{
       		  $region[$i]['TITLE'] = $row['nosaukums'];
          	$region[$i]['URL'] = RequestHandler::popupHref($oPopLink -> getQuery().'&region='.$row['nosaukums']);
       	}
       }
       unset($res);
       unset($oPopLink);

      $aWorks = array();
      if ($oFormWorkTab -> isFormSubmitted() && $isNew === false)
      {
          $isRowValid = true;
          $aRows =  $oFormWorkTab->getValue('id');
          if(is_array($aRows) )
          {

            $totalTime = 0;
            foreach ($aRows as $i => $val)
            {
                 $aWorks[$i]['RCFD_ID'] = $oFormWorkTab->getValue('id['.$i.']');
				 $aWorks[$i]['RCFD_KDBV_ID'] = $oFormWorkTab->getValue('kdbv_id['.$i.']');
                 $aWorks[$i]['RCFD_WORK_DESCRIPTION'] = $oFormWorkTab->getValue('description['.$i.']');
                 $aWorks[$i]['RCFD_DATUMS_NO'] = $oFormWorkTab->getValue('date_from['.$i.']');
                 $aWorks[$i]['RCFD_DATUMS_LIDZ'] = $oFormWorkTab->getValue('date_to['.$i.']');
                 $aWorks[$i]['RCFD_KPRF_VARDS_UZVARDS'] = $oFormWorkTab->getValue('person_name['.$i.']');
                 $aWorks[$i]['RCFD_KPRF_KODS'] = $oFormWorkTab->getValue('person_code['.$i.']');
                 $aWorks[$i]['RCFD_STUNDAS'] = $oFormWorkTab->getValue('time['.$i.']');
                 $aWorks[$i]['WORK_TITLE'] = dbProc::getWorkTypeName($oFormWorkTab->getValue('id['.$i.']'));

                 // validate baseTime value
    			 if (empty($aWorks[$i]['RCFD_STUNDAS']))
    			 {
    			    // assign error message
    				$oFormWorkTab->addErrorPerElem('time['.$i.']', text::get('ERROR_REQUIRED_FIELD'));
    				$isRowValid = false;
    			 }
                 elseif (!is_numeric($aWorks[$i]['RCFD_STUNDAS']))
    			 {
    				 // assign error message
    				 $oFormWorkTab->addErrorPerElem('time['.$i.']', text::get('ERROR_DOUBLE_VALUE'));
    				 $isRowValid = false;
    			 }
                 elseif ($aWorks[$i]['RCFD_STUNDAS'] >= 1000)
    			 {
    			 	// assign error message
    			 	$oFormWorkTab->addErrorPerElem('time['.$i.']', text::get('ERROR_AMOUNT_VALUE'));
    			 	$isRowValid = false;
    			 }
                 else
                 {
                   $totalTime = round(($totalTime + $aWorks[$i]['RCFD_STUNDAS']), 2);
                 }

                // prepare database record data
  			    $aValidRows[] = array(
                    'id' => $aWorks[$i]['RCFD_ID'],
				    'kdbv_id' => $aWorks[$i]['RCFD_KDBV_ID'],
      				'description' => $aWorks[$i]['RCFD_WORK_DESCRIPTION'],
      				'date_from' => $aWorks[$i]['RCFD_DATUMS_NO'],
      				'date_to' => $aWorks[$i]['RCFD_DATUMS_LIDZ'],
      				'person_name' => $aWorks[$i]['RCFD_KPRF_VARDS_UZVARDS'],
      				'person_code' => $aWorks[$i]['RCFD_KPRF_KODS'],
                    'time' => $aWorks[$i]['RCFD_STUNDAS'],
                    'rowNum' => $i
  			    );
             }
           }
      }
      else
      {
        //Ja eksistee akta darbi, tad nolasaam taas darbus no datu baazes
  	    $aWorkDb=dbProc::getActRfcWorkList($actId);
        //print_r($aWorkDb);

        foreach ($aWorkDb as $i => $work)
        {
             $aWorks[$i]['RCFD_ID'] = $work['RCFD_ID'];
             $aWorks[$i]['RCFD_KDBV_ID'] = $work['RCFD_KDBV_ID'];
             $aWorks[$i]['WORK_TITLE'] = $work['WORK_TITLE'];
             $aWorks[$i]['RCFD_WORK_DESCRIPTION'] = $work['RCFD_WORK_DESCRIPTION'];
             $aWorks[$i]['RCFD_DATUMS_NO'] = $work['RCFD_DATUMS_NO'];
             $aWorks[$i]['RCFD_DATUMS_LIDZ'] = $work['RCFD_DATUMS_LIDZ'];
             $aWorks[$i]['RCFD_KPRF_VARDS_UZVARDS'] = $work['RCFD_KPRF_VARDS_UZVARDS'];
             $aWorks[$i]['RCFD_KPRF_KODS'] = $work['RCFD_KPRF_KODS'];
             $aWorks[$i]['RCFD_STUNDAS'] = $work['RCFD_STUNDAS'];
        }
      }
      $oFormWorkTab -> addElement('hidden', 'isSearch', '', $isSearch);
      $oFormWorkTab -> addElement('hidden', 'searchNum', '', $searchNum);
      $oFormWorkTab->addElement('hidden','actId','',$actId);
      $oFormWorkTab->addElement('hidden','maxAmaunt','','1000.00');
      $oFormWorkTab->addElement('hidden','minAmaunt','','0.00');

      $totalTime = 0;
      // form elements
      foreach ($aWorks as $i => $actWork)
      {

        $oFormWorkTab->addElement('hidden', 'id['.$i.']', '', $actWork['RCFD_ID']);
		$oFormWorkTab->addElement('hidden', 'kdbv_id['.$i.']', '', $actWork['RCFD_KDBV_ID']);
        $oFormWorkTab->addElement('label', 'work['.$i.']', '', $actWork['WORK_TITLE']);
        $oFormWorkTab->addElement('label', 'description['.$i.']', '', $actWork['RCFD_WORK_DESCRIPTION']);
        $oFormWorkTab->addElement('label', 'date_from['.$i.']', '', $actWork['RCFD_DATUMS_NO']);
        $oFormWorkTab->addElement('label', 'date_to['.$i.']', '', $actWork['RCFD_DATUMS_LIDZ']);
        $oFormWorkTab->addElement('label', 'person_name['.$i.']', '', $actWork['RCFD_KPRF_VARDS_UZVARDS']);
        $oFormWorkTab->addElement('label', 'person_code['.$i.']', '', $actWork['RCFD_KPRF_KODS']);

        $oFormWorkTab->addElement('text', 'time['.$i.']', '', $actWork['RCFD_STUNDAS'], 'maxlength="6"');
        $oFormWorkTab -> addRule('time['.$i.']', text::get('ERROR_REQUIRED_FIELD'), 'required');
        $oFormWorkTab -> addRule('time['.$i.']', text::get('ERROR_DOUBLE_VALUE'), 'double', 2);
        // check Amaunt field
        $v1=&$oFormWorkTab->createValidation('stundasValidation_'.$i, array('time['.$i.']'), 'ifonefilled');
        // ja Amount ir defin�ts un > 1000
        $r1=&$oFormWorkTab->createRule(array('time['.$i.']', 'maxAmaunt'), text::get('ERROR_AMOUNT_VALUE'), 'comparedouble','<');
        $oFormWorkTab->addRule(array($r1), 'groupRuleStundas_'.$i, 'group', $v1);

        $totalTime += $actWork['RCFD_STUNDAS'];

        // ajax handler link for delete
	    $delLink = new urlQuery();
	    $delLink->addPrm(FORM_ID, 'f.akt.s.19');
	    $delLink->addPrm(DONT_USE_GLB_TPL, '1');
        $delLink->addPrm('actId', $actId);
        $delLink->addPrm('xmlHttp', '1');
		// delete button
        $delButtonJs = "if(isDelete()) {
							var index=" . $i . ";
							var rowId = document.forms['frmWorkTab']['id['+index+']'];
							rowId = rowId ? rowId.value : '';
							eval(xmlHttpGetValue('" .$delLink -> getQuery() ."&deleteId='+rowId));
						}";
		$oFormWorkTab->addElement('button', 'work_del['.$i.']', '', '', 'onclick="' . $delButtonJs . ';return false;" style="background: url(img/ico_del.gif); cursor:hand; border:0px; width: 20px; height: 20px;" title="' . text::get('DELETE') . '"');
        unset($delLink);
      }
      $totalTime = number_format($totalTime, 2, '.', '');
      $oFormWorkTab->addElement('text', 'totalTime', '', $totalTime, ' style="background-color: #FAF9F3; font-weight:bold; border:none; "', '');
      // form buttons
      if (!$isReadonly)
      {
	foreach ($favorit as $i => $group)
        {
           $oFormWorkTab->addElement('link', 'favorit['.$i.']', $group['TITLE'], '#', 'onClick="'.$group['URL'].'"', '');
        }
        foreach ($region as $i => $r)
        {
           $oFormWorkTab->addElement('link', 'regions['.$i.']', $r['TITLE'], '#', 'onClick="'.$r['URL'].'"', '');
        }

       
        $oFormWorkTab -> addElement('submitImg', 'save', text::get('SAVE'), 'img/btn_save_work.gif', 'width="120" height="20" ');

      }

      // if form iz submit (save button was pressed)
	  if ($oFormWorkTab -> isFormSubmitted()&& $isNew === false)
	  {
		if($oFormWorkTab ->isValid()  && $isRowValid)
		{
		   $r=dbProc::deleteActWorks($actId);
		   if ($r === false)
		   {
		   	$oFormWorkTab->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
		   }
		   else
		   {
		        if(is_array($aValidRows))
                {
        	    	foreach($aValidRows as $i => $row)
        			{
                         $r=dbProc::writeActRfcWorkTime($actId,
                                                $row['id'],
                                                $row['time']
						                        );
                         // if work not inserted in to the DB
    					 // delete  task record and show error message
    					 if($r === false)
    					 {
    					 	dbProc::deleteActWorks($actId);
    					 	$oFormWorkTab->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
    				        break;
    					 }
                         else
                         {
                           $Id=dbLayer::getInsertId();
                           $oFormWorkTab -> setNewValue('id['.$row['rowNum'].']', $Id);
                           $oFormWorkTab -> setNewValue('totalTime', $r);
                           $oFormWorkTab->addSuccess(text::get('RECORD_WAS_SAVED'));

                           /*$oLink=new urlQuery();
                		   $oLink->addPrm(FORM_ID, 'f.akt.s.19');
                		   $oLink->addPrm('actId', $actId);
                           $oLink->addPrm('tab', TAB_WORKS);
                           $oLink->addPrm('isNew', RequestHandler::getMicroTime());
                		   $oLink->addPrm('successMessageTab', text::get('RECORD_WAS_SAVED'));
                		   //RequestHandler::makeRedirect($oLink->getQuery());*/
                         }
                    }
                    // saglaba lietotaja darbiibu
                    dbProc::setUserActionDate($userId, ACT_UPDATE);
                }
           }
        }
      }
       $oFormWorkTab -> makeHtml();
       include('f.akt.s.19.tpl');
    }



}
else
{
    RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}

?>