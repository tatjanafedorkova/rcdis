<?
// Created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);
$isEditor = ( dbProc::isUserInRole($userId, ROLE_EDITOR) ||  dbProc::isUserInRole($userId, ROLE_EDITOR_VIEWER));

// act ID
$actId  = reqVar::get('actId');
// catalog
$catalog  = reqVar::get('catalog');
// all material
$all  = reqVar::get('all');
// search form
$isSearch  = reqVar::get('isSearch');
// search act by number
$searchNum  = reqVar::get('searchNum');
// EPLA act
$isEpla  = reqVar::get('isEpla');
$tame  = reqVar::get('tame');

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
    $oLink=new urlQuery();
    $oLink->addPrm(FORM_ID, 'f.akt.s.11');
    $oFormPop = new Form('frmMain','post',$oLink->getQuery());
    unset($oLink);

    if($actId !== false && $catalog == KL_MATERIALS )
    {
      // get info about act
      $actInfo = dbProc::getActInfo($actId);
      //print_r($actInfo);
      if(count($actInfo)>0)
      {
      	$act = $actInfo[0];
        $status = $act['RAKT_STATUS'];
        $isAuto = $act['RAKT_IS_AUTO'];
      }

      $isReadonly   = true;
      // Ir pieejams mainīšanai, ja  AKTS:LIETOTĀJS = ’Tekošais lietotājs’ un lietotāja loma ir ‘Ievadītais’ un AKTS:STATUS = ‘Izveide’ vai ‘Atgriezts’ vai ‘Tāme’.
      // un aktām nav uzlikta atzīme par automatīsku materiāla ielādi
      // vai lietotājs ir administrātops
      if(
        (
          $act['RAKT_RLTT_ID'] == $userId && $isEditor && !$isAuto &&
          ($status == STAT_INSERT || $status == STAT_RETURN || $status == STAT_ESTIMATE)
        ) || $isAdmin
      )
      {
        $isReadonly   = false;
      }

      $showResults = false;
      $serchText = '';
      $materTitle = '';
      $materId = '';
      if ($oFormPop -> isFormSubmitted() && $oFormPop->getValue('action')==OP_SEARCH &&
      $oFormPop->getValue('searchCode') != '')
      {
         $serchText = $oFormPop->getValue('searchCode');
         $res = dbProc::getFreeMatByCode($oFormPop->getValue('searchCode'), $oFormPop->getValue('actId'), $tame);
         if(is_array($res) && count($res) > 0)
         {
            $material = $res[0];
            $materTitle = $material['KMAT_KODS'] .' : '. $material['KMAT_NOSAUKUMS'].' : '. $material['KMAT_DAUDZUMS'].' '.$material['KMAT_MERVIENIBA'] .' : '.$material['KMAT_CENA'].' EUR';
            $materId = $material['KMAT_ID'];
            $showResults = true;
         }
         else
         {
           $oFormPop->addError(text::get('NO_SEARCH_RESULTS'));
         }
      }

      if ($oFormPop -> isFormSubmitted() && $oFormPop->getValue('action')==OP_UPDATE)
      {
        $isRowValid = true;
        // validate material amount value
  		if ($oFormPop->getValue('amount') == '')
		{
    		// assign error message
  			$oFormPop->addErrorPerElem('amount', text::get('ERROR_REQUIRED_FIELD'));
  			$isRowValid = false;
  		}
        // validate material amount value
  		elseif (!is_numeric($oFormPop->getValue('amount')))
    	{
  			// assign error message
  			$oFormPop->addErrorPerElem('amount', text::get('ERROR_DOUBLE_VALUE'));
  			$isRowValid = false;
  		}
        // validate material amount value
  		elseif ($oFormPop->getValue('amount') >= 100000)
  		{
  			// assign error message
  			$oFormPop->addErrorPerElem('amount', text::get('ERROR_AMOUNT_VALUE'));
  			$isRowValid = false;
  		}
        if($isRowValid === false)
        {

           $showResults = true;

          // $materTitle = $oFormPop->getValue('title');
          // $materId = $oFormPop->getValue('id');
        }
      }

      $oFormPop -> addElement('hidden', 'action', '');
      $oFormPop -> addElement('hidden', 'actId', '', $actId);
      $oFormPop -> addElement('hidden', 'catalog', '', $catalog);
      $oFormPop -> addElement('hidden', 'all', '', 1);
      $oFormPop->addElement('hidden','maxAmaunt','','1000.00');
      $oFormPop->addElement('hidden','minAmaunt','','0.00');
      $oFormPop -> addElement('hidden', 'isSearch', '', $isSearch);
      $oFormPop -> addElement('hidden', 'isEpla', '', $isEpla);
      $oFormPop -> addElement('hidden', 'isAuto', '', $isAuto);
      $oFormPop -> addElement('hidden', 'searchNum', '', $searchNum);
      $oFormPop -> addElement('hidden', 'tame', '', $tame);

      $oFormPop->addElement('static','jsRefresh2','','');

      $oFormPop->addElement('text', 'searchCode', text::get('CODE'), $serchText, (($isReadonly)?' disabled ':'').'maxlength="7"');
      // form buttons
      if (!$isReadonly)
      {
        $oFormPop -> addElement('submitImg', 'search', text::get('SEARCH'),'img/btn_meklet.gif', 'width="70" height="20" onclick="if (!isEmptyFormField(\'frmMain\',\'actId\')) {setValue(\'action\',\''.OP_SEARCH.'\');} else {return false;}"');
      }
      $oFormPop -> addElement('button', 'close', '', text::get('CLOSE'), 'class="btn70" onclick="javascript:window.close();return false;"; ');

      if($showResults)
      {
        $oFormPop->addElement('label', 'title', '', '');
        $oFormPop->addElement('hidden', 'id', '', '');
        $oFormPop->addElement('text', 'amount', '', '', (($isReadonly)?' disabled ':'').'maxlength="6"');

        // form buttons
        if (!$isReadonly)
        {
          $oFormPop -> addElement('submitImg', 'save', text::get('SAVE'), 'img/btn_saglabat.gif', 'width="70" height="20" onclick="if (!isEmptyFormField(\'frmMain\',\'actId\')) {setValue(\'action\',\''.OP_UPDATE.'\');} else {return false;}"');
        }
      }

      if ($oFormPop -> isFormSubmitted())
      {
        if($oFormPop->getValue('action')==OP_SEARCH)
        {
            $oFormPop -> setNewValue('title', $materTitle);
            $oFormPop -> setNewValue('id', $materId);
        }
        if($oFormPop->getValue('action')==OP_UPDATE)
        {
          if($isRowValid)
          {
              $r = dbProc::saveActMaterialRecord($oFormPop->getValue('actId'),
                                                $oFormPop->getValue('id'),
                                                $oFormPop->getValue('amount'),
                                                $tame);
              if(!$r)
              {
                 $oFormPop->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
              }
              else
              {
                $oRedirectLink=new urlQuery();
                $oRedirectLink->addPrm(FORM_ID, 'f.akt.s.1');
                $oRedirectLink->addPrm('actId', $oFormPop->getValue('actId'));
                $oRedirectLink->addPrm('isSearch', $oFormPop->getValue('isSearch'));
                $oRedirectLink->addPrm('searchNum', $oFormPop->getValue('searchNum'));
                if($tame) {
                  $oRedirectLink->addPrm('tametab', TAB_MATERIAL_T);
                } else  {
                  $oRedirectLink->addPrm('tab', TAB_MATERIAL);	
                }
                $oRedirectLink->addPrm('isEpla', $isEpla);                
                $oRedirectLink->addPrm('tame_mat', $tame);
                $oRedirectLink->addPrm('isNew', RequestHandler::getMicroTime());
                $oFormPop -> setNewValue('jsRefresh2', '<script>window.opener.location.replace( "'.$oRedirectLink->getQuery().'" );</script>');
                $oFormPop -> setNewValue('searchCode', '');
                unset($oRedirectLink);

                //Popup saites sagatave
                $oPopLink=new urlQuery;
                $oPopLink->addPrm(FORM_ID, 'f.akt.s.11');
                $oPopLink->addPrm('actId',$oFormPop->getValue('actId'));
                $oPopLink->addPrm('catalog',KL_MATERIALS);
                $oPopLink->addPrm('all',1);
                $oPopLink->addPrm('tame', $tame);
		            $oPopLink->addPrm('isEpla', $isEpla);

                //RequestHandler::makeRedirect($oPopLink->getQuery());
                unset($oPopLink);
              }
          }

        }
      }
      $title = text::toUpper(text::get(TAB_MATERIAL).'/'.text::get('SEARCH'));
      $oFormPop -> makeHtml();
      include('f.akt.s.11.tpl');
    }
    else
	{
		$oFormPop->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
	}
}
else
{
    RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}

?>