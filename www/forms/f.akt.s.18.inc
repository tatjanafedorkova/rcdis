﻿<?
// Created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);
$isEditor = ( dbProc::isUserInRole($userId, ROLE_EDITOR) ||  dbProc::isUserInRole($userId, ROLE_EDITOR_VIEWER));
$isEconomist = dbProc::isUserInRole($userId, ROLE_HEADOFDIVISION);
$isNew  = reqVar::get('isNew');
// act ID
$actId  = reqVar::get('actId');
// tab name
$tab  = reqVar::get('tab');
// search form
$isSearch  = reqVar::get('isSearch');
// search act by number
$searchNum  = reqVar::get('searchNum');
// paz?me vai ir akta turpin?jums
$isCopy  = reqVar::get('isCopy');
if($tab === false)
{
  $tab = TAB_WORKS;
}
$foundsId  = reqVar::get('foundsId');
if($foundsId === false)
{
  $foundsId = KFNA_RFC_WORK_ID;
}


// tikai  sist?mas lietotajam vai administr?toram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
  $oLink=new urlQuery();
  $oLink->addPrm(FORM_ID, 'f.akt.s.18');
  $oLink->addPrm('isSearch', $isSearch);
  $oLink->addPrm('searchNum', $searchNum);
  $oLink->addPrm('foundsId', $foundsId);
  $oForm = new Form('frmMain','post',$oLink->getQuery());
  unset($oLink);

  // get user info
  $userInfo = dbProc::getUserInfo($userId);
  if(count($userInfo) >0 )
  {
  	$user = $userInfo[0];
  }

// get info about system
  $systemInfo = dbProc::getSystemInfo();
  if(count($systemInfo)>0)
  {
  	$info = $systemInfo[0];
  }

  // get DV area list
  $dvarea = array();
  if(isset($user['RLTT_KDVI_ID']) )
  {
    $dvarea[$user['RLTT_KDVI_ID']] = dbProc::getDVAreaName($user['RLTT_KDVI_ID']);
  }
  elseif(isset($user['RLTT_REGIONS']) )
  {
    $res=dbProc::getDVAreaForRegion($user['RLTT_REGIONS']);
	if (is_array($res))
	{
	    $dvarea['-1']=text::get('ALL');
		foreach ($res as $row)
		{
		    $dvarea[$row['KDVI_ID']]=$row['KDVI_REGIONS'].$row['KDVI_KODS'].' : '.$row['KDVI_NOSAUKUMS'];
		}
	}
  }
  else
  {
    $res=dbProc::getDVAreaActivList();
	if (is_array($res))
	{
	    $dvarea['-1']=text::get('ALL');
		foreach ($res as $row)
		{
		    $dvarea[$row['KDVI_ID']]=$row['KDVI_REGIONS'].$row['KDVI_KODS'].' : '.$row['KDVI_NOSAUKUMS'];
		}
	}
  }

  // if operation is success, show success message and redirect top frame
  if (reqVar::get('successMessage'))
  {
  	$oForm->addSuccess(reqVar::get('successMessage'));
  	//$oForm->addElement('static','jsRefresh2','','<script>refreshFrame(1);</script>');
  }

  if (reqVar::get('errorMessage'))
  {
  	$oForm->addError(reqVar::get('errorMessage'));
  }

  $dvAreaId = '';
  // inicial state: actId=0;
  if($actId != false)
  {
  	// get info about voltage
  	$actInfo = dbProc::getActInfo($actId);
  	//print_r($actInfo);
  	if(count($actInfo)>0)
  	{
  		$act = $actInfo[0];
        $status = $act['RAKT_STATUS'];
        $statusLabel =  $act['STATUS_NAME'];

  	}
  }
  else
  {

    // get default statuss
    $statusInfo = dbProc::getKrfkInfoByName(STAT_INSERT);
    $status = $statusInfo['KRFK_VERTIBA'];
    $statusLabel = $statusInfo['KRFK_NOZIME'];

    // get default DV area
    $dvAreaId = $user['RLTT_KDVI_ID'];
    
  }

  $isReadonly   = true;
  $isReadonlyExceptAdmin   = true;
  $isReadonlyExceptEconomist   = true;
  $isAllowEstimate = false;
  $isDelete   = false;
  if($actId != false)
  {
    // Ir pieejams main??anai, ja  AKTS:LIETOT?JS = �Teko?ais lietot?js� un lietot?ja loma ir �Ievad?tais� un AKTS:STATUS = �Izveide� vai �Atgriezts� vai �T?me�.
    if(($act['RAKT_RLTT_ID'] == $userId && ($isEditor || $isAdmin) && ($status == STAT_INSERT || $status == STAT_RETURN || $status == STAT_ESTIMATE)) )
    {
      $isReadonly   = false;
    }
    // Ir pieejams main??anai, ja  �Teko?ais lietot?js� ir Sist?mas administrators un AKTS:STATUS = �Izveide� vai �Atgriezts� vai �T?me�.
    if($isAdmin && ($status == STAT_INSERT || $status == STAT_RETURN || $status == STAT_ESTIMATE))
    {
      $isReadonlyExceptAdmin   = false;
    }
    //Ir pieejams main??anai, ja   Teko?a lietot?ja loma = �Ekonomists�  un AKTS:STATUS = �Saska?o?ana�.
    if($isEconomist && $status == STAT_ACCEPT )
    {
      $isReadonlyExceptEconomist   = false;
    }
    //Ir pieejams main??anai, ja  AKTS:LIETOT?JS = �Teko?ais lietot?js� un objekts nav pabeigts
    if(($act['RAKT_RLTT_ID'] == $userId && ($isEditor || $isAdmin) && ($status == STAT_INSERT || $status == STAT_RETURN )))
    {
       $isAllowEstimate = true;
    }

    
    if(($act['RAKT_RLTT_ID'] == $userId && $isEditor && ($status == STAT_INSERT) ) ||
        ($isAdmin && ($status != STAT_DELETE))  )
    {
      $isDelete   = true;
    }
  }
  else
  {
     if(($isEditor || $isAdmin) && $status == STAT_INSERT)
     {
        $isReadonly   = false;
     }
  }
  // form elements

  $oForm -> addElement('hidden', 'isSearch', '', $isSearch);
  $oForm -> addElement('hidden', 'searchNum', '', $searchNum);
  $oForm -> addElement('hidden', 'action', '');
  $oForm -> addElement('hidden', 'actId', null, isset($act['RAKT_ID'])? $act['RAKT_ID']:'');
  $oForm -> addElement('hidden', 'status', null, $status);
  $oForm -> addElement('hidden', 'foundsId', null, isset($act['RAKT_KFNA_ID'])?$act['RAKT_KFNA_ID']: $foundsId);
  $oForm -> addElement('label', 'statusTxt',  text::get('STATUS'), $statusLabel);
  $oForm -> addElement('hidden', 'actNumber',  '', isset($act['RAKT_NUMURS'])?$act['RAKT_NUMURS']:'');

  $oLink=new urlQuery();
  $oLink->addPrm(DONT_USE_GLB_TPL, 1);
  $oLink->addPrm(FORM_ID, 'f.akt.s.18');
  $oLink->addPrm('actId', $actId);


  $oForm -> addElement('kls', 'type',  text::get('SINGLE_ACT_TYPE'), array('classifName'=>KL_SOURCE_OF_FOUNDS_ACTA_TYPE,'value'=>isset($act['RAKT_KFNA_ID'])?$act['RAKT_KFNA_ID']:$foundsId,'readonly'=>false), ' disabled ');


  $oForm -> addElement('kls', 'ouner',  text::get('ACT_OUNER'), array('classifName'=>KL_USERS,'value'=>isset($act['RAKT_RLTT_ID'])?$act['RAKT_RLTT_ID']:$userId,'readonly'=>false), ' disabled ');

  $actNumber = '';
  if($actId != false)
  {
     $actNumber = $act['RAKT_FULL_NUMBER'];
  }
  $oForm -> addElement('label', 'actFullNumber',  text::get('ACT_NUMBER'), $actNumber);

  $oForm -> addElement('select', 'dvArea',  text::get('ED_IECIKNIS'), isset($act['RAKT_KDVI_ID'])?$act['RAKT_KDVI_ID']:$dvAreaId, ' disabled', '', '', $dvarea);

  $oForm -> addElement('kls', 'sourceOfFounds',  text::get('SINGLE_SOURCE_OF_FOUNDS'), array('classifName'=>KL_SOURCE_OF_FOUNDS,'value'=>isset($act['RAKT_KFNA_ID'])?$act['RAKT_KFNA_ID']:$foundsId,'readonly'=>false), 'disabled');

  //datums
  $oForm -> addElement('date', 'proces_date',  text::get('PROCESS_DATE'), isset($act['RAKT_IZPILDES_DATUMS'])?$act['RAKT_IZPILDES_DATUMS']:dtime::now(), 'tabindex=4 maxlength="10"'.(($isReadonly && $isReadonlyExceptEconomist)?' disabled ':''));
  if (!$isReadonly || !$isReadonlyExceptEconomist)
  {
    $oForm -> addRule('proces_date', text::get('ERROR_REQUIRED_FIELD'), 'required');
    $oForm -> addRule('proces_date', text::get('ERROR_INCORRECT_DATE'),'validatedate');
  }

  $oForm -> addElement('text', 'description',  text::get('COMMENT'), isset($act['RAKT_PIEZIMES'])?$act['RAKT_PIEZIMES']:'', 'maxlength="150"'.(($isReadonly)?' disabled':''));



  // form buttons
  if (!$isReadonly || !$isReadonlyExceptAdmin)
  {
    $oForm -> addElement('submitImg', 'save', text::get('SAVE'), 'img/btn_saglabat.gif', 'width="70" height="20" onclick="if (!isEmptyFormField(\'frmMain\',\'actId\')) {setValue(\'action\',\''.OP_UPDATE.'\');} else {setValue(\'action\',\''.OP_INSERT.'\');}"');

  }
  if (!$isReadonly)
  {

    $oForm -> addElement('clearImg', 'clear', text::get('CLEAR'), 'img/btn_attirit.gif', 'width="70" height="20"','','',array('onClick'=>'refreshField();'));
    $oForm -> addElement('static', 'jsButtonsControl', '', '
			<script>
				function refreshField()
				{

                    document.all["status"].value = "'.$status.'";
				}
				refreshField();

			</script>
		');

  }
  if (!$isReadonly && ($actId != false))
  {
    $oForm -> addElement('submitImg', 'export', text::get('EXPORT'), 'img/btn_eksportet.gif', 'width="70" height="20" onclick="if (!isEmptyFormField(\'frmMain\',\'actId\')) {setValue(\'action\',\''.OP_EXPORT.'\');} else {return false;}"');
  }
  
  if (!$isReadonlyExceptEconomist)
  {
    $oForm -> addElement('submitImg', 'return', text::get('RETURN'), 'img/btn_atgriezt.gif', 'width="70" height="20" onclick="if (!isEmptyFormField(\'frmMain\',\'actId\')) {setValue(\'action\',\''.OP_RETURN.'\');} else {return false;}"');
    $oForm -> addElement('submitImg', 'accept', text::get('ACCEPT'), 'img/btn_apstiprinat.gif', 'width="70" height="20" onclick="if (!isEmptyFormField(\'frmMain\',\'actId\')) {setValue(\'action\',\''.OP_ACCEPT.'\');} else {return false;}"');
  }
  if ($isAdmin && ($actId != false) && ($status == STAT_DELETE || $status == STAT_CLOSE))
  {
    $oForm -> addElement('submitImg', 'return', text::get('RETURN'), 'img/btn_atgriezt.gif', 'width="70" height="20" onclick="if (!isEmptyFormField(\'frmMain\',\'actId\')) {setValue(\'action\',\''.OP_RETURN.'\');} else {return false;}"');
  }
  if($actId != false)
  {
      // back button
      // act advanced search form
    	$oLink=new urlQuery();
    	$oLink->addPrm('isSearch', $isSearch);
      $oLink->addPrm('selectedId', ($isSearch == 1)?$actId : $searchNum);
    	$oLink->addPrm(FORM_ID, 'f.akt.s.9');
    	$actSearchLink=$oLink ->getQuery();
    	unset($oLink);
      // list of acts
    	$oLink=new urlQuery();
      $oLink->addPrm('isNumber', ($isSearch == 1)?0 : $searchNum);
      $oLink->addPrm('selectedId', $actId);
    	$oLink->addPrm(FORM_ID, 'f.akt.s.9');
    	$actListLink=$oLink ->getQuery();
    	unset($oLink);
      $oForm -> addElement('static', 'jsBackButtons', '', '
    			<script>
    			    function reloadBack()
                    {
                        parent["frameTop"].enableFrameControl();
                        window.top.enableResize();
                        window.top.normal();
                        reloadFrame(1,"'.$actSearchLink.'");
                        reloadFrame(2,"'.$actListLink.'");
                        reloadFrame(3,"");
                    }
    			</script>
    		');
      $oForm -> addElement('buttonImg', 'back', text::get('BACK'), 'img/btn_atpakal.gif', 'width="70" height="20" onClick="reloadBack();"');
  }
  $isAllowEstimate = false; // RCDIS-219
  if($isAllowEstimate)
  {
      
      //Popup saites sagatave
      $oPopLink=new urlQuery;
      $oPopLink->addPrm(FORM_ID, 'f.akt.s.21');
      $oPopLink->addPrm('actId',$actId);
      $oForm -> addElement('buttonImg', 'view', text::get('VIEW_ACT'), 'img/btn_aplukot.gif', 'width="79" height="20" onclick="window.open(\''.$oPopLink ->getQuery().'\', \'frmMain\',\'\')"');
  }
  if ($isDelete)
  {
    $oForm -> addElement('submitImg', 'delete', text::get('DELETE'), 'img/btn_dzest.gif', 'width="70" height="20" onclick="if (!isEmptyFormField(\'frmMain\',\'actId\') && isDelete()) {setValue(\'action\',\''.OP_DELETE.'\');} else {return false;}"');
  }

  if ($oForm -> isFormSubmitted())
  {
        if ($oForm->getValue('action')==OP_INSERT ||
        ($oForm->getValue('action')==OP_UPDATE  && is_numeric($oForm->getValue('actId'))) ||
        ($oForm->getValue('action')==OP_EXPORT && is_numeric($oForm->getValue('actId'))) )
	{
         $check = false;
    	   $r = false;
         $r1 = true;
         $r2 = true;
         if (($isEditor ||$isAdmin) && $oForm -> getValue('status') &&
            $oForm -> getValue('type') && $oForm -> getValue('dvArea') &&
            $oForm -> getValue('sourceOfFounds') && $oForm -> getValue('proces_date') )
          {
                $check = true;
          }
            if ($isAdmin  && !$oForm -> getValue('ouner') )
          {
                $check = false;
          }

          // if all is ok, do operation
          if($check)
          {
            // save
            if($oForm->getValue('action')==OP_INSERT || $oForm->getValue('action')==OP_UPDATE)
            {
    		    // get next act number
                /*if($oForm->getValue('action')==OP_INSERT)
                {
                  $actNumber =  dbProc::getNextActNumber($oForm->getValue('dvArea'));
                }*/

    		  	      $r=dbProc::saveAct(($oForm->getValue('action')==OP_UPDATE?$oForm->getValue('actId'):false),
    		    			          99,
                            false,
                            $oForm->getValue('proces_date'),
                            $oForm->getValue('description'),
                            false,
                            false,
                            false,
                            $oForm->getValue('status'),
                            ($oForm->getValue('ouner'))? $oForm->getValue('ouner'):$userId,
                            $oForm->getValue('sourceOfFounds'),
                            false,
                            false,
                            false,
                            $oForm->getValue('dvArea'),
                            false,
                            false,
                            false,
                            false,
                            false,                            
                            false,
                            false,
                            1
    						);

                // get inserted record ID
                if($oForm->getValue('action')==OP_INSERT)
                {
                    $actId=dbLayer::getInsertId();
                    // saglaba lietotaja darbiibu
                    dbProc::setUserActionDate($userId, ACT_INSERT);
                }
                else
                {
                    $actId =  $oForm->getValue('actId');
                    // saglaba lietotaja darbiibu
                    dbProc::setUserActionDate($userId, ACT_UPDATE);
                }

                // save status changed time
                if($oForm->getValue('action')==OP_INSERT)
                {
                   $r1 = dbProc::saveAuditRecord($actId, $userId, $oForm->getValue('status'));
                }
    			if (!$r || !$r1 )
    			{
    				$oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
    			}

    		}
            // export
            if($oForm->getValue('action')==OP_EXPORT)
            {
              if(TRUE)
              {
                $r=dbProc::saveAct($oForm->getValue('actId'),
                            99,
                            0,
                            $oForm->getValue('proces_date'),
                            $oForm->getValue('description'),
                            false,
                            false,
                            false,
                            $oForm->getValue('status'),
                            ($oForm->getValue('ouner'))? $oForm->getValue('ouner'):$userId,
                            $oForm->getValue('sourceOfFounds'),
                            false,
                            false,
                            false,
                            $oForm->getValue('dvArea'),
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            1
    						);

                  if($oForm->getValue('action')==OP_EXPORT )
		          {
                    $pdf=new FPDF();
                    $pdf->AddFont('Arial','','arial.php');
                    $pdf->AddFont('Arial','BI','arialbi.php');
                    $pdf->AddFont('Arial','I','ariali.php');
                    $pdf->AddFont('Arial','B','arialbd.php');
                    $pdf->SetDisplayMode('real');
                    $pdf->SetAutoPageBreak(true,20);
                    $pdf->AliasNbPages();
                    $pdf->SetTitle(iconv('UTF-8', 'windows-1257',text::get('RFC_ACT_HEADER').' '.$act['NUMURS']));
                    $pdf->SetLeftMargin(20);
                    $pdf->AddPage();
                    $pdf->SetFont('Arial','B',10);
                    $pdf->Cell(0,0,iconv('UTF-8', 'windows-1257',text::get('RFC_ACT_HEADER')), 0,1,'C');


                    // Create new PHPExcel object
                    $objPHPExcel = new PHPExcel();

                    // create act header
                    $actHeader= array(
                       array(
                         array('', 'L', '', 8),
                         array('', 'L', '', 8)
                            ) ,
                       array(
                         array('', 'L', '', 8),
                         array('', 'L', '', 8)
                            ) ,
                      array(

                         array(iconv('UTF-8', 'windows-1257',dtime::dateToLvStringEmpty(date('d.m.Y'))), 'L', '', 8),
                         array(iconv('UTF-8', 'windows-1257',text::get('NUMBER_SIMBOL').'. '.$act['NUMURS']), 'R', 'B', 10)
                            ),
                      array(
                         array('', 'L', '', 8),
                         array('', 'L', '', 8)
                            ) ,
                     array(
                         array(iconv('UTF-8', 'windows-1257',text::get('REPORT_WORK_OFFER').':'), 'L', '', 8),
                         array(iconv('UTF-8', 'windows-1257',text::get('SINGLE_SOURCE_OF_FOUNDS').':'), 'L', '', 8)
                            ) ,
                      array(
                         array(iconv('UTF-8', 'windows-1257',text::get('ST_COMPANY_NAME').', '.text::get('REGISTRATION_SIMBOL').'. '.text::get('NUMBER_SIMBOL').': '.text::get('ST_REG_NR')), 'L', 'B', 8),
                         array(iconv('UTF-8', 'windows-1257',$act['FOUNDS']), 'L', '', 8)
                            ) ,

                       array(
                         array(iconv('UTF-8', 'windows-1257',text::get('WORKS_AND_TERITORY').':'), 'L', '', 8),
                         array(iconv('UTF-8', 'windows-1257',$act['DV_AREA']), 'L', '', 8)
                            ) ,

                    ) ;
                    $pdf->HeaderStyledTable($actHeader);

                    $pdf->SetFont('Arial','BI',8);
                    $pdf->Cell(0,8,iconv('UTF-8', 'windows-1257', $act['WORK_TITLE']),0,1);

                    $actDetales= array(
                       array(
                         array(iconv('UTF-8', 'windows-1257',text::get('COMMENT')).':',  'L', 'B', 8),
                         array(iconv('UTF-8', 'windows-1257',$act['RAKT_PIEZIMES']), 'L', '', 8)
                            ),
                    );
                    $pdf->DescriptionAsimetricalTable($actDetales);

                    // darbi
                    $pdf->SetFont('Arial','BI',10);
                    $pdf->Cell(0,10,iconv('UTF-8', 'windows-1257',text::get(TAB_WORKS)), 0,1);

                    $width=array(40,40,20,20,20,20,20);
                    $alignHeader=array('L','L','C','C','C','C','R');
                    $align=array('L','L','C','C','L','C','R');

                    $headers = array(
                        'WORK_TYPE' => iconv('UTF-8', 'windows-1257',text::get('WORK_TYPE')),
                        'WORK_DESCRIPTION' => iconv('UTF-8', 'windows-1257',text::get('WORK_DESCRIPTION')),
                        'RCFD_DATUMS_NO' => iconv('UTF-8', 'windows-1257',text::get('DATE').' '.text::get('FROM')),
                        'RCFD_DATUMS_LIDZ' => iconv('UTF-8', 'windows-1257',text::get('DATE').' '.text::get('UNTIL')),
                        'RCFD_KPRF_VARDS_UZVARDS' => iconv('UTF-8', 'windows-1257',text::get('USER_NAME').' '.text::get('USER_SURNAME')),
                        'RCD_KODS' => iconv('UTF-8', 'windows-1257',text::get('RCD_KODS')),
                        'TRANSPORT_WORK_TIME' => iconv('UTF-8', 'windows-1257',text::get('TRANSPORT_WORK_TIME'))
                        );

                    $aWorkDb=dbProc::getActRfcWorkList($actId);
                    $data = array();
					          $lineHeight = array();
                    $totalD = 0;
                    foreach ($aWorkDb as $i => $work)
                    {
                      $data[$i] = array(
                        'WORK_TYPE' => iconv('UTF-8', 'windows-1257',$work['WORK_TITLE']),
                        'WORK_DESCRIPTION' => text::returnsplit(iconv('UTF-8', 'windows-1257',$work['RCFD_WORK_DESCRIPTION']),  5, "\n", 30),
                        'RCFD_DATUMS_NO' => iconv('UTF-8', 'windows-1257',$work['RCFD_DATUMS_NO']),
                        'RCFD_DATUMS_LIDZ' => iconv('UTF-8', 'windows-1257',$work['RCFD_DATUMS_LIDZ']),
                        'RCFD_KPRF_VARDS_UZVARDS' => iconv('UTF-8', 'windows-1257',$work['RCFD_KPRF_VARDS_UZVARDS']),
                        'RCD_KODS' => $work['RCFD_KPRF_KODS'],
                        'TRANSPORT_WORK_TIME' => $work['RCFD_STUNDAS']
                        );

                       $lineHeight[$i] = text::returnLineCount(iconv('UTF-8', 'windows-1257',$work['RCFD_WORK_DESCRIPTION']), 30);

                      $totalD +=  $work['RCFD_STUNDAS'];
                    }

                    $pdf->FancyTableRfc($headers,$data, $width, $align, $alignHeader, $lineHeight);


                    $pdf->SetFont('Arial','B',8);
                    $pdf->Cell(160,6,iconv('UTF-8', 'windows-1257',text::get(TOTAL)).":",0,0,'R');
                    $pdf->SetFont('Arial','B',8);
                    $pdf->Cell(20,6,number_format($totalD, 2),0,1,'R');

                    $rows=dbProc::getUserInfo($userId);
                    if (count($rows)==1)
                    {
                      $actAutorName =  $rows[0]['RLTT_VARDS'].' '.$rows[0]['RLTT_UZVARDS'];
                    }


                     $actFooter = array(
                     array(
                      iconv('UTF-8', 'windows-1257',text::get('ACT_CREATER').': '.$act['AUTORS']),
                      ''
                         ),
                      array(
                      iconv('UTF-8', 'windows-1257',text::get('DATE').': '.date('d.m.Y')),
                      iconv('UTF-8', 'windows-1257','')
                         ),
                      array(
                      iconv('UTF-8', 'windows-1257',''),
                      iconv('UTF-8', 'windows-1257','')
                          ),
                      array(
                      iconv('UTF-8', 'windows-1257',text::get('REPORT_WORKER_FINISHED').': '),
                      ''
                         ),

                      array(
                      iconv('UTF-8', 'windows-1257',$act['AUTORS']),
                      ''
                         )
                      );
                    $pdf->FooterTablePortraitAsimetrikal($actFooter);
                    $pdf->Cell(0,10,iconv('UTF-8', 'windows-1257',text::get('EXPORT_FOOTER')), 0,1);
		          }
 

                     // saglaba faila datus
                  $res=dbProc::replaceFile(
                      'R_'.$act['NUMURS'].'_'.dtime::now().'.'.EXPORT_FILE_EXTENTION,
                      $userId, $actId,
                      FILE_EXPORT,
                      EXPORT_FILE_TYPE);
  		        if ($res)
  		        {
  		        	$idFile=dbLayer::getInsertId();
  		        }
  		        else
  		        {
  		        	$oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
  		        }
  		        if ($idFile!==false)
  		        {
                      $pdf->Output(FILES_STORE_PATH."/".$idFile, 'F');

                      // update act status
                      $r=dbProc::saveAct($oForm->getValue('actId'),
      		    			          false,false,false,false,false,false,
                              true,
                              STAT_ACCEPT);
                      // save status changed time
                      $r1 = dbProc::saveAuditRecord($actId, $userId, STAT_ACCEPT);
                      // saglaba lietotaja darbiibu
                      dbProc::setUserActionDate($userId, ACT_EXPORT);

          			if (!$r || !$r1)
          			{
          				$oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
          			}


                }
                else
                {
                  $oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
                }
            }
         }
        // if operation was compleated succefully, show success mesage and redirect current frame
		if ($r)
		{
			$oLink=new urlQuery();
			$oLink->addPrm(FORM_ID, 'f.akt.s.18');
			$oLink->addPrm('actId', $actId);
      $oLink->addPrm('isSearch', $isSearch);
      $oLink->addPrm('searchNum', $searchNum);
			switch($oForm->getValue('action'))
			{
				case OP_INSERT:
					$oLink->addPrm('successMessage', text::get('RECORD_WAS_ADDED'));
					break;
				case OP_UPDATE:
					$oLink->addPrm('successMessage', text::get('RECORD_WAS_SAVED'));
					break;
                case OP_EXPORT:
					$oLink->addPrm('successMessage', text::get('INFO_WAS_EXPORTED'));
                    $oLink->addPrm('tab', TAB_EXPORT);
					break;
                case OP_ESTIMATE:
					$oLink->addPrm('successMessage', text::get('INFO_WAS_ESTIMATED'));
                    $oLink->addPrm('tab', TAB_ESTIMATE);
					break;
        	}
			RequestHandler::makeRedirect($oLink->getQuery());
		}
     }
	 else
	 {
	 	$oForm->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
	 }
    }

    if (($oForm->getValue('action')==OP_RETURN && is_numeric($oForm->getValue('actId'))) ||
    ($oForm->getValue('action')==OP_RETURN_CONFIRMED && is_numeric($oForm->getValue('actId'))) ||
    ($oForm->getValue('action')==OP_ACCEPT && is_numeric($oForm->getValue('actId'))) )
	{
       	 $r = false;
         $r1 = true;
       	 $checkRequiredFields = false;
         if ($isEconomist && $oForm -> getValue('proces_date')  )
    	 {
    	    $checkRequiredFields = true;
            $_proces_date = $oForm->getValue('month');
            $returnStatus  = STAT_RETURN;
            $returnAction =  ACT_RETURN_INSERT;
         }
         if ($isAdmin && ($oForm -> getValue('status') == STAT_DELETE || $oForm -> getValue('status') == STAT_CLOSE) )
    	 {
    	    $checkRequiredFields = true;
            $_proces_date = false;
            if($oForm -> getValue('status') == STAT_DELETE)
            {
               $returnStatus  = STAT_INSERT;
               $returnAction =  ACT_RETURN_INSERT;
            }
            if($oForm -> getValue('status') == STAT_CLOSE)
            {
               $returnStatus  = STAT_ACCEPT;
               $returnAction =  ACT_RETURN_ACCEPT;
            }
         }
         if($checkRequiredFields)
         {
            // atgriezt vai apstiprin?t
            if($oForm->getValue('action')==OP_RETURN || $oForm->getValue('action')==OP_ACCEPT
                || $oForm->getValue('action')==OP_RETURN_CONFIRMED)
            {
                            $r=dbProc::saveAct($oForm->getValue('actId'),
    		    			99,
                            false,
                            $_proces_date,                            
                            false,
                            false,
                            false,
                            false,
                            ($oForm->getValue('action')==OP_RETURN || $oForm->getValue('action')==OP_RETURN_CONFIRMED)? $returnStatus : STAT_CLOSE,
                             false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,                            
                            false,
                            false,
                            1
    						);
                if($r)
                {
                    // save status changed time
                    $r1 = dbProc::saveAuditRecord($actId, $userId, ($oForm->getValue('action')==OP_RETURN)? $returnStatus : STAT_CLOSE);
                    dbProc::setUserActionDate($userId, ($oForm->getValue('action')==OP_RETURN)? $returnAction : ACT_CLOSE);

            	 	if (!$r1)
            		{
            			$oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
            		}

                }
                else
                {
                   $oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
                }
                // if operation was compleated succefully, show success mesage and redirect current frame
        		if ($r && $r1)
        		{
        			$oLink=new urlQuery();
        			$oLink->addPrm(FORM_ID, 'f.akt.s.18');
        			$oLink->addPrm('actId', $actId);
              $oLink->addPrm('isSearch', $isSearch);
              $oLink->addPrm('searchNum', $searchNum);
              $oLink->addPrm('successMessage', text::get('RECORD_WAS_SAVED'));

        			RequestHandler::makeRedirect($oLink->getQuery());
        		}
            }
         }
         else
         {
            $oForm->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
         }
    }

    if (($oForm->getValue('action')==OP_DELETE && is_numeric($oForm->getValue('actId'))) ||
        ($oForm->getValue('action')==OP_DELETE_CONFIRMED && is_numeric($oForm->getValue('actId'))))
	{
          if(($act['RAKT_RLTT_ID'] == $userId && $isEditor && ($status == STAT_INSERT) ) || ($isAdmin && ($status != STAT_DELETE))  )
           {
               $r=dbProc::saveAct($oForm->getValue('actId'),
    		    			99,
                            false,
                            false,                            
                            false,
                            false,                            
                            false,
                            false,
                            STAT_DELETE,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,                            
                            false,
                            1
    						);
                if($r)
                {
                    // save status changed time
                    $r1 = dbProc::saveAuditRecord($actId, $userId, STAT_DELETE);
                    dbProc::setUserActionDate($userId, ACT_DELETE);

        			if (!$r1)
        			{
        				$oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
        			}

                }
                else
                {
                   $oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
                }
                // if operation was compleated succefully, show success mesage and redirect current frame
        		if ($r && $r1)
        		{
        			$oLink=new urlQuery();
        			$oLink->addPrm(FORM_ID, 'f.akt.s.18');
        			$oLink->addPrm('actId', $actId);
              $oLink->addPrm('isSearch', $isSearch);
              $oLink->addPrm('searchNum', $searchNum);
              $oLink->addPrm('successMessage', text::get('RECORD_WAS_SAVED'));

        			RequestHandler::makeRedirect($oLink->getQuery());
        		}
            }
            else
            {
              $oForm->addError(text::get('ERROR_NO_ALL_INPUT_DATES'));
            }

    }

    if ($oForm->getValue('action')==OP_COPY && is_numeric($oForm->getValue('actId')))
	{
         $r = false;
         $r=dbProc::saveAct(false,
                            99,
                            0,
                            $oForm->getValue('proces_date'),
                            $oForm->getValue('description'),
                            false,
                            false,
                            false,
                            STAT_INSERT,
                             ($oForm->getValue('ouner'))? $oForm->getValue('ouner'):$userId,
                            $oForm->getValue('sourceOfFounds'),
                            false,
                            false,
                            false,
                            $oForm->getValue('dvArea'),
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,                           
                            false,
                            1
    						);

          // save status changed time
          if ($r)
          {
            $newActId =  dbLayer::getInsertId();
            $r1 = dbProc::saveAuditRecord($newActId, $userId, STAT_INSERT);
            dbProc::setUserActionDate($userId, ACT_COPY);

              // if operation was compleated succefully, show success mesage and redirect current frame
              if ($r1)
              {
                $oLink=new urlQuery();
        	      $oLink->addPrm(FORM_ID, 'f.akt.s.18');
        	      $oLink->addPrm('actId', $newActId);
                $oLink->addPrm('successMessage', text::get('RECORD_WAS_ADDED'));
                $oLink->addPrm('isSearch', $isSearch);
                $oLink->addPrm('searchNum', $searchNum);
                $oLink->addPrm('isCopy', 1);

                RequestHandler::makeRedirect($oLink->getQuery());
              }
              else
              {
                   $oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
              }
          }
          else
          {
               $oForm->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
          }
    }
  }

    $oForm -> makeHtml();
    include('f.akt.s.18.tpl');

    $tabs = new tabs("example1");

	$tabs->start(text::get(TAB_WORKS));
    include('f.akt.s.19.inc');
    $tabs->end();


    $tabs->start(text::get(TAB_EXPORT));
    include('f.akt.s.6.inc');
   	$tabs->end();

    $tabs->start(text::get(TAB_AUDIT));
    include('f.akt.s.7.inc');
  	$tabs->end();

    // RCDIS-219
    /*
    $tabs->start(text::get(TAB_ESTIMATE));
    include('f.akt.s.10.inc');
  	$tabs->end();
    */
    $tabs->active = text::get($tab);

    $tabs->run();
  
}
else
{
    RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>