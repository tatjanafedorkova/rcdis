﻿<h1><?=text::toUpper(text::get(TAB_PERONAL));?></h1>
<?= $oFormPersonalTab -> getFormHeader(); ?>
<table cellpadding="5" cellspacing="1" border="0" width="100%">
	<tr>
		<td align=center colspan="2"><?= $oFormPersonalTab -> getMessage(); ?></td>
	</tr>
    <tr>
		<td valign="top">
        <?
        $x = 0;
        foreach ($aCustomers as $d => $aDayCust)
        {
        ?>
        <table cellpadding="5" cellspacing="1" border="0" width="100%">
		   <tr class="table_head_2">

                <th width="10%"><?=text::get('RCD_KODS');?></th>
                <th  width="10%"><?=text::get('FINISHING_DATE');?><br />(<?=explode('-',$d)[0];?>)</th>
                <th  width="5%"><?=text::get('BRIGADE');?></th>
				<th width="37%"><?=text::get('USER_NAME');?> <?=text::get('USER_SURNAME');?></th>
				<th  width="10%"><?=text::get('PROPORTION');?></th>
                <th  width="10%"><?=text::get('BASE_TIME');?><br />(<?=$totalTimeFixed[$d]['cvs'];?>)</th>
                <? if($isEpla == 0) { ?>
                <th  width="10%"><?=text::get('OVER_TIME');?><br />(<?=$totalTimeFixed[$d]['koef'];?>)</th>
                <? } ?>
				<th  width="3%"><br /></th>
				<th  width="10%"><?=text::get('ROUT_TIME');?><br /></th>
                <th width="3%">&nbsp;</th>
                		
			</tr>

          <?
          
        foreach ($aDayCust as $j => $actCustomer)
		{
            if(!isset($actCustomer['PRSN_PLAN_DATE'])) continue;
		?>
			<tr class="table_cell_3"  id="blank_mat<?= $i; ?>">

				<td>
                    <?= $oFormPersonalTab->getElementHtml('id['.$x.']'); ?>
                    <?= $oFormPersonalTab->getElementHtml('code['.$x.']'); ?>
                </td>
                <td><?= $oFormPersonalTab->getElementHtml('plan_date['.$x.']'); ?></td>
                <td><?= $oFormPersonalTab->getElementHtml('brigade['.$x.']'); ?></td>
				<td><?= $oFormPersonalTab->getElementHtml('nameSurname['.$x.']'); ?>
                    <?= $oFormPersonalTab->getElementHtml('place['.$x.']'); ?>
                    <?= $oFormPersonalTab->getElementHtml('region['.$x.']'); ?>
                </td>
                <td><?= $oFormPersonalTab->getElementHtml('proportion['.$x.']'); ?></td>
                <td><?= $oFormPersonalTab->getElementHtml('baseTime['.$x.']'); ?></td>
                <? if($isEpla == 0) { ?>
                <td><?= $oFormPersonalTab->getElementHtml('overTime['.$x.']'); ?></td>
                <? } ?>
                <td><?= $oFormPersonalTab->getElementHtml('isProportional['.$x.']'); ?></td>
		        <td><?= $oFormPersonalTab->getElementHtml('laiksCela['.$x.']'); ?></td>		

             	<td>
                    <? if(!$isReadonly)  {?>
                    <?= $oFormPersonalTab->getElementHtml('cust_del['.$x.']'); ?>
                    <? } else{ ?>
                    &nbsp;
                    <? } ?>
                </td>
			</tr>

        <?
        $x++;
		}
		?>
        <tr>
             <td align="right" colspan="5"><b><?=text::get('TOTAL');?>:</b></td>
             <td align="center"><b><?= $totalWorkTime[$d]; ?></b></td>
             <? if($isEpla == 0) { ?>
             <td align="center"><b><?= $totalOverTime[$d]; ?></b></td>
             <? } ?>
        </tr>
        <?
		}
        ?>
        <tr class="table_head_2">
			<td align="right" colspan="5"><b><?=text::get('TOTAL');?>:</b></td>
			<td align="center"><b><?= $totalWork; ?></b></td>
             <? if($isEpla == 0) { ?>
             <td align="center"><b><?= $totalOver; ?></b></td>
             <? } ?>
			<td>&nbsp;</td>
	   </tr>
	    </table>
        </td>
        <td width="20%"  valign="top">
            <table cellpadding="5" cellspacing="1" border="0" align="center" width="100%">
            <? if(!$isReadonly)  {
              foreach($favorit as $i=>$val){?>
             <tr>
                <td bgcolor="white" colspan="5"><?= $oFormPersonalTab->getElementHtml('favorit['.$i.']'); ?></td>
             </tr>
            <? } ?>
             

            <?} ?>
            </table>
            <br />
            <table cellpadding="5" cellspacing="0" border="0" align="center" >
            <tr>
             <? if(!$isReadonly)  {?>
            	<td><?= $oFormPersonalTab -> getElementHtml('save'); ?></td>
             <? } ?>

             </tr>
            </table>
        </td>
	</tr>

</table>
<?= $oFormPersonalTab -> getFormBottom(); ?>

