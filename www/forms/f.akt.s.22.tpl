﻿<body class="frame_1">
<?=$oForm->getElementHtml('jsRefresh2');?>
<h1><?= $actTitle; ?></h1>
<?= $oForm -> getFormHeader(); ?>
<table cellpadding="5" cellspacing="1" border="0" width="100%">
	<tr>
		<td align=center colspan="5"><?= $oForm -> getMessage(); ?></td>
	</tr>
	<tr>
		<td colspan="4" class="table_head" align="right">
            <?= $oForm -> getElementLabel('statusTxt'); ?>:
            <font color="red"><?=text::toUpper($oForm -> getElementHtml('statusTxt'));?></font></td>
        <td width="20%" rowspan="<?=(($actId === false)?'14':'15');?>" valign="bottom">
            <table cellpadding="5" cellspacing="0" border="0" align="center">
             <? if(!$isReadonly || !$isReadonlyExceptAdmin)  {?>
            	<tr><td><?= $oForm -> getElementHtml('save'); ?></td> </tr>
             <? } ?>
             <? if(!$isReadonly && ($actId != false))  {?>
                <tr><td><?=$oForm->getElementHtml('export');?></td></tr>
             <? } ?>
             <? if(!$isReadonlyExceptEconomist)  {?>
                <tr><td><?=$oForm->getElementHtml('return');?></td></tr>
             <? } ?>
             <? if($isAdmin && ($actId != false) && ($status == STAT_DELETE || $status == STAT_CLOSE))  {?>
                <tr><td><?=$oForm->getElementHtml('return');?></td></tr>
             <? } ?>
             <? if(!$isReadonlyExceptEconomist)  {?>
                <tr><td><?=$oForm->getElementHtml('accept');?></td></tr>
             <? } ?>
             <? if($isAllowEstimate)  {?>
                <tr><td><?=$oForm->getElementHtml('estimate');?></td></tr>
             <? } ?>
             <? if($isAllowContinue)  {?>
                <tr><td><?=$oForm->getElementHtml('continue');?></td></tr>
             <? } ?>
             <? if(!$isReadonly)  {?>
                <tr><td><?=$oForm->getElementHtml('clear');?></td></tr>
             <? } ?>
             <? if($isAllowEstimate)  {?>
                <tr><td><?=$oForm->getElementHtml('view');?></td></tr>
             <? } ?>
             <? if($isDelete)  {?>
                <tr><td><?=$oForm->getElementHtml('delete');?></td></tr>
             <? } ?>
             <? if($actId != false)  {?>
                <tr><td><?=$oForm->getElementHtml('back');?></td></tr>
             <? } ?>
            </table>
        </td>
	</tr>
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('EDarea'); ?>:<?=(($isReadonly)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" colspan="3"><?= $oForm -> getElementHtml('EDarea'); ?></td>
   	</tr>
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('type'); ?>:<?=(($isReadonly)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('type'); ?></td>
        <td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('ouner'); ?>:<?=(($isReadonlyExceptAdmin)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('ouner'); ?></td>
   	</tr>
    <? if($actId != false)  {?>
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('actFullNumber'); ?>:</td>
		<td class="table_cell_2" colspan="3"><?= $oForm -> getElementHtml('actFullNumber'); ?><?= $oForm -> getElementHtml('actNumPostfixLink'); ?></td>
   	</tr>
    <? } ?>
    
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('designation'); ?>:<?=(($isReadonly)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" colspan="3"><?= $oForm -> getElementHtml('designation'); ?></td>
   	</tr>
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('voltage'); ?>:<?=(($isReadonly)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" colspan="3"><?= $oForm -> getElementHtml('voltage'); ?></td>
   	</tr>
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('object'); ?>:<?=(($isReadonly)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" colspan="3"><?= $oForm -> getElementHtml('object'); ?></td>
   	</tr>
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('sourceOfFounds'); ?>:<?=(($isReadonly)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" colspan="3"><?= $oForm -> getElementHtml('sourceOfFounds'); ?></td>
   	</tr>
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('month'); ?>:<?=(($isReadonly && $isReadonlyExceptEconomist)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('month'); ?></td>
        <td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('year'); ?>:<?=(($isReadonly && $isReadonlyExceptEconomist)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('year'); ?></td>
   	</tr>
     <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('otherPayment'); ?>:</td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('otherPayment'); ?></td>
        <td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('overtimeKoef'); ?>:<?=(($isReadonly)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('overtimeKoef'); ?></td>
   	</tr>    
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('description'); ?>:</td>
		<td class="table_cell_2" colspan="3"><?= $oForm -> getElementHtml('description'); ?></td>
   	</tr>
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('mmsWork'); ?>:<span id="mmsWorkStar" style="display:<?=(($isMmsWorkDisabled)?'none':'inline');?>"><font color="red">*</font></span></td>
		<td class="table_cell_2" colspan="3"><?= $oForm -> getElementHtml('mmsWork'); ?></td>
   	</tr>
    <tr>
     	<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('worktitle'); ?>:<?=(($isReadonly && $isReadonlyExceptEconomist)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" colspan="3"><?= $oForm -> getElementHtml('worktitle'); ?></td>

 	</tr>

     <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('numPostfix'); ?>:<font color="red">*</font></td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('numPostfix'); ?></td>
        <td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('isFinished'); ?>:</td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('isFinished'); ?></td>
   	</tr>
    <tr>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('stock'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('stock'); ?></td>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('middlact'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('middlact'); ?></td>
   	</tr>
    <tr>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('toidn'); ?></td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('toidn'); ?></td>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('toid'); ?>:<font color="red">*</font></td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('toid'); ?></td>
   	</tr>
</table>
<a name="tab"></a>

<?= $oForm -> getFormBottom(); ?>
<?=$oForm->getElementHtml('jsButtonsControl');?>
<?=$oForm->getElementHtml('jsBackButtons');?>
</body>
