<?
// Created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);
$isEditor = ( dbProc::isUserInRole($userId, ROLE_EDITOR) ||  dbProc::isUserInRole($userId, ROLE_EDITOR_VIEWER));

// act ID
$actId  = reqVar::get('actId');
// search form
$isSearch  = reqVar::get('isSearch');
// search act by number
$searchNum  = reqVar::get('searchNum');
// EPLA act
$isEpla  = reqVar::get('isEpla');
// Auto
if(isset($tame))
  $tameWork  = $tame;
else 
  $tameWork   = reqVar::get('tame');
// akta veids 
if(!isset($isInvestition))
  $isInvestition  = reqVar::get('isInvestition');

// Main form with XMLHTTP method load this code:
if (reqVar::get('xmlHttp') == 1)
{
    // if operation is DELETE , delete record from DB
    if (reqVar::get('deleteId')!='')
	  {
	   dbProc::deleteActWorkRecord(reqVar::get('deleteId'), reqVar::get('tame'));
    ?>
	    rowNode = this.parentNode.parentNode;
		  rowNode.parentNode.removeChild(rowNode);
		<?
    }
    // if operation is SAVE , save daudzumu in DB
    if (reqVar::get('saveId')!='' )
	  {
      if (reqVar::get('amount')!='')
      {
        dbProc::updateWorkDaudzums(reqVar::get('saveId'), reqVar::get('amount'), 
                                    reqVar::get('tame'),reqVar::get('isInvestition'),
                                    reqVar::get('investition_work'));
      }

      if (reqVar::get('brigade')!='')
      {
        dbProc::updateWorkBrigade(reqVar::get('saveId'), reqVar::get('brigade'), reqVar::get('tame'));
      }

      if (reqVar::get('plan_date')!='')
      {
        dbProc::updateWorkPlanDate(reqVar::get('saveId'), reqVar::get('plan_date'), reqVar::get('tame'));
      }
	   
    }
    exit();
}

// tikai  sist�mas lietotajam vai administr�toram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
    if($actId !== false )
    {
      $oLink=new urlQuery();
      $oLink->addPrm(FORM_ID, 'f.akt.s.1');
      $oLink->addPrm('actId', $actId);
      $oLink->addPrm('tab', TAB_WORKS);
      if($tameWork==1) {
      	$oLink->addPrm('tametab', TAB_WORKS_T);
      }
      $oLink->addPrm('isSearch', $isSearch);
      $oLink->addPrm('searchNum', $searchNum);
      $oLink->addPrm('isEpla', $isEpla);
      $oLink->addPrm('isInvestition', $isInvestition);
      $oFormWorkTab = new Form('frmWorkTab'.(($tameWork==1) ? 'T' : ''), 'post', $oLink->getQuery() . '#tab');

      unset($oLink);
      

      // if operation is success, show success message and redirect top frame
      if (reqVar::get('successMessageTab') && $isNew === false)
      {
      	$oFormWorkTab->addSuccess(reqVar::get('successMessageTab'));

      }

      $isReadonly   = true;
      // Ir pieejams main��anai, ja  AKTS:LIETOT�JS = �Teko�ais lietot�js� un lietot�ja loma ir �Ievad�tais� un AKTS:STATUS = �Izveide� vai �Atgriezts� vai �T�me�.
      if($isAdmin || ($act['RAKT_RLTT_ID'] == $userId || ($isAuto && $isEditor)) 
            && ($status == STAT_INSERT || $status == STAT_RETURN )  )       {
        $isReadonly   = false;
      }

      //Popup saites sagatave
      $oPopLink=new urlQuery;
      $oPopLink->addPrm(FORM_ID, 'f.akt.s.8');
      $oPopLink->addPrm('actId',$actId);
      $oPopLink->addPrm('catalog',KL_CALCULALATION);
      $oPopLink->addPrm('isSearch', $isSearch);
      $oPopLink->addPrm('searchNum', $searchNum);
      $oPopLink->addPrm('isEpla', $isEpla);      
      $oPopLink->addPrm('tame', $tameWork ?'1':'0');

      
      $tameGroup['TITLE'] = text::get('TAB_ESTIMATE');   ;
      $tameGroup['URL'] = RequestHandler::popupHref($oPopLink -> getQuery().'&estimate=1', false, 700, 450);
      
     
      // get calgulation group
      $calcGroups=array();
      $res=dbProc::getCalculationGroupActivList();
      if (is_array($res))
      {
       	foreach ($res as $i => $row)
       	{
            $calcGroups[$i]['TITLE'] = $row['KKLG_NOSAUKUMS'];
        	  $calcGroups[$i]['URL'] = RequestHandler::popupHref($oPopLink -> getQuery().'&calcGroup='.$row['KKLG_KODS'], false, 700, 450);
        }
      }
      
      unset($res);
      // get favorit
      $favorit=array();
      $krfk = dbProc::getKrfkInfo(KRFK_CALCULATION_ID);
      if($krfk !== false)
      {

        $res=dbProc::getFavoritTypeDefinitionList($userId, $krfk['KRFK_VERTIBA']);
        if (is_array($res))
        {
         	foreach ($res as $i => $row)
         	{
                $favorit[$i]['TITLE'] = $row['LTFV_NOSAUKUMS'];
          	    $favorit[$i]['URL'] = RequestHandler::popupHref($oPopLink -> getQuery().'&favoritId='.$row['LTFV_ID']);
          }
        }
       //print_r($favorit);
        unset($res);
      }
      unset($oPopLink);

      // brigade
      $brigade = array();
      for ($i = 1; $i<16; $i++){
        $brigade[$i] = $i;
      }

      $investitionPrices = dbProc::getInvesticijasPrices(false);

      $aWorks = array();
      $aWorkExist = false;
      $work_key;
    /*  if ($oFormWorkTab -> isFormSubmitted() && $isNew === false)
      {
          //print_r($_POST);
          $isRowValid = true;
          $aChiphers =  $oFormWorkTab->getValue('chipher');
		  
          if(is_array($aChiphers) )
          {
		        //echo '<pre>',print_r($aMeasures),'</pre>';
            $aWorkFr = array();
            foreach ($aChiphers as $i => $val)
            {
                 //echo $oFormWorkTab->getValue('measure['.$i.']').'-'.$i.'<br>';
				         $aWorkFr[$i]['DRBI_KKAL_ID'] = $oFormWorkTab->getValue('kid['.$i.']');
                 $aWorkFr[$i]['DRBI_KKAL_SHIFRS'] = $oFormWorkTab->getValue('chipher['.$i.']');
                 $aWorkFr[$i]['DRBI_KKAL_NOSAUKUMS'] = $oFormWorkTab->getValue('title['.$i.']');
				         $aWorkFr[$i]['DRBI_KKAL_NORMATIVS'] = $oFormWorkTab->getValue('standart['.$i.']');                      
                 $aWorkFr[$i]['DRBI_MERVIENIBA'] = $oFormWorkTab->getValue('measure['.$i.']');
                 $aWorkFr[$i]['DRBI_ID'] = $oFormWorkTab->getValue('id['.$i.']');
                 $aWorkFr[$i]['DRBI_APPROVE_DATE'] = $oFormWorkTab->getValue('edited['.$i.']');
                 $aWorkFr[$i]['DRBI_PLAN_DATE'] = $oFormWorkTab->getValue('plan_date['.$i.']');
                 $aWorkFr[$i]['DRBI_BRIGADE'] = $oFormWorkTab->getValue('brigade['.$i.']');
		 $aWorkFr[$i]['DRBI_WORK_APROVE_DATE'] = $oFormWorkTab->getValue('work_aprove_date['.$i.']');
                 if($oFormWorkTab->getValue('tame') == 1 && $isInvestition){
                  $aWorkFr[$i]['DRBI_INVESTITION_WORK'] = $oFormWorkTab->getValue('investition_work['.$i.']');
                  $aWorkFr[$i]['DRBI_TOTAL_COAST'] = $oFormWorkTab->getValue('total_coast['.$i.']');
                 }
                 
                 if($tameWork && !$isInvestition){
                   
                    $aWorkFr[$i]['DRBI_DAUDZUMS_KOR'] = $oFormWorkTab->getValue('amount_kor['.$i.']');
                    // validate work progress value
                    if (empty($aWorkFr[$i]['DRBI_DAUDZUMS_KOR']))
                    {
                        // assign error message
                        $oFormWorkTab->addErrorPerElem('amount_kor['.$i.']', text::get('ERROR_REQUIRED_FIELD'));
                        $isRowValid = false;
                    }
                    // validate work progress value
                    elseif (!is_numeric($aWorkFr[$i]['DRBI_DAUDZUMS_KOR']))
                    {
                        // assign error message
                        $oFormWorkTab->addErrorPerElem('amount_kor['.$i.']', text::get('ERROR_DOUBLE_VALUE'));
                        $isRowValid = false;
                    }
                    // validate work progress value
                    elseif ($aWorkFr[$i]['DRBI_DAUDZUMS_KOR'] >= 100000)
                    {
                        // assign error message
                        $oFormWorkTab->addErrorPerElem('amount_kor['.$i.']', text::get('ERROR_AMOUNT_VALUE_100000'));
                        $isRowValid = false;
                    }
                    else
                    {
                        $aWorkFr[$i]['DRBI_CILVEKSTUNDAS'] = number_format(($aWorkFr[$i]['DRBI_KKAL_NORMATIVS']*$aWorkFr[$i]['DRBI_DAUDZUMS_KOR']), 2, '.', '');
                    }
                    $aWorkFr[$i]['DRBI_DAUDZUMS'] = $oFormWorkTab->getValue('amount_k['.$i.']');
                    
                } else {
                  
                    $aWorkFr[$i]['DRBI_DAUDZUMS'] = $oFormWorkTab->getValue('amount['.$i.']');
                    // validate work progress value
                    if (empty($aWorkFr[$i]['DRBI_DAUDZUMS'])) {
                        // assign error message
                        $oFormWorkTab->addErrorPerElem('amount[' . $i . ']', text::get('ERROR_REQUIRED_FIELD'));
                        $isRowValid = false;
                    } // validate work progress value
                    elseif (!is_numeric($aWorkFr[$i]['DRBI_DAUDZUMS'])) {
                        // assign error message
                        $oFormWorkTab->addErrorPerElem('amount[' . $i . ']', text::get('ERROR_DOUBLE_VALUE'));
                        $isRowValid = false;
                    } // validate work progress value
                    elseif ($aWorkFr[$i]['DRBI_DAUDZUMS'] >= 100000) {
                        // assign error message
                        $oFormWorkTab->addErrorPerElem('amount[' . $i . ']', text::get('ERROR_AMOUNT_VALUE_100000'));
                        $isRowValid = false;
                    } else {
                        $aWorkFr[$i]['DRBI_CILVEKSTUNDAS'] = number_format(($aWorkFr[$i]['DRBI_KKAL_NORMATIVS'] * $aWorkFr[$i]['DRBI_DAUDZUMS']), 2, '.', '');

                    }
                }

                  // prepare database record data
                  $aAllRows[] = array(
                    'kid' => $aWorkFr[$i]['DRBI_KKAL_ID'],
                    'chipher' => $aWorkFr[$i]['DRBI_KKAL_SHIFRS'],
                    'title' => $aWorkFr[$i]['DRBI_KKAL_NOSAUKUMS'],
                    'standart' => $aWorkFr[$i]['DRBI_KKAL_NORMATIVS'],
                    'amount' => $aWorkFr[$i]['DRBI_DAUDZUMS'],
                    'amount_kor' => ($oFormWorkTab->getValue('tame') == 1 && !$isInvestition) ? $aWorkFr[$i]['DRBI_DAUDZUMS_KOR'] :'',
                    'total' => $aWorkFr[$i]['DRBI_CILVEKSTUNDAS'],
                    'measure' => $aWorkFr[$i]['DRBI_MERVIENIBA'],
                    'edited' => $aWorkFr[$i]['DRBI_APPROVE_DATE'],
                    'plan_date' => $aWorkFr[$i]['DRBI_PLAN_DATE'],
                    'brigade' => $aWorkFr[$i]['DRBI_BRIGADE'],
		    'work_aprove_date' => $aWorkFr[$i]['DRBI_WORK_APROVE_DATE'],
                    'investition_work' => ($oFormWorkTab->getValue('tame') == 1 && $isInvestition) ? $aWorkFr[$i]['DRBI_INVESTITION_WORK'] : '',
                    'total_coast' => ($oFormWorkTab->getValue('tame') == 1 && $isInvestition) ? $aWorkFr[$i]['DRBI_CILVEKSTUNDAS'] * $aWorkFr[$i]['DRBI_INVESTITION_WORK'] : '',
                    'rowNum' => $i
                  );   
                  
                  
             }
            // transform for calculate tota summ for the date
            $aSumms = array();
            $aTotalCoasts = array();
            foreach ($aAllRows as $i => $row){
              $work_key = $row['plan_date'].'-'.$row['brigade'];
              if(isset($aSumms[$work_key])) {
                $aSumms[$work_key] += $row['total'];
              } else {
                $aSumms[$work_key] = $row['total'];
              } 
              if($oFormWorkTab->getValue('tame') == 1 && $isInvestition)     {
                if(isset($aTotalCoasts[$work_key])) {
                  $aTotalCoasts[$work_key] += $row['total_coast'];
                } else {
                  $aTotalCoasts[$work_key] = $row['total_coast'];
                } 
              }        
            }
            foreach ($aAllRows as $i => $row){
              $work_key = $row['plan_date'].'-'.$row['brigade'];
              $aValidRows[$i] = $row;
              $aValidRows[$i]['day_total'] = $aSumms[$work_key];
              if($oFormWorkTab->getValue('tame') == 1 && $isInvestition)     {
                $aValidRows[$i]['coast_total'] = $aTotalCoasts[$work_key];
              }
            }
	    //print_r($aValidRows);
            foreach ($aWorkFr as $i=>$work)
            {
              $work_key = $work['DRBI_PLAN_DATE'].'-'.$work['DRBI_BRIGADE'];
              $aWorks[$work_key][] = $work;
              //$aWorks[$work['DRBI_PLAN_DATE']][$i]['DRBI_DAY_TOTAL'] = $aSumms[$work['DRBI_PLAN_DATE']];
            }
            foreach ($aWorks as $d => $aDayWorks)
            {
              foreach ($aDayWorks as $i => $actWork)
              { 
                $work_key  = $actWork['DRBI_PLAN_DATE'].'-'.$actWork['DRBI_BRIGADE'];              
                $aWorks[$d][$i]['DRBI_DAY_TOTAL'] = $aSumms[$work_key];
                if($oFormWorkTab->getValue('tame') == 1 && $isInvestition)     {
                  $aWorks[$d][$i]['DRBI_TOTAL_COAST'] = $aTotalCoasts[$work_key];
                }
                $aWorkExist = true;   
              }
            }

            //echo '<pre>',print_r($aWorkFr,true),'</pre>';
            //echo '<pre>',print_r($aWorks,true),'</pre>';
           }
      }
      else
      { */
        //Ja eksistee akta darbi, tad nolasaam taas darbus no datu baazes
  	    $aWorkDb=dbProc::getActWorkList($actId, $tameWork, false, $isInvestition );
        $aWorkExist = false;        

        foreach ($aWorkDb as $work)
        {
          $work_key = $work['DRBI_PLAN_DATE'].'-'.$work['DRBI_BRIGADE'];
          $aWorks[$work_key][] = $work;     
          $aWorkExist = true;     
        }
        
        if($isEpla == 1) {
          $aWorkSums = dbProc::getActWorkSumms($actId, $tameWork);
          foreach ($aWorkSums as $d => $aSum)
          {
              $oFormWorkTab->addElement('label', 'sum_m3', 'M3', $aSum['m3']);
              $oFormWorkTab->addElement('label', 'sum_ha', 'HA', $aSum['ha']);
              $oFormWorkTab->addElement('label', 'sum_km', 'KM', $aSum['km']);
              $oFormWorkTab->addElement('label', 'sum_gab', 'GAB', $aSum['gab']);
              break;
          }
        }
     // }

     
      if(!$aWorkExist) {
        $aWorks[0] = array();          
      }
      

      //echo '<pre>',print_r($aWorks,true),'</pre>';

      $oFormWorkTab -> addElement('hidden', 'isSearch', '', $isSearch);
      $oFormWorkTab -> addElement('hidden', 'searchNum', '', $searchNum);
      $oFormWorkTab->addElement('hidden','actId','',$actId);
      $oFormWorkTab->addElement('hidden','maxAmaunt','','100000.00');
      $oFormWorkTab->addElement('hidden','minAmaunt','','0.00');
      $oFormWorkTab->addElement('hidden','tame','',($tameWork) ? 1 : 0);
      
      $totalTime = array();
      $totalCoast = array();
      $workApproveDate = array();
      $total_coast = 0;

      $i = 0;
      // form elements
      foreach ($aWorks as $d => $aDayWorks)
      {
	
        foreach ($aDayWorks as $j => $actWork)
        {          
          $oFormWorkTab->addElement('hidden', 'id['.$i.']', '', $actWork['DRBI_ID']);
          $oFormWorkTab->addElement('hidden', 'kid['.$i.']', '', $actWork['DRBI_KKAL_ID']);
          $oFormWorkTab->addElement('hidden', 'edited['.$i.']', '', $actWork['DRBI_APPROVE_DATE']);
	        $oFormWorkTab->addElement('hidden', 'work_aprove_date['.$i.']', '', $actWork['DRBI_WORK_APROVE_DATE']);

          
          $oFormWorkTab->addElement('label', 'standart['.$i.']', '', $actWork['DRBI_KKAL_NORMATIVS']);
          $oFormWorkTab->addElement('label', 'measure['.$i.']', '', $actWork['DRBI_MERVIENIBA']);  
		      $oFormWorkTab->addElement('label', 'chipher['.$i.']', '', $actWork['DRBI_KKAL_SHIFRS']);
          $oFormWorkTab->addElement('label', 'title['.$i.']', '', $actWork['DRBI_KKAL_NOSAUKUMS']);
          $oFormWorkTab->addElement('label', 'total['.$i.']', '', $actWork['DRBI_CILVEKSTUNDAS'], ' disabled maxlength="10"');

          if ($oFormWorkTab -> isFormSubmitted()){
            $oFormWorkTab->setNewValue('chipher['.$i.']',$actWork['DRBI_KKAL_SHIFRS']);
            $oFormWorkTab->setNewValue('title['.$i.']',$actWork['DRBI_KKAL_NOSAUKUMS']);
            $oFormWorkTab->setNewValue('standart['.$i.']',$actWork['DRBI_KKAL_NORMATIVS']);
            $oFormWorkTab->setNewValue('measure['.$i.']',$actWork['DRBI_MERVIENIBA']);
            $oFormWorkTab->setNewValue('total['.$i.']',$actWork['DRBI_CILVEKSTUNDAS']);
          }

	          $actWork['DRBI_DAUDZUMS'] = number_format($actWork['DRBI_DAUDZUMS'], 3, '.', '');
	          
            // ajax handler link for delete
            $saveLink = new urlQuery();
            $saveLink->addPrm(FORM_ID, 'f.akt.s.2');
            $saveLink->addPrm(DONT_USE_GLB_TPL, '1');
            $saveLink->addPrm('xmlHttp', '1');   
            $saveLink->addPrm('tame',($tameWork) ? 1 : 0);
            $saveLink->addPrm('isInvestition',($isInvestition) ? 1 : 0); 
            $saveLink->addPrm('investition_work',($isInvestition) ? $investitionPrices[0]['INVC_CENA_DARBAST'] : 0); 
           

          if(($tameWork || $oFormWorkTab->getValue('tame') == 1) && !$isInvestition  ){

              $actWork['DRBI_DAUDZUMS_KOR'] = number_format($actWork['DRBI_DAUDZUMS_KOR'], 3, '.', '');

              $oFormWorkTab->addElement('label', 'amount_k['.$i.']', '', $actWork['DRBI_DAUDZUMS']);  
              
              if(empty($actWork['DRBI_APPROVE_DATE']) ) {
                 // save amount
                 $saveJs = "var amount = isDoubleAuto(this.value,3);               
                 if(amount != '') { 
                   if(this.parentNode.firstChild.nodeName == 'SPAN') {
                     this.parentNode.removeChild(sp2);
                   }
                   this.value = amount;
                   var rowId = document.forms['frmWorkTab".(($tameWork ) ? 'T' : '')."']['id[" .$i. "]'];
                   var standart = document.forms['frmWorkTab".(($tameWork ) ? 'T' : '')."']['standart[" .$i. "]'];
                   (xmlHttpGetValue('" .$saveLink -> getQuery() ."&amount='+this.value+'&saveId='+rowId.value));
                     this.style.background = '#ffffff'; this.style.borderColor = '#16bb24cc';                 
                     this.parentNode.prepend(sp1);
                     setTimeout(savedField, 3000, this, '#8c8c8c'); 
                 } else { 
                   this.parentNode.prepend(sp2);
                 }";
                $oFormWorkTab->setNewValue('amount_k['.$i.']', $actWork['DRBI_DAUDZUMS']); 
                //$oFormWorkTab->addElement('text', 'amount_kor['.$i.']', '', $actWork['DRBI_DAUDZUMS_KOR'], (($isReadonly)?' disabled ':'').'maxlength="9"');
                $oFormWorkTab->addElement('text', 'amount_kor['.$i.']', '', $actWork['DRBI_DAUDZUMS_KOR'], (($isReadonly)?' disabled ':'').'maxlength="9"'.
                ((isset($actWork['DRBI_DAUDZUMS_KOR'])) ? '' : ' style="background:#fda8a5;border: 1px solid #f80f07;" ') .'
                onclick="' . $saveJs . ';return false;" onkeypress = "this.onchange();" onpaste = "this.onchange();"' );
                /*$oFormWorkTab -> addRule('amount_kor['.$i.']', text::get('ERROR_REQUIRED_FIELD'), 'required');
                $oFormWorkTab -> addRule('amount_kor['.$i.']', text::get('ERROR_DOUBLE_VALUE'), 'double', 3);
                // check Amaunt field
                $v1=&$oFormWorkTab->createValidation('amauntKOrValidation_'.$i, array('amount_kor['.$i.']'), 'ifonefilled');
                // ja Cilv�kstundas normat�vs ir defin�ts un > 100000
                $r1=&$oFormWorkTab->createRule(array('amount_kor['.$i.']', 'maxAmaunt'), text::get('ERROR_AMOUNT_VALUE_100000'), 'comparedouble','<');
                // ja koeficent ir defin�ts un < 0
                $r2=&$oFormWorkTab->createRule(array('amount_kor['.$i.']', 'minAmaunt'), text::get('ERROR_AMOUNT_VALUE_100000'), 'comparedouble','>');
                $oFormWorkTab->addRule(array($r1, $r2), 'groupRuleAmauntKor_'.$i, 'group', $v1);*/
              }
              else {
                $oFormWorkTab->addElement('label', 'amount_kor['.$i.']', '', $actWork['DRBI_DAUDZUMS_KOR']);                 
              }
              if ($oFormWorkTab -> isFormSubmitted()){
                $oFormWorkTab->setNewValue('amount_kor['.$i.']', $actWork['DRBI_DAUDZUMS_KOR']);
              }
              
          } else {
            if( empty($actWork['DRBI_APPROVE_DATE'])  ) {
                   // save amount
                  $saveJs = "var amount = isDoubleAuto(this.value,3);               
                  if(amount != '') { 
                    if(this.parentNode.firstChild.nodeName == 'SPAN') {
                      this.parentNode.removeChild(sp2);
                    }
                    this.value = amount;
                    var rowId = document.forms['frmWorkTab".(($tameWork ) ? 'T' : '')."']['id[" .$i. "]'];
                    var standart = document.forms['frmWorkTab".(($tameWork ) ? 'T' : '')."']['standart[" .$i. "]'];
                    eval(xmlHttpGetValue('" .$saveLink -> getQuery() ."&amount='+this.value+'&saveId='+rowId.value));
                      this.style.background = '#ffffff'; this.style.borderColor = '#16bb24cc';                 
                      this.parentNode.prepend(sp1);
                      setTimeout(savedField, 3000, this, '#8c8c8c'); 
                  } else { 
                    this.parentNode.prepend(sp2);
                  }";
                //$oFormWorkTab->addElement('text', 'amount['.$i.']', '', $actWork['DRBI_DAUDZUMS'], (($isReadonly)?' disabled ':'').'maxlength="9"');
                $oFormWorkTab->addElement('text', 'amount['.$i.']', '', $actWork['DRBI_DAUDZUMS'], (($isReadonly)?' disabled ':'').'maxlength="9"'.
                ((isset($actWork['DRBI_DAUDZUMS'])) ? '' : ' style="background:#fda8a5;border: 1px solid #f80f07;" ') .'
                onchange="' . $saveJs . ';return false;" onkeypress = "this.onchange();" onpaste = "this.onchange();"' );
               /* $oFormWorkTab -> addRule('amount['.$i.']', text::get('ERROR_REQUIRED_FIELD'), 'required');
                $oFormWorkTab -> addRule('amount['.$i.']', text::get('ERROR_DOUBLE_VALUE'), 'double', 3);
                // check Amaunt field
                $v1=&$oFormWorkTab->createValidation('amauntValidation_'.$i, array('amount['.$i.']'), 'ifonefilled');
                // ja Cilv�kstundas normat�vs ir defin�ts un > 100000
                $r1=&$oFormWorkTab->createRule(array('amount['.$i.']', 'maxAmaunt'), text::get('ERROR_AMOUNT_VALUE_100000'), 'comparedouble','<');
                // ja koeficent ir defin�ts un < 0
                $r2=&$oFormWorkTab->createRule(array('amount['.$i.']', 'minAmaunt'), text::get('ERROR_AMOUNT_VALUE_100000'), 'comparedouble','>');
                $oFormWorkTab->addRule(array($r1, $r2), 'groupRuleAmaunt_'.$i, 'group', $v1);
                */
                //datums
                $saveJs = "
                  var borderColur = this.style.borderColor;
                  console.log(borderColur.substring(0,3));
                  //if (borderColur.substring(0,3)=='rgb') { 
                  var rowId = document.forms['frmWorkTab".(($tameWork ) ? 'T' : '')."']['id[" .$i. "]'];
                  eval(xmlHttpGetValue('" .$saveLink -> getQuery() ."&plan_date='+this.value+'&saveId='+rowId.value));
                  this.style.borderColor = '#16bb24cc'; 
                  this.parentNode.prepend(sp1);
                  setTimeout(savedField, 3000, this, '#8c8c8c'); 
                  //}
                ";
                //$oFormWorkTab->addElement('text', 'work_type_text['.$i.']', '', $actWork['DRBI_WORK_TYPE_TEXT'], (($isReadonly)?' disabled ':'').'maxlength="50"'.'
                //onchange="' . $saveJs . ';return false;" onkeypress = "this.onchange();" onpaste = "this.onchange();"');
                $oFormWorkTab -> addElement('date', 'plan_date['.$i.']',  '', $actWork['DRBI_PLAN_DATE'], (($isReadonly)?' disabled ':'').'maxlength="50"'.'
                oninput="' . $saveJs . ';return false;" ');
                $oFormWorkTab -> addRule('plan_date['.$i.']', text::get('ERROR_REQUIRED_FIELD'), 'required');
                $oFormWorkTab -> addRule('plan_date['.$i.']', text::get('ERROR_INCORRECT_DATE'),'validatedate');
                // brigade
                $saveJs = "if(this.value != '') { 
                  var rowId = document.forms['frmWorkTab".(($tameWork ) ? 'T' : '')."']['id[" .$i. "]'];
                  eval(xmlHttpGetValue('" .$saveLink -> getQuery() ."&brigade='+this.value+'&saveId='+rowId.value));
                  this.style.borderColor = '#16bb24cc'; 
                  this.parentNode.prepend(sp1);
                  setTimeout(savedField, 3000, this, '#8c8c8c'); 
                }";
                $oFormWorkTab -> addElement('select', 'brigade['.$i.']',  '', $actWork['DRBI_BRIGADE'], (($isReadonly)?' disabled ':''). '
                onchange="' . $saveJs . ';return false;" onkeypress = "this.onchange();" onpaste = "this.onchange();"', '', '', $brigade);
                //$oFormWorkTab -> addElement('select', 'brigade['.$i.']',  '', $actWork['DRBI_BRIGADE'],(($isReadonly)?' disabled ':'').' ', '','', $brigade);
                $oFormWorkTab -> addRule('brigade', text::get('ERROR_REQUIRED_FIELD'), 'required');
            } 
            else {
                $oFormWorkTab->addElement('label', 'amount['.$i.']', '', $actWork['DRBI_DAUDZUMS']); 
                $oFormWorkTab->addElement('label', 'plan_date['.$i.']', '', $actWork['DRBI_PLAN_DATE']);                
                $oFormWorkTab->addElement('label', 'brigade['.$i.']', '', $actWork['DRBI_BRIGADE']); 
            }
            if ($oFormWorkTab -> isFormSubmitted()){
              $oFormWorkTab->setNewValue('amount['.$i.']', $actWork['DRBI_DAUDZUMS']);
              $oFormWorkTab->setNewValue('plan_date['.$i.']', $actWork['DRBI_PLAN_DATE']); 
              $oFormWorkTab->setNewValue('brigade['.$i.']', $actWork['DRBI_BRIGADE']); 
            }
          }          

          
          if(($tameWork || $oFormWorkTab->getValue('tame') == 1) && $isInvestition)     {
            $oFormWorkTab->addElement('label', 'investition_work['.$i.']', '', $investitionPrices[0]['INVC_CENA_DARBAST']);
            $oFormWorkTab->addElement('label', 'total_coast['.$i.']', '', $actWork['DRBI_TOTAL_COAST']);
            if ($oFormWorkTab -> isFormSubmitted()){
              $oFormWorkTab->setNewValue('investition_work['.$i.']',$actWork['INVC_CENA_DARBAST']);
              $oFormWorkTab->setNewValue('total_coast['.$i.']',$actWork['DRBI_TOTAL_COAST']);
             
            }
          }
          

          $totalTime[$d] = number_format($actWork['DRBI_DAY_TOTAL'], 2, '.', ''); 
	        $workApproveDate[$d] =  $actWork['DRBI_WORK_APROVE_DATE'];
          if(($tameWork || $oFormWorkTab->getValue('tame') == 1) && $isInvestition)     {
            $totalCoast[$d] = number_format($totalCoast[$d] + $actWork['DRBI_TOTAL_COAST'], 2, '.', ''); 
          }
          // delete button
          if( empty($actWork['DRBI_APPROVE_DATE']) ) {
            // ajax handler link for delete
            $delLink = new urlQuery();
            $delLink->addPrm(FORM_ID, 'f.akt.s.2');
            $delLink->addPrm(DONT_USE_GLB_TPL, '1');
            $delLink->addPrm('tame', $tameWork ?'1':'0');
            $delLink->addPrm('xmlHttp', '1');

            $delButtonJs = "if(isDelete()) { 
                var rowId = document.forms['frmWorkTab".(($tameWork ) ? 'T' : '')."']['id[" .$i. "]'];
                eval(xmlHttpGetValue('" .$delLink -> getQuery() ."&deleteId='+rowId.value));
              }";
          
              $oFormWorkTab->addElement('button', 'work_del['.$i.']', '', '', 'onclick="' . $delButtonJs . ';return false;" style="background: url(img/ico_del.gif); cursor:hand; border:0px; width: 20px; height: 20px;" title="' . text::get('DELETE') . '"');
              unset($delLink);
          }
          $i++;
          
        }
        $total = 0;
        $total_coast = 0;
        foreach($totalTime as $d => $t) {
          $total += $t;
        }
        $total = number_format($total, 2, '.', '');
        if($tameWork  && $isInvestition)     {
          foreach($totalCoast as $d => $t) {
            $total_coast += $t;
          }        
          $total_coast = number_format($total_coast, 2, '.', '');
        }
      }
      // form buttons
      if (!$isReadonly)
      {
        $oFormWorkTab->addElement('link', 'tameLink', $tameGroup['TITLE'], '#', 'onClick="'.$tameGroup['URL'].'"', '');
        foreach ($calcGroups as $i => $group)
        {
           $oFormWorkTab->addElement('link', 'group['.$i.']', $group['TITLE'], '#', 'onClick="'.$group['URL'].'"', '');
        }
       foreach ($favorit as $i => $group)
        {
           $oFormWorkTab->addElement('link', 'favorit['.$i.']', $group['TITLE'], '#', 'onClick="'.$group['URL'].'"', '');
        }

        $oFormWorkTab -> addElement('submitImg', 'save', text::get('SAVE'), 'img/btn_120_parrekinat.gif', 'width="120" height="20" ');
        //$oFormWorkTab -> addElement('clearImg', 'clear', text::get('CLEAR'), 'img/btn_attirit.gif', 'width="70" height="20"');
      }

      // if form iz submit (save button was pressed)
	  if ($oFormWorkTab -> isFormSubmitted()&& $isNew === false)
	  { 
	/*	if($oFormWorkTab ->isValid()  && $isRowValid)
		{ 
      
		   $r=dbProc::deleteActWorks($actId, $tameWork);
		   if ($r === false)
		   {
		   	$oFormWorkTab->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
		   }
		   else
		   {
		        if(isset($aValidRows) && is_array($aValidRows))
            {
              //print_r($aValidRows);
                  foreach($aValidRows as $i => $row)
                  {
                    
                      //if(!empty($row['edited'])) continue;
                      
                      $r=dbProc::writeActWork($actId,
                                                    $row['kid'],
                                                    $row['chipher'],
                                                    $row['title'],
                                                    $row['standart'],
                                                    $row['amount'],
                                                    $row['total'],
                                                    $row['measure'],
                                                    ($tameWork && !$isInvestition) ? $row['amount_kor']:false,
                                                    $row['edited'],
                                                    ($tameWork) ? false : $row['plan_date'],
                                                    $row['day_total'],
                                                    $row['brigade'],
						                                        $row['work_aprove_date'],
                                                    ($tameWork && $isInvestition ) ? $row['investition_work']: false,
                                                    ($tameWork && $isInvestition ) ? $row['total_coast']: false
                                                    );
                            // if work not inserted in to the DB
                      // delete  task record and show error message
                      if($r === false)
                      {
                        //dbProc::deleteActWorks($actId, $tameWork);
                        $oFormWorkTab->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
                        break;
                      }
                      else
                      {
                           $Id=dbLayer::getInsertId();
                           $oFormWorkTab -> setNewValue('id['.$row['rowNum'].']', $Id);
                           $oFormWorkTab -> setNewValue('total['.$row['rowNum'].']', $row['total']);
                           if($tameWork && $isInvestition ) {
                            $oFormWorkTab -> setNewValue('total_coast['.$row['rowNum'].']', $row['total_coast']);
                           }
                           $oFormWorkTab->addSuccess(text::get('RECORD_WAS_SAVED'));
                           
                           $oLink=new urlQuery();
                           $oLink->addPrm(FORM_ID, 'f.akt.s.1');
                	         $oLink->addPrm('actId', $actId);
                           $oLink->addPrm('tab', TAB_WORKS);
                          if($tameWork==1) {
                                $oLink->addPrm('tametab', TAB_WORKS_T);
                          }
                          $oLink->addPrm('isNew', RequestHandler::getMicroTime());
                          $oLink->addPrm('successMessageTab', text::get('RECORD_WAS_SAVED'));
                          //RequestHandler::makeRedirect($oLink->getQuery());
                        }
                    }
                    // pārsaglabājam summas
                    dbProc::updateWorkSumms($actId, $tameWork);
                    // parrekinat darba summu
                    if(!$tameWork){
                      dbProc::recalculateWorkTime($actId) ;
                    }
                    // saglaba lietotaja darbiibu
                    dbProc::setUserActionDate($userId, ACT_UPDATE);

                    $tameWork = 0;
                }
           }
        } */
      }
       $oFormWorkTab -> makeHtml();
       include('f.akt.s.2.tpl');
    }



}
else
{
    RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}

?>