
<body class="print">
<div class="print_header"><?= text::get('REPORT_CUSTOMER_WORK_TIME_DETALS'); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >
<?
  foreach($searchCr as $i=>$s)
  {
    if($i%3 == 0)
    {
      ?>
        <tr>
      <?
    }
    ?>
      <td align="right" class="print_table_total" width="13%"><?=$s['label'];?>:</td>
      <td align="left" class="print_table_data" width="20%"><?=$s['value'];?></td>
    <?
  }
?>
</table>
<br />
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >
		<tr >
			<td width="5%" class="print_table_header1"><?= text::get('WORKING_PLACE_NUMBER'); ?></td>
			<td width="25%" class="print_table_header1"><?= text::get('RCD_KODS'); ?></td>
            <td width="52%" class="print_table_header1"><?=text::get('USER_SURNAME');?> <?=text::get('USER_NAME');?></td>
            <td width="6%" class="print_table_header1"><?=text::get('BASE_TIME');?></td>
            <td width="6%" class="print_table_header1"><?=text::get('OVER_TIME');?></td>
 	         <td width="6%" class="print_table_header1"><?=text::get('ROUT_TIME');?></td>


 		</tr>

<?
	if (is_array($customer) && count($customer) > 0)
	{
		foreach ($customer as $row)
		{
?>
			<tr  >
			   	<td align="center" class="print_table_data1"><b><?= $row['workPlace']; ?></b></td>
                <td align="center" class="print_table_data1"><b><?= $row['code']; ?></b></td>
                <td align="left" class="print_table_data1"><b><?= $row['name']; ?></b></td>
                <td align="center" class="print_table_data1"><b><?= number_format($row['totalMainTime'],2); ?></b></td>
                <td align="center" class="print_table_data1"><b><?= number_format($row['totalOverTime'],2); ?></b></td>
		<td align="center" class="print_table_data1"><b><?= number_format($row['totalRoutTime'],2); ?></b></td>
			</tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="5">
                    <table class="print_table_portrait1" width="100%" cellpadding="0" cellspacing="0">
                    <?
                       if (is_array($row['act']) && count($row['act']) > 0)
                       {
                            ?>
                               <tr>
                                    <th width="8%" class="print_table_header2"><?= text::get('ACT_NUMBER'); ?></th>
                                    <th width="8%" class="print_table_header2"><?= text::get('ACT_NUM_POSTFIX'); ?></th>
                                    <th width="6%" class="print_table_header2"><?= text::get('STATUS'); ?></th>
                                    <th width="18%" class="print_table_header2"><?= text::get('MAIN_DESIGNATION'); ?></th>
                                    <th width="18%" class="print_table_header2"><?= text::get('SINGLE_SOURCE_OF_FOUNDS'); ?></th>
                                    <th width="18%" class="print_table_header2"><?= text::get('COMMENT'); ?></th>
                                    <th width="6%" class="print_table_header2"><?= text::get('OBJECT_IS_FINISHED'); ?></th>
                                    <th width="6%" class="print_table_header2"><?= text::get('BASE_TIME'); ?></th>
                                    <th width="6%" class="print_table_header2"><?= text::get('OVER_TIME'); ?></th>
				    <th width="6%" class="print_table_header2"><?= text::get('ROUT_TIME'); ?></th>
                               </tr>
                            <?
                    		foreach ($row['act'] as $r)
                    		{
                             ?>
                                <tr>
                                    <td><?= $r['actNumber']; ?></td>
					 <td><?= $r['actPostfix']; ?></td>
                                    <td><?= $r['status']; ?></td>
                                    <td><?= $r['designation']; ?></td>
                                    <td><?= $r['founds']; ?></td>
                                    <td><?= $r['description']; ?></td>
                                    <td><?= (($r['isFinished']==1) ? text::get('YES') : text::get('NO') ); ?></td>
                                    <td><?= $r['mainTime']; ?></td>
                                    <td><?= $r['overTime']; ?></td>
				    <td><?= $r['routTime']; ?></td>
                                </tr>
                             <?
                            }
                       }
                    ?>
                    </table>
                </td>
            </tr>
<?
		}
	}
?>

</table>

</body>