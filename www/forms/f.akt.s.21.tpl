﻿<body class="print">
<h4 align="right"><?= text::toUpper(text::get('PROJECT')); ?></h4>
<div class="print_header"><?= text::get('EXPORT_TITLE').' '.$act['NUMURS']; ?><br />
</div>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >
    <tr>
        <td width="15%" ><b><i><?= text::get('ACT_NUMBER').': '; ?></i></b></td>
        <td width="35%" ><?= ''.$act['NUMURS']; ?></td>
        <td width="15%" ></td>
        <td width="35%" ></td>
    </tr>

    <tr>
        <td width="15%" ><b><i><?= text::get('SINGLE_SOURCE_OF_FOUNDS').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['FOUNDS']; ?></td>
        <td width="15%" ><b><i><?= text::get('SINGL_DV_AREA').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['DV_AREA']; ?></td>
    </tr>

    <tr>
        <td width="15%" ><b><i><?= text::get('YEAR').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.$act['RAKT_GADS']; ?></td>
        <td width="15%" ><b><i><?= text::get('MONTH').': '; ?></i></b></td>
        <td width="35%" ><?= ' '.dtime::getMonthName($act['RAKT_MENESIS']); ?></td>
    </tr>
    <tr>
        <td width="15%" ><b><i><?= text::get('COMMENT').': '; ?></i></b></td>
        <td width="35%" colspan="3" ><?= ' '.$act['RAKT_PIEZIMES']; ?></td>

    </tr>
    <tr>
     <td colspan="4">&nbsp;</td>
  </tr>
  <tr>
     <td colspan="4"><?= $act['WORK_TITLE']; ?></td>
  </tr>
  <tr>
     <td colspan="4">&nbsp;</td>
  </tr>

</table>
<br />
<div class="print_header"><?= text::get('TAB_WORKS'); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_portrait" >

		<tr>
            <td width="30%" class="print_table_header"><?= text::get('WORK_TYPE'); ?></td>
			<td width="30%" class="print_table_header"><?= text::get('WORK_DESCRIPTION'); ?></td>
            <td width="12%" class="print_table_header"><?=text::get('DATE');?> <?=text::get('FROM');?></td>
            <td width="12%" class="print_table_header"><?=text::get('DATE');?> <?=text::get('UNTIL');?></td>
            <td width="12%" class="print_table_header"><?=text::get('USER_NAME');?> <?=text::get('USER_SURNAME');?></td>
            <td width="12%" class="print_table_header"><?=text::get('RCD_KODS');?></td>
            <td width="12%" class="print_table_header"><?= text::get('TRANSPORT_WORK_TIME'); ?></td>
 		</tr>

<?
	if (is_array($aWorkDb) && count($aWorkDb) > 0)
	{
		foreach ($aWorkDb as $i => $work)
		{
?>
			<tr >
                <td align="left" class="print_table_data">&nbsp;<?= $work['WORK_TITLE']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $work['RCFD_WORK_DESCRIPTION']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $work['RCFD_DATUMS_NO']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $work['RCFD_DATUMS_LIDZ']; ?></td>
                <td align="left" class="print_table_data">&nbsp;<?= $work['RCFD_KPRF_VARDS_UZVARDS']; ?></td>
                <td align="center" class="print_table_data">&nbsp;<?= $work['RCFD_KPRF_KODS']; ?></td>
                <td align="right" class="print_table_data">&nbsp;<?= $work['RCFD_STUNDAS']; ?></td>
			</tr>
<?
		}
?>
        <tr>
             <td align="right" class="print_table_total" colspan="6"><?=text::get('TOTAL');?>:</td>
             <td align="center" class="print_table_total"><?= $workTotal; ?></td>
        </tr>
<?
	}
?>
</table>

<br />

<table cellpadding="0" cellspacing="0" class="print_table_portrait" >
    <tr>
        <td><b><i><?= text::get('EXPORT_ACT_AUTHOR').': '.$act['AUTORS']; ?></i></b></td>
    </tr>
    <tr>
        <td><b><i><?= text::get('DATE').': '.date('d.m.Y H:i:s'); ?></i></b></td>
    </tr>
    <tr>
        <td><b><i><?= text::get('EXPORT_FOOTER'); ?></i></b></td>
    </tr>
</table>
</body>