﻿<?
// Created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);
$isEditor = ( dbProc::isUserInRole($userId, ROLE_EDITOR) ||  dbProc::isUserInRole($userId, ROLE_EDITOR_VIEWER));

$isSearch  = reqVar::get('isSearch');
// search act by number
$searchNum  = reqVar::get('searchNum');
// EPLA act
$isEpla  = reqVar::get('isEpla');
// Auto
if(isset($tame))
  $tameMaterial  = $tame;
else 
  $tameMaterial   = reqVar::get('tame_mat');
if(!isset($isInvestition))
  $isInvestition  = reqVar::get('isInvestition');
// Main form with XMLHTTP method load this code:
if (reqVar::get('xmlHttp') == 1)
{
    // if operation is DELETE , delete record from DB
    if (reqVar::get('deleteId')!='')
	  {
	   dbProc::deleteActMaterialRecord(reqVar::get('deleteId'), reqVar::get('tame_mat'));
       ?>
	    rowNode = this.parentNode.parentNode;
		  rowNode.parentNode.removeChild(rowNode);
		<?
    }
    // if operation is SAVE , save daudzumu in DB
    if (reqVar::get('saveId')!='' )
	  {
	    if (reqVar::get('price')!='')
      {
        dbProc::updateMaterialPrice(reqVar::get('saveId'), reqVar::get('price'));
      }
      if (reqVar::get('total_mat')!='')
      {
        dbProc::updateMaterialTotalPrice(reqVar::get('saveId'), reqVar::get('total_mat'));
      }
    }
    exit();
}


// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
    if($actId !== false )
    {
      $oLink=new urlQuery();
      $oLink->addPrm(FORM_ID, 'f.akt.s.1');
      $oLink->addPrm('actId', $actId);
      $oLink->addPrm('isSearch', $isSearch);
      $oLink->addPrm('searchNum', $searchNum);
      $oLink->addPrm('tab', TAB_MATERIAL);
      if($tameMaterial) {
      	$oLink->addPrm('tametab', TAB_MATERIAL_T);
      }
      $oLink->addPrm('isEpla', $isEpla);
      $oFormMaterialTab = new Form('frmMaterialTab'.(($tameMaterial==1) ? 'T' : ''), 'post', $oLink->getQuery() . '#tab');
      unset($oLink);

      // if operation is success, show success message and redirect top frame
      if (reqVar::get('successMessageTab') && $isNew === false)
      {
      	$oFormMaterialTab->addSuccess(reqVar::get('successMessageTab'));
      }

      // check is defined auto material upload
      $isAuto = dbProc::checkAutoUploadAllow($actId);
      
      $isReadonly   = true;
      // Ir pieejams mainīšanai, ja  AKTS:LIETOTĀJS = ’Tekošais lietotājs’ un lietotāja loma ir ‘Ievadītais’ un AKTS:STATUS = ‘Izveide’ vai ‘Atgriezts’ vai ‘Tāme’.
      // un aktām nav uzlikta atzīme par automatīsku materiāla ielādi
      // vai lietotājs ir administrātops
      if(
          (
            ($isAdmin || ($act['RAKT_RLTT_ID'] == $userId && $isEditor && !$isAuto)) &&
            ($status == STAT_INSERT || $status == STAT_RETURN || $status == STAT_ESTIMATE)
          )
        )
      {
        $isReadonly   = false;
      }

      $canChangeMaterialSumm = false;
      /*Loma "RCDIS administrators"
      Akta veids = Investīcijas
      Akta statuss = Ievads, Saskaņošana
      ļaut labot ielādēto materiālu Cenu un Summu*/
      if($isAdmin && $isInvestition && ($status == STAT_INSERT || status == STAT_ACCEPT) && $tameMaterial!=1)
      {
        $canChangeMaterialSumm = true;
      }
      
      //Popup saites sagatave
      $oPopLink=new urlQuery;
      $oPopLink->addPrm(FORM_ID, 'f.akt.s.8');
      $oPopLink->addPrm('actId',$actId);
      $oPopLink->addPrm('catalog',KL_MATERIALS);
      $oPopLink->addPrm('isSearch', $isSearch);
      $oPopLink->addPrm('searchNum', $searchNum);
      $oPopLink->addPrm('isEpla', $isEpla);
      $oPopLink->addPrm('tame', $tameMaterial ?'1':'0');

      // get favorit  
      $favorit=array();     
      $i = 0;  
      if($tameMaterial== false) {
        $favorit[$i]['TITLE'] = text::get('TAB_ESTIMATE');  
        $favorit[$i]['URL'] = RequestHandler::popupHref($oPopLink -> getQuery().'&estimate=1', false, 700, 450);
        $i++;
      } 
        
      $krfk = dbProc::getKrfkInfo(KRFK_MATERIAL_ID);
      if($krfk !== false)
      {

        $res=dbProc::getFavoritTypeDefinitionList($userId, $krfk['KRFK_VERTIBA']);
        if (is_array($res))
        {
         	foreach ($res as $row)
         	{
              $favorit[$i]['TITLE'] = $row['LTFV_NOSAUKUMS'];
              $favorit[$i]['URL'] = RequestHandler::popupHref($oPopLink -> getQuery().'&favoritId='.$row['LTFV_ID']);
              $i++;
          }
        }
      }
      unset($oPopLink);
      unset($res);

      //Popup saites sagatave
      $oPopLink=new urlQuery;
      $oPopLink->addPrm(FORM_ID, 'f.akt.s.11');
      $oPopLink->addPrm('actId',$actId);
      $oPopLink->addPrm('catalog',KL_MATERIALS);
      $oPopLink->addPrm('isSearch', $isSearch);
      $oPopLink->addPrm('searchNum', $searchNum);
      $oPopLink->addPrm('isEpla', $isEpla);
      $oPopLink->addPrm('all',1);
      $oPopLink->addPrm('tame', $tameMaterial ?'1':'0');
     
      $favorit[$i]['TITLE'] = text::get('SEARCH');   ;
      $favorit[$i]['URL'] = RequestHandler::popupHref($oPopLink -> getQuery());
      unset($oPopLink);

      $aMaterials = array();
      if ($oFormMaterialTab -> isFormSubmitted() && $isNew === false)
      {
          $isRowValid = true;
          $aCodes =  $oFormMaterialTab->getValue('code');
          if(is_array($aCodes) )
          {
            foreach ($aCodes as $i => $val)
            {
                 $cena = $oFormMaterialTab->getValue('price['.$i.']');
                 $cena_origin = $oFormMaterialTab->getValue('price_origin['.$i.']');
                 $total_mat = $oFormMaterialTab->getValue('total_mat['.$i.']');
                 $total_mat_origin = $oFormMaterialTab->getValue('total_mat_origin['.$i.']');

                 $aMaterials[$i]['MATR_ID'] = $oFormMaterialTab->getValue('id['.$i.']');
                 $aMaterials[$i]['MATR_KODS'] = $oFormMaterialTab->getValue('code['.$i.']');
                 $aMaterials[$i]['MATR_NOSAUKUMS'] = $oFormMaterialTab->getValue('title['.$i.']');
                 $aMaterials[$i]['MATR_MERVIENIBA'] = $oFormMaterialTab->getValue('unit['.$i.']');
                 $aMaterials[$i]['MATR_CENA'] = $cena;   
                 $aMaterials[$i]['MATR_TRANSACTION_ID'] = $oFormMaterialTab->getValue('transaction_id['.$i.']');
                 $aMaterials[$i]['MATR_IS_MANUAL'] = $oFormMaterialTab->getValue('is_manual['.$i.']');
                 
                 if($tameMaterial && !$isInvestition ){
                    $aMaterials[$i]['MATR_DAUDZUMS_KOR'] = $oFormMaterialTab->getValue('amount_kor_mat['.$i.']');

                    // validate work progress value
                    if (empty($aMaterials[$i]['MATR_DAUDZUMS_KOR'])) {
                        // assign error message
                        $oFormMaterialTab->addErrorPerElem('amount_kor_mat[' . $i . ']', text::get('ERROR_REQUIRED_FIELD'));
                        $isRowValid = false;
                    } // validate work progress value
                    elseif (!is_numeric($aMaterials[$i]['MATR_DAUDZUMS_KOR'])) {
                        // assign error message
                        $oFormMaterialTab->addErrorPerElem('amount_kor_mat[' . $i . ']', text::get('ERROR_DOUBLE_VALUE'));
                        $isRowValid = false;
                    } // validate work progress value
                    elseif ($aMaterials[$i]['MATR_DAUDZUMS_KOR'] >= 100000) {
                        // assign error message
                        $oFormMaterialTab->addErrorPerElem('amount_kor_mat[' . $i . ']', text::get('ERROR_AMOUNT_VALUE_100000'));
                        $isRowValid = false;
                    } else {
                        $aMaterials[$i]['MATR_CENA_KOPA'] = number_format(($aMaterials[$i]['MATR_CENA'] * $aMaterials[$i]['MATR_DAUDZUMS_KOR']), 2, '.', '');
                    }
                    $aMaterials[$i]['MATR_DAUDZUMS'] = $oFormMaterialTab->getValue('amount_km['.$i.']');
                } else { 
                    $aMaterials[$i]['MATR_DAUDZUMS'] = $oFormMaterialTab->getValue('amount_mat['.$i.']');
                    if (empty($aMaterials[$i]['MATR_DAUDZUMS']))
                    {
                        // assign error message
                        $oFormMaterialTab->addErrorPerElem('amount_mat['.$i.']', text::get('ERROR_REQUIRED_FIELD'));
                        $isRowValid = false;
                    }
                    // validate work progress value
                    elseif (!is_numeric($aMaterials[$i]['MATR_DAUDZUMS']))
                    {
                        // assign error message
                        $oFormMaterialTab->addErrorPerElem('amount_mat['.$i.']', text::get('ERROR_DOUBLE_VALUE'));
                        $isRowValid = false;
                    }
                    // validate work progress value
                    elseif ($aMaterials[$i]['MATR_DAUDZUMS'] >= 100000)
                    {
                        // assign error message
                        $oFormMaterialTab->addErrorPerElem('amount_mat['.$i.']', text::get('ERROR_AMOUNT_VALUE_100000'));
                        $isRowValid = false;
                    }
                    else
                    {
                        $aMaterials[$i]['MATR_CENA_KOPA'] = number_format(($aMaterials[$i]['MATR_CENA']*$aMaterials[$i]['MATR_DAUDZUMS']), 2, '.', '');
                    }
                }
                //echo (($isInvestition)?1:0) .'-'.$cena . '-' . $cena_origin . '-' .$total_mat . '-'.$total_mat_origin . '-'. $aMaterials[$i]['MATR_CENA_KOPA'] .'<br>';
                if(    $isInvestition 
                    && ( ($cena != $cena_origin ) || ($total_mat != $total_mat_origin) || $aMaterials[$i]['MATR_IS_MANUAL'] == 1 ) 
                 ) {                   
                    $aMaterials[$i]['MATR_CENA_KOPA'] = $total_mat;
                    $aMaterials[$i]['MATR_IS_MANUAL'] = 1;
                }
                
                // prepare database record data
                $aMaterialValidRows[] = array(
                  'code' => $aMaterials[$i]['MATR_KODS'],
                  'title' => $aMaterials[$i]['MATR_NOSAUKUMS'],
                  'unit' => $aMaterials[$i]['MATR_MERVIENIBA'],
                  'price' => $aMaterials[$i]['MATR_CENA'],
                  'amount_mat' => $aMaterials[$i]['MATR_DAUDZUMS'],
                  'amount_kor_mat' => ($oFormMaterialTab->getValue('tame_mat') == 1 && !$isInvestition) ? $aMaterials[$i]['MATR_DAUDZUMS_KOR'] :'',
                  'total_mat' => $aMaterials[$i]['MATR_CENA_KOPA'],
                  'transaction_id' => $aMaterials[$i]['MATR_TRANSACTION_ID'],
                  'is_manual' => $aMaterials[$i]['MATR_IS_MANUAL'],
                  'rowNum' => $i
                );
             }
           }
      }
      else
      {
        //Ja eksistee akta materiāli, tad nolasaam taas darbus no datu baazes
        $aMaterialDb=dbProc::getActMaterialList($actId,  $tameMaterial, $isInvestition );
        
        foreach ($aMaterialDb as $i => $material)
        {
           $aMaterials[$i]['MATR_ID'] = $material['MATR_ID'];
           $aMaterials[$i]['MATR_KODS'] = $material['MATR_KODS'];
           $aMaterials[$i]['MATR_NOSAUKUMS'] = $material['MATR_NOSAUKUMS'];
           $aMaterials[$i]['MATR_MERVIENIBA'] = $material['MATR_MERVIENIBA'];
           $aMaterials[$i]['MATR_CENA'] = $material['MATR_CENA'];
           $aMaterials[$i]['MATR_DAUDZUMS'] = $material['MATR_DAUDZUMS'];
           $aMaterials[$i]['MATR_CENA_KOPA'] = $material['MATR_CENA_KOPA'];
           $aMaterials[$i]['MATR_TRANSACTION_ID'] = $material['MATR_TRANSACTION_ID'];
           $aMaterials[$i]['MATR_IS_MANUAL'] = $material['MATR_IS_MANUAL'];
           if($tameMaterial && !$isInvestition){
                $aMaterials[$i]['MATR_DAUDZUMS_KOR'] = $material['MATR_DAUDZUMS_KOR'];
            }
        }
      }
      $oFormMaterialTab -> addElement('hidden', 'isSearch', '', $isSearch);
      $oFormMaterialTab -> addElement('hidden', 'searchNum', '', $searchNum);
      $oFormMaterialTab->addElement('hidden','actId','',$actId);
      $oFormMaterialTab->addElement('hidden','maxAmaunt','','100000.00');
      $oFormMaterialTab->addElement('hidden','minAmaunt','','0.00');
      $oFormMaterialTab->addElement('hidden','tame_mat','',($tameMaterial) ? 1 : 0);
      
      // form elements
      //print_r($aMaterials);
      $totalPrice = 0;
      foreach ($aMaterials as $i => $actMaterial)
      {
        // ajax handler link for delete
        $saveLink = new urlQuery();
        $saveLink->addPrm(FORM_ID, 'f.akt.s.2');
        $saveLink->addPrm(DONT_USE_GLB_TPL, '1');
        $saveLink->addPrm('xmlHttp', '1');

        $oFormMaterialTab->addElement('hidden', 'id['.$i.']', '', $actMaterial['MATR_ID']);
        $oFormMaterialTab->addElement('label', 'code['.$i.']', '', $actMaterial['MATR_KODS']);
        $oFormMaterialTab->addElement('label', 'title['.$i.']', '', $actMaterial['MATR_NOSAUKUMS']);
        $oFormMaterialTab->addElement('label', 'unit['.$i.']', '', $actMaterial['MATR_MERVIENIBA']);
        $oFormMaterialTab->addElement('label', 'price_origin['.$i.']', '', $actMaterial['MATR_CENA']);
        $oFormMaterialTab->addElement('label', 'total_mat_origin['.$i.']', '', $actMaterial['MATR_CENA_KOPA']);
        $oFormMaterialTab->addElement('label', 'is_manual['.$i.']', '', $actMaterial['MATR_IS_MANUAL']);
        if($canChangeMaterialSumm ) {
          // save price         
          $oFormMaterialTab->addElement('text', 'price['.$i.']', '', $actMaterial['MATR_CENA'], 'maxlength="9"');
          $oFormMaterialTab->addRule('price[' . $i . ']', text::get('ERROR_REQUIRED_FIELD'), 'required');
          $oFormMaterialTab->addRule('price[' . $i . ']', text::get('ERROR_DOUBLE_VALUE'), 'double', 3);
        }else {
             $oFormMaterialTab->addElement('label', 'price['.$i.']', '', $actMaterial['MATR_CENA']);
        }
        
        $oFormMaterialTab->addElement('hidden', 'transaction_id['.$i.']', '', $actMaterial['MATR_TRANSACTION_ID']);

        
        if(($tameMaterial || $oFormMaterialTab->getValue('tame_mat') == 1) && !$isInvestition ) {

            $oFormMaterialTab->addElement('label', 'amount_km['.$i.']', '', $actMaterial['MATR_DAUDZUMS']);  
            $oFormMaterialTab->setNewValue('amount_km['.$i.']', number_format($actMaterial['MATR_DAUDZUMS'], 3, '.', '')); 
            
            $oFormMaterialTab->addElement('text', 'amount_kor_mat['.$i.']', '', $actMaterial['MATR_DAUDZUMS_KOR'], (($isReadonly)?' disabled ':'').'maxlength="9"');

            $oFormMaterialTab->addRule('amount_kor_mat[' . $i . ']', text::get('ERROR_REQUIRED_FIELD'), 'required');
            $oFormMaterialTab->addRule('amount_kor_mat[' . $i . ']', text::get('ERROR_DOUBLE_VALUE'), 'double', 3);
            // check Amaunt field
            $v1 =& $oFormMaterialTab->createValidation('amauntKorValidation_' . $i, array('amount_kor_mat[' . $i . ']'), 'ifonefilled');
            // ja Amount ir definēts un > 100000
            $r1 =& $oFormMaterialTab->createRule(array('amount_kor_mat[' . $i . ']', 'maxAmaunt'), text::get('ERROR_AMOUNT_VALUE_100000'), 'comparedouble', '<');
            // ja Amount ir definēts un < 0
            $r2 =& $oFormMaterialTab->createRule(array('amount_kor_mat[' . $i . ']', 'minAmaunt'), text::get('ERROR_AMOUNT_VALUE_100000'), 'comparedouble', '>');
            $oFormMaterialTab->addRule(array($r1, $r2), 'groupRuleAmauntKor_' . $i, 'group', $v1);
        } else {
            $oFormMaterialTab->addElement('text', 'amount_mat['.$i.']', '', $actMaterial['MATR_DAUDZUMS'], (($isReadonly)?' disabled ':'').'maxlength="9"'.
            ((isset($actMaterial['MATR_DAUDZUMS'])) ? '' : ' style="background:#fda8a5;border: 1px solid #f80f07;" '));
            $oFormMaterialTab -> addRule('amount_mat['.$i.']', text::get('ERROR_REQUIRED_FIELD'), 'required');
            $oFormMaterialTab -> addRule('amount_mat['.$i.']', text::get('ERROR_DOUBLE_VALUE'), 'double', 3);
            // check Amaunt field
            $v1=&$oFormMaterialTab->createValidation('amaunt1Validation_'.$i, array('amount_mat['.$i.']'), 'ifonefilled');
            // ja Amount ir definēts un > 100000
            $r1=&$oFormMaterialTab->createRule(array('amount_mat['.$i.']', 'maxAmaunt'), text::get('ERROR_AMOUNT_VALUE_100000'), 'comparedouble','<');
            // ja Amount ir definēts un < 0
            $r2=&$oFormMaterialTab->createRule(array('amount_mat['.$i.']', 'minAmaunt'), text::get('ERROR_AMOUNT_VALUE_100000'), 'comparedouble','>');
            $oFormMaterialTab->addRule(array($r1, $r2), 'groupRuleAmaunt1_'.$i, 'group', $v1);
        }
        if($canChangeMaterialSumm ) {
          // save total price
          $oFormMaterialTab->addElement('text', 'total_mat['.$i.']', '', $actMaterial['MATR_CENA_KOPA'], 'maxlength="9"' );
          $oFormMaterialTab->addRule('total_mat[' . $i . ']', text::get('ERROR_REQUIRED_FIELD'), 'required');
          $oFormMaterialTab -> addRule('total_mat['.$i.']', text::get('ERROR_DOUBLE_VALUE'), 'double', 3);
        } else {
          $oFormMaterialTab->addElement('label', 'total_mat['.$i.']', '', $actMaterial['MATR_CENA_KOPA'], ' disabled maxlength="13"');
        }
        unset($saveLink);
        $totalPrice += $actMaterial['MATR_CENA_KOPA'];
        
        // delete button        
        // ajax handler link for delete
        $delLink = new urlQuery();
        $delLink->addPrm(FORM_ID, 'f.akt.s.3');
        $delLink->addPrm(DONT_USE_GLB_TPL, '1');
        $delLink->addPrm('xmlHttp', '1');
        $delLink->addPrm('tame_mat', $tameMaterial ?'1':'0');
          
        $delButtonJs = "if(isDelete()) {
                var rowId = document.forms['frmMaterialTab".(($tameMaterial ) ? 'T' : '')."']['id[" .$i. "]'];
                eval(xmlHttpGetValue('" .$delLink -> getQuery() ."&deleteId='+rowId.value));
              }";
        $oFormMaterialTab->addElement('button', 'mat_del['.$i.']', '', '', 'onclick="' . $delButtonJs . ';return false;" style="background: url(img/ico_del.gif); cursor:hand; border:0px; width: 20px; height: 20px;" title="' . text::get('DELETE') . '"');
        unset($delLink);
        
      }
      $totalPrice = number_format($totalPrice, 2, '.', '');

      // form buttons
      if (!$isReadonly)
      {
        foreach ($favorit as $i => $group)
        {
           $oFormMaterialTab->addElement('link', 'group['.$i.']', $group['TITLE'], '#', 'onClick="'.$group['URL'].'"', '');
        }
        $oFormMaterialTab -> addElement('submitImg', 'save', text::get('SAVE'), 'img/btn_save_material.gif', 'width="120" height="20" ');
        
      }

      // if form iz submit (save button was pressed)
	  if ($oFormMaterialTab -> isFormSubmitted()&& $isNew === false)
	  {
		if($oFormMaterialTab ->isValid()  && $isRowValid)
		{
      
		   $r=dbProc::deleteActMaterials($actId, $tameMaterial);
		   if ($r === false)
		   {
		   	$oFormMaterialTab->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
		   }
		   else
		   {
		        if(is_array($aMaterialValidRows))
            {
		          //print_r($aMaterialValidRows);

        	    foreach($aMaterialValidRows as $i => $row)
        			{
                      $r=dbProc::writeActMaterial($actId,
                                                $row['code'],
                                                $row['title'],
                                                $row['unit'],
                                                $row['price'],
                                                $row['amount_mat'],
                                                $row['total_mat'],
                                                $row['transaction_id'],
                                               ($tameMaterial) ? $row['amount_kor_mat']:false,
                                                $row['is_manual']
                                                );
                         		// if work not inserted in to the DB
                      // delete  task record and show error message
                      if($r === false)
                      {
                          dbProc::deleteActMaterials($actId, $tameMaterial);
                          $oFormMaterialTab->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
                          break;
                      }
                			else
                			{
                          $Id=dbLayer::getInsertId();
                          $oFormMaterialTab -> setNewValue('id['.$row['rowNum'].']', $Id);
                          $oFormMaterialTab -> setNewValue('total_mat['.$row['rowNum'].']', $row['total_mat']);
                          //$oFormMaterialTab -> setNewValue('total_mat_origin['.$row['rowNum'].']', $row['total_mat']);
                          //$oFormMaterialTab -> setNewValue('price_origin['.$row['rowNum'].']', $row['price']);
                          $oFormMaterialTab->addSuccess(text::get('RECORD_WAS_SAVED'));
                			}
              }

              $oLink=new urlQuery();
              $oLink->addPrm(FORM_ID, 'f.akt.s.1');
              $oLink->addPrm('actId', $actId);
              $oLink->addPrm('tab', TAB_MATERIAL);
              if($tameMaterial) {
                    $oLink->addPrm('tametab', TAB_MATERIAL_T);
              }
          
              $oLink->addPrm('isNew', RequestHandler::getMicroTime());
              $oLink->addPrm('tame_mat', $tameMaterial ?'1':'0');
              $oLink->addPrm('successMessageTab', text::get('RECORD_WAS_SAVED'));
              //RequestHandler::makeRedirect($oLink->getQuery());

              // saglaba lietotaja darbiibu
              dbProc::setUserActionDate($userId, ACT_UPDATE);
              $tameMaterial = 0;
            }
        }
      }
    }
    $oFormMaterialTab -> makeHtml();
    include('f.akt.s.3.tpl');
  }
}
else
{
    RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}

?>