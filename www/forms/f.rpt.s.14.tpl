﻿<body class="frame_2">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><h1><?= text::toUpper(text::get('REPORT_WORK_WITH_PLAN')); ?></h1></td>
		<td align="right">
            <img src="img/ico_print.gif" alt="" width="16" height="16" border="0">&nbsp;
            <a href="<?= $printUrl; ?>" target="new"><?=text::get('PRINT');?></a>&nbsp;&nbsp;
            <!--<img src="img/ico_excel.gif" alt="" width="16" height="16">&nbsp;
            <a href="<?= $exportUrl; ?>" ><?=text::get('EXPORT');?></a>&nbsp;-->
        </td>
	</tr>
</table>

<table cellpadding="5" cellspacing="1" border="0" width="100%" >
<?
    $cilvekstundasRegion = 0;
    $materIzmaksasRegion = 0;

    if (is_array($area) && count($area) > 0)
	{
		foreach ($area as $row)
		{
          $cilvekstundasSection = 0;
          $materIzmaksasSection = 0;
          if (is_array($row['ed']) && count($row['ed']) > 0)
          {
            foreach ($row['ed'] as $rrS)
            {
?>
            <tr class="table_head_2">
                <td colspan="2" align="left"><b><?=text::get('ED_IECIKNIS');?>:</b></td>
                <td colspan="2" align="left"><b><?= $rrS['name']; ?></b></td>
                <td colspan="6" align="left"><b><?= $rrS['code']; ?></b></td>
            </tr>
            <?

                $cilvekstundasArea = 0;
                $materIzmaksasArea = 0;
                if (is_array($rrS['mms']) && count($rrS['mms']) > 0)
                {
                   foreach ($rrS['mms'] as $rr)
                   {
                    ?>
                    <tr class="table_cell_3">
                        <td colspan="2" align="left"><b><?=text::get('SINGLE_MMS');?>:</b></td>
                        <td colspan="8" align="left"><b><?= $rr['code']; ?></b></td>
                    </tr>
                    <tr class="table_head_2">
            			<td width="10%"><?= text::get('ACT_NUMBER'); ?></td>
                        <td width="8%"><?=text::get('BASE_TIME');?></td>
                        <td width="10%"><?= text::get('REPORT_MATERIAL_PRICE_TOTAL'); ?></td>
                        <td width="8%"><?=text::get('TRANSPORT_KM');?><br /></td>
                        <td width="8%"><?=text::get('TRANSPORT_AVTO_TIME');?></td>
                        <td width="8%"><?= text::get('TRANSPORT_WORK_TIME'); ?></td>
                        <td width="8%"><?=text::get('OVER_TIME');?></td>
                        <td width="15%"><?= text::get('MAIN_DESIGNATION'); ?></td>
                        <td width="10%"><?= text::get('OBJECT_IS_FINISHED'); ?></td>
                        <td width="15%"><?= text::get('COMMENT'); ?></td>
                    </tr>
                    <?
                        if (is_array($rr['act']) && count($rr['act']) > 0)
                        {
                           foreach ($rr['act'] as $r)
                           {
                             ?>
                           <tr class="table_cell_3" >
              			      <td align="center"><?= $r['actNumber']; ?></td>
                              <td align="center"><?= $r['mainTime']; ?></td>
                              <td align="center"><?= $r['cena']; ?></td>
                              <td align="center"><?= $r['km']; ?></td>
                              <td align="center"><?= $r['stundas']; ?></td>
                              <td align="center"><?= $r['darbaStundas']; ?></td>
                              <td align="center"><?= $r['overTime']; ?></td>
                              <td align="left"><?= $r['designation']; ?></td>
                              <td align="left"><?= (($r['isFinished'] == 1)?text::get('YES'):text::get('NO')); ?></td>
                              <td align="left"><?= $r['comment']; ?></td>
                          </tr>
                             <?
                           }
                        }
                        ?>
                          <tr class="table_cell_3">
                            <td align="left"><b><?=text::get('TOTAL_EXECUTION');?>:</b></td>
                            <td align="center"><?=number_format($rr['totalMainTime'],2,'.','');?></td>
                            <td align="center"><?=number_format($rr['totalCena'],2,'.','');?></td>
                            <td align="center"><?=number_format($rr['totalKm'],2,'.','');?></td>
                            <td align="center"><?=number_format($rr['totalStundas'],2,'.','');?></td>
                            <td align="center"><?=number_format($rr['totalDarbaStundas'],2,'.','');?></td>
                            <td align="center"><?=number_format($rr['totalOverTime'],2,'.','');?></td>
                            <td colspan="3">&nbsp;</td>
                         </tr>
                         <tr class="table_cell_3">
                            <td align="left"><b><?=text::get('PLAN');?>:</b></td>
                            <td align="center"><?=number_format($rr['cilvekstundas'],2,'.','');?></td>
                            <td align="center"><?=number_format($rr['materIzmaksas'],2,'.','');?></td>
                            <td colspan="4" align="left"><b><?= $rr['code']; ?></b></td>
                            <td colspan="3">&nbsp;</td>
                         </tr>
                         <tr class="table_cell_3">
                            <td align="left"><b><?=text::get('DIFFERENCE');?>:</b></td>
                            <td align="center"><?=number_format(($rr['cilvekstundas'] - $rr['totalMainTime']),2,'.','');?></td>
                            <td align="center"><?=number_format(($rr['materIzmaksas'] - $rr['totalCena']),2,'.','');?></td>
                            <td colspan="7">&nbsp;</td>
                         </tr>

                        <?
                        $cilvekstundasArea = $cilvekstundasArea + $rr['cilvekstundas'];
                        $materIzmaksasArea = $materIzmaksasArea + $rr['materIzmaksas'];
                    }?>

            <?}?>
                        <tr class="table_head_2">
                            <td align="left"><b><?=text::get('TOTAL_EXECUTION');?>:</b></td>
                            <td align="center"><?=number_format($rrS['totalMainTime'],2,'.','');?></td>
                            <td align="center"><?=number_format($rrS['totalCena'],2,'.','');?></td>
                            <td align="center"><?=number_format($rrS['totalKm'],2,'.','');?></td>
                            <td align="center"><?=number_format($rrS['totalStundas'],2,'.','');?></td>
                            <td align="center"><?=number_format($rrS['totalDarbaStundas'],2,'.','');?></td>
                            <td align="center"><?=number_format($rrS['totalOverTime'],2,'.','');?></td>
                            <td colspan="3" align="left"><b><?= $rrS['name']; ?></b></td>
                         </tr>
                         <tr class="table_head_2">
                            <td align="left"><b><?=text::get('PLAN');?>:</b></td>
                            <td align="center"><?=number_format($cilvekstundasArea,2,'.','');?></td>
                            <td align="center"><?=number_format($materIzmaksasArea,2,'.','');?></td>
                            <td colspan="7">&nbsp;</td>
                         </tr>
                         <tr class="table_head_2">
                            <td align="left"><b><?=text::get('DIFFERENCE');?>:</b></td>
                            <td align="center"><?=number_format(($cilvekstundasArea - $rrS['totalMainTime']),2,'.','');?></td>
                            <td align="center"><?=number_format(($materIzmaksasArea - $rrS['totalCena']),2,'.','');?></td>
                            <td colspan="7">&nbsp;</td>
                         </tr>
                         <tr><td colspan="10">&nbsp;</td></tr>

        <?
        $cilvekstundasSection = $cilvekstundasSection + $cilvekstundasArea;
        $materIzmaksasSection = $materIzmaksasSection + $materIzmaksasArea;
		}
        }
        ?>
          <tr class="table_head_2">
          <td align="left"><b><?=text::get('TOTAL_EXECUTION');?>:</b></td>
          <td align="center"><?=number_format($row['totalMainTime'],2,'.','');?></td>
          <td align="center"><?=number_format($row['totalCena'],2,'.','');?></td>
          <td align="center"><?=number_format($row['totalKm'],2,'.','');?></td>
          <td align="center"><?=number_format($row['totalStundas'],2,'.','');?></td>
          <td align="center"><?=number_format($row['totalDarbaStundas'],2,'.','');?></td>
          <td align="center"><?=number_format($row['totalOverTime'],2,'.','');?></td>
          <td colspan="3" align="left"><b><?= $row['section']; ?></b></td>
       </tr>
       <tr class="table_head_2">
          <td align="left"><b><?=text::get('PLAN');?>:</b></td>
          <td align="center"><?=number_format($cilvekstundasSection,2,'.','');?></td>
          <td align="center"><?=number_format($materIzmaksasSection,2,'.','');?></td>
          <td colspan="7">&nbsp;</td>
       </tr>
       <tr class="table_head_2">
          <td align="left"><b><?=text::get('DIFFERENCE');?>:</b></td>
          <td align="center"><?=number_format(($cilvekstundasSection - $row['totalMainTime']),2,'.','');?></td>
          <td align="center"><?=number_format(($materIzmaksasSection - $row['totalCena']),2,'.','');?></td>
          <td colspan="7">&nbsp;</td>
       </tr>
       <tr><td colspan="10">&nbsp;</td></tr>
        <?

        $cilvekstundasRegion = $cilvekstundasRegion + $cilvekstundasSection;
        $materIzmaksasRegion = $materIzmaksasRegion + $materIzmaksasSection;
        }
        ?>
          <tr>
              <td align="left"><b><?=text::get('TOTAL_EXECUTION');?>:</b></td>
              <td align="center"><?=$pamatstundas;?></td>
              <td align="center"><?=$summa;?></td>
              <td align="center"><?=$km;?></td>
              <td align="center"><?=$stundas;?></td>
              <td align="center"><?=$darbaStundas;?></td>
              <td align="center"><?=$virsstundas;?></td>
              <td colspan="3" align="left"><b><?=text::get('TOTAL');?></b></td>
           </tr>
           <tr>
              <td align="left"><b><?=text::get('PLAN');?>:</b></td>
              <td align="center"><?=number_format($cilvekstundasRegion, 2,'.','');?></td>
              <td align="center"><?=number_format($materIzmaksasRegion, 2,'.','');?></td>
              <td colspan="7">&nbsp;</td>
           </tr>
           <tr>
              <td align="left"><b><?=text::get('DIFFERENCE');?>:</b></td>
              <td align="center"><?=number_format(($cilvekstundasRegion - $pamatstundas),2,'.','');?></td>
              <td align="center"><?=number_format(($materIzmaksasRegion - $summa),2,'.','');?></td>
              <td colspan="7">&nbsp;</td>
           </tr>
<?
	}
?>

</table>

</body>