<?
// get info about system
$systemInfo = dbProc::getSystemInfo();
if(count($systemInfo)>0)
{
  	$info = $systemInfo[0];
}

$year = reqVar::get('year');

if($year === false)
{
   $year =  dtime::getSmartCurrentYear();
//echo $year;
}
else
{
     session::set('CURRENT_YEAR', $year);
     //$_SESSION[CURRENT_YEAR] = $year;
     $oLink=new urlQuery();
     $oLink->addPrm(FORM_ID, 'f.lgn.s.1');
     RequestHandler::makeRedirect($oLink->getQuery());
}

$linkYear2010 = 2010;
$oLink=new urlQuery();
$oLink->addPrm(FORM_ID, 'f.lgn.s.1');
$oLink->addPrm('year', 2010);
$year2010Url = $oLink->getQuery();
unset($oLink);

$linkYear2011 = 2011;
$oLink=new urlQuery();
$oLink->addPrm(FORM_ID, 'f.lgn.s.1');
$oLink->addPrm('year', 2011);
$year2011Url = $oLink->getQuery();
unset($oLink);

$linkYear2012 = dtime::getSmartCurrentYear();
$linkYear2012Label = '2012,...';
$oLink=new urlQuery();
$oLink->addPrm(FORM_ID, 'f.lgn.s.1');
$oLink->addPrm('year', dtime::getSmartCurrentYear());
$year2012Url = $oLink->getQuery();
unset($oLink);


$oForm = new Form('frmLogin','post','/');
$oForm -> addElement('text', 'login', text::get('LOGIN'), RequestHandler::getLastAccessLogin(), 'maxlength="30"',false, false,false,true);
$oForm -> addRule('login', text::get('ERROR_REQUIRED_FIELD'), 'required');
$oForm -> addElement('password', 'password', text::get('PASSWORD'), null, 'maxlength="30"',false, false,false,true);
$oForm -> addRule('password', text::get('ERROR_REQUIRED_FIELD'), 'required');

$oForm -> addElement('literal', 'warningIcon', '', isset($info['SNFO_WARNING'])? '<img src="img/ico_attention.gif" alt="" width="41" height="37" class="block">':'');
$oForm -> addElement('literal', 'warning', '', isset($info['SNFO_WARNING'])? nl2br($info['SNFO_WARNING']):'');
$oForm -> addElement('literal', 'yearVersion2010', $linkYear2010.'.'.text::get('LOGIN_YEAR_VERSION'), '');  $oForm -> addElement('literal', 'yearVersion2011', $linkYear2011.'.'.text::get('LOGIN_YEAR_VERSION'), '');  $oForm -> addElement('literal', 'yearVersion2012', $linkYear2012Label.'.'.text::get('LOGIN_YEAR_VERSION'), '');
$oForm -> addElement('literal', 'instruction', text::get('LOGIN_INSTRUCTION'), '');
$oForm -> addElement('literal', 'problem', text::get('LOGIN_PROBLEM'), '');
$oForm -> addElement('literal', 'curYearVersion', (($year > 2011 ) ? '' : $year.'.'.text::get('LOGIN_YEAR_VERSION')), '');


$oForm -> addElement('submitImg', 'submit', text::get('CONNECT'), 'img/btn_pieslegties.gif', 'width="70" height="20"');

if ($oForm -> isFormSubmitted())
{
	if(dbProc::isExistsUserLoginAndPassword($oForm -> getValue('login'), $oForm -> getValue('password')) && dbProc::isUserNotInRole($oForm -> getValue('login'),$oForm -> getValue('password')))
	{
		$oForm -> addError(text::get('ERROR_USER_HAVE_NO_RIGHTS'));
	}
	else
	{
		$oForm -> addError(text::get('ERROR_INCORRECT_LOGIN_OR_PASSWORD'));
	}
}
$oForm -> makeHtml();
include('f.lgn.s.1.tpl');
?>