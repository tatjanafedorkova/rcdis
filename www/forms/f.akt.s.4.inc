﻿<?
// Created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);
$isEditor = ( dbProc::isUserInRole($userId, ROLE_EDITOR) ||  dbProc::isUserInRole($userId, ROLE_EDITOR_VIEWER));

// act ID
$actId  = reqVar::get('actId');
// search form
$isSearch  = reqVar::get('isSearch');
// search act by number
$searchNum  = reqVar::get('searchNum');
// EPLA act
$isEpla  = reqVar::get('isEpla');
// Auto
if(isset($tame))
  $tameTransport  = $tame;
else 
  $tameTransport   = reqVar::get('tame_trans');
// akta veids 
if(!isset($isInvestition))
  $isInvestition  = reqVar::get('isInvestition');

// Main form with XMLHTTP method load this code:
if (reqVar::get('xmlHttp') == 1)
{
    // if operation is DELETE , delete record from DB
    if (reqVar::get('deleteId')!='')
	{
	   dbProc::deleteActTransportRecord(reqVar::get('deleteId', reqVar::get('tame_trans')));
       ?>
	    rowNode = this.parentNode.parentNode;
		rowNode.parentNode.removeChild(rowNode);
		<?
    }
    exit();
}


// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
    if($actId !== false )
    {
      $oLink=new urlQuery();
      $oLink->addPrm(FORM_ID, 'f.akt.s.1');
      $oLink->addPrm('actId', $actId);
      $oLink->addPrm('isSearch', $isSearch);
      $oLink->addPrm('searchNum', $searchNum);
      $oLink->addPrm('tab', TAB_TRANSPORT);
      if($tameTransport && $isInvestition) {
      	$oLink->addPrm('tametab', TAB_TRANSPORT_T);
      }
      $oLink->addPrm('isEpla', $isEpla);
      $oLink->addPrm('isInvestition', $isInvestition);
 
      $oFormTransportTab = new Form('frmTransportTab'.(($tameTransport==1) ? 'T' : ''), 'post', $oLink->getQuery() . '#tab');
      unset($oLink);

      // if operation is success, show success message and redirect top frame
      if (reqVar::get('successMessageTab') && $isNew === false)
      {
      	$oFormTransportTab->addSuccess(reqVar::get('successMessageTab'));
      }

      $isReadonly   = true;
      // Ir pieejams main��anai, ja  AKTS:LIETOT�JS = �Teko�ais lietot�js� un lietot�ja loma ir �Ievad�tais� un AKTS:STATUS = �Izveide� vai �Atgriezts� vai �T�me�.
      if($isAdmin || ($act['RAKT_RLTT_ID'] == $userId || ($isAuto && $isEditor)) 
            && ($status == STAT_INSERT || $status == STAT_RETURN )  )       {
        $isReadonly   = false;
      }
     
      //Popup saites sagatave
      $oPopLink=new urlQuery;
      $oPopLink->addPrm(FORM_ID, 'f.akt.s.8');
      $oPopLink->addPrm('actId',$actId);
      $oPopLink->addPrm('catalog',KL_TRANSPORT);
      $oPopLink->addPrm('isSearch', $isSearch);
      $oPopLink->addPrm('searchNum', $searchNum);
      $oPopLink->addPrm('isEpla', $isEpla);
      $oPopLink->addPrm('tame', $tameTransport ?'1':'0');  

      // get favorit 
      $favorit=array();
      $i = 0;
      if($tameTransport== false) {
        $favorit[$i]['TITLE'] = text::get('TAB_ESTIMATE');  
        $favorit[$i]['URL'] = RequestHandler::popupHref($oPopLink -> getQuery().'&estimate=1', false, 700, 450);
        $i++;
      } 
         
      $krfk = dbProc::getKrfkInfo(KRFK_TRANSPORT_ID);
      if($krfk !== false)
      {
        $res=dbProc::getFavoritTypeDefinitionList($userId, $krfk['KRFK_VERTIBA']);
        if (is_array($res))
        {
         	foreach ($res as $row)
         	{
              $favorit[$i]['TITLE'] = $row['LTFV_NOSAUKUMS'];
              $favorit[$i]['URL'] = RequestHandler::popupHref($oPopLink -> getQuery().'&favoritId='.$row['LTFV_ID']);
              $i++;
            }
        }

        unset($res);
      }
      
      $region[$i]['TITLE'] = text::get('TRANSPORT_DEPARTMENT');   ;
      $region[$i]['URL'] = RequestHandler::popupHref($oPopLink -> getQuery().'&region='.text::get('TRANSPORT_DEPARTMENT'));
      $i++;      
      $favorit[$i]['TITLE'] = text::get('ALL_VALUES');   ;
      $favorit[$i]['URL'] = RequestHandler::popupHref($oPopLink -> getQuery().'&all=1');


       unset($res);
       unset($oPopLink);

     

      $aTransports = array();
      if ($oFormTransportTab -> isFormSubmitted() && $isNew === false)
      {
          $isRowValid = true;
          $aCodes =  $oFormTransportTab->getValue('code');
          if(is_array($aCodes) )
          {
            foreach ($aCodes as $i => $val)
            {
                 $aTransports[$i]['TRNS_ID'] = $oFormTransportTab->getValue('id['.$i.']');
                 $aTransports[$i]['TRNS_VALSTS_NUMURS'] = $oFormTransportTab->getValue('code['.$i.']');
                 $aTransports[$i]['TRNS_GRUPAS_NOSAUKUMS'] = $oFormTransportTab->getValue('group['.$i.']');
                 $aTransports[$i]['TRNS_APAKSGRUPAS_KODS'] = $oFormTransportTab->getValue('apGroupCode['.$i.']');
                 $aTransports[$i]['TRNS_APAKSGRUPAS_NOSAUKUMS'] = $oFormTransportTab->getValue('apGroup['.$i.']');
                 $aTransports[$i]['TRNS_PIEDERIBA'] = $oFormTransportTab->getValue('ouner['.$i.']');
                 $aTransports[$i]['TRNS_KM'] = $oFormTransportTab->getValue('km['.$i.']');
                 $aTransports[$i]['TRNS_STUNDAS'] = $oFormTransportTab->getValue('stundas['.$i.']');
                 $aTransports[$i]['TRNS_DARBA_STUNDAS'] = $oFormTransportTab->getValue('total2['.$i.']');
                 $aTransports[$i]['TRNS_APPROVE_DATE'] = $oFormTransportTab->getValue('edited['.$i.']');
                 $aTransports[$i]['TRNS_PLAN_DATE'] = $oFormTransportTab->getValue('plan_date['.$i.']');
                 if($oFormTransportTab->getValue('tame') == 1 && $isInvestition){
                    $aTransports[$i]['TRNS_INVESTITION_KM'] = $oFormTransportTab->getValue('inv_km['.$i.']');
                    $aTransports[$i]['TRNS_INVESTITION_MOTOR'] = $oFormTransportTab->getValue('inv_stundas['.$i.']');
                    $aTransports[$i]['TRNS_INVESTITION_WORK'] = $oFormTransportTab->getValue('inv_total2['.$i.']');
                    $aTransports[$i]['TRNS_TOTAL_COAST_KM'] = $oFormTransportTab->getValue('coast_km['.$i.']');
                    $aTransports[$i]['TRNS_TOTAL_COAST_MOTOR'] = $oFormTransportTab->getValue('coast_motor['.$i.']');
                    $aTransports[$i]['TRNS_TOTAL_COAST_WORK'] = $oFormTransportTab->getValue('coast_work['.$i.']');
                    $aTransports[$i]['INVC_CENA_KM'] = $oFormTransportTab->getValue('inv_km['.$i.']');
                    $aTransports[$i]['INVC_CENA_MOTORST'] = $oFormTransportTab->getValue('inv_stundas['.$i.']');
                    $aTransports[$i]['INVC_CENA_DARBAST'] = $oFormTransportTab->getValue('inv_total2['.$i.']');
                 }

                // validate km
                if (!empty($aTransports[$i]['TRNS_KM']) && !is_numeric($aTransports[$i]['TRNS_KM']))
                {
                  // assign error message
                  $oFormTransportTab->addErrorPerElem('km['.$i.']', text::get('ERROR_NUMERIC_VALUE'));
                  $isRowValid = false;
                }
                    // validate stundas
                if (!empty($aTransports[$i]['TRNS_STUNDAS']) && !is_numeric($aTransports[$i]['TRNS_STUNDAS']))
                {
                  // assign error message
                  $oFormTransportTab->addErrorPerElem('stundas['.$i.']', text::get('ERROR_DOUBLE_VALUE'));
                  $isRowValid = false;
                }
                    // validate darba stundas
                  if (empty($aTransports[$i]['TRNS_DARBA_STUNDAS']))
                {
                  // assign error message
                  $oFormTransportTab->addErrorPerElem('total2['.$i.']', text::get('ERROR_REQUIRED_FIELD'));
                  $isRowValid = false;
                }
                        // validate work progress value
                elseif (!is_numeric($aTransports[$i]['TRNS_DARBA_STUNDAS']))
                {
                  // assign error message
                  $oFormTransportTab->addErrorPerElem('total2['.$i.']', text::get('ERROR_DOUBLE_VALUE'));
                  $isRowValid = false;
                }
                        // validate work progress value
                elseif ($aTransports[$i]['TRNS_DARBA_STUNDAS'] >= 1000)
                {
                  // assign error message
                  $oFormTransportTab->addErrorPerElem('total2['.$i.']', text::get('ERROR_AMOUNT_VALUE'));
                  $isRowValid = false;
                }

                  // prepare database record data
                  $aValidRows[] = array(
                    'code' => $aTransports[$i]['TRNS_VALSTS_NUMURS'],
                    'group' => $aTransports[$i]['TRNS_GRUPAS_NOSAUKUMS'],
                    'apGroupCode' => $aTransports[$i]['TRNS_APAKSGRUPAS_KODS'],
                    'apGroup' => $aTransports[$i]['TRNS_APAKSGRUPAS_NOSAUKUMS'],
                    'ouner' => $aTransports[$i]['TRNS_PIEDERIBA'],
                    'km' => $aTransports[$i]['TRNS_KM'],
                    'stundas' => $aTransports[$i]['TRNS_STUNDAS'],
                    'total2' => $aTransports[$i]['TRNS_DARBA_STUNDAS'],
                    'rowNum' => $i,
                    'edited' => $aTransports[$i]['TRNS_APPROVE_DATE'],
                    'plan_date'=> $aTransports[$i]['TRNS_PLAN_DATE'],
                    'investition_km' => ($oFormTransportTab->getValue('tame') == 1 && $isInvestition) ? $aTransports[$i]['INVC_CENA_KM'] : '',
                    'investition_motor' => ($oFormTransportTab->getValue('tame') == 1 && $isInvestition) ? $aTransports[$i]['INVC_CENA_MOTORST'] : '',
                    'investition_work' => ($oFormTransportTab->getValue('tame') == 1 && $isInvestition) ? $aTransports[$i]['INVC_CENA_DARBAST'] : '',
                    'coast_km' => ($oFormTransportTab->getValue('tame') == 1 && $isInvestition && !empty($aTransports[$i]['INVC_CENA_KM'] )) ? 
                            $aTransports[$i]['INVC_CENA_KM'] * $aTransports[$i]['TRNS_KM']: '',
                    'coast_motor' => ($oFormTransportTab->getValue('tame') == 1 && $isInvestition && !empty($aTransports[$i]['INVC_CENA_MOTORST'])) ? 
                            $aTransports[$i]['INVC_CENA_MOTORST'] * $aTransports[$i]['TRNS_STUNDAS'] : '',
                    'coast_work' => ($oFormTransportTab->getValue('tame') == 1 && $isInvestition && !empty($aTransports[$i]['INVC_CENA_DARBAST'])) ? 
                            $aTransports[$i]['INVC_CENA_DARBAST'] * $aTransports[$i]['TRNS_DARBA_STUNDAS']: '',
                  );
             }
           }
      }
      else
      {
        //Ja eksistee akta materiāli, tad nolasaam taas darbus no datu baazes
  	    $aTransportDb=dbProc::getActTransportList($actId, false, $tameTransport);

        foreach ($aTransportDb as $i => $transport)
        {
           $aTransports[$i]['TRNS_ID'] = $transport['TRNS_ID'];
           $aTransports[$i]['TRNS_VALSTS_NUMURS'] = $transport['TRNS_VALSTS_NUMURS'];
           $aTransports[$i]['TRNS_GRUPAS_NOSAUKUMS'] = $transport['TRNS_GRUPAS_NOSAUKUMS'];
           $aTransports[$i]['TRNS_APAKSGRUPAS_KODS'] = $transport['TRNS_APAKSGRUPAS_KODS'];
           $aTransports[$i]['TRNS_APAKSGRUPAS_NOSAUKUMS'] = $transport['TRNS_APAKSGRUPAS_NOSAUKUMS'];
           $aTransports[$i]['TRNS_PIEDERIBA'] = $transport['TRNS_PIEDERIBA'];
           $aTransports[$i]['TRNS_KM'] = $transport['TRNS_KM'];
           $aTransports[$i]['TRNS_STUNDAS'] = $transport['TRNS_STUNDAS'];
           $aTransports[$i]['TRNS_DARBA_STUNDAS'] = $transport['TRNS_DARBA_STUNDAS'];
           $aTransports[$i]['TRNS_APPROVE_DATE'] = $transport['TRNS_APPROVE_DATE'];
           $aTransports[$i]['TRNS_PLAN_DATE'] = $transport['TRNS_PLAN_DATE'];
           if($tameTransport && $isInvestition) {
            $aTransports[$i]['TRNS_INVESTITION_KM'] = $transport['TRNS_INVESTITION_KM'];
            $aTransports[$i]['TRNS_INVESTITION_MOTOR'] = $transport['TRNS_INVESTITION_MOTOR'];
            $aTransports[$i]['TRNS_INVESTITION_WORK'] = $transport['TRNS_INVESTITION_WORK'];
            $aTransports[$i]['TRNS_TOTAL_COAST_KM'] = $transport['TRNS_TOTAL_COAST_KM'];
            $aTransports[$i]['TRNS_TOTAL_COAST_MOTOR'] = $transport['TRNS_TOTAL_COAST_MOTOR'];
            $aTransports[$i]['TRNS_TOTAL_COAST_WORK'] = $transport['TRNS_TOTAL_COAST_WORK'];
            $aTransports[$i]['INVC_CENA_KM'] = $transport['INVC_CENA_KM'];
            $aTransports[$i]['INVC_CENA_MOTORST'] = $transport['INVC_CENA_MOTORST'];
            $aTransports[$i]['INVC_CENA_DARBAST'] = $transport['INVC_CENA_DARBAST'];
           }
        }
      }
      $oFormTransportTab -> addElement('hidden', 'isSearch', '', $isSearch);
      $oFormTransportTab -> addElement('hidden', 'searchNum', '', $searchNum);
      $oFormTransportTab->addElement('hidden','actId','',$actId);
      $oFormTransportTab->addElement('hidden','maxAmaunt','','1000.00');
      $oFormTransportTab->addElement('hidden','minAmaunt','','0.00');
      $oFormTransportTab->addElement('hidden','tame','',($tameTransport) ? 1 : 0);

      $totalKm = 0;
      $totalTime = 0;
      $totalWorkTime = 0;
      $coast_totalKm = 0;
      $coast_totalTime = 0;
      $coast_totalWorkTime = 0;
      
      // form elements
      foreach ($aTransports as $i => $actTransport)
      {
        

        $oFormTransportTab->addElement('hidden', 'id['.$i.']', '', $actTransport['TRNS_ID']);
        $oFormTransportTab->addElement('label', 'code['.$i.']', '', $actTransport['TRNS_VALSTS_NUMURS']);
        $oFormTransportTab->addElement('label', 'group['.$i.']', '', $actTransport['TRNS_GRUPAS_NOSAUKUMS']);
        $oFormTransportTab->addElement('hidden', 'apGroupCode['.$i.']', '', $actTransport['TRNS_APAKSGRUPAS_KODS']);
        $oFormTransportTab->addElement('label', 'apGroup['.$i.']', '', $actTransport['TRNS_APAKSGRUPAS_NOSAUKUMS']);
        $oFormTransportTab->addElement('hidden', 'ouner['.$i.']', '', $actTransport['TRNS_PIEDERIBA']);
        $oFormTransportTab->addElement('hidden', 'edited['.$i.']', '', $actTransport['TRNS_APPROVE_DATE']);
       

        if(empty($actTransport['TRNS_APPROVE_DATE'])  ) {
          $oFormTransportTab->addElement('text', 'km['.$i.']', '', $actTransport['TRNS_KM'], (($isReadonly)?' disabled ':'').'maxlength="4"');
          $oFormTransportTab -> addRule('km['.$i.']', text::get('ERROR_REQUIRED_FIELD'), 'required');
          $oFormTransportTab -> addRule('km['.$i.']', text::get('ERROR_NUMERIC_VALUE'), 'numeric');

          $oFormTransportTab->addElement('text', 'stundas['.$i.']', '', $actTransport['TRNS_STUNDAS'], (($isReadonly)?' disabled ':'').'maxlength="6"');
          $oFormTransportTab -> addRule('stundas['.$i.']', text::get('ERROR_REQUIRED_FIELD'), 'required');
          $oFormTransportTab -> addRule('stundas['.$i.']', text::get('ERROR_DOUBLE_VALUE'), 'double', 2);
          // check Amaunt field
          $v1=&$oFormTransportTab->createValidation('stundasValidation_'.$i, array('stundas['.$i.']'), 'ifonefilled');
          // ja Amount ir definēts un > 1000
          $r1=&$oFormTransportTab->createRule(array('stundas['.$i.']', 'maxAmaunt'), text::get('ERROR_AMOUNT_VALUE'), 'comparedouble','<');
          $oFormTransportTab->addRule(array($r1), 'groupRuleStundas_'.$i, 'group', $v1);

          $oFormTransportTab->addElement('text', 'total2['.$i.']', '', $actTransport['TRNS_DARBA_STUNDAS'], (($isReadonly)?' disabled ':'').'maxlength="6"');
          $oFormTransportTab -> addRule('total2['.$i.']', text::get('ERROR_REQUIRED_FIELD'), 'required');
          $oFormTransportTab -> addRule('total2['.$i.']', text::get('ERROR_DOUBLE_VALUE'), 'double', 2);
          // check Amaunt field
          $v2=&$oFormTransportTab->createValidation('total2Validation_'.$i, array('total2['.$i.']'), 'ifonefilled');
          // ja Amount ir definēts un > 1000
          $r1=&$oFormTransportTab->createRule(array('total2['.$i.']', 'maxAmaunt'), text::get('ERROR_AMOUNT_VALUE'), 'comparedouble','<');
          // ja Amount ir definēts un < 0
          //$r2=&$oFormTransportTab->createRule(array('total2['.$i.']', 'minAmaunt'), text::get('ERROR_AMOUNT_VALUE'), 'comparedouble','>=');
          $oFormTransportTab->addRule(array($r1/*, $r2*/), 'groupRuleTotal2_'.$i, 'group', $v2);

          //datums
          if(!$tameTransport && $oFormTransportTab->getValue('tame') == 0  ){     
            $oFormTransportTab -> addElement('date', 'plan_date['.$i.']',  '', $actTransport['TRNS_PLAN_DATE'], (($isReadonly)?' disabled ':''));
            $oFormTransportTab -> addRule('plan_date['.$i.']', text::get('ERROR_REQUIRED_FIELD'), 'required');
            $oFormTransportTab -> addRule('plan_date['.$i.']', text::get('ERROR_INCORRECT_DATE'),'validatedate');
          }

        } else {
          $oFormTransportTab->addElement('label', 'km['.$i.']', '', $actTransport['TRNS_KM']);
          $oFormTransportTab->addElement('label', 'stundas['.$i.']', '', $actTransport['TRNS_STUNDAS']);
          $oFormTransportTab->addElement('label', 'total2['.$i.']', '', $actTransport['TRNS_DARBA_STUNDAS']);
          if(!$tameTransport && $oFormTransportTab->getValue('tame') == 0  ){     
           $oFormTransportTab->addElement('label', 'plan_date['.$i.']', '', $actTransport['TRNS_PLAN_DATE']); 
          }
        }

        if(($tameTransport || $oFormTransportTab->getValue('tame') == 1) && $isInvestition  ){  

          $oFormTransportTab->addElement('label', 'inv_km['.$i.']', '', $actTransport['INVC_CENA_KM']);
          $oFormTransportTab->addElement('label', 'inv_stundas['.$i.']', '', $actTransport['INVC_CENA_MOTORST']);
          $oFormTransportTab->addElement('label', 'inv_total2['.$i.']', '', $actTransport['INVC_CENA_DARBAST']);
          
          $_km = number_format(empty($actTransport['TRNS_TOTAL_COAST_KM']) ? 
          (empty($actTransport['INVC_CENA_KM'])? 0 : $actTransport['INVC_CENA_KM'] * $actTransport['TRNS_KM']): 
          $actTransport['TRNS_TOTAL_COAST_KM'], 2, '.', '');
          $_motor = number_format(empty($actTransport['TRNS_TOTAL_COAST_MOTOR']) ? 
          (empty($actTransport['INVC_CENA_MOTORST']) ? 0 : $actTransport['INVC_CENA_MOTORST'] * $actTransport['TRNS_STUNDAS'] ) : 
          $actTransport['TRNS_TOTAL_COAST_MOTOR'], 2, '.', '');
          $_work = number_format(empty($actTransport['TRNS_TOTAL_COAST_WORK']) ? 
          (empty($actTransport['INVC_CENA_DARBAST']) ? 0 : $actTransport['INVC_CENA_DARBAST'] * $actTransport['TRNS_DARBA_STUNDAS']) : 
          $actTransport['TRNS_TOTAL_COAST_WORK'], 2, '.', '');
         
          $oFormTransportTab->addElement('label', 'coast_km['.$i.']', '', $_km );          
          $oFormTransportTab->addElement('label', 'coast_stundas['.$i.']', '', $_motor );
          $oFormTransportTab->addElement('label', 'coast_total2['.$i.']', '', $_work);

         
        }

        $totalKm += $actTransport['TRNS_KM'];
        $totalTime += $actTransport['TRNS_STUNDAS'];
        $totalWorkTime += $actTransport['TRNS_DARBA_STUNDAS'];
        if($tameTransport  && $isInvestition)  {
          $coast_totalKm += (empty($actTransport['INVC_CENA_KM']) ? 0 :$actTransport['TRNS_KM'] * $actTransport['INVC_CENA_KM']);
          $coast_totalTime += (empty($actTransport['INVC_CENA_MOTORST']) ? 0 : $actTransport['TRNS_STUNDAS'] * $actTransport['INVC_CENA_MOTORST']);
          $coast_totalWorkTime += (empty($actTransport['INVC_CENA_DARBAST']) ? 0 : $actTransport['TRNS_DARBA_STUNDAS'] * $actTransport['INVC_CENA_DARBAST']);
        }

      // delete button
      if(empty($actTransport['TRNS_APPROVE_DATE']) ) {
        // ajax handler link for delete
        $delLink = new urlQuery();
        $delLink->addPrm(FORM_ID, 'f.akt.s.4');
        $delLink->addPrm(DONT_USE_GLB_TPL, '1');
        $delLink->addPrm('xmlHttp', '1');
        $delLink->addPrm('tame_trans', $tameTransport ?'1':'0');
        $delButtonJs = "if(isDelete()) {
          var rowId = document.forms['frmTransportTab".(($tameTransport ) ? 'T' : '')."']['id[" .$i. "]'];
          eval(xmlHttpGetValue('" .$delLink -> getQuery() ."&deleteId='+rowId.value));
        }";
        
        $oFormTransportTab->addElement('button', 'trans_del['.$i.']', '', '', 'onclick="' . $delButtonJs . ';return false;" style="background: url(img/ico_del.gif); cursor:hand; border:0px; width: 20px; height: 20px;" title="' . text::get('DELETE') . '"');
          unset($delLink);
        }
      }

      $totalKm = number_format($totalKm, 2, '.', '');
      $totalTime = number_format($totalTime, 2, '.', '');
      $totalWorkTime = number_format($totalWorkTime, 2, '.', '');

      if($tameTransport  && $isInvestition)  {
        $coast_totalKm  = number_format($coast_totalKm, 2, '.', '');
        $coast_totalTime = number_format($coast_totalTime, 2, '.', '');
        $coast_totalWorkTime = number_format($coast_totalWorkTime, 2, '.', '');
      }

      // form buttons
      if (!$isReadonly)
      {
        foreach ($favorit as $i => $group)
        {
           $oFormTransportTab->addElement('link', 'favorit['.$i.']', $group['TITLE'], '#', 'onClick="'.$group['URL'].'"', '');
        }
       
        $oFormTransportTab -> addElement('submitImg', 'save', text::get('SAVE'), 'img/btn_save_transport.gif', 'width="120" height="20" ');
        //$oFormTransportTab -> addElement('clearImg', 'clear', text::get('CLEAR'), 'img/btn_attirit.gif', 'width="70" height="20"');
      }

      // if form iz submit (save button was pressed)
	  if ($oFormTransportTab -> isFormSubmitted()&& $isNew === false)
	  {
		if($oFormTransportTab ->isValid() && $isRowValid)
		{
		   $r=dbProc::deleteActTransports($actId, $tameTransport);
		   if ($r === false)
		   {
		   	$oFormTransportTab->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
		   }
		   else
		   {
		        if(isset($aValidRows) && is_array($aValidRows))
            {
        	    	foreach($aValidRows as $i => $row)
        			{
                        //if(!empty($row['edited'])) continue;
                         $r=dbProc::writeActTransport($actId,
                                                $row['code'],
                                                $row['group'],
                                                $row['apGroupCode'],
                                                $row['apGroup'],
                                                $row['ouner'],
                                                $row['km'],
                                                $row['stundas'],
                                                $row['total2'],
                                                $row['edited'],
                                                $row['plan_date'],
                                                ($tameTransport && $isInvestition ),
                                                ($tameTransport && $isInvestition ) ? $row['investition_km']: false,
                                                ($tameTransport && $isInvestition ) ? $row['investition_motor']: false,
                                                ($tameTransport && $isInvestition ) ? $row['investition_work']: false,
                                                ($tameTransport && $isInvestition ) ? $row['coast_km']: false,
                                                ($tameTransport && $isInvestition ) ? $row['coast_motor']: false,
                                                ($tameTransport && $isInvestition ) ? $row['coast_work']: false
                                              );
                                // if work not inserted in to the DB
                                // delete  task record and show error message
                                if($r === false)
                                {
                                  dbProc::deleteActTransports($actId,  $tameTransport);
                                  $oFormTransportTab->addError(text::get('ERROR_IN_DB_QUERY').dbProc::getError());
                                      break;
                                }
                                else
                                {
                                  $Id=dbLayer::getInsertId();
                                  $oFormTransportTab -> setNewValue('id['.$row['rowNum'].']', $Id);
                                  $oFormTransportTab->addSuccess(text::get('RECORD_WAS_SAVED'));
                                  if($tameTransport && $isInvestition ) {
                                    $oFormTransportTab -> setNewValue('coast_km['.$row['rowNum'].']', number_format((float)$row['coast_km'], 2, '.', ''));
                                    $oFormTransportTab -> setNewValue('coast_stundas['.$row['rowNum'].']', number_format((float)$row['coast_motor'], 2, '.', ''));
                                    $oFormTransportTab -> setNewValue('coast_total2['.$row['rowNum'].']', number_format((float)$row['coast_work'], 2, '.', ''));
                                  }
                                }
                            }
                            $oLink=new urlQuery();
                		        $oLink->addPrm(FORM_ID, 'f.akt.s.1');
                		        $oLink->addPrm('actId', $actId);
                            $oLink->addPrm('tab', TAB_TRANSPORT);
                            if($tameTransport && $isInvestition ) {
                                $oLink->addPrm('tametab', TAB_TRANSPORT_T);
                            }
                            $oLink->addPrm('isNew', RequestHandler::getMicroTime());
                            $oLink->addPrm('successMessageTab', text::get('RECORD_WAS_SAVED'));
                            //RequestHandler::makeRedirect($oLink->getQuery());
                    // saglaba lietotaja darbiibu
                    dbProc::setUserActionDate($userId, ACT_UPDATE);
                    $tameTransport = 0;
                }
           }
        }
      }
       $oFormTransportTab -> makeHtml();
       include('f.akt.s.4.tpl');
    }



}
else
{
    RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}

?>