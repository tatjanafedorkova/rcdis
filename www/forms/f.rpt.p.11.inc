﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
     $sCriteria = reqVar::get('search');

        // gel list of users
		$res = dbProc::getCalculationByGroupList($sCriteria);

        $baseTime = 0;
        $overTime = 0;
        if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
               $baseTime = $baseTime + $row['PAMATSTUNDAS'];
               $overTime = $overTime + $row['VIRSTUNDAS'];
			}
		}
        $baseTime = number_format($baseTime,2, '.', '');
        $overTime = number_format($overTime,2, '.', '');

        $searchCr = array();
        $sCr = explode("^", $sCriteria);
        if(isset($sCr[0]) && $sCr[0] != -1  && isset($sCr[1]) && $sCr[1] != -1)
        {
           $searchCr[] = array('label' =>  text::get('YEAR'), 'value' => $sCr[0].'-'.$sCr[1]);
        }
        if(isset($sCr[2]) && $sCr[2] != -1  && isset($sCr[3]) && $sCr[3] != -1)
        {
           $searchCr[] = array('label' =>  text::get('MONTH'), 'value' => dtime::getMonthName($sCr[2]).'-'.dtime::getMonthName($sCr[3]));
        }
        if(isset($sCr[4]) && $sCr[4] != -1)
        {
           $searchCr[] = array('label' =>  text::get('RCD_REGION'), 'value' => $sCr[4]);
        }
        if(isset($sCr[5]) && $sCr[5] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGL_DV_AREA'), 'value' => dbProc::getDVAreaName($sCr[5]));
        }
        if(isset($sCr[6]) && $sCr[6] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ED_REGION'), 'value' => $sCr[6]);
        }
        if(isset($sCr[7]) && $sCr[7] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ED_IECIKNIS'), 'value' => dbProc::getEDAreaName($sCr[7]));
        }

        if(isset($sCr[8]) && $sCr[8] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_SOURCE_OF_FOUNDS'), 'value' => dbProc::getSourceOfFoundsName($sCr[8]));
        }
        if(isset($sCr[9]) && $sCr[9] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_VOLTAGE'), 'value' => dbProc::getVoltageName($sCr[9]));
        }
        if(isset($sCr[10]) && $sCr[10] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ACT_OUNER'), 'value' => dbProc::getUserName($sCr[10]));
        }
        $type = explode("*", $sCr[18]);
        $typeString = '';
        if($type[0] == -1)
        {
           $typeString = text::get('ALL');
        }
        else
        {
          $typeKls = classif::getValueArray(KL_ACT_TYPE);
          foreach($type as $t)
          {
              $typeString .= $typeKls[$t].",";
          }
          $typeString  = substr($typeString, 0, -1);
        }
        $searchCr[] = array('label' =>  text::get('SINGLE_ACT_TYPE'), 'value' => $typeString);
        if(isset($sCr[12]) && $sCr[12] != -1)
        {
           $searchCr[] = array('label' =>  text::get('MMS_WORK_FINISHED'), 'value' => (($sCr[12] == 1) ? text::get('YES'): text::get('NO')));
        }
        $status = explode("*", $sCr[14]);
        $statusString = '';
        if($status[0] == -1)
        {
           $statusString = text::get('ALL_STATUS');
        }
        else
        {
          foreach($status as $s)
          {
              $statusInfo = dbProc::getKrfkInfoByName($s);
              $statusString .= $statusInfo['KRFK_NOZIME'].",";
          }
          $statusString  = substr($statusString, 0, -1);
        }
        $searchCr[] = array('label' =>  text::get('STATUS'), 'value' => $statusString);
        if(isset($sCr[15]) && $sCr[15] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_OBJECT'), 'value' => dbProc::getObjectName($sCr[15]));
        }
        if(isset($sCr[16]) && $sCr[16] != -1)
        {
           $searchCr[] = array('label' =>  text::get('SINGLE_OTHER_PAYMENTS'), 'value' => (($sCr[16] == 1) ? text::get('YES'): text::get('NO')));
        }
        if(isset($sCr[17]) && $sCr[17] != '')
        {
          // get info about calculation group
            $favoritInfo = dbProc::getFavoritDefinition($sCr[17]);
            if(count($favoritInfo)>0)
            {
            	$favorit = $favoritInfo[0];
                $favoritTitle = $favorit['LTFV_NOSAUKUMS'];
            }
           $searchCr[] = array('label' =>  text::get('CALCULATION_GROUP'), 'value' => $favoritTitle);
        }
        if(isset($sCr[19]) && $sCr[19] != -1)
        {
           $searchCr[] = array('label' =>  text::get('ED_SECTION'), 'value' => urldecode($sCr[19]));
        }
        

		include('f.rpt.p.11.tpl');
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
