﻿<body class="frame_1">

<?=$oForm->getElementHtml('jsRefresh2');?>
<h1><?=$searchTitle;?></h1>
<?= $oForm -> getFormHeader(); ?>
<table cellpadding="3" cellspacing="1" border="0" width="100%">
	<tr>
		<td align=center colspan="4"><?= $oForm -> getMessage(); ?></td>
	</tr>
   <tr>
		<td class="table_cell_c" width="10%"><?= $oForm -> getElementLabel('year'); ?>:</td>
		<td class="table_cell_2" width="20%">
            <?= $oForm -> getElementLabel('yearFrom'); ?>&nbsp;
            <?= $oForm -> getElementHtml('yearFrom'); ?>&nbsp;&nbsp;
            <?= $oForm -> getElementLabel('yearUntil'); ?>&nbsp;
            <?= $oForm -> getElementHtml('yearUntil'); ?>
        </td>
        <td class="table_cell_c" width="10%"><?= $oForm -> getElementLabel('quarter'); ?>:</td>
		<td class="table_cell_2" width="23%">
            <?= $oForm -> getElementLabel('quarterFrom'); ?>
            <?= $oForm -> getElementHtml('quarterFrom'); ?>&nbsp;
            <?= $oForm -> getElementLabel('quarterUntil'); ?>
            <?= $oForm -> getElementHtml('quarterUntil'); ?>
        </td>
        <td class="table_cell_c" width="10%">&nbsp;</td>
		<td class="table_cell_2" width="27%">&nbsp;</td>
   </tr>

    <tr>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('edRegion'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('edRegion'); ?></td>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('section'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('section'); ?></td>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('edArea'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('edArea'); ?></td>
   	</tr>

    <tr>
		<td colspan="4" align="right">
        <span id="loading" style="position:absolute; width:32; height:32; margin-left:30px; display: none; ">
        <img src="./img/loading.gif" widht="32" height="32" border="0" />
        </span><?=$oForm->getElementHtml('search');?></td>
        <td colspan="2"><a href="#" onClick="reloadFrame(1,'<?= $criteriaLink; ?>');reloadFrame(23,'');"><?=text::get('RETURN_TO_SEARCH_CRRITERIA');?></a></td>
    </tr>
</table>

<?= $oForm -> getFormBottom(); ?>
</body>
