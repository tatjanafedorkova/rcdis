<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);
function getActLinks($mms)
{
    $result = '';
    $res = dbProc::detAllActsForMms($mms);
    if (is_array($res))
    {
        foreach ($res as $i=>$row)
	    {
              $oLinkFile=new urlQuery();
              $oLinkFile->addPrm(IS_GET_FILE_VARIABLE, 1);
              $oLinkFile->addPrm(GET_FILE_VARIABLE,$row['RFLS_ID']);
              $result = $result . "<a href=".$oLinkFile ->getQuery().">".$row['ACT_NR']."</a><br>";
              unset($oLinkFile);
        }
    }

    return $result;
}

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
	$searchMode = reqVar::get('isSearch');
	// top frame
    if($searchMode)
	{
        $oLink=new urlQuery();
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.18');
    	$oLink->addPrm('isSearch', 1 );
    	// form inicialization
    	$oForm = new Form('frmMain','post',$oLink->getQuery());
    	unset($oLink);

        $oLink=new urlQuery();
	    $oLink->addPrm('isReturn', '1');
        $oLink->addPrm('isSearch', 1 );
	    $oLink->addPrm(FORM_ID, 'f.rpt.s.18');
	    $criteriaLink=$oLink ->getQuery();
	    unset($oLink);

        // if operation
    	if ($oForm -> isFormSubmitted())
    	{
            $oListLink=new urlQuery();
            $oListLink->addPrm(FORM_ID, 'f.rpt.s.18');
        }
        $searchTitle = text::toUpper(text::toUpper(text::get('REPORT_MMS_GROUPS')));
        $searchName = 'REPORT_MMS_GROUPS';
        $notPlan = true;
        include('f.akt.m.11.inc');
	}
	// bottom frale
	else
	{
        $sCriteria = reqVar::get('search');

        /*// URL  export
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.e.16');
    	$oLink->addPrm('search', $sCriteria);
        $oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $exportUrl =  $oLink ->getQuery();
        unset($oLink); */

        // URL  print
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.p.18');
    	$oLink->addPrm('search', $sCriteria);
        //$oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $printUrl =  $oLink ->getQuery();
        unset($oLink);

		// gel list of users
		$res = dbProc::getActWithMMSGroup($sCriteria);
        $area = array();

        $summa = 0;
        $stundas = 0;

        if (is_array($res))
		{
            foreach ($res as $i=>$row)
			{


               $summa = $summa + $row['CENA_KOPA'];
               $stundas = $stundas + $row['CILVEKSTUNDAS'];

            }
        }
        $summa = number_format($summa, 2, '.', '');
        $stundas = number_format($stundas,2, '.', '');



		include('f.rpt.s.18.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
