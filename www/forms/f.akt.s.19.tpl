﻿<h1><?=text::toUpper(text::get(TAB_WORKS));?></h1>
<?= $oFormWorkTab -> getFormHeader(); ?>
<table cellpadding="5" cellspacing="1" border="0" width="100%">
	<tr>
		<td align=center colspan="2"><?= $oFormWorkTab -> getMessage(); ?></td>
	</tr>
    <tr>
		<td valign="top">
        <table cellpadding="5" cellspacing="1" border="0" width="100%">
		   <tr class="table_head_2">

				<th width="15%"><?=text::get('WORK_TYPE');?></th>
				<th width="30%"><?=text::get('WORK_DESCRIPTION');?></th>
				<th  width="10%"><?=text::get('DATE');?> <?=text::get('FROM');?></th>
                <th  width="10%"><?=text::get('DATE');?> <?=text::get('UNTIL');?></th>
				<th  width="15%"><?=text::get('USER_NAME');?> <?=text::get('USER_SURNAME');?></th>
				<th  width="5%"><?=text::get('RCD_KODS');?></th>
                <th  width="12%"><?=text::get('TRANSPORT_WORK_TIME');?></th>
			 	<th width="3%">&nbsp;</th>
			</tr>

	  <?
	   	foreach($aWorks as $i=>$val)
		{
		?>
			<tr class="table_cell_3"  id="blank_work<?= $i; ?>">

				<td>
                    <?= $oFormWorkTab->getElementHtml('id['.$i.']'); ?>
                    <?= $oFormWorkTab->getElementHtml('kdbv_id['.$i.']'); ?>
                    <?= $oFormWorkTab->getElementHtml('work['.$i.']'); ?>
                </td>
				<td><?= $oFormWorkTab->getElementHtml('description['.$i.']'); ?></td>
				<td><?= $oFormWorkTab->getElementHtml('date_from['.$i.']'); ?></td>
                <td><?= $oFormWorkTab->getElementHtml('date_to['.$i.']'); ?></td>
				<td><?= $oFormWorkTab->getElementHtml('person_name['.$i.']'); ?></td>
				<td><?= $oFormWorkTab->getElementHtml('person_code['.$i.']'); ?></td>
                <td><?= $oFormWorkTab->getElementHtml('time['.$i.']'); ?></td>
             	<td>
                    <? if(!$isReadonly)  {?>
                    <?= $oFormWorkTab->getElementHtml('work_del['.$i.']'); ?>
                    <? } else{ ?>
                    &nbsp;
                    <? } ?>
                </td>
			</tr>

		<?
		}
		?>
        <tr>
             <td align="right" colspan="6"><b><?=text::get('TOTAL');?>:</b></td>
             <td align="center"><b><?= $oFormWorkTab->getElementHtml('totalTime'); ?></b></td>
        </tr>
	    </table>
        </td>
        <td width="20%"  valign="top">
            <table cellpadding="5" cellspacing="1" border="0" align="center" width="100%">
            <? if(!$isReadonly)  {
              foreach($favorit as $i=>$val){?>
             <tr>
                <td bgcolor="white" colspan="5"><?= $oFormWorkTab->getElementHtml('favorit['.$i.']'); ?></td>
             </tr>
            <? } ?>
             <tr>
             <?
                for($i = 0; $i < 5; $i++)
                {
                   ?>
                   <td bgcolor="white"><?= $oFormWorkTab->getElementHtml('regions['.$i.']'); ?></td>
                   <?
                }
             ?>
             </tr>

            <?} ?>
            </table>
            <br />
            <table cellpadding="5" cellspacing="0" border="0" align="center">
            <tr>
             <? if(!$isReadonly)  {?>
            	<td><?= $oFormWorkTab -> getElementHtml('save'); ?></td>
             <? } ?>
            
             </tr>
            </table>
        </td>
	</tr>

</table>
<?= $oFormWorkTab -> getFormBottom(); ?>

