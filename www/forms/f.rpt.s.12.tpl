﻿<body class="frame_2">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><h1><?= text::toUpper(text::get('REPORT_WORK_PLAN_AND_NOT_PLAN')); ?></h1></td>
		<td align="right">
            <img src="img/ico_print.gif" alt="" width="16" height="16" border="0">&nbsp;
            <a href="<?= $printUrl; ?>" target="new"><?=text::get('PRINT');?></a>&nbsp;&nbsp;
            <img src="img/ico_excel.gif" alt="" width="16" height="16">&nbsp;
            <a href="<?= $exportUrl; ?>" ><?=text::get('EXPORT');?></a>&nbsp;
        </td>
	</tr>
</table>

<table cellpadding="5" cellspacing="1" border="0" width="100%" >
<?

	if (is_array($area) && count($area) > 0)
	{
		foreach ($area as $row)
		{
?>
            <tr class="table_head_2">
                <td align="left"><b><?=text::get('ED_IECIKNIS');?>:</b></td>
                <td colspan="2" align="left"><b><?= $row['name']; ?></b></td>
                <td align="left"><b><?= $row['code']; ?></b></td>
            </tr>

            <tr class="table_head_2">
                <td width="30%">&nbsp;</td>
                <td width="20%"><?=text::get('BASE_TIME');?></td>
                <td width="20%"><?= text::get('REPORT_MATERIAL_PRICE_TOTAL'); ?></td>
                <td width="20%"><?=text::get('OVER_TIME');?></td>
            </tr>

            <tr class="table_cell_3" >
                 <td align="left"><b><?=text::get('MMS_PLAN_WORKS');?>:</b></td>
                 <td align="center"><?= $row['mmsMainTime']; ?></td>
                 <td align="center"><?= $row['mmsCena']; ?></td>
                 <td align="center"><?= $row['mmsOverTime']; ?></td>
            </tr>
            <tr class="table_cell_3" >
                 <td align="left"><b><?=text::get('REAL_PLAN_WORKS');?>:</b></td>
                 <td align="center"><?= $row['planMainTime']; ?></td>
                 <td align="center"><?= $row['planCena']; ?></td>
                 <td align="center"><?= $row['planOverTime']; ?></td>
            </tr>
            <tr class="table_cell_3" >
                 <td align="left"><b><?=text::get('MMS_PLAN_MINUS_REAL_PLAN');?>:</b></td>
                 <td align="center"><?= $row['difMainTime']; ?></td>
                 <td align="center"><?= $row['difCena']; ?></td>
                 <td align="center"><?= $row['difOverTime']; ?></td>
            </tr>
            <tr class="table_cell_3" >
                 <td align="left"><b><?=text::get('NOT_PLAN_WORK');?>:</b></td>
                 <td align="center"><?= $row['notPlanMainTime']; ?></td>
                 <td align="center"><?= $row['notPlanCena']; ?></td>
                 <td align="center"><?= $row['notPlanOverTime']; ?></td>
            </tr>
            <tr class="table_cell_3" >
                 <td align="left"><b><?=text::get('DAMAGE_REAL_WORK');?>:</b></td>
                 <td align="center"><?= $row['damageMainTime']; ?></td>
                 <td align="center"><?= $row['damageCena']; ?></td>
                 <td align="center"><?= $row['damageOverTime']; ?></td>
            </tr>
            <tr class="table_cell_3" >
                 <td align="left"><b><?=text::get('DEFECT_REAL_WORK');?>:</b></td>
                 <td align="center"><?= $row['defectMainTime']; ?></td>
                 <td align="center"><?= $row['defectCena']; ?></td>
                 <td align="center"><?= $row['defectOverTime']; ?></td>
            </tr>
            <tr class="table_cell_3" >
                 <td align="left"><b><?=text::get('STIHIJA_REAL_WORK');?>:</b></td>
                 <td align="center"><?= $row['stihijaMainTime']; ?></td>
                 <td align="center"><?= $row['stihijaCena']; ?></td>
                 <td align="center"><?= $row['stihijaOverTime']; ?></td>
            </tr>
            <tr class="table_cell_3" >
                 <td align="left"><b><?=text::get('NOT_PLAN_TO_PLAN');?>:</b></td>
                 <td align="center"><?= $row['notPlanProcentMainTime']; ?> %</td>
                 <td align="center"><?= $row['notPlanProcentCena']; ?> %</td>
                 <td align="center"><?= $row['notPlanProcentOverTime']; ?> %</td>
            </tr>
            <tr class="table_cell_3" >
                 <td align="left"><b><?=text::get('NOT_PLAN_AND_PLAN_TOTAL');?>:</b></td>
                 <td align="center"><?= $row['totalMainTime']; ?></td>
                 <td align="center"><?= $row['totalCena']; ?></td>
                 <td align="center"><?= $row['totalOverTime']; ?></td>
            </tr>
            <tr><td colspan="4">&nbsp;</td></tr>
        <?

		}
        ?>

<?
	}
?>

</table>

</body>