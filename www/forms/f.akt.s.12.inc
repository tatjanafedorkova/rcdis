﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);
// act ID
$actId  = reqVar::get('actId');
// EPLA act
$isEpla  = reqVar::get('isEpla');

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
    // inicial state: actId=0;
  if($actId != false)
  {
  	// get info about act
  	$actInfo = dbProc::getActInfo($actId);
  	//print_r($actInfo);
  	if(count($actInfo)>0)
  	{
  		$act = $actInfo[0];
       	// get info about work
        $aWorkDb=dbProc::getActWorkList($actId);
        $workTotal = 0;
        foreach ($aWorkDb as $i => $work)
        {
           $workTotal +=  $work['DRBI_CILVEKSTUNDAS'];
        }
        $workTotal = number_format($workTotal, 2);

        if($isEpla == 0)
        {
          // get info about material
          $aMaterialDb=dbProc::getActMaterialList($actId);
          $materialTotal = 0;
          foreach ($aMaterialDb as $i => $material)
          {
            $materialTotal += $material['MATR_CENA_KOPA'];
          }
          $materialTotal = number_format($materialTotal,2);
        }
         // if((isset($_SESSION[CURRENT_YEAR])  && $_SESSION[CURRENT_YEAR] == 2010) || date('Y') == 2010)
         // {
              // get info about transport
              $aTransportDb=dbProc::getActTransportList($actId);
              $transportTotal = 0;
              $transportKm = 0;
              $transportStundas = 0;
              foreach ($aTransportDb as $i => $transport)
              {
                 $transportTotal += $transport['TRNS_DARBA_STUNDAS'];
                 $transportKm += $transport['TRNS_KM'];
                 $transportStundas += $transport['TRNS_STUNDAS'];
              }
              $transportTotal = number_format($transportTotal,2);
              $transportKm = number_format($transportKm,2);
              $transportStundas = number_format($transportStundas,2);

              // get info about customer
              $aCustomerDb=dbProc::getActPersonalList($actId);
              $customerTotalBase = 0;
              $customerTotalOver = 0;
              foreach ($aCustomerDb as $i => $customer)
              {
                 $customerTotalBase += $customer['PRSN_PAMATSTUNDAS'];
                 $customerTotalOver += $customer['PRSN_VIRSSTUNDAS'];
              }
              $customerTotalBase = number_format($customerTotalBase,2);
              $customerTotalOver = number_format($customerTotalOver,2);
         // }
    }
  }


   include('f.akt.s.12.tpl');
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
