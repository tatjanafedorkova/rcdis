﻿<body class="frame_2">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><h1><?= text::toUpper(text::get('REPORT_CUSTOMER_WORK_TIME')); ?></h1></td>
		<td align="right">
            <img src="img/ico_print.gif" alt="" width="16" height="16" border="0">&nbsp;
            <a href="<?= $printUrl; ?>" target="new"><?=text::get('PRINT');?></a>&nbsp;&nbsp;
            <img src="img/ico_excel.gif" alt="" width="16" height="16">&nbsp;
            <a href="<?= $exportUrl; ?>" ><?=text::get('EXPORT');?></a>&nbsp;
        </td>
	</tr>
</table>

<table cellpadding="5" cellspacing="1" border="0" width="100%" >
	<thead>
		<tr class="table_head_2">
			<td width="15%"><?= text::get('WORKING_PLACE_NUMBER'); ?></td>
			<td width="15%"><?= text::get('RCD_KODS'); ?></td>
            <td width="40%"><?=text::get('USER_SURNAME');?> <?=text::get('USER_NAME');?></td>
            <td width="10%"><?=text::get('BASE_TIME');?></td>
            <td width="10%"><?=text::get('OVER_TIME');?></td>
                        <td width="10%"><?=text::get('ROUT_TIME');?></td>

 		</tr>
	</thead>
	<tbody>
<?
	if (is_array($res) && count($res) > 0)
	{
		foreach ($res as $row)
		{
?>
			<tr class="table_cell_3" >
			   	<td align="center"><?= $row['PRSN_DARBA_VIETA']; ?></td>
                <td align="center"><?= $row['PRSN_RCD_KODS']; ?></td>
                <td align="left"><?= $row['PRSN_VARDS_UZVARDS']; ?></td>
                <td align="center"><?= $row['PAMATSTUNDAS']; ?></td>
                <td align="center"><?= $row['VIRSSTUNDAS']; ?></td>
		<td align="center"><?= $row['LAIKSCELA']; ?></td>


			</tr>
<?
		}
?>
        <tr>
             <td align="right" colspan="3"><b><?=text::get('TOTAL');?>:</b></td>
             <td align="center"><b><?= $pamatstundas; ?></b></td>
             <td align="center"><b><?= $virsstundas; ?></b></td>
	          <td align="center"><b><?= $laikscela; ?></b></td>

        </tr>
<?
	}
?>
	</tbody>
</table>

</body>