﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);

define ('COLUMN_SUBGROUP_CODE','t.TRNS_APAKSGRUPAS_KODS');

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
	$searchMode = reqVar::get('isSearch');
	// top frame
    if($searchMode)
	{
        $oLink=new urlQuery();
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.5');
    	$oLink->addPrm('isSearch', 1 );
    	// form inicialization
    	$oForm = new Form('frmMain','post',$oLink->getQuery());
    	unset($oLink);

        $oLink=new urlQuery();
	    $oLink->addPrm('isReturn', '1');
        $oLink->addPrm('isSearch', 1 );
	    $oLink->addPrm(FORM_ID, 'f.rpt.s.5');
	    $criteriaLink=$oLink ->getQuery();
	    unset($oLink);

        // if operation
    	if ($oForm -> isFormSubmitted())
    	{
            $oListLink=new urlQuery();
            $oListLink->addPrm(FORM_ID, 'f.rpt.s.5');
        }
        $searchTitle = text::toUpper(text::toUpper(text::get('REPORT_TRANSPORT_GROUP')));
        $searchName = 'REPORT_TRANSPORT_GROUP';
        include('f.akt.m.2.inc');
	}
	// bottom frale
	else
	{
        // Check if this form was requested via xmlHttpRequest and sort
    	if (isset($_GET['xml']) && isset($_GET['sort_k']) && isset($_GET['sort_o']))
    	{
    		ob_clean();
    		$o = $_GET['sort_o'];
            if (isset($o))
    		{
    			echo 'o = "'.$o.'";';
    		}
            else
            {
    			echo 'o = "";';
    		}
            $k = $_GET['sort_k'];
            if ($k)
    		{
    			echo 'k = "'.$k.'";';
    		}
            else
            {
    			echo 'k = "";';
    		}
		    exit;
    	}

	    $sCriteria = reqVar::get('search');
        $sOrder = reqVar::get('sortOrder');

        $sortColumn = (reqVar::get('kol') && reqVar::get('kol') != '') ? reqVar::get('kol') : false;
        $sortOrder = (reqVar::get('order') && reqVar::get('order') != '') ? reqVar::get('order') : false;
        $orderString = 'TRNS_APAKSGRUPAS_KODS';

        if($sortColumn !== false && $sortOrder !== false)
        {
            $orderString = $sortColumn.' '.$sortOrder;
        }

        // URL to search Zinojums by number.
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.s.5');
    	$oLink->addPrm('search', $sCriteria);
        $oLink->addPrm('sortOrder', $orderString);
        $searchLink = $oLink -> getQuery();

        // URL  export
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.e.5');
    	$oLink->addPrm('search', $sCriteria);
        $oLink->addPrm('sortOrder', $orderString);
        $oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $exportUrl =  $oLink ->getQuery();
        unset($oLink);

        // URL  print
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.p.5');
    	$oLink->addPrm('search', $sCriteria);
        $oLink->addPrm('sortOrder', $orderString);
        //$oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $printUrl =  $oLink ->getQuery();
        unset($oLink);

		// gel list of users
		$res = dbProc::getTransportByGroupList($sCriteria, $orderString);
        $km = 0;
        $stundas = 0;
        $darbaStundas = 0;
        if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
               $km = $km + $row['KM'];
               $stundas = $stundas + $row['STUNDAS'];
               $darbaStundas = $darbaStundas + $row['DARBA_STUNDAS'];
			}
		}
        $km = number_format($km,2, '.', '');
        $stundas = number_format($stundas,2, '.', '');
        $darbaStundas = number_format($darbaStundas, 2, '.', '');

		include('f.rpt.s.5.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
