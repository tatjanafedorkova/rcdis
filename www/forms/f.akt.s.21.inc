﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);
// act ID
$actId  = reqVar::get('actId');
// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
    // inicial state: actId=0;
  if($actId != false)
  {
  	// get info about act
  	$actInfo = dbProc::getActInfo($actId);
  	//print_r($actInfo);
  	if(count($actInfo)>0)
  	{
  		$act = $actInfo[0];
       	// get info about work
        $aWorkDb=dbProc::getActRfcWorkList($actId);
        $workTotal = 0;
        foreach ($aWorkDb as $i => $work)
        {
           $workTotal +=  $work['RCFD_STUNDAS'];
        }
        $workTotal = number_format($workTotal, 2);


    }
  }


   include('f.akt.s.21.tpl');
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
