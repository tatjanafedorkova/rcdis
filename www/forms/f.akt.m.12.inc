<?
         // get user info
      	$userInfo = dbProc::getUserInfo($userId);
        $isEconomist = dbProc::isUserInRole($userId, ROLE_ECONOMIST);
        $isAdmin=userAuthorization::isAdmin();

        if(count($userInfo) >0 )
      	{
      		$user = $userInfo[0];
       	}

                 // get DV area list
         $dvarea = array();
         if(isset($user['RLTT_KDVI_ID']) )
         {
            $dvarea[$user['RLTT_KDVI_ID']] = dbProc::getDVAreaName($user['RLTT_KDVI_ID']);
         }
         elseif(isset($user['RLTT_REGIONS']) && isset($user['RLTT_ROLE']) && $user['RLTT_ROLE'] != ROLE_ED_USER )
         {
            $res=dbProc::getDVAreaForRegion($user['RLTT_REGIONS'], false);
			if (is_array($res))
			{
			    $dvarea['-1']=text::get('ALL');
				foreach ($res as $row)
				{
					$dvarea[$row['KDVI_ID']]=$row['KDVI_REGIONS'].$row['KDVI_KODS'].' : '.$row['KDVI_NOSAUKUMS'];
				}
			}
         }
         else
         {
            $res=dbProc::getDVAreaList(false,false,false,false,false,'ASC','`KDVI_REGIONS`, `KDVI_KODS`');
			if (is_array($res))
			{
			    $dvarea['-1']=text::get('ALL');
				foreach ($res as $row)
				{
					$dvarea[$row['KDVI_ID']]=$row['KDVI_REGIONS'].$row['KDVI_KODS'].' : '.$row['KDVI_NOSAUKUMS'];
				}
			}
         }

         // get ED region list
         $edRegion=array();
         if(isset($user['RLTT_REGIONS']) && isset($user['RLTT_ROLE']) && $user['RLTT_ROLE'] == ROLE_ED_USER )
         {
            $edRegion[$user['RLTT_REGIONS']]=$user['RLTT_REGIONS'];
         }
         else
         {
           $edRegion['-1']=text::get('ALL');
           $res=dbProc::getEDRegionList();
           if (is_array($res))
           {
           	foreach ($res as $row)
           	{
           		$edRegion[$row['nosaukums']]=$row['nosaukums'];
           	}
           }
         }
         unset($res);

         // get users list
         $usersList = array();
         if(isset($user['RLTT_KDVI_ID']) && !$isEdUser)
         {
            $res=dbProc::getUserDVAreaList($user['RLTT_KDVI_ID'], false);
			if (is_array($res))
			{
			  $usersList['-1']=text::get('ALL');
			  foreach ($res as $row)
			  {
			  	$usersList[$row['RLTT_ID']]=$row['RLTT_VARDS'].' '.$row['RLTT_UZVARDS'];
			  }
			}
         }
         elseif(isset($user['RLTT_REGIONS']) && !$isEdUser)
         {
            $res=dbProc::getUserRegionList($user['RLTT_REGIONS'], false);
			if (is_array($res))
			{
			  $usersList['-1']=text::get('ALL');
			  foreach ($res as $row)
			  {
			  	$usersList[$row['RLTT_ID']]=$row['RLTT_VARDS'].' '.$row['RLTT_UZVARDS'];
			  }
			}
         }
         else
         {
            $res=dbProc::getUsersList(false,false,false,false,false,'ASC','RLTT_VARDS, RLTT_UZVARDS');
			if (is_array($res))
			{
			  $usersList['-1']=text::get('ALL');
			  foreach ($res as $row)
			  {
			  	$usersList[$row['RLTT_ID']]=$row['RLTT_VARDS'].' '.$row['RLTT_UZVARDS'];
			  }
			}
         }


       $searcCriteria = array();

       // if operation
    	if ($oForm -> isFormSubmitted())
    	{
           $searcCriteria['yearFrom'] = $oForm->getValue('yearFrom');
           $searcCriteria['yearUntil'] = $oForm->getValue('yearUntil');
           $searcCriteria['monthFrom'] = $oForm->getValue('monthFrom');
           $searcCriteria['monthUntil'] = $oForm->getValue('monthUntil');
           $searcCriteria['sourceOfFounds'] = $oForm->getValue('sourceOfFounds');
           $searcCriteria['type'] = $oForm->getValue('type');
           $searcCriteria['object']  = $oForm->getValue('object');
           $searcCriteria['dvArea'] = $oForm->getValue('dvArea');
           $searcCriteria['edRegion'] = $oForm->getValue('edRegion');
           $searcCriteria['ouner'] = $oForm->getValue('ouner');


           $cr = '';
           foreach($searcCriteria as $s)
           {
             $cr .= $s.'^';
           }
           $cr = substr($cr, 0, -1);  
           $oListLink->addPrm('search', $cr);
           $actListLink=$oListLink ->getQuery();
           $oForm->addElement('static','jsRefresh2','','<script>reloadFrame(2, "'.$actListLink.'");</script>');
           unset($oListLink);
    	}
        else
        {

           $searcCriteria['yearFrom'] = dtime::getCurrentYear();
           $searcCriteria['yearUntil'] = dtime::getCurrentYear();
           $searcCriteria['monthFrom'] = dtime::getCurrentMonth();
           $searcCriteria['monthUntil'] = dtime::getCurrentMonth();
           $searcCriteria['sourceOfFounds'] = '-1';
           $searcCriteria['type'] = '-1';
           $searcCriteria['object'] = '-1';
           $searcCriteria['dvArea'] = $user['RLTT_KDVI_ID'];
           $searcCriteria['edRegion'] = '-1';
           $searcCriteria['ouner'] = (($isEconomist) ? -1 : $userId);
        }

        $oForm -> addElement('label', 'year',  text::get('YEAR'), '');
        $oForm -> addElement('select', 'yearFrom',  text::get('FROM'), $searcCriteria['yearFrom'], 'style="width:50;"', '', '', dtime::getYearArray());
        $oForm -> addElement('select', 'yearUntil',  text::get('UNTIL'), $searcCriteria['yearUntil'], 'style="width:50;"', '', '', dtime::getYearArray());

        $oForm -> addElement('label', 'month',  text::get('MONTH'), '');
        $oForm -> addElement('select', 'monthFrom',  text::get('FROM'), $searcCriteria['monthFrom'], 'style="width:70;"', '', '', dtime::getMonthArray());
        $oForm -> addElement('select', 'monthUntil',  text::get('UNTIL'), $searcCriteria['monthUntil'], 'style="width:70;"', '', '', dtime::getMonthArray());

        $oForm -> addElement('kls2', 'sourceOfFounds',  text::get('SINGLE_SOURCE_OF_FOUNDS'), array('classifName'=>KL_SOURCE_OF_FOUNDS,'value'=>$searcCriteria['sourceOfFounds'],'readonly'=>false), 'maxlength="2000"');

        $oForm -> addElement('kls2', 'type',  text::get('SINGLE_ACT_TYPE'), array('classifName'=>KL_ACT_TYPE,'value'=>$searcCriteria['type'],'readonly'=>false), 'maxlength="2000"');

        $oForm -> addElement('kls2', 'object',  text::get('SINGLE_OBJECT'), array('classifName'=>KL_OBJECTS,'value'=>$searcCriteria['object'],'readonly'=>false), 'maxlength="2000"');

        $oForm -> addElement('select', 'dvArea',  text::get('SINGL_DV_AREA'), $searcCriteria['dvArea'], '', '', '', $dvarea);

        $oForm -> addElement('select', 'ouner',  text::get('ACT_OUNER'), $searcCriteria['ouner'], '', '', '', $usersList);

        $oForm -> addElement('select', 'edRegion',  text::get('ED_REGION'), $searcCriteria['edRegion'], '', '', '', $edRegion);


       	// form buttons
       	$oForm -> addElement('submitImg', 'search', text::get('SEARCH'), 'img/btn_meklet.gif', 'width="70" height="20" onClick="setPosition1(\'loading\'); return true;"');

        $oForm -> makeHtml();
		include('f.akt.m.12.tpl');
?>