<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
     $sCriteria = reqVar::get('search');

       	// gel list of users
		$res = dbProc::getActWithMMSGroup($sCriteria);
        $area = array();

        $summa = 0;
        $stundas = 0;

        if (is_array($res))
		{
            foreach ($res as $i=>$row)
			{


               $summa = $summa + $row['CENA_KOPA'];
               $stundas = $stundas + $row['CILVEKSTUNDAS'];

            }
        }
        $summa = number_format($summa, 2, '.', '');
        $stundas = number_format($stundas,2, '.', '');


        $searchCr = array();
        $sCr = explode("^", $sCriteria);
        if(isset($sCr[0]) && $sCr[0] != -1  && isset($sCr[1]) && $sCr[1] != -1)
        {
           $searchCr[] = array('label' =>  text::get('YEAR'), 'value' => $sCr[0].'-'.$sCr[1]);
        }
         if(isset($sCr[2]) && $sCr[2] != -1  && isset($sCr[3]) && $sCr[3] != -1)
        {
           $searchCr[] = array('label' =>  text::get('MONTH'), 'value' => dtime::getMonthName($sCr[2]).'-'.dtime::getMonthName($sCr[3]));
        }
        if(isset($sCr[4]) && $sCr[4] != '')
        {
          // get info about calculation group
            $favoritInfo = dbProc::getFavoritDefinition($sCr[4]);
            if(count($favoritInfo)>0)
            {
            	$favorit = $favoritInfo[0];
                $favoritTitle = $favorit['LTFV_NOSAUKUMS'];
            }
           $searchCr[] = array('label' =>  text::get('SINGLE_MMS'), 'value' => $favoritTitle);
        }
		include('f.rpt.p.18.tpl');
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
