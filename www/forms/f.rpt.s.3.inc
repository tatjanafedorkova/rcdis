<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
	$searchMode = reqVar::get('isSearch');
	// top frame
    if($searchMode)
	{
        $oLink=new urlQuery();
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.3');
    	$oLink->addPrm('isSearch', 1 );
    	// form inicialization
    	$oForm = new Form('frmMain','post',$oLink->getQuery());
    	unset($oLink);

        $oLink=new urlQuery();
	    $oLink->addPrm('isReturn', '1');
        $oLink->addPrm('isSearch', 1 );
	    $oLink->addPrm(FORM_ID, 'f.rpt.s.3');
	    $criteriaLink=$oLink ->getQuery();
	    unset($oLink);

        // if operation
    	if ($oForm -> isFormSubmitted())
    	{
            $oListLink=new urlQuery();
            $oListLink->addPrm(FORM_ID, 'f.rpt.s.3');
        }
        $searchTitle = text::toUpper(text::toUpper(text::get('REPORT_CUSTOMER_WORK_TIME_DETALS')));
        $searchName = 'REPORT_CUSTOMER_WORK_TIME_DETALS';
        include('f.akt.m.1.inc');
	}
	// bottom frale
	else
	{
	    $sCriteria = reqVar::get('search');

        // URL  export
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.e.3');
    	$oLink->addPrm('search', $sCriteria);
        $oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $exportUrl =  $oLink ->getQuery();
        unset($oLink);

        // URL  print
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.p.3');
    	$oLink->addPrm('search', $sCriteria);
        //$oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $printUrl =  $oLink ->getQuery();
        unset($oLink);

		// gel list of users
		$res = dbProc::getCustomerWorkTimeDescriptionList($sCriteria);
        $customer = array();
        if (is_array($res))
		{

			foreach ($res as $i=>$row)
			{

               $customer[$row['PRSN_RCD_KODS']]  = array (
                'code' => $row['PRSN_RCD_KODS'],
                'workPlace' => $row['PRSN_DARBA_VIETA'],
                'name' => $row['PRSN_VARDS_UZVARDS'],
                'totalMainTime' => (isset($customer[$row['PRSN_RCD_KODS']]['totalMainTime'])?$customer[$row['PRSN_RCD_KODS']]['totalMainTime'] : 0) + $row['PRSN_PAMATSTUNDAS'],
                'totalOverTime' => (isset($customer[$row['PRSN_RCD_KODS']]['totalOverTime'])?$customer[$row['PRSN_RCD_KODS']]['totalOverTime'] : 0) + $row['PRSN_VIRSSTUNDAS'],
	                        'totalRoutTime' => (isset($customer[$row['PRSN_RCD_KODS']]['totalRoutTime'])?$customer[$row['PRSN_RCD_KODS']]['totalRoutTime'] : 0) + $row['PRSN_LAIKSCELA'],

                'act' => array()
               );
            }
            foreach ($res as $i=>$row)
			{

               $customer[$row['PRSN_RCD_KODS']]['act'][$row['PRSN_RAKT_ID']]  = array(
                    'mainTime' => $row['PRSN_PAMATSTUNDAS'],
                    'overTime' => $row['PRSN_VIRSSTUNDAS'],
		    'routTime' => $row['PRSN_LAIKSCELA'],
                    'actNumber' => $row['NUMURS'],
                     'actPostfix' => $row['POSTFIX'],
                    'designation' => $row['RAKT_OPERATIVAS_APZIM'],
                    'founds' => $row['KFNA_NOSAUKUMS'],
                    'description' => $row['RAKT_PIEZIMES'],
                    'status' => $row['STATUS_NAME'],
                    'isFinished' => $row['RAKT_IR_OBJEKTS_PABEIGTS']

               );
            }
		}
       /* echo "<pre>";
        print_r($customer);
        echo "</pre>"; */
		include('f.rpt.s.3.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
