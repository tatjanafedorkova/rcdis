﻿<body class="frame_2">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><h1><?= text::toUpper(text::get('REPORT_MAIN_ACT')); ?></h1></td>
		<td align="right">
            <img src="img/ico_print.gif" alt="" width="16" height="16" border="0">&nbsp;
            <a href="<?= $printUrl; ?>" target="new"><?=text::get('PRINT');?></a>&nbsp;&nbsp;
            <img src="img/ico_excel.gif" alt="" width="16" height="16">&nbsp;
            <a href="<?= $exportUrl; ?>" ><?=text::get('EXPORT');?></a>&nbsp;
        </td>
	</tr>
</table>

<table cellpadding="5" cellspacing="1" border="0" width="100%" >
	<thead>
		<tr class="table_head_2">
			<td width="10%"><?= text::get('ACT_NUMBER'); ?></td>
			<td width="10%"><?= text::get('SINGLE_ACT_TYPE'); ?></td>
            <td width="10%"><?=text::get('STATUS');?></td>
            <td width="5%"><?=text::get('TRANSPORT_KM');?><br /></td>
            <td width="5%"><?=text::get('TRANSPORT_AVTO_TIME');?></td>
            <td width="5%"><?= text::get('TRANSPORT_WORK_TIME'); ?></td>
			<td width="15%"><?= text::get('SINGLE_MMS'); ?></td>
            <td width="5%"><?= text::get('OBJECT_IS_FINISHED'); ?></td>
            <td width="5%"><?=text::get('PERSONAL_COUNT');?></td>
            <td width="5%"><?=text::get('BASE_TIME');?></td>
            <td width="5%"><?=text::get('OVER_TIME');?></td>
            <td width="5%"><?= text::get('REPORT_MATERIAL_PRICE_TOTAL'); ?></td>
            <td width="15%" ><?=text::get('SINGL_DV_AREA');?></td>
 		</tr>
	</thead>
	<tbody>
<?
	if (is_array($res) && count($res) > 0)
	{
		foreach ($res as $row)
		{
?>
			<tr class="table_cell_3" >
			   	<td align="center"><?= $row['NUMURS']; ?></td>
                <td align="left"><?= $row['TYPE']; ?></td>
                <td align="left"><?= $row['STATUS_NAME']; ?></td>
                <td align="center"><?= $row['KM']; ?></td>
                <td align="center"><?= $row['STUNDAS']; ?></td>
                <td align="center"><?= $row['DARBA_STUNDAS']; ?></td>
                <td align="left"><?= $row['KMSD_KODS']; ?></td>
                <td align="center"><?= (($row['RAKT_IR_OBJEKTS_PABEIGTS'] == 1) ? text::get('YES') : text::get('NO')); ?></td>
                <td align="center"><?= $row['CILVEKI']; ?></td>
                <td align="center"><?= $row['PAMATSTUNDAS']; ?></td>
                <td align="center"><?= $row['VIRSSTUNDAS']; ?></td>
                <td align="center"><?= $row['CENA_KOPA']; ?></td>
                <td align="center"><?= $row['DV']; ?></td>
			</tr>
<?
		}
?>
        <tr>
             <td align="right" colspan="3"><b><?=text::get('TOTAL');?>:</b></td>
             <td align="center"><b><?= $km; ?></b></td>
             <td align="center"><b><?= $stundas; ?></b></td>
             <td align="center"><b><?= $darbaStundas; ?></b></td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td align="center"><b><?= $cilveki; ?></b></td>
             <td align="center"><b><?= $pamatstundas; ?></b></td>
             <td align="center"><b><?= $virsstundas; ?></b></td>
             <td align="center"><b><?= $summa; ?></b></td>
        </tr>
<?
	}
?>
	</tbody>
</table>

</body>