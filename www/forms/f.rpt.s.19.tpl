﻿<body class="frame_2">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><h1><?= text::toUpper(text::get('REPORT_CALCULATION_PLAN_NPLAN')); ?></h1></td>
		<td align="right">
            <img src="img/ico_print.gif" alt="" width="16" height="16" border="0">&nbsp;
            <a href="<?= $printUrl; ?>" target="new"><?=text::get('PRINT');?></a>&nbsp;&nbsp;
            <img src="img/ico_excel.gif" alt="" width="16" height="16">&nbsp;
            <a href="<?= $exportUrl; ?>" ><?=text::get('EXPORT');?></a>&nbsp;
        </td>
	</tr>
</table>

<table cellpadding="5" cellspacing="1" border="0" width="100%" >
	<thead>
		<tr class="table_head_2">
            <td width="10%" ><?= text::get('CHIPHER'); ?></td>
			<td width="30%" ><?= text::get('NAME'); ?></td>
            <td width="12%" ><?=text::get('UNIT_OF_MEASURE');?></td>
            <td width="12%" ><?=text::get('AMOUNT_PLAN');?></td>
            <td width="12%" ><?=text::get('BASE_TIME_PLAN');?></td>
            <td width="12%" ><?=text::get('AMOUNT_NOT_PLAN');?></td>
		<td width="12%" ><?=text::get('BASE_TIME_NOT_PLAN');?></td>

 		</tr>
	</thead>
	<tbody>
<?
	if (is_array($res) && count($res) > 0)
	{
		foreach ($res as $row)
		{
?>
			<tr class="table_cell_3" >
                <td align="center"><?= $row['DRBI_KKAL_SHIFRS']; ?></td>
                <td align="left" ><?= $row['DRBI_KKAL_NOSAUKUMS']; ?></td>
                <td align="center"><?= $row['DRBI_MERVIENIBA']; ?></td>
                <td align="center"><?= number_format($row['DAUDZUMS_PLAN'],2, '.', ''); ?></td>
                <td align="center"><?= number_format($row['PAMATSTUNDAS_PLAN'],2, '.', ''); ?></td>
                <td align="center"><?= number_format($row['DAUDZUMS_NOT_PLAN'],2, '.', ''); ?></td>
		<td align="center"><?= number_format($row['PAMATSTUNDAS_NOT_PLAN'],2, '.', ''); ?></td>

			</tr>
<?
		}
?>
        <tr>
             <td align="right"  colspan="4"><b><?=text::get('TOTAL');?></b>:</td>
             <td align="center"><b><?= $baseTimeP; ?></b></td>
		<td align="center">&nbsp;</td>
             <td align="center"><b><?= $baseTimeNP; ?></b></td>
             <td>&nbsp;</td>
       </tr>
<?
	}
?>
	</tbody>
</table>

</body>