﻿<body class="frame_1">
<?=$oForm->getElementHtml('jsRefresh2');?>
<h1><?=$searchTitle;?></h1>
<?= $oForm -> getFormHeader(); ?>
<table cellpadding="3" cellspacing="1" border="0" width="100%">

	<tr>
		<td align=center colspan="6"><?= $oForm -> getMessage(); ?></td>
    </tr>

   <tr>
        <td class="table_cell_c" ><?= $oForm -> getElementLabel('plan_date'); ?>:</td>
        <td class="table_cell_2" >
            <table cellpadding="1" cellspacing="1" border="0" width="100%">
            <tr>
                <td><?= $oForm -> getElementLabel('planDateFrom'); ?></td>
                <td><?= $oForm -> getElementHtml('planDateFrom'); ?></td>
                <td><?= $oForm -> getElementLabel('planDateUntil'); ?></td>
                <td><?= $oForm -> getElementHtml('planDateUntil'); ?></td>
            </tr>
            </table>
        </td>
        <? if($isSearchForm) { ?>
            <td class="table_cell_c" width="10%"><?= $oForm -> getElementLabel('designation'); ?>:</td>
		    <td class="table_cell_2" width="23%"><?= $oForm -> getElementHtml('designation'); ?></td>
        <? } else { ?>
            <td class="table_cell_c" width="10%">&nbsp;</td>
    		<td class="table_cell_2" width="23%">&nbsp;</td>
        <? } ?>
        <td class="table_cell_c" width="10%"><?= $oForm -> getElementLabel('ouner'); ?>:</td>
		<td class="table_cell_2" width="27%"><?= $oForm -> getElementHtml('ouner'); ?></td>
   </tr>
   <tr>
        <td class="table_cell_c" ><?= $oForm -> getElementLabel('work_aprove_date'); ?>:</td>
        <td class="table_cell_2" >
            <table cellpadding="1" cellspacing="1" border="0" width="100%">
            <tr>
                <td><?= $oForm -> getElementLabel('workAproveDateFrom'); ?></td>
                <td><?= $oForm -> getElementHtml('workAproveDateFrom'); ?></td>
                <td><?= $oForm -> getElementLabel('workAproveDateUntil'); ?></td>
                <td><?= $oForm -> getElementHtml('workAproveDateUntil'); ?></td>
            </tr>
            </table>
        </td>

        <td class="table_cell_c"><?= $oForm -> getElementLabel('rcdRegion'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('rcdRegion'); ?></td>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('dvArea'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('dvArea'); ?></td>
   	</tr>
    <tr>
         <td class="table_cell_c"><?= $oForm -> getElementLabel('sourceOfFounds'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('sourceOfFounds'); ?></td>

        <td class="table_cell_c"><?= $oForm -> getElementLabel('edRegion'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('edRegion'); ?></td>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('edArea'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('edArea'); ?></td>
   	</tr>
    <tr>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('voltage'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('voltage'); ?></td>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('section'); ?>:</td>
        <td class="table_cell_2"><?= $oForm -> getElementHtml('section'); ?></td>        
        <td class="table_cell_c" rowspan="3"><?= $oForm -> getElementLabel('status'); ?>:</td>
		<td class="table_cell_2" rowspan="3"><?= $oForm -> getElementHtml('status'); ?></td>
   	</tr>

    <tr>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('type'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('type'); ?></td>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('isFinished'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('isFinished'); ?></td>
    </tr>
    <tr>
        <?  
        if($searchName == 'REPORT_MATERIALS_IN_MONTH')
        {
        ?>
            <td class="table_cell_c"><?= $oForm -> getElementLabel('object'); ?></td>
            <td class="table_cell_2"><?= $oForm -> getElementHtml('object'); ?></td>
        <? }
        else 
        {
        ?>   
            <td class="table_cell_c">&nbsp;</td>
            <td class="table_cell_2">&nbsp;</td>
        <?
        }
        ?>
        <td class="table_cell_c"><?= $oForm -> getElementLabel('isepla'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('isepla'); ?></td>
    </tr>
	<tr>
        <td colspan="2">&nbsp;</td>
		<td colspan="2" align="right">
        <span id="loading" style="position:absolute; width:32; height:32; margin-left:30px; display: none; ">
        <img src="./img/loading.gif" widht="32" height="32" border="0" />
        </span><?=$oForm->getElementHtml('search');?></td>
        <td colspan="2"><a href="#" onClick="reloadFrame(1,'<?= $criteriaLink; ?>');reloadFrame(23,'');"><?=text::get('RETURN_TO_SEARCH_CRRITERIA');?></a></td>
    </tr>
</table>

<?= $oForm -> getFormBottom(); ?>
</body>
