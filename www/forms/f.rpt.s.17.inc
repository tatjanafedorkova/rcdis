﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);
function getActLinks($mms)
{
    $result = '';
    $res = dbProc::detAllActsForMms($mms);
    if (is_array($res))
    {
        foreach ($res as $i=>$row)
	    {
              $oLinkFile=new urlQuery();
              $oLinkFile->addPrm(IS_GET_FILE_VARIABLE, 1);
              $oLinkFile->addPrm(GET_FILE_VARIABLE,$row['RFLS_ID']);
              $result = $result . "<a href=".$oLinkFile ->getQuery().">".$row['ACT_NR']."</a><br>";
              unset($oLinkFile);
        }
    }

    return $result;
}
// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
	$searchMode = reqVar::get('isSearch');
	// top frame
    if($searchMode)
	{
        $oLink=new urlQuery();
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.17');
    	$oLink->addPrm('isSearch', 1 );
    	// form inicialization
    	$oForm = new Form('frmMain','post',$oLink->getQuery());
    	unset($oLink);

        $oLink=new urlQuery();
	    $oLink->addPrm('isReturn', '1');
        $oLink->addPrm('isSearch', 1 );
	    $oLink->addPrm(FORM_ID, 'f.rpt.s.17');
	    $criteriaLink=$oLink ->getQuery();
	    unset($oLink);

        // if operation
    	if ($oForm -> isFormSubmitted())
    	{
            $oListLink=new urlQuery();
            $oListLink->addPrm(FORM_ID, 'f.rpt.s.17');
        }
        $searchTitle = text::toUpper(text::toUpper(text::get('REPORT_FINISHED_WORK')));
        $searchName = 'REPORT_FINISHED_WORK';
        $notPlan = true;
        include('f.akt.m.10.inc');
	}
	// bottom frale
	else
	{
        $sCriteria = reqVar::get('search');

        /*// URL  export
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.e.16');
    	$oLink->addPrm('search', $sCriteria);
        $oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $exportUrl =  $oLink ->getQuery();
        unset($oLink); */

        // URL  print
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.p.17');
    	$oLink->addPrm('search', $sCriteria);
        //$oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $printUrl =  $oLink ->getQuery();
        unset($oLink);

		// gel list of users
		$res = dbProc::getMMSFinishedWorksList($sCriteria);
        $area = array();

        $summa = 0;
        $stundas = 0;

        if (is_array($res))
		{
            foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]  = array (
                'section' => $row['KEDI_SECTION'],
                'totalStundas' => (isset($area[$row['KEDI_SECTION']]['totalStundas'])?$area[$row['KEDI_SECTION']]['totalStundas']:0) + $row['MMS_CILVEKSTUNDAS'],
                'totalCena' => (isset($area[$row['KEDI_SECTION']]['totalCena'])?$area[$row['KEDI_SECTION']]['totalCena']:0) + $row['MMS_MATR_IZMAKSAS'],
                'ed' => array()
               );

               $summa = $summa + $row['MMS_MATR_IZMAKSAS'];
               $stundas = $stundas + $row['MMS_CILVEKSTUNDAS'];

            }
            foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]  = array (
                'code' => $row['KEDI_KODS'],
                'name' => $row['ED'],
                'totalStundas' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalStundas'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalStundas']:0) + $row['MMS_CILVEKSTUNDAS'],
                'totalCena' => (isset($area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalCena'])?$area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['totalCena']:0) + $row['MMS_MATR_IZMAKSAS'],
                'mms' => array()
               );


            }
            //Popup saites sagatave

            foreach ($res as $i=>$row)
			{

               $area[$row['KEDI_SECTION']]['ed'][$row['KEDI_KODS']]['mms'][$row['KMSD_KODS']]  = array(
                    'code' => $row['KMSD_KODS'],
                    'quarter' => $row['KMSD_CETUKSNIS'],
                    'stundas' => $row['MMS_CILVEKSTUNDAS'],
                    'cena' => $row['MMS_MATR_IZMAKSAS'],
                    'priority' => $row['KMSD_PRIORITATE'] ,
                    'act' => getActLinks($row['KMSD_ID'])

               );

            }
		}

        $summa = number_format($summa, 2, '.', '');
        $stundas = number_format($stundas,2, '.', '');



		include('f.rpt.s.17.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
