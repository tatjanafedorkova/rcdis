<body class="head">
<?= $oForm -> getFormHeader(); ?>
<table cellpadding="0" cellspacing="0" border="0" align="center" height="100%" width="600">
<tr><td  height="15%">&nbsp;</td></tr>
<tr><td  height="15%" align="center"><img src="/img/rcf.png" alt="<?=text::get('APPLICATION_NAME');?>" width="182" height="85" class="block">
</td></tr>
<tr><td  height="5%" align="center"><?= $oForm -> getErrors(); ?></td></tr>
<tr><td height="20%" >
<table cellpadding="1" cellspacing="0" border="0" align="center"  width="35%" bgcolor="#153E7E" >
    <tr><td>
    <table cellpadding="3" cellspacing="0" border="0" align="center"  width="100%" bgcolor="#FAF9F3" >
        <tr>
    		<td><?= $oForm -> getElementLabel('login') ?>:</td>
        </tr>
    	<tr>
    		<td><?= $oForm -> getElementHtml('login'); ?></td>
       	</tr>
    	<tr>
    		<td><?= $oForm -> getElementLabel('password'); ?>:</td>
        </tr>
    	<tr>
    		<td><?= $oForm -> getElementHtml('password'); ?></td>
        </tr>
    	<tr>
    		<td><?= $oForm -> getElementHtml('submit'); ?> &nbsp;<?= $oForm -> getElementLabel('curYearVersion'); ?></td>
    	</tr>
    </table>
   </td></tr>
</table>
</td></tr>
<tr><td  height="15%" align="center"><img src="/img/st.gif" alt="" width="114" height="48" hspace="15" vspace="15" class="block"></td></tr>
<tr><td height="25%">
<table cellpadding="0" cellspacing="3" border="0" align="center" >
	<tr>
		<td width="41"><?= $oForm -> getElementHtml('warningIcon'); ?>&nbsp;</td>
        <td><?= $oForm -> getElementHtml('warning'); ?>&nbsp;</td>
	</tr>
</table>
</td></tr>
<tr><td height="5%">
<table cellpadding="0" cellspacing="3" border="0" align="center"  color="#666666">
	<tr>
		<td>
        <? if($year != $linkYear2010){  ?>
        <a href="<?= $year2010Url ?>" style="color: #666666;"><?= $oForm -> getElementLabel('yearVersion2010'); ?></a>&nbsp;|&nbsp;
        <? } ?>
        <? if($year != $linkYear2011){  ?>
        <a href="<?= $year2011Url ?>" style="color: #666666;"><?= $oForm -> getElementLabel('yearVersion2011'); ?></a>&nbsp;|&nbsp;
        <? } ?>
        <? if($year != $linkYear2012){  ?>
        <a href="<?= $year2012Url ?>" style="color: #666666;"><?= $oForm -> getElementLabel('yearVersion2012'); ?></a>&nbsp;|&nbsp;
         <? } ?>
        </td>
        <td><a href="" target="new" style="color: #666666;"><?= $oForm -> getElementLabel('instruction'); ?></a>&nbsp;|&nbsp;</td>
        <td><a href="" target="new" style="color: #666666;"><?= $oForm -> getElementLabel('problem'); ?></a></td>
	</tr>
</table>
</td></tr>
</table>
<?= $oForm -> getFormBottom(); ?>
<script language="JavaScript">
if(document.all['login'].value != '')
{
	document.all['password'].focus();
}
else
{
	document.all['login'].focus();
}
var openInFrame = this.name;
if((openInFrame == 'frameTop')||(openInFrame == 'frame_1')||(openInFrame == 'frame_2')||(openInFrame == 'frameButtons') )  
{
	parent.location.reload();
}
</script>
</body>