﻿<script type="text/javascript">

	function savedField (el, color) { 
		el.style.borderColor = color; 
		parent = el.parentNode;
		
		if(parent.firstChild.nodeName == 'SPAN') {
			parent.removeChild(sp1);
		}
		//parent.removeChild(parent.childNodes[0]); 
	}
	// succsess
	var sp1 = document.createElement('span');
	sp1.append('Saglabāts');
	sp1.style.color = 'green';
	// error
	var sp2 = document.createElement('span');
	sp2.append('<?=text::get('ERROR_DOUBLE_VALUE');?>');
	sp2.style.color = 'red';
	
	
</script>
<?= $oFormWorkTab -> getFormHeader(); ?>
<table cellpadding="5" cellspacing="1" border="0" width="100%">
	<tr>
		<td align=center colspan="2"><?= $oFormWorkTab -> getMessage(); ?></td>
	</tr>
	<tr>
        <td align=center>
        <table width="100%">
            <tr>
                <td><h1><?=text::toUpper(text::get(TAB_WORKS));?></h1></td>
                <? if($isEpla == 1) {   ?>
                    <td>
                    <table cellpadding="5" cellspacing="1" border="0" width="100%">
                    <tr>
						<td width="12%" class="table_cell_c"><?= $oFormWorkTab-> getElementLabel('sum_m3'); ?></td>
						<td width="13%" class="table_cell_2"><?= $oFormWorkTab-> getElementHtml('sum_m3'); ?></td>
						<td width="12%" class="table_cell_c"><?= $oFormWorkTab-> getElementLabel('sum_ha'); ?></td>
						<td width="13%" class="table_cell_2"><?= $oFormWorkTab-> getElementHtml('sum_ha'); ?></td>
						<td width="12%" class="table_cell_c"><?= $oFormWorkTab-> getElementLabel('sum_km'); ?></td>
						<td width="13%" class="table_cell_2"><?= $oFormWorkTab-> getElementHtml('sum_km'); ?></td>
						<td width="12%" class="table_cell_c"><?= $oFormWorkTab-> getElementLabel('sum_gab'); ?></td>
						<td width="13%" class="table_cell_2"><?= $oFormWorkTab-> getElementHtml('sum_gab'); ?></td>
                    </tr>
                    </table>
                    </td>
                    <? } ?>
            </tr>
        </table>
        </td>
        <td></td>
    </tr>
    <tr>
		<td valign="top">
		<?
		$x = 0;
		foreach ($aWorks as $d => $aDayWorks)
      	{
		?>
        <table cellpadding="5" cellspacing="1" border="0" width="100%">
		   <tr class="table_head_2">
				
				<th width="10%"><?=text::get('CHIPHER');?></th>
				<? if(!$tame) { ?>
					<th  width="12%"><?=text::get('FINISHING_DATE');?></th>
					<th  width="5%"><?=text::get('BRIGADE');?></th>
				<? } ?>
				<th width="32%"><?=text::get('NAME');?></th>
				<th  width="15%"><?=text::get('MAN_HOUR_STANDART');?></th>
                <th  width="10%"><?=text::get('UNIT_OF_MEASURE');?></th>
			    <th  width="10%"><?=text::get('AMOUNT');?></th>
			   <? if($tame && !$isInvestition) { ?>
			   		<th  width="10%"><?=text::get('AMOUNT_KOR');?></th>
			   <? } ?>

				<th  width="15%"><?=text::get('MAN_HOUR_STANDART_TOTAL');?></th>
				<? if($tame && $isInvestition) { ?>
					<th  width="10%"><?=text::get('INVESTICIJAS_PRICE');?></th>
					<th  width="15%"><?=text::get('COAST_TOTAL');?></th>
				<? } ?>
			 	<th width="3%">&nbsp;</th>
			</tr>

	  	<?
	  	foreach ($aDayWorks as $j => $actWork)
		{ 
			
		?>
			<tr class="table_cell_3"  id="blank_work<?= $x; ?>">

				<td><?= $oFormWorkTab->getElementHtml('chipher['.$x.']'); ?></td>
				<? if(!$tame) { ?>
					<td><?= $oFormWorkTab->getElementHtml('plan_date['.$x.']'); ?></td>	
					<td><?= $oFormWorkTab->getElementHtml('brigade['.$x.']'); ?></td>					
				<? }  ?>				
				<td><?= $oFormWorkTab->getElementHtml('title['.$x.']'); ?></td>
				<td><?= $oFormWorkTab->getElementHtml('standart['.$x.']'); ?></td>
                <td><?= $oFormWorkTab->getElementHtml('measure['.$x.']'); ?></td>
				
				<? if($tame && !$isInvestition) { ?>
					<td><?= $oFormWorkTab->getElementHtml('amount_k['.$x.']'); ?></td>
					<td><?= $oFormWorkTab->getElementHtml('amount_kor['.$x.']'); ?></td>
				<? } else { ?>
					<td><?= $oFormWorkTab->getElementHtml('amount['.$x.']'); ?></td>
				<? } ?> 
				<td><?= $oFormWorkTab->getElementHtml('total['.$x.']'); ?></td>
				<? if($tame && $isInvestition) { ?>
					<td><?= $oFormWorkTab->getElementHtml('investition_work['.$x.']'); ?></td>
					<td><?= $oFormWorkTab->getElementHtml('total_coast['.$x.']'); ?></td>
				<? } ?>
             	<td>
                    <? if(!$isReadonly  )  { ?>
                   		 <?= $oFormWorkTab->getElementHtml('work_del['.$x.']'); ?>
                    <? } else{ ?>
                    &nbsp;
                    <? } ?>
                </td>
			</tr>
		<?
		$x++;
		}
		?>
        <tr>
             <td align="right" colspan="2"><b><?=text::get('WORK_APPROVE_DATE');?>:</b></td>
	     <td align="left" colspan="2"><b><?=isset($workApproveDate[$d])?$workApproveDate[$d]:''; ?></b></td>
             <td align="right" colspan="<?= (($tame) ? (($isInvestition) ? "1" : "2") : "3") ?>"><b><?=text::get('TOTAL');?>:</b></td>
	     <td align="center"><b><?= isset($totalTime[$d])?$totalTime[$d]:''; ?></b></td>
			 <? if($tame && $isInvestition) { ?>
				<td>&nbsp;</td>
				<td align="center"><b><?= isset($totalCoast[$d])?$totalCoast[$d]:''; ?></b></td>
			<? } ?>
			<td>&nbsp;</td>

		</tr>
		<?
		}
		?>
		<tr class="table_head_2">
			<td align="right" colspan="<?= (($tame) ? (($isInvestition) ? "5" : "6") : "7") ?>"><b><?=text::get('TOTAL');?>:</b></td>
			<td align="center"><b><?= isset($total)?$total:''; ?></b></td>
			<? if($tame && $isInvestition) { ?>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			<? } ?>
			<td>&nbsp;</td>
	   </tr>
	    </table>
        </td>
        <td width="20%"  valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
		<tr>
	        		<td>
			<table cellpadding="3" cellspacing="2" border="0" align="center" width="150">
           			 <? if(!$isReadonly)  {
						if($tame == false) {
							?>
						<tr>
							<td bgcolor="white"><?= $oFormWorkTab->getElementHtml('tameLink'); ?></td>
						</tr>
						<? }
              			foreach($calcGroups as $i=>$val){ ?>
            			 <tr>
                			<td bgcolor="white"><?= $oFormWorkTab->getElementHtml('group['.$i.']'); ?></td>
             			</tr>
            			<? }} ?>
            			</table>
	       		</td>
			<td valign="top">
			<table cellpadding="3" cellspacing="2" border="0" align="center" width="150">
           			 <? if(!$isReadonly)  {
              			foreach( $favorit as $i=>$val){ ?>
            			 <tr>
                			<td bgcolor="white"><?= $oFormWorkTab->getElementHtml('favorit['.$i.']'); ?></td>
             			</tr>
            			<? }} ?>
            			</table>
	       		</td>

                       	</tr>
                                <tr>
			<td colspan="2">
			 <table cellpadding="10" cellspacing="0" border="0" width="150">
           			 <tr>
             			<? if(!$isReadonly)  { ?>
            				<td><?= $oFormWorkTab -> getElementHtml('save'); ?></td>
            			 <? } ?>
            
             			</tr>
           			 </table>
			</td>
		</tr>
                        </table>
            
            <br />
           
        </td>
	</tr>

</table>
<?= $oFormWorkTab -> getFormBottom(); ?>

