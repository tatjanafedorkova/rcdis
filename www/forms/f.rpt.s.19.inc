﻿<?
//created by Tatjana Fedorkova
$userId = userAuthorization::getUserId();
$isAdmin=userAuthorization::isAdmin();
$isSystemUser = dbProc::isExistsUserRole($userId);

// tikai  sistēmas lietotajam vai administrātoram ir pieeja!
if ( $isAdmin || $isSystemUser)
{
	$searchMode = reqVar::get('isSearch');
	// top frame
    if($searchMode)
	{
        $oLink=new urlQuery();
    	$oLink->addPrm(FORM_ID, 'f.rpt.s.19');
    	$oLink->addPrm('isSearch', 1 );
    	// form inicialization
    	$oForm = new Form('frmMain','post',$oLink->getQuery());
    	unset($oLink);

        $oLink=new urlQuery();
	    $oLink->addPrm('isReturn', '1');
        $oLink->addPrm('isSearch', 1 );
	    $oLink->addPrm(FORM_ID, 'f.rpt.s.19');
	    $criteriaLink=$oLink ->getQuery();
	    unset($oLink);

        // if operation
    	if ($oForm -> isFormSubmitted())
    	{
            $oListLink=new urlQuery();
            $oListLink->addPrm(FORM_ID, 'f.rpt.s.19');
        }
        $searchTitle = text::toUpper(text::toUpper(text::get('REPORT_CALCULATION_PLAN_NPLAN')));
        $searchName = 'REPORT_CALCULATION_PLAN_NPLAN';
        include('f.akt.m.1.inc');
	}
	// bottom frale
	else
	{
	    $sCriteria = reqVar::get('search');

        // URL  export
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.e.19');
    	$oLink->addPrm('search', $sCriteria);
        $oLink->addPrm(DONT_USE_GLB_TPL, '1');

        // gel list of calculation
		$res = dbProc::getActCalculationList($sCriteria, false);
        if(count($res) > MAX_EXCEL_ROW_COUNT)
        {
           $exportUrl = "javascript:alert('".text::get('TOO_MATCH_SEARCH_RESULTS')."');" ;
        }
        else
        {
          $exportUrl =  $oLink ->getQuery();
        }

        unset($oLink);

        // URL  print
    	$oLink = new urlQuery;
    	$oLink -> addPrm(FORM_ID, 'f.rpt.p.19');
    	$oLink->addPrm('search', $sCriteria);
        //$oLink->addPrm(DONT_USE_GLB_TPL, '1');
        $printUrl =  $oLink ->getQuery();
        unset($oLink);

        // gel list of users
		$res = dbProc::getActCalculationPlanNplanList($sCriteria);

        $baseTimeP = 0;
        $baseTimeNP = 0;
        if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
               $baseTimeP = $baseTimeP + $row['PAMATSTUNDAS_PLAN'];
               $baseTimeNP = $baseTimeNP + $row['PAMATSTUNDAS_NOT_PLAN'];
			}
		}
        $baseTimeP = number_format($baseTimeP,2, '.', '');
        $baseTimeNP = number_format($baseTimeNP,2, '.', '');

		include('f.rpt.s.19.tpl');
	}
}
else
{
	RequestHandler::showErrorAndDie(text::get('ERROR_NOT_PERMISSION'));
}
?>
