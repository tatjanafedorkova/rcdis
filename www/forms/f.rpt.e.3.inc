<?
   $sCriteria = reqVar::get('search');
   $searchCr = array();
        $sCr = explode("^", $sCriteria);
        if(isset($sCr[0]) && $sCr[0] != -1  && isset($sCr[1]) && $sCr[1] != -1)
        {
           $searchCr[] = text::get('YEAR').': '.$sCr[0].'-'.$sCr[1];
        }
        if(isset($sCr[2]) && $sCr[2] != -1  && isset($sCr[3]) && $sCr[3] != -1)
        {
           $searchCr[] = text::get('MONTH').': '.dtime::getMonthName($sCr[2]).'-'.dtime::getMonthName($sCr[3]);
        }
        if(isset($sCr[4]) && $sCr[4] != -1)
        {
           $searchCr[] = text::get('RCD_REGION').': '.$sCr[4];
        }
        if(isset($sCr[5]) && $sCr[5] != -1)
        {
           $searchCr[] = text::get('SINGL_DV_AREA').': '.dbProc::getDVAreaName($sCr[5]);
        }
        if(isset($sCr[6]) && $sCr[6] != -1)
        {
           $searchCr[] = text::get('ED_REGION').': '.$sCr[6];
        }
        if(isset($sCr[7]) && $sCr[7] != -1)
        {
           $searchCr[] = text::get('ED_IECIKNIS').': '.dbProc::getEDAreaName($sCr[7]);
        }

        if(isset($sCr[8]) && $sCr[8] != -1)
        {
           $searchCr[] = text::get('SINGLE_SOURCE_OF_FOUNDS').': '.dbProc::getSourceOfFoundsName($sCr[8]);
        }
        if(isset($sCr[9]) && $sCr[9] != -1)
        {
           $searchCr[] = text::get('SINGLE_VOLTAGE').': '.dbProc::getVoltageName($sCr[9]);
        }
        if(isset($sCr[10]) && $sCr[10] != -1)
        {
           $searchCr[] = text::get('ACT_OUNER').': '.dbProc::getUserName($sCr[10]);
        }
        if(isset($sCr[11]) && $sCr[11] != -1)
        {
           $searchCr[] = text::get('SINGLE_ACT_TYPE').': '.dbProc::getActTypeName($sCr[11]);
        }
        if(isset($sCr[12]) && $sCr[12] != -1)
        {
           $searchCr[] = text::get('MMS_WORK_FINISHED').': '.(($sCr[12] == 1) ? text::get('YES'): text::get('NO'));
        }
        $status = explode("*", $sCr[14]);
        $statusString = '';
        if($status[0] == -1)
        {
           $statusString = text::get('ALL_STATUS');
        }
        else
        {
          foreach($status as $s)
          {
              $statusInfo = dbProc::getKrfkInfoByName($s);
              $statusString .= $statusInfo['KRFK_NOZIME'].",";
          }
          $statusString  = substr($statusString, 0, -1);
        }
      $searchCr[] = text::get('STATUS').': '.$statusString;
      if(isset($sCr[16]) && $sCr[16] != -1)
      {
           $searchCr[] = text::get('ED_SECTION').': '.urldecode($sCr[16]);
      }
      if(isset($sCr[17]) && $sCr[17] != -1)
      {
        $searchCr[] = text::get('EPLA_ACT').': '.(($sCr[17] == 1) ? text::get('YES'): text::get('NO'));
      }

    $excel = '<head><meta http-equiv=Content-Type content="text/html; charset=utf-8"></head>';

   $excel .= '<table style="font-family:Arial; font-size:9pt;">';

   $excel .= '<tr><td colspan="11" style="font-size:12pt; font-weight:bold;">'.text::get('REPORT_CUSTOMER_WORK_TIME_DETALS').'</td></tr>';
   $excel .= '<tr><td colspan="11">'.implode('; ',$searchCr).'</td></tr>';
   $excel .= '<tr>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('WORKING_PLACE_NUMBER').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('RCD_KODS').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('USER_SURNAME').', '.text::get('USER_NAME').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('ACT_NUMBER').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('ACT_NUM_POSTFIX').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('STATUS').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('MAIN_DESIGNATION').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('SINGLE_SOURCE_OF_FOUNDS').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('COMMENT').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('OBJECT_IS_FINISHED').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('BASE_TIME').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('OVER_TIME').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('ROUT_TIME').'</td>';

   $excel .= '</tr>';

        $res = dbProc::getCustomerWorkTimeDescriptionList($sCriteria);

        $currentRow = 4;
        if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
              $excel .= '<tr>';
              $excel .= '<td style="border: 1px solid black;">'.$row['PRSN_DARBA_VIETA'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['PRSN_RCD_KODS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['PRSN_VARDS_UZVARDS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['NUMURS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['POSTFIX'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['STATUS_NAME'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['RAKT_OPERATIVAS_APZIM'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['KFNA_NOSAUKUMS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['RAKT_PIEZIMES'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['RAKT_IR_OBJEKTS_PABEIGTS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.number_format($row['PRSN_PAMATSTUNDAS'],2,'.','').'</td>';
              $excel .= '<td style="border: 1px solid black;">'.number_format($row['PRSN_VIRSSTUNDAS'],2,'.','').'</td>';
	      $excel .= '<td style="border: 1px solid black;">'.number_format($row['PRSN_LAIKSCELA'],1,'.','').'</td>';

              $excel .= '</tr>';

              $currentRow ++;
			}
		}

// redirect output to client browser

  header('Content-Type: application/vnd.ms-excel;');
  header('Content-Disposition: attachment;filename="myfile.xls"');
  header('Cache-Control: max-age=0');

  echo $excel;

?>