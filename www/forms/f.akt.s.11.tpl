﻿<body class="frame_1">
<?= $oFormPop -> getFormHeader(); ?>

<h1><?=$title;?></h1>
<div align=center><?= $oFormPop -> getMessage(); ?></div>
<table cellpadding="5" cellspacing="1" border="0" width="100%">
<tr>
    <td class="table_cell_c" width="30%"><?= $oFormPop -> getElementLabel('searchCode'); ?></td>
    <td class="table_cell_2" width="40%"><?= $oFormPop -> getElementHtml('searchCode'); ?></td>
    <td class="table_cell_2" width="30%">
    <? if(!$isReadonly)  {?>
        <?= $oFormPop -> getElementHtml('search'); ?>
    <?}?>
    </td>
</tr>

</table>
<br />
<? if($showResults) { ?>
<table cellpadding="5" cellspacing="1" border="0" width="100%">
   <tr class="table_head_2">
	   <th width="60%"><?=text::get('NAME');?></th>
       <th  width="20%"><?=text::get('AMOUNT');?></th>
	   <th width="20%">&nbsp;</th>
   </tr>
   <tr class="table_cell_3">
       <td>
            <?= $oFormPop->getElementHtml('id'); ?>
            <?= $oFormPop->getElementHtml('title'); ?>
       </td>
       <td><?= $oFormPop->getElementHtml('amount'); ?></td>
       <td>
        <? if(!$isReadonly)  {?>
            <?= $oFormPop->getElementHtml('save'); ?>
        <?}?>
       </td>
   </tr>
</table>
<? } ?>
<table cellpadding="5" cellspacing="0" border="0" align="center">
    <tr>
      <td><?=$oFormPop->getElementHtml('close');?></td>
    </tr>
</table>
<?= $oFormPop->getFormBottom(); ?>
<?=$oFormPop->getElementHtml('jsRefresh2');?>
</body>