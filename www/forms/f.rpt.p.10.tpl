﻿<body class="print">
<div class="print_header"><?= text::get('REPORT_MATERIAL_BY_GROUP'); ?></div>
<table cellpadding="0" cellspacing="0" class="print_table_landscape" >
<?
  foreach($searchCr as $i=>$s)
  {
    if($i%3 == 0)
    {
      ?>
        <tr>
      <?
    }
    ?>
      <td align="right" class="print_table_total" width="13%"><?=$s['label'];?>:</td>
      <td align="left" class="print_table_data" width="20%"><?=$s['value'];?></td>
    <?
  }
?>
</table>
<br />
<table cellpadding="0" cellspacing="0" class="print_table_landscape" >

		<tr>
			<td width="10%" class="print_table_header"><?= text::get('REPORT_MATERIAL_CODE'); ?></td>
			<td width="30%" class="print_table_header"><?= text::get('REPORT_MATERIAL_TITLE'); ?></td>
            <td width="15%" class="print_table_header"><?=text::get('REPORT_MATERIAL_MIASURE');?></td>
            <td width="15%" class="print_table_header"><?=text::get('REPORT_MATERIAL_PRICE');?></td>
            <td width="15%" class="print_table_header"><?=text::get('REPORT_MATERIAL_AMOUNT');?></td>
            <td width="15%" class="print_table_header"><?=text::get('REPORT_MATERIAL_PRICE_TOTAL');?></td>

 		</tr>

<?
	if (is_array($res) && count($res) > 0)
	{
		foreach ($res as $row)
		{
?>
			<tr >
			   	<td align="center" class="print_table_data"><?= $row['MATR_KODS']; ?></td>
                <td align="left" class="print_table_data"><?= $row['MATR_NOSAUKUMS']; ?></td>
                <td align="center" class="print_table_data"><?= $row['MATR_MERVIENIBA']; ?></td>
                <td align="center" class="print_table_data"><?= $row['MATR_CENA']; ?></td>
                <td align="center" class="print_table_data"><?= $row['DAUDZUMS']; ?></td>
                <td align="center" class="print_table_data"><?= $row['CENA_KOPA']; ?></td>

               
			</tr>
<?
		}
?>
        <tr>
             <td align="right" class="print_table_total" colspan="5"><?=text::get('TOTAL');?>:</td>
             <td align="center" class="print_table_total"><?= $summa; ?></td>
       </tr>
<?
	}
?>

</table>

</body>