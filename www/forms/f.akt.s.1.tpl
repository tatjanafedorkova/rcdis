﻿<body class="frame_1">
<?=$oForm->getElementHtml('jsRefresh2');?>
<h1><?= $actTitle; ?></h1>
<?= $oForm -> getFormHeader(); ?>
<table cellpadding="5" cellspacing="1" border="0" width="100%">
	<tr>
		<td align=center colspan="5"><?= $oForm -> getMessage(); ?></td>
	</tr>
	<tr>
		<td colspan="4" class="table_head" align="right">
         <? if( $actId != false && $isAdmin && ($status == STAT_AUTO || $status == STAT_ACCEPT) ) { ?>
            <?= $oForm -> getElementLabel('statusSel'); ?>:
            <?= $oForm -> getElementHtml('statusSel'); ?>
         <? } else { ?>
            <?= $oForm -> getElementLabel('statusTxt'); ?>:
            <font color="red"><?=text::toUpper($oForm -> getElementHtml('statusTxt'));?></font>
         <? } ?>
           
      </td>
        <td width="20%" rowspan="<?=(($actId === false)?'14':'15');?>" valign="bottom">
            <table cellpadding="5" cellspacing="0" border="0" align="center">
             <? if(!$isReadonly || !$isReadonlyExceptAdmin)  {?>
            	<tr><td><?= $oForm -> getElementHtml('save'); ?></td> </tr>
             <? } ?>
             <? if($isAllowEstimate && $isInvestition)  {?>
               <tr><td><?=$oForm->getElementHtml('estimate');?></td></tr>
            <? } ?>
             <? if(!$isReadonly && ($actId != false))  {?>
                    <? if($isAuto == 1)  {?>
                        <tr><td><?=$oForm->getElementHtml('export_part');?></td></tr>
                    <? } ?>
                    <tr><td><?=$oForm->getElementHtml('export');?></td></tr>
             <? } ?>
             <? if(!$isReadonlyExceptEconomist)  {?>
                <tr><td><?=$oForm->getElementHtml('return');?></td></tr>
             <? } ?>
             <? if($isAdmin && ($actId != false) && ($status == STAT_DELETE || $status == STAT_CLOSE))  {?>
                <tr><td><?=$oForm->getElementHtml('return');?></td></tr>
             <? } ?>
             <? if(!$isReadonlyExceptEconomist)  {?>
                <tr><td><?=$oForm->getElementHtml('accept');?></td></tr>
             <? } ?>
             <? if(!$isReadonly  && $isAuto == 0 )  {?>
                <tr><td><?=$oForm->getElementHtml('clear');?></td></tr>
             <? } ?>
             <? if($isAllowEstimate)  {?>
                <tr><td><?=$oForm->getElementHtml('view');?></td></tr>
             <? } ?>
             <? if($isDelete  )  {?>
                <tr><td><?=$oForm->getElementHtml('delete');?></td></tr>
             <? } ?>
             <? if($actId != false)  {?>
                <tr><td><?=$oForm->getElementHtml('back');?></td></tr>
             <? } ?>
            </table>
        </td>
	</tr>
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('EDarea'); ?>:<?=(($isReadonly)?'':'<font color="red">*</font>');?></td>
      <td class="table_cell_2" ><?= $oForm -> getElementHtml('EDarea'); ?></td>
      <? if($actId != false)  {?>
         <td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('actFullNumber'); ?>:</td>
		   <td class="table_cell_2"><?= $oForm -> getElementHtml('actFullNumber'); ?><?= $oForm -> getElementHtml('actNumPostfixLink'); ?></td>
      <? } else { ?>
         <td class="table_cell_c" width="16%">&nbsp</td>
		   <td class="table_cell_2">&nbsp</td>
      <? }  ?>
   </tr>
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('type'); ?>:<?=(($isReadonly)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('type'); ?></td>
        <td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('ouner'); ?>:<?=(($isReadonlyExceptAdmin)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('ouner'); ?></td>
   </tr>
    
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('designation'); ?>:<?=(($isReadonly)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('designation'); ?></td>
   	<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('voltage'); ?>:<?=(($isReadonly)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('voltage'); ?></td>
   	</tr>
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('object'); ?>:<?=(($isReadonly)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('object'); ?></td>
   	<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('sourceOfFounds'); ?>:<?=(($isReadonly)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('sourceOfFounds'); ?></td>
   	</tr>
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('proces_date'); ?>:<?=(($isReadonly && $isReadonlyExceptEconomist)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('proces_date'); ?></td>
      <td class="table_cell_c"><?= $oForm -> getElementLabel('toid'); ?>:<font color="red">*</font></td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('toid'); ?></td>
   </tr>
       
    <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('description'); ?>:</td>
		<td class="table_cell_2" colspan="3"><?= $oForm -> getElementHtml('description'); ?></td>
   </tr>
   <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('project_no'); ?>:<?=((!$isReadonly && $isInvestition)?'<font color="red">*</font>':'');?></td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('project_no'); ?></td>
      <td class="table_cell_c"><?= $oForm -> getElementLabel('teh_req_no'); ?>:<?=((!$isReadonly && $isInvestition)?'<font color="red">*</font>':'');?></td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('teh_req_no'); ?></td>
   </tr>
   <tr> 
     	<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('worktitle'); ?>:<?=(($isReadonly && $isReadonlyExceptEconomist)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" colspan="3"><?= $oForm -> getElementHtml('worktitle'); ?></td>

 	</tr>

     <tr>
		<td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('numPostfix'); ?>:<font color="red">*</font></td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('numPostfix'); ?></td>
        <td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('isFinished'); ?>:</td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('isFinished'); ?></td>
   	</tr>
    <tr>
      <td class="table_cell_c"><?= $oForm -> getElementLabel('stock'); ?>:</td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('stock'); ?></td>
      <td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('overtimeKoef'); ?>:<?=(($isReadonly)?'':'<font color="red">*</font>');?></td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('overtimeKoef'); ?></td>
   </tr>
   <? if($isInvestition)  {?>
   <tr>
      <td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('signatories1'); ?>:<font color="red">*</font></td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('signatories1'); ?></td>
      <td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('signatories2'); ?>:</td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('signatories2'); ?></td>
   </tr>
   <tr>
      <td class="table_cell_c"><?= $oForm -> getElementLabel('signatories1position'); ?>:<font color="red">*</font></td>
		<td class="table_cell_2"><?= $oForm -> getElementHtml('signatories1position'); ?></td>
      <td class="table_cell_c" width="16%"><?= $oForm -> getElementLabel('signatories2position'); ?>:</td>
		<td class="table_cell_2" width="24%"><?= $oForm -> getElementHtml('signatories2position'); ?></td>
   </tr>
   <tr>
     
      <td class="table_cell_c"><?= $oForm -> getElementLabel('io_approve_date'); ?>:</td>
      <td class="table_cell_2"><?= $oForm -> getElementHtml('io_approve_date'); ?></td>
      <td class="table_cell_c">&nbsp;</td>
		<td class="table_cell_2">&nbsp;</td>
   </tr>
   <? if($isAdmin) { ?>
   <tr>
     
      <td class="table_cell_c"><?= $oForm -> getElementLabel('atlidziba'); ?>:</td>
      <td class="table_cell_2"><?= $oForm -> getElementHtml('atlidziba'); ?></td>
      <td class="table_cell_c"><?= $oForm -> getElementLabel('soc_apdr'); ?>:</td>
      <td class="table_cell_2"><?= $oForm -> getElementHtml('soc_apdr'); ?></td>
   </tr>
   <tr>
     
      <td class="table_cell_c"><?= $oForm -> getElementLabel('atlidziba_transport'); ?>:</td>
      <td class="table_cell_2"><?= $oForm -> getElementHtml('atlidziba_transport'); ?></td>
      <td class="table_cell_c">&nbsp;</td>
		<td class="table_cell_2">&nbsp;</td>
   </tr>
   <tr>
     
      <td class="table_cell_c"><?= $oForm -> getElementLabel('atlidziba_material'); ?>:</td>
      <td class="table_cell_2"><?= $oForm -> getElementHtml('atlidziba_material'); ?></td>
      <td class="table_cell_c">&nbsp;</td>
		<td class="table_cell_2">&nbsp;</td>
   </tr>
  
   <? }} ?>
    
</table>
<a name="tab"></a>

<?= $oForm -> getFormBottom(); ?>
<?=$oForm->getElementHtml('jsButtonsControl');?>
<?=$oForm->getElementHtml('jsBackButtons');?>
</body>
