﻿<body class="frame_2">
<h1><?= $title; ?></h1>
<script type="text/javascript">
   
    function setRowStyle(row)
    {
      var rowName =  'recordRow' + row;
      var row = document.getElementById(rowName);
      row.style.color = '#999999';
    }
</script>


<table cellpadding="5" cellspacing="1" border="0" width="100%">
	<tr class="table_head_2">

         <?
	     if (is_array($columns))
	     {
		    foreach ($columns as $col)
		    {
            ?>
                <td class="block"><?=$col['nosaukums'];?> </td>
            <?
            }
         }
        ?>
 	</tr>
	<?
	if (is_array($res))
	{
	$i = 0;

		foreach ($res as $row)
		{
		?>
			<tr id="recordRow<?=$i;?>"  class="table_cell_3" >

                <?
                if (is_array($columns))
	            {
		      foreach ($columns as $col)
		      {
		              if((substr($col['kolonna'], -9) == 'IR_AKTIVS') || substr($col['kolonna'], -11) == 'IR_PABEIGTS' || substr($col['kolonna'], -10) == 'PRIORITATE')
                      {	
			if((substr($col['kolonna'], -9) == 'IR_AKTIVS') && ($row[$col['kolonna']] == 0))
                        {
                          ?>
                             <script>setRowStyle(<?=$i;?>);</script>
                          <?
                        }
		

                        ?>
                           <td><?=($row[$col['kolonna']])?text::get('YES'):text::get('NO');?></td>
                        <?
                      }
                      else
                      {
                        ?>
				<td><?=$row[$col['kolonna']];?></td>
                        <?
                       }
                    }
                }
                ?>

			</tr>
		<?
		$i ++;


		}
	}
	?>

</table>

</body>
