﻿<?
   $sCriteria = reqVar::get('search');
   $searchCr = array();
        $sCr = explode("^", $sCriteria);
        if(isset($sCr[0]) && $sCr[0] != -1  && isset($sCr[1]) && $sCr[1] != -1)
        {
           $searchCr[] = text::get('YEAR').': '.$sCr[0].'-'.$sCr[1];
        }
        if(isset($sCr[2]) && $sCr[2] != -1  && isset($sCr[3]) && $sCr[3] != -1)
        {
           $searchCr[] = text::get('MONTH').': '.dtime::getMonthName($sCr[2]).'-'.dtime::getMonthName($sCr[3]);
        }
        
        if(isset($sCr[4]) && $sCr[4] != -1)
        {
           $searchCr[] = text::get('SINGL_DV_AREA').': '.dbProc::getDVAreaName($sCr[4]);
        }
        
        if(isset($sCr[5]) && $sCr[5] != -1)
        {
           $searchCr[] = text::get('ACT_OUNER').': '.dbProc::getUserName($sCr[5]);
        }
       
        $status = explode("*", $sCr[7]);
        $statusString = '';
        if($status[0] == -1)
        {
           $statusString = text::get('ALL_STATUS');
        }
        else
        {
          foreach($status as $s)
          {
              $statusInfo = dbProc::getKrfkInfoByName($s);
              $statusString .= $statusInfo['KRFK_NOZIME'].",";
          }
          $statusString  = substr($statusString, 0, -1);
        }
        $searchCr[] = text::get('STATUS').': '.$statusString;

   $excel = '<head><meta http-equiv=Content-Type content="text/html; charset=utf-8"></head>';

   $excel .= '<table style="font-family:Arial; font-size:9pt;">';

   $excel .= '<tr><td colspan="11" style="font-size:12pt; font-weight:bold;">'.text::get('REPORT_RFC_INNER_ACT').'</td></tr>';
   $excel .= '<tr><td colspan="11">'.implode('; ',$searchCr).'</td></tr>';
   $excel .= '<tr>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('NRPK').'</td>';
      $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('YEAR').'</td>';
   
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('MONTH').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('ACT_NUMBER').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('ED_IECIKNIS').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('USER_NAME'). ' '. text::get('USER_SURNAME').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('RCD_KODS').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('TRANSPORT_WORK_TIME').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('DATE'). ' ' . text::get('FROM').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('DATE'). ' ' . text::get('UNTIL').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('WORK_TYPE').'</td>';
$excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('WORK_DESCRIPTION').'</td>';
$excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('DATE').'</td>';
   $excel .= '<td style="border: 1px solid black; font-size:10pt; font-weight:bold; text-align:center">'.text::get('USER').'</td>';
   $excel .= '</tr>';

        $res = dbProc::getRfcActReportList($sCriteria, false);

        
        if (is_array($res))
		{
			foreach ($res as $i=>$row)
			{
              $excel .= '<tr>';
			  $excel .= '<td style="border: 1px solid black;">'.$i.'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['RAKT_GADS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['RAKT_MENESIS'].'</td>';
	            $excel .= '<td style="border: 1px solid black;">'.$row['RAKT_FULL_NUMBER'].'</td>';
              
              $excel .= '<td style="border: 1px solid black;">'.$row['STRUCTURVIENIBA'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['RCFD_KPRF_VARDS_UZVARDS'].'</td>';
			  $excel .= '<td style="border: 1px solid black;">'.$row['RCFD_KPRF_KODS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.number_format($row['RCFD_STUNDAS'],2,'.','').'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['RCFD_DATUMS_NO'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['RCFD_DATUMS_LIDZ'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['WORK_TITLE'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['RCFD_WORK_DESCRIPTION'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['RAKT_DATUMS'].'</td>';
              $excel .= '<td style="border: 1px solid black;">'.$row['OUNER'].'</td>';

              $excel .= '</tr>';

           
			}
		}

 // redirect output to client browser

  header('Content-Type: application/vnd.ms-excel;');
  header('Content-Disposition: attachment;filename="myfile.xls"');
  header('Cache-Control: max-age=0');

  echo $excel;
?>