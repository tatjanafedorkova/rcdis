<body class="frame_2">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><h1><?= text::toUpper(text::get('REPORT_CUSTOMER_WORK_TIME_DETALS')); ?></h1></td>
		<td align="right">
            <img src="img/ico_print.gif" alt="" width="16" height="16" border="0">&nbsp;
            <a href="<?= $printUrl; ?>" target="new"><?=text::get('PRINT');?></a>&nbsp;&nbsp;
            <img src="img/ico_excel.gif" alt="" width="16" height="16">&nbsp;
            <a href="<?= $exportUrl; ?>" ><?=text::get('EXPORT');?></a>&nbsp;
        </td>
	</tr>
</table>

<table cellpadding="5" cellspacing="1" border="0" width="100%" >
	<thead>
		<tr class="table_head_2">
			<td width="5%"><?= text::get('WORKING_PLACE_NUMBER'); ?></td>
			<td width="25%"><?= text::get('RCD_KODS'); ?></td>
            <td width="52%"><?=text::get('USER_SURNAME');?> <?=text::get('USER_NAME');?></td>
            <td width="6%"><?=text::get('BASE_TIME');?></td>
            <td width="6%"><?=text::get('OVER_TIME');?></td>
            <td width="6%"><?=text::get('ROUT_TIME');?></td>
	 	
 		</tr>
	</thead>
	<tbody>
<?
	if (is_array($customer) && count($customer) > 0)
	{
		foreach ($customer as $row)
		{
?>
			<tr class="table_cell_3" >
			   	<td align="center"><b><?= $row['workPlace']; ?></b></td>
                <td align="center"><b><?= $row['code']; ?></b></td>
                <td align="left"><b><?= $row['name']; ?></b></td>
                <td align="center"><b><?= number_format($row['totalMainTime'],2); ?></b></td>
                <td align="center"><b><?= number_format($row['totalOverTime'],2); ?></b></td>
	            <td align="center"><b><?= number_format($row['totalRoutTime'],2); ?></b></td>
			</tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="5">
                    <table class="list" width="100%" cellpadding="0" cellspacing="1">
                    <?
                       if (is_array($row['act']) && count($row['act']) > 0)
                       {
                            ?>
                               <tr>
                                    <th width="8%"><?= text::get('ACT_NUMBER'); ?></th>
                                    <th width="8%"><?= text::get('ACT_NUM_POSTFIX'); ?></th>
                                    <th width="6%"><?= text::get('STATUS'); ?></th>
                                    <th width="18%"><?= text::get('MAIN_DESIGNATION'); ?></th>
                                    <th width="18%"><?= text::get('SINGLE_SOURCE_OF_FOUNDS'); ?></th>
                                    <th width="18%"><?= text::get('COMMENT'); ?></th>
                                    <th width="6%"><?= text::get('OBJECT_IS_FINISHED'); ?></th>
                                    <th width="6%"><?= text::get('BASE_TIME'); ?></th>
                                    <th width="6%"><?= text::get('OVER_TIME'); ?></th>
				    <th width="6%"><?= text::get('ROUT_TIME'); ?></th>
 
                               </tr>
                            <?
                    		foreach ($row['act'] as $r)
                    		{
                             ?>
                                <tr>
                                    <td><?= $r['actNumber']; ?></td>
                                    <td><?= $r['actPostfix']; ?></td>
                                    <td><?= $r['status']; ?></td>
                                    <td><?= $r['designation']; ?></td>
                                    <td><?= $r['founds']; ?></td>
                                    <td><?= $r['description']; ?></td>
                                    <td><?= (($r['isFinished']==1) ? text::get('YES') : text::get('NO') ); ?></td>
                                    <td><?= $r['mainTime']; ?></td>
                                    <td><?= $r['overTime']; ?></td>
				    <td><?= $r['routTime']; ?></td>

                                </tr>
                             <?
                            }
                       }
                    ?>
                    </table>
                </td>
            </tr>
<?
		}
	}
?>
	</tbody>
</table>

</body>