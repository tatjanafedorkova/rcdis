<body class="frame_1">
<h1><?=text::get('REPORT');?></h1>

<table class="list" width="90%">
	<thead>
		<tr>
			<th><?= text::get('RCD_REPORTS'); ?></th>
		</tr>
	</thead>
	<tbody>
    <!--tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $actMainReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_MAIN_ACT'); ?></td>
	</tr-->
   <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $workTimeReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_CUSTOMER_WORK_TIME'); ?></td>
	</tr>
    <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $workTimeDetailsReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_CUSTOMER_WORK_TIME_DETALS'); ?></td>
	</tr>
	<tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $RfcActReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_RFC_INNER_ACT'); ?></td>
	</tr>
    <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $materialReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_MATERIALS_IN_MONTH'); ?></td>
	</tr>
	<tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $calculationReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_CALCULATION'); ?></td>
	</tr>
    <!--tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $transportGroupReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_TRANSPORT_GROUP'); ?></td>
	</tr>
    <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $transportUsageReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_TRANSPORT_USAGE'); ?></td>
	</tr>
    
    <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $calculationPlanNplanReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_CALCULATION_PLAN_NPLAN'); ?></td>
	</tr-->
	
	</tbody>
</table>
<? if(false /* $isEdUser || $isAdmin || $isAuditor || (userAuthorization::getUserId() == 119) */){ ?>
<table class="list" width="90%">
	<thead>
		<tr>
			<th><?= text::get('ED_REPORTS'); ?></th>
		</tr>
	</thead>
	<tbody>
    <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $actEdMainReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_MAIN_ACT'); ?></td>
	</tr>
    <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $materialByCodeReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_MATERIAL_BY_CODE'); ?></td>
	</tr>
    <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $materialByGroupReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_MATERIAL_BY_GROUP'); ?></td>
	</tr>
    <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $calculationByGroupReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_CALCULATION_BY_GROUP'); ?></td>
	</tr>

    <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $workPlanNotPlanReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_WORK_PLAN_AND_NOT_PLAN'); ?></td>
	</tr>
    <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $workPhisicalShowingReportLink; ?>');reloadFrame(23,'');">
        <!-- -->
		<td><?= text::get('REPORT_PHISICAL_SHOWING'); ?></td>
	</tr>
    <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $workPlanReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_WORK_WITH_PLAN'); ?></td>
	</tr>
    <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $workNotPlanReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_WORK_WITH_NO_PLAN'); ?></td>
	</tr>
    <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $norStartedWorkReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_NOT_STARTED_WORK'); ?></td>
	</tr>
    <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $finishedWorkReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_FINISHED_WORK'); ?></td>
	</tr>
    <tr onmouseover="makeActiveRow2(this,'x');"
		onClick="parent['frameTop'].enableFrameControl();window.top.min(0);window.top.normal();reloadFrame(1,'<?= $mmsWorkReportLink; ?>');reloadFrame(23,'');">
		<td><?= text::get('REPORT_MMS_GROUPS'); ?></td>
	</tr>
	</tbody>
</table>
<? } ?>
</body>