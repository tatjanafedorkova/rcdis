<body class="head" onscroll="this.scrollTo(0,0);return false" >
<script language="JavaScript">
function subMenu(subMenuName,tdName)
{

	var a=getElement(subMenuName,true); 
	var b=getElement(tdName,true);

	if (a.style.visibility=='visible') 
	{
		a.style.visibility='hidden';
		b.className='menu_cell_top';
	} 
	else 
	{
		hideMenu();
		a.style.visibility='visible';
		b.className='menu_cell_a';
	} 
}

function hideMenu()
{
	var c=getElement('settingSubMenuDiv',true); c.style.visibility="hidden";
	var c=getElement('tdSetting',true); c.className="menu_cell_top";
	var c=getElement('catalogSubMenuDiv',true); c.style.visibility="hidden";
	var c=getElement('tdCatalog',true); c.className="menu_cell_top";
	var c=getElement('favoritSubMenuDiv',true); c.style.visibility="hidden";
	var c=getElement('tdFavorit',true); c.className="menu_cell_top";
	var c=getElement('actSubMenuDiv',true); c.style.visibility="hidden";
	var c=getElement('tdAct',true); c.className="menu_cell_top";
	var c=getElement('reportSubMenuDiv',true); c.style.visibility="hidden";
	var c=getElement('tdReport',true); c.className="menu_cell_top";
    var c=getElement('optionSubMenuDiv',true); c.style.visibility="hidden";
	var c=getElement('tdOptions',true); c.className="menu_cell_top";
}

function disableFrameControl()
{
	var a=getElement('disabledFrameControl',true);
	a.style.visibility='visible';
	window.top.disableResize();
}

function enableFrameControl()
{
	
	var a=getElement('disabledFrameControl',true);
	a.style.visibility='hidden';
	window.top.enableResize();
	
}

</script>


<div id="settingSubMenuDiv"  class="sub_menu" style="position: absolute;left: 98px;top: 26px;visibility:hidden;">
<?
if($isAdmin || $isSystemUser)
{
  ?>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td class="menu_cell_2" nowrap><a target="frame_1" href="<?=$changePassLink;?>" onclick="reloadFrame(23,'');disableFrameControl();window.top.min(1);hideMenu();"><?=text::get('CHANGE_PASSWORD');?></a></td>
        </tr>
	</table>
  <?
}
?>
</div>

<div id="catalogSubMenuDiv"  class="sub_menu" style="position: absolute;left: 33px;top: 26px;visibility:hidden; z-index:5; padding-bottom: 0px; ">
<?
if($isAdmin || $isEdUser || $isAuditor || $isEconomist)
{
  	?>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>

		   <td class="menu_cell"><a  target="frame_1" href="<?=$materialListLink;?>" onclick="reloadFrame(2,'<?=$editMaterialLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('MATERIALS');?></a></td>
           <?
           if(($isAdmin && !$isEdUser) || $isAuditor)
           {
           ?>
           <td class="menu_cell"><a  target="frame_1" href="<?=$customerListLink;?>" onclick="reloadFrame(2,'<?=$editCustomerLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('CUSTOMERS');?></a></td>
           <td class="menu_cell"><a  target="frame_1" href="<?=$transportListLink;?>" onclick="reloadFrame(2,'<?=$editTransportLink;?>');reloadFrame(3,'');enableFrameControl();window.top.normal();hideMenu();"><?=text::get('TRANSPORT');?></a></td>
           <?
           }
           if($isAdmin || $isEdUser || $isAuditor)
                      {
  	       ?>
           <td class="menu_cell" ><a  target="frame_1" href="<?=$dvAreaListLink;?>" onclick="reloadFrame(2,'<?=$editDvAreaLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('DV_AREA');?></a></td>
           <td class="menu_cell" ><a  target="frame_1" href="<?=$edAreaListLink;?>" onclick="reloadFrame(2,'<?=$editEdAreaLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('ED_AREA');?></a></td>
           <td class="menu_cell" ><a  target="frame_1" href="<?=$voltageListLink;?>" onclick="reloadFrame(2,'<?=$editVoltageLink;?>');reloadFrame(3,'');enableFrameControl();window.top.normal();hideMenu();"><?=text::get('VOLTAGE');?></a></td>
           <td class="menu_cell" ><a  target="frame_1" href="<?=$objectListLink;?>" onclick="reloadFrame(2,'<?=$editObjectLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('OBJECTS');?></a></td>
           <td class="menu_cell" ><a  target="frame_1" href="<?=$sourceOfFoundsListLink;?>" onclick="reloadFrame(2,'<?=$editSourceOfFoundsLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('SOURCE_OF_FOUNDS');?></a></td>
           <td class="menu_cell" ><a  target="frame_1" href="<?=$calculationGroupListLink;?>" onclick="reloadFrame(2,'<?=$editCalculationGroupLink;?>');reloadFrame(3,'');enableFrameControl();window.top.normal();hideMenu();"><?=text::get('CALCULATION_GROUP');?></a></td>
           <td class="menu_cell" ><a  target="frame_1" href="<?=$calculationListLink;?>" onclick="reloadFrame(2,'<?=$editCalculationLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('CALCULATION');?></a></td>
           <td class="menu_cell" ><a  target="frame_1" href="<?=$actTypeListLink;?>" onclick="reloadFrame(2,'<?=$editActTypeLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('ACT_TYPE');?></a></td>
           <td class="menu_cell" ><a  target="frame_1" href="<?=$workTypesListLink;?>" onclick="reloadFrame(2,'<?=$editWorkTypeLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('WORK_TYPES');?></a></td>       
           <td class="menu_cell" ><a  target="frame_1" href="<?=$userListLink;?>" onclick="reloadFrame(2,'<?=$editUserLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('USERS');?></a></td>
		   <td class="menu_cell" ><a  target="frame_1" href="<?=$mmsListLink;?>" onclick="reloadFrame(2,'<?=$editMMSLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('SINGLE_MMS');?></a></td>
		   <td class="menu_cell" ><a  target="frame_1" href="<?=$mmsCalculationListLink;?>" onclick="reloadFrame(2,'<?=$editMmsCalculationLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('MMS_CALCULATION');?></a></td>
		   <td class="menu_cell" ><a  target="frame_1" href="<?=$calculationMaterialListLink;?>" onclick="reloadFrame(2,'<?=$editCalculationMaterialLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('CALCULATION_MATERIAL');?></a></td>
           <td class="menu_cell" ><a  target="frame_1" href="<?=$regionListLink;?>" onclick="reloadFrame(2,'<?=$editRegionLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('REGIONS');?></a></td>
		   <td class="menu_cell" ><a  target="frame_1" href="<?=$efOrderListLink;?>" onclick="reloadFrame(2,'<?=$editefOrderLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('EF_ORDER_TITLE');?></a></td>
		   <td class="menu_cell" ><a  target="frame_1" href="<?=$investitionPricesListLink;?>" onclick="reloadFrame(2,'<?=$editInvesticijasPriceLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('INVESTITION_PRICES');?></a></td>
		   <td class="menu_cell" ><a  target="frame_1" href="<?=$investitionSignListLink;?>" onclick="reloadFrame(2,'<?=$editInvesticijasSignLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('INVESTICION_SIGN_PERS');?></a></td>
           <td class="menu_cell" ><a  target="frame_1" href="<?=$StockListLink;?>" onclick="reloadFrame(2,'<?=$editStockLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('STOCK_TITLE');?></a></td>
	      

           <?
	      }
           if($isAdmin || $isEdUser )
           {
           ?>
           <td class="menu_cell_2" ><a target="frame_1" href="<?=$catalogImportLink;?>" onclick="reloadFrame(23,'');disableFrameControl();window.top.min(1);hideMenu();"><?=text::get('DATA_IMPORT');?></a></td>
            <?
            }
            ?>
		</tr>


	</table>
	<?

}
?>
</div>

<div id="optionSubMenuDiv"  class="sub_menu" style="position: absolute;left: 340px;top: 26px;visibility:hidden;">
<?
if($isAdmin && !$isEdUser)
{
  	?>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
           <td class="menu_cell" nowrap><a target="frame_1" href="<?=$warningLink;?>" onclick="reloadFrame(23,'');disableFrameControl();window.top.min(1);hideMenu();"><?=text::get('WARNING');?></a></td>
           <td class="menu_cell" nowrap><a target="frame_1" href="<?=$userActionsLink;?>" onclick="reloadFrame(23,'');disableFrameControl();window.top.min(1);hideMenu();"><?=text::get('USERS_ACTIONS');?></a></td>
		   <td class="menu_cell" nowrap><a target="frame_1" href="<?=$kvikStepLink;?>" onclick="reloadFrame(23,'');disableFrameControl();window.top.min(1);hideMenu();"><?=text::get('ACT_NUMBER_TEMPLATE');?></a></td>
		   <td class="menu_cell" nowrap><a target="frame_1" href="<?=$CalcStartDateLink;?>" onclick="reloadFrame(23,'');disableFrameControl();window.top.min(1);hideMenu();"><?=text::get('CALC_PERIOD_START_DATE');?></a></td>
		   <td class="menu_cell" nowrap><a target="frame_1" href="<?=$InvestitionCoefsLink;?>" onclick="reloadFrame(23,'');disableFrameControl();window.top.min(1);hideMenu();"><?=text::get('INV_KOEFICENTS');?></a></td>
		   <td class="menu_cell" ><a  target="frame_1" href="<?=$jobListLink;?>" onclick="reloadFrame(2,'<?=$editJobLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('SYSTEM_PROCESS');?></a></td>
		   <td class="menu_cell_2" nowrap><a  target="frame_1" href="<?=$eventSearchLink;?>" onclick="reloadFrame(2,'<?=$eventListLink;?>');reloadFrame(3,'');enableFrameControl();window.top.normal();hideMenu();"><?=text::get('EVENT_LOG');?></a></td>
		</tr>

	</table>
	<?

}
?>
</div>


<div id="favoritSubMenuDiv"  class="sub_menu" style="position: absolute;left: 221px;top: 26px;visibility:hidden;">
<?
if($isEditor || $isEdUser || $isAdmin)
{
?>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>

			<td class="menu_cell" nowrap><a target="frame_1" href="<?=$favoritAddLink;?>" onclick="reloadFrame(23,'');disableFrameControl();window.top.min(1);hideMenu();"><?=text::get('MAKE_NEW');?></a></td>
			<td class="menu_cell" nowrap><a target="frame_1" href="<?=$favoritDeleteLink;?>" onclick="reloadFrame(23,'');disableFrameControl();window.top.min(1);hideMenu();"><?=text::get('DELETE');?></a></td>
			<td class="menu_cell_2" nowrap><a target="frame_1" href="<?=$favoritEditLink;?>" onclick="reloadFrame(23,'');disableFrameControl();window.top.min(1);hideMenu();"><?=text::get('EDIT');?></a></td>
		</tr>
	</table>
	<?
}
?>
</div>

<div id="actSubMenuDiv"  class="sub_menu" style="position: absolute;left: 284   px;top: 26px;visibility:hidden;">
<?
if($isAdmin || $isSystemUser)
{
  ?>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
           <td class="menu_cell<?= (!$isAdmin && !$isEditor)? '_2': '';?>" nowrap><a  target="frame_1" href="<?=$actSearchLink;?>" onclick="reloadFrame(2,'<?=$actListLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('ADVANCED_SEARCH');?></a></td>
            <td class="menu_cell<?= (!$isAdmin && !$isEditor)? '_2': '';?>" nowrap><a  target="frame_1" href="<?=$actSearchByNumberLink;?>" onclick="reloadFrame(2,'<?=$actListLink;?>');reloadFrame(3,'');enableFrameControl();window.top.min(0);window.top.normal();hideMenu();"><?=text::get('SEARCH_BY_NUMBER');?></a></td>

            <?
            if($isAdmin || $isEditor)
            {
                ?>
				<td class="menu_cell<?= (!$isAdmin && !$isEditor)? '_2': '';?>" nowrap><a target="frame_1" href="<?=$efActLink;?>" onclick="reloadFrame(23,'');disableFrameControl();window.top.min(1);hideMenu();"><?=text::get('EF_ACT');?></a></td>
				<?
            }
	    if($isAdmin)
            {
                ?>
				<td class="menu_cell<?= (!$isAdmin && !$isEditor)? '_2': '';?>" nowrap><a target="frame_1" href="<?=$eplaActLink;?>" onclick="reloadFrame(23,'');disableFrameControl();window.top.min(1);hideMenu();"><?=text::get('EPLA_ACT');?></a></td>
		<?
            }
	    if($isAdmin || $isEditor)
            {
                ?>
				<td class="menu_cell_2" nowrap><a target="frame_1" href="<?=$rfcActLink;?>" onclick="reloadFrame(23,'');disableFrameControl();window.top.min(1);hideMenu();"><?=text::get('RFC_ACT');?></a></td>
                <?
            }
            ?>              
        </tr>
	</table>
<?
}
?>
</div>

<div id="reportSubMenuDiv"  class="sub_menu" style="position: absolute;left: 377px;top: 26px;visibility:hidden;">

</div>

<table cellpadding="5" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="head_table_top">

			<table cellpadding="3" cellspacing="0" border="0">
				<tr>
                 <?

                  if(
                    ((date('Y') == 2011 || date('Y') == 2012) && $year == 2010)||
                    ((dtime::dateToInt(dtime::now()) > 20120131) && $year == 2011)
                    )
                  {
                  ?>
					<td nowrap class="menu_cell_left"><img src="img/0.gif" alt="" width="10" height="1" class="block"></td>
					<td nowrap id="tdCatalog" class="menu_cell_top" disabled><?=text::get('CATALOGS');?></td>
                    <td nowrap id="tdSetting" class="menu_cell_top" disabled><?=text::get('USER_SETTINGS');?></td>

                    <? if( $isEdUser || $isAdmin)  {    ?>
                    <td nowrap id="tdFavorit" class="menu_cell_top" <?=($isAdmin || $isEdUser)?'':'disabled';?>><?=($isAdmin || $isEdUser)?'<a onclick="subMenu(\'favoritSubMenuDiv\',\'tdFavorit\');return false;" href="#">':'';?><?=text::get('FAVORITES');?><?=($isAdmin || $isEdUser)?'</a>':'';?></td>
                    <td nowrap id="tdAct" class="menu_cell_top" <?=($isRight)?'':'disabled';?>><?=($isRight)?'<a onclick="subMenu(\'actSubMenuDiv\',\'tdAct\');return false;" href="#">':'';?><?=text::get('ACTS');?><?=($isRight)?'</a>':'';?></td>
					<td nowrap id="tdReport" class="menu_cell_top" <?=($isOldSystemUser)?'':'disabled';?>><?=($isOldSystemUser)?'<a onclick="reloadFrame(23,\'\');disableFrameControl();window.top.min(1);hideMenu();" href="'.$cRepMain.'" target="frame_1">':'';?><?=text::get('REPORT');?><?=($isOldSystemUser)?'</a>':'';?></td>
                     <?} else { ?>
                     <td nowrap id="tdFavorit" class="menu_cell_top" disabled><?=text::get('FAVORITES');?></td>
                     <td nowrap id="tdOptions" class="menu_cell_top" disabled><?=text::get('ACTS');?></td>

                    <td nowrap id="tdOptions" class="menu_cell_top" disabled><?=text::get('REPORT');?></td>

                     <? } ?>
                    <td nowrap id="tdOptions" class="menu_cell_top" disabled><?=text::get('OPTIONS');?></td>
					<td nowrap class="menu_cell_right">&nbsp;</td>

                  <?} else {?>
                    <td nowrap class="menu_cell_left"><img src="img/0.gif" alt="" width="10" height="1" class="block"></td>
					<td nowrap id="tdCatalog" class="menu_cell_top" <?=($isAdmin || $isEdUser || $isAuditor ||  $isEconomist)?'':'disabled';?>><?=($isAdmin || $isEdUser || $isAuditor ||  $isEconomist)?'<a onclick="subMenu(\'catalogSubMenuDiv\',\'tdCatalog\');return false;" href="#">':'';?><?=text::get('CATALOGS');?><?=($isAdmin || $isEdUser || $isAuditor ||  $isEconomist)?'</a>':'';?></td>
                    <td nowrap id="tdSetting" class="menu_cell_top" <?=($isAdmin || $isSystemUser)?'':'disabled';?>><?=($isAdmin || $isSystemUser)?'<a onclick="subMenu(\'settingSubMenuDiv\',\'tdSetting\');return false;" href="#">':'';?><?=text::get('USER_SETTINGS');?><?=($isAdmin || $isSystemUser)?'</a>':'';?></td>
                   	<td nowrap id="tdFavorit" class="menu_cell_top" <?=($isEditor || $isEdUser)?'':'disabled';?>><?=($isEditor || $isEdUser)?'<a onclick="subMenu(\'favoritSubMenuDiv\',\'tdFavorit\');return false;" href="#">':'';?><?=text::get('FAVORITES');?><?=($isEditor || $isEdUser)?'</a>':'';?></td>
                    <td nowrap id="tdAct" class="menu_cell_top" <?=($isRight)?'':'disabled';?>><?=($isRight)?'<a onclick="subMenu(\'actSubMenuDiv\',\'tdAct\');return false;" href="#">':'';?><?=text::get('ACTS');?><?=($isRight)?'</a>':'';?></td>
					<td nowrap id="tdReport" class="menu_cell_top" <?=($isSystemUser)?'':'disabled';?>><?=($isSystemUser)?'<a onclick="reloadFrame(23,\'\');disableFrameControl();window.top.min(1);hideMenu();" href="'.$cRepMain.'" target="frame_1">':'';?><?=text::get('REPORT');?><?=($isSystemUser)?'</a>':'';?></td>
                    <td nowrap id="tdOptions" class="menu_cell_top" <?=($isAdmin && !$isEdUser)?'':'disabled';?>><?=($isAdmin && !$isEdUser)?'<a onclick="subMenu(\'optionSubMenuDiv\',\'tdOptions\');return false;" href="#">':'';?><?=text::get('OPTIONS');?><?=($isAdmin && !$isEdUser)?'</a>':'';?></td>
					<td nowrap class="menu_cell_right">&nbsp;</td>
                  <? } ?>
				</tr>
			</table>

		</td>

		<td class="head_table_top_brown"><?= $versionYear;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=text::get('USER');?>: <?=$userName;?> <?=$userSurName;?>   </td>
		<td class="head_table_top">
		
			<table cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td><a target="_top" href="<?=$exitUrl;?>"><img src="img/ico_end.gif" alt="<?=text::get('EXIT');?>" width="15" height="15" border="0" class="block"></a></td>
					<td><a target="_top" href="<?=$exitUrl;?>" class="red"><?=text::get('EXIT');?></a></td>
				</tr>
			</table>

		 </td>
	</tr>
	<tr valign=top>
		<td class="head_table_bot">

		</td>
		<td class="head_table_bot_brown"><table cellpadding="0" cellspacing="0" border="0"><tr><td style="color: #716F60;"></td><td nowrap ><img src="img/0.gif" alt="" width="1" height="20" class="block"></td></tr></table></td>
		<td class="head_table_bot">
			<table cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td><a onclick="window.top.normal();return false;" href="#"><img src="img/ico_layout_1.gif" alt="<?=text::get('NORMAL_VIEW');?>" width="15" height="15" border="0" class="block"></a></td>
					<td><a onclick="window.top.min(1);return false;" href="#"><img src="img/ico_layout_2.gif" alt="<?=text::get('MIN_BOTTOM_VIEW');?>" width="15" height="15" border="0" class="block"></a></td>
					<td><a onclick="window.top.min(2);return false;" href="#"><img src="img/ico_layout_3.gif" alt="<?=text::get('MIN_TOP_VIEW');?>" width="15" height="15" border="0" class="block"></a></td>
				</tr>
			</table>
			<div id="disabledFrameControl"  style="position: relative;left: 0px;top: -19px;visibility:hidden;z-index:4;">
				<table cellpadding="2" cellspacing="0" border="0">
					<tr>
						<td><img src="img/ico_layout_1_disabled.gif" alt="<?=text::get('NORMAL_VIEW');?>" width="15" height="15" border="0" class="block"></td>
						<td><img src="img/ico_layout_2_disabled.gif" alt="<?=text::get('MIN_BOTTOM_VIEW');?>" width="15" height="15" border="0" class="block"></td>
						<td><img src="img/ico_layout_3_disabled.gif" alt="<?=text::get('MIN_TOP_VIEW');?>" width="15" height="15" border="0" class="block"></td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
</table>

</body>
